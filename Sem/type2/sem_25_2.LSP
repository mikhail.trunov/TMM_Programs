;���� ��� ����������� �� ��������������� ������� � 25 ��� 2
(defun vvod (/ f le dmu)
  (initget 6)
  (setq f (getreal "\n������� ���� �� ")
        ab (getdist "\n������� �B [mm] ")
        omeg (getreal "\n������� ����� ")
        lab (getreal "\n������� AB [m] ")
        mul (/ lab ab)
	;����� �� �����
	bc (* 2.4 ab)
	ad (* 2.2 ab)
	cd (* 1.3 ab)
	de cd	
	ef (* 3 ab)
	bl (* 3.2 ab)
	bk ab			
	;�������� �����
	lbc (* lab 3.3)
	lcd (* lab 1.76)
	lef (* lab 4.7)
	;�������� � ���������
	mvb (* omeg lab)
	mab (* omeg omeg lab)
	fi (- pi (* f (/ pi 180)))
  );end of setq
  (ishodn omeg lab f)
  (tabl_razmer (list "\\A1;l\\H0.7x;\\S^AB;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^BC;\\H1.4286x;"
		      "\\A1;l\\H0.7x;\\S^AD;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^CD;\\H1.4286x;"
		      "\\A1;l\\H0.7x;\\S^DE;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^EF;\\H1.4286x;"
		      "\\A1;l\\H0.7x;\\S^BL;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^BK;\\H1.4286x;"
		      "\\A1;l\\H0.7x;\\S^ES4;\\H1.4286x;"
		)
    (list ab bc ad cd de ef bl bk (* 0.5 ef)) mul)
);end of vvod

;;;;;;;;;;;;;;;;;;;;;�������, ������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;----------------------------------------------------------------------------------
;;;;;;---=================������� ���������� ����� ���������================-------
(defun tochkim ()
  (setq
    a (getpoint "\n��������: ")
    d (polar a 0 ad)
    b (polar a fi ab)    
    c (car (vl-sort
	     (circint b bc d cd)
	     (function (lambda (e1 e2) (> (cadr e1) (cadr e2)))) 
	   )
      )
    e (polar d (+ (angle d c) (* pi (/ 5.0 6))) de)    
    f (car (vl-sort
	     (dwgru a d e ef)
	     (function (lambda (e1 e2) (> (car e1) (car e2)))) 
	   )
      )
    k (polar b (+ (angle b c) (arccos (/ bk bl))) bk)
    l (polar b (angle b c) bl)
    s2 (inters k (polar b (angle b l) (* 0.5 (distance b l)))
	       b (polar k (angle k l) (* 0.5 (distance k l)))
	       nil
       )
    s4 (polar e (angle e f) (/ (distance e f) 2))       
    masivm (list a b c d e f k l s2 s4)
  )
)
;----------------------------------------------------------------------------------
;--==������� ����� ����� ���������
(defun plansk (/)
  (initget 1)
  (setq pb (getdist "\n������� ����� ������� �������� Va [mm] ")
  	pol (getpoint "\n������� ����� ����� ��������� ")
	muv (/ mvb pb)
  ;======����� ����� ���������========  
  	bv (polar pol (- (angle a b) (/ pi 2.)) pb)
	cv (inters bv (polar bv (+ (angle b c) (* 0.5 pi)) 10.)
		   pol (polar pol (+ (angle d c) (* 0.5 pi)) 10.)
		   nil
	   )
	ev (polar pol (+ (angle pol cv) (* pi (/ 5. 6.))) (distance pol cv))
	fv (inters pol (polar pol 0. 10.)
		   ev (polar ev (+ (angle f e) (/ pi 2.)) 5)
		   nil
	   )
	lv (polar bv (angle bv cv) (* (/ bl bc) (distance bv cv)))
	kv (polar bv (+ (angle bv lv) (arccos (/ bk bl))) (* (/ bk bc) (distance bv cv)))
	
	s2v (inters kv (polar bv (angle bv lv) (* 0.5 (distance bv lv)))
		    bv (polar kv (angle kv lv) (* 0.5 (distance kv lv)))
		    nil
	    )
	s4v (polar ev (angle ev fv) (/ (distance ev fv) 2))
	;========������ ��������=======
    ;����������
        mvc (* muv (distance pol cv))
	mve (* muv (distance pol ev))
	mvf (* muv (distance pol fv))
	mvk (* muv (distance pol kv))
	mvl (* muv (distance pol lv))
	mvs4 (* muv (distance pol s4v))
	mvs2 (* muv (distance pol s2v))
   ;�������������
	mvcb (* muv (distance cv bv))
	mvfe (* muv (distance fv ev))
  ;========������� ��������====================
        ome2 (/ mvcb lbc)
	ome3 (/ mvc lcd)
	ome4 (/ mvfe lef)
	masom (list ome2 ome3 ome4)
  ;=====���������� ���������=====
	macbn (/ (expt mvcb 2) lbc)
	macdn (/ (expt mvc 2) lcd)
	mafen (/ (expt mvfe 2) lef)
	masv (list mvc mvl mvk mve mvf mvs2 mvs4 mvcb mvfe)
	masb (list "C" "L" "K" "E" "F" "\\A1;S\\H0.7x;\\S^2;" "\\A1;S\\H0.7x;\\S^4;" "CB" "FE")
  );end of setq
);plansk

;----------------------------------------------------------------------------------
;---------========������� ����� ����� ��������� � ������ ���������========---------
(defun planusk (/)
  (initget 1)
  (setq pa (getint "\n������� ����� ������� ��������� ����� B [mm] ")	
	polu (getpoint "\n������� ����� ����� ��������� ")
	mua (atof (rtos (/ mab pa) 2 3))
	;=====����� �������� ���������� ���������=====
	lcbn (/ macbn mua)
	lcdn (/ macdn mua)
	lfen (/ mafen mua)
	;=====����� ����� ���������=====
	ba (polar polu (angle b a) pa)
	cbn (polar ba (angle c b) lcbn)
	cdn (polar polu (angle c d) lcdn)	
	ca (inters cbn (polar cbn (+ (angle b c) (* 0.5 pi)) 10.)
		   cdn (polar cdn (+ (angle d c) (* 0.5 pi)) 10.)
		   nil
	   )
	ea (polar polu (+ (angle polu ca) (* pi (/ 5. 6))) (distance polu ca))
	fen (polar ea (angle f e) lfen)
	fa (inters polu (polar polu 0. 10.)
		   fen (polar fen (+ (angle f e) (/ pi 2)) 5)
		   nil
	   )
	la (polar ba (angle ba ca) (* (/ bl bc) (distance ba ca)))
	ka (polar ba (+ (angle ba la) (arccos (/ bk bl))) (* (/ bk bc) (distance ba ca)))

	s2a (inters ka (polar ba (angle ba la) (* 0.5 (distance ba la)))
		    ba (polar ka (angle ka la) (* 0.5 (distance ka la)))
		    nil
	    )
	s4a (polar ea (angle ea fa) (/ (distance ea fa) 2))
	mac (* mua (distance polu ca))
	mae (* mua (distance polu ea))
	maf (* mua (distance polu fa))
        mak (* mua (distance polu ka))
	mal (* mua (distance polu la))
	mas4 (* mua (distance polu s4a))
	mas2 (* mua (distance polu s2a))
	;=====������ �������������� ���������=========
	macbt (* mua (distance cbn ca))
	macdt (* mua (distance cdn ca))
	mafet (* mua (distance fen fa))
	macb (* mua (distance ca ba))
	mafe (* mua (distance fa ea))
	;������� ���������
	eps2 (/ macbt lbc)
	eps3 (/ macdt lcd)
	eps4 (/ mafet lef)
	masep (list eps2 eps3 eps4)
	masa (list mac mal mak mae maf mas2 mas4 macb mafe)
  );setq
);planusk
;=============================================================================================


(defun cherm (/)
  (li (list a b l k b)) (li (list c d e f))
  (duga a b b (polar b (- (angle a b) (* 0.5 pi)) 10.) 0.5 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^1;\\H2;")
  (mapcar '(lambda (t1) (add_circ 0.7 t1)) masivm)
  (add_text a "A")(add_text b "B")(add_text c "C")(add_text d "D")(add_text e "E")(add_text f "F")
  (add_text k "K")(add_text l "L")(add_text s2 "\\A1;S\\H0.7x;\\S^2;\\H1.4286x;")
  (add_text s4 "\\A1;S\\H0.7x;\\S^4;\\H1.4286x;")
  (add_block f 0.)
  (add_opor a (+ (* 0.5 pi) fi)) (add_opor d (+ (angle d c) (* pi (/ 5. 6.)) ))
  (muul lab ab)
);cherm

(defun cherv (/ dmu le)
  (add_text pol "p,a,d")
  (li (list kv s2v bv kv))
  (li (list kv lv s2v))
  (li (list lv cv))
  ;����������
  (risskor pol bv "b" "v^B")
  (risskor pol cv "c" "v^C")  
  (risskor pol ev "e" "v^E")
  (risskor pol fv "f" "v^F")
  (risskor pol s2v "s^2" "v^S2")
  (risskor pol lv "l" "v^L")
  (risskor pol kv "k" "v^K")
  (risskor pol s4v "s^4" "v^S4")
  ;�������������
  (risskor bv cv nil "v^CB") 
  (risskor ev fv nil "v^FE")
  (duga b c bv cv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^2;\\H2;")
  (duga d c pol cv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^3;\\H2;")
  (duga e f ev fv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^4;\\H2;")
  ;���������
  (muuv mvb pb)
  (formv omeg lab)
)

(defun chera (/)
  (add_text polu "\\A1;p\\H0.7x;\\S^1;\\H1.42857x;,a,d" )
  ;����� 1
  (li (list ka s2a ba ka))
  (li (list ka la s2a))
  (li (list la ca))
  (risskor polu ba "b" "a^B")
  ;����� 3
  (risskor polu ca "c" "a^C")
  (risskor polu ea "e" "a^E")
  ;����� 2
  (risskor polu fa "f" "a^F")
  (risskor polu la "l" "a^L")
  (risskor polu s4a "s^4" "a^S4")
  (risskor polu ka "k" "a^K")
  (risskor polu s2a "s^2" "a^S2")
  (risskor ea fa nil "a^FE")
  (risskor ba ca nil "a^CB") 
  ;������������� ���������
  (risskor ba cbn nil "an^CB")
  (risskor polu cdn nil "an^CD") 
  (risskor ea fen nil "an^FE")
  (risskor cdn ca nil "a\U+03C4^CD")
  (risskor cbn ca nil "a\U+03C4^CB") 
  (risskor fen fa nil "a\U+03C4^FE")
  (duga b c cbn ca 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^2;\\H2;")
  (duga d c cdn ca 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^3;\\H2;")
  (duga e f fen fa 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^4;\\H2;")
  (muua mab pa)
  (forma omeg lab)
)

(defun omep ()
  ;���������
  ;��� ���������� ���������
  (formsk2 muv (distance pol cv) "C" "pc" t)
  (formsk2 muv (distance pol ev) "E" "pe" t)
  (formsk2 muv (distance pol fv) "F" "pf" t)
  (formsk2 muv (distance pol kv) "K" "pk" t)
  (formsk2 muv (distance pol lv) "L" "pl" t)
  (formsk2 muv (distance pol s2v) "S2" "ps\\H0.7x;\\S^2;\\H1.4286x;" t)
  (formsk2 muv (distance pol s4v) "S4" "ps\\H0.7x;\\S^4;\\H1.4286x;" t)
  (add_text (getpoint "\n��������������� speed point L: ")
	    (strcat "\\A1;bl=bc\U+00D7\\H0.7x;\\SBL/BC;\\H1.4286x;="
		    (rtos (distance bv cv) 2 0) "\U+00D7\\H0.7x;\\S" (rtos bl 2 0) "/" (rtos bc 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance bv cv) (/ bl bc)) 2 1) " ��"
	    )
  )
  (add_text (getpoint "\n��������������� speed point K: ")
	    (strcat "\\A1;bk=bc\U+00D7\\H0.7x;\\SBK/BC;\\H1.4286x;="
		    (rtos (distance bv cv) 2 0) "\U+00D7\\H0.7x;\\S" (rtos bk 2 0) "/" (rtos bc 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance bv cv) (/ bk bc)) 2 1) " ��"
	    )
  )
  (add_text (getpoint "\n��������������� speed point E: ")
	    (strcat "\\A1;pe=pc\U+00D7\\H0.7x;\\SDE/DC;\\H1.4286x;=pc="
		    (rtos (distance pol cv) 2 0) " ��"
	    )
  )
  (add_text (getpoint "\n��������������� speed point S: ")
	    (strcat "\\A1;es\\H0.7x;\\S^4;\\H1.4286x;=fs\\H0.7x;\\S^4;\\H1.4286x;="
		    "=\\H0.7x;\\Sef/2;\\H1.4286x;=\\H0.7x;\\S"(rtos (distance ev fv) 2 0)
		    "/2;\\H1.4286x;=" (rtos (/ (distance ev fv) 2.) 2 0) " ��"
	    )
  )
  ;��� ������������� ���������
  (formsk2 muv (distance cv bv) "CB" "cb" t)
  (formsk2 muv (distance fv ev) "FE" "fe" t)
  ;���������
  (formsk2 mua (distance polu ca) "C" "p\\H0.7x;\\S^1;\\H1.4286x;c" nil)
  (formsk2 mua (distance polu fa) "F" "p\\H0.7x;\\S^1;\\H1.4286x;f" nil)
  (formsk2 mua (distance polu ea) "E" "p\\H0.7x;\\S^1;\\H1.4286x;e" nil)
  (formsk2 mua (distance polu ka) "K" "p\\H0.7x;\\S^1;\\H1.4286x;k" nil)
  (formsk2 mua (distance polu la) "L" "p\\H0.7x;\\S^1;\\H1.4286x;l" nil)
  (formsk2 mua (distance polu s2a) "S2" "p\\H0.7x;\\S^1;\\H1.4286x;s\\H0.7x;\\S^2;\\H1.4286x;" nil)
  (formsk2 mua (distance polu s4a) "S4" "p\\H0.7x;\\S^1;\\H1.4286x;s\\H0.7x;\\S^4;\\H1.4286x;" nil)
  (formsk2 mua (distance ca ba) "CB" "bc" nil)
  (formsk2 mua (distance fa ea) "FE" "fe" nil)
  (add_text (getpoint "\n��������������� acel point L: ")
	    (strcat "\\A1;bl=bc\U+00D7\\H0.7x;\\SBL/BC;\\H1.4286x;="
		    (rtos (distance ba ca) 2 0) "\U+00D7\\H0.7x;\\S" (rtos bl 2 0) "/" (rtos bc 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance ba ca) (/ bl bc)) 2 1) " ��"
	    )
  )
  (add_text (getpoint "\n��������������� acel point K: ")
	    (strcat "\\A1;bk=bc\U+00D7\\H0.7x;\\SBK/BC;\\H1.4286x;="
		    (rtos (distance ba ca) 2 0) "\U+00D7\\H0.7x;\\S" (rtos bk 2 0) "/" (rtos bc 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance ba ca) (/ bk bc)) 2 1) " ��"
	    )
  )
  (add_text (getpoint "\n��������������� acel point E: ")
	    (strcat "\\A1;de=p\\H0.7x;\\S^1;\\H1.4286x;c\U+00D7\\H0.7x;\\SDE/DC;\\H1.4286x;="
		    "p\\H0.7x;\\S^1;\\H1.4286x;c="(rtos (distance polu ca) 2 0) " ��"
	    )
  )
  (add_text (getpoint "\n��������������� acel point S: ")
	    (strcat "\\A1;es\\H0.7x;\\S^4;\\H1.4286x;=fs\\H0.7x;\\S^4;\\H1.4286x;="
		    "=\\H0.7x;\\Sef/2;\\H1.4286x;=\\H0.7x;\\S"(rtos (distance ea fa) 2 0)
		    "/2;\\H1.4286x;=" (rtos (/ (distance ea fa) 2.) 2 0) " ��"
	    )
  )
  ;���������� ���������
  (formusk "aCB" mvcb lbc)
  (formusk "aCD" mvc lcd)
  (formusk "aFE" mvfe lef)
  ;�������������� ���������
  (formuskt2 "CB" (distance cbn ca) mua)
  (formuskt2 "CD" (distance cdn ca) mua)
  (formuskt2 "FE" (distance fen fa) mua)
  ;�����
  (ugsu "ome2CB" mvcb lbc)
  (ugsu "ome3CD" mvc lcd)
  (ugsu "ome4FE" mvfe lef)
  ;��������
  (ugsu "eps2CB" macbt lbc)
  (ugsu "eps3CD" macdt lcd)
  (ugsu "eps4FE" mafet lef)
  (tablica_va masb masv masa masom masep)
)

(defun c:sem_25 (/ osm le ab omeg lab mul bc ad cd de ef bl bk lbc lcd lef mvb mab fi
a d b c e f k l s2 s4 masivm pb pol muv bv cv ev fv lv kv s2v s4v mvc mve mvf mvk mvl mvs4 mvs2 
mvcb mvfe ome2 ome3 ome4 masom macbn macdn mafen masv masb pa polu mua lcbn lcdn lfen ba cbn cdn ca 
ea fen fa la ka s2a s4a pca pfa pea pka pla ps4a ps2a dcb dcd dfe mac mae maf mak mal mas4 mas2 macbt 
macdt mafet macb mafe eps2 eps3 eps4 masep masa)
  (VL-LOAD-COM)
  (setq osm (getvar "OSMODE"))
  (setvar "OSMODE" 20919)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "TIMESI.TTF" )))
	le (append le (list (cons 2 "�����")))	
  )
  (entmakex le)
  (setvar "textstyle" "�����")
  (load "funkcii_tmm.lsp")
  (load "tmm_semi.lsp")
  (vvod)
  (tochkim)
  (cherm)
  (plansk)
  (cherv)
  (planusk)
  (chera)  
  (omep)
  (setvar "OSMODE" osm)
  (setvar "dimtxsty" "�����")
  (princ)
)
(c:sem_25)