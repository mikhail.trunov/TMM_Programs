;���� ��� ����������� �� ��������������� ������� � 10 ��� 2
(defun vvod (/ f)
  (initget 6)
  (setq f (getreal "\n������� ���� �� ")
        ab (getreal "\n������� �B [mm] ")
        omeg (getreal "\n������� ����� ")
        lab (getreal "\n������� AB [m] ")
        mul (/ lab ab)
	;����� �� �����
	bc (* 3.12 ab)
	ed (* 1.24 ab)
	o1e ab
	o1f (* 2.24 ab)
	o2k o1f
	kl (* 2.72 ab)
	kf (* 0.6 ab)
	x (* 1.9 ab)	
	y (* 1.6 ab)
	y1 (* 0.64 ab)
	;�������� �����
	lbc (* lab 3.12)
	led (* lab 1.24)
	lo1e lab
	lkf (* lab 0.6)
	lo2k (* lab 2.24)
	;�������� � ���������
	mvb (* omeg lab)
	mab (* omeg omeg lab)
	fi (* (- 180. f) (/ pi 180.))
  );end of setq
  (ishodn omeg lab f)
  (tabl_razmer (list "\\A1;l\\H0.7x;\\S^AB;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^BC;\\H1.4286x;"
		      "\\A1;l\\H0.7x;\\S^ED;\\H1.4286x;" "\\A1;O\\H0.7x;\\S^1;\\H1.4286x;E"
		      "\\A1;O\\H0.7x;\\S^1;\\H1.4286x;F" "\\A1;O\\H0.7x;\\S^2;\\H1.4286x;K"
		      "\\A1;l\\H0.7x;\\S^KL;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^KF;\\H1.4286x;" "x" "y"
		      "\\A1;y\\H0.7x;\\S^1;\\H1.4286x;"
		)
    (list ab bc ed o1e o1f o2k kl kf x y y1) mul)
);end of vvod


;;;;;;;;;;;;;;;;;;;;;�������, ������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;----------------------------------------------------------------------------------
;;;;;;---=================������� ���������� ����� ���������================-------
(defun tochkim ( / ooo)
  (setq
    a (getpoint "\n��������: ")
    ooo (polar a pi x)
    o1 (polar ooo (* 0.5 pi) y)
    o2 (polar ooo (* 0.5 pi) (+ y y1))
    b (polar a fi ab)    
    c (car (vl-sort
	     (dwgru a (polar a pi 1e5) b bc)
	     (function (lambda (e1 e2) (< (car e1) (car e2)))) 
	   )
      )
    d (inters
	b (polar b (- (angle b c) (/ pi 12)) 5)
	c (polar c (+ (angle c b) (/ pi 12)) 5)
	nil
      )
    e (car (vl-sort
	     (circint d ed o1 o1e)
	     (function (lambda (e1 e2) (> (car e1) (car e2)))) 
	   )
      )
    f (polar o1 (angle e o1) o1f)
    k (car (vl-sort
	     (circint f kf o2 o2k)
	     (function (lambda (e1 e2) (> (cadr e1) (cadr e2)))) 
	   )
      )     
    l (polar k (angle k f) kl)
    bd (distance b d)
    masivm (list a b c d e f)       
  )  
)

;----------------------------------------------------------------------------------
;--==������� ����� ����� ��������� � ���������� ��������� � ������ ���������===----
(defun plansk ()
  (initget 1)
  (setq pb (getint "\n������� ����� ������� �������� Va [mm] ")
 	muv (/ mvb pb)
	pol (getpoint "\n������� ����� ����� ��������� ") 
  ;======����� ����� ���������========  
 	bv (polar pol (- (angle a b) (/ pi 2)) pb)
	cv (inters
	     pol (polar pol 0. 15)
	     bv (polar bv (- (angle c b) (* 0.5 pi)) 5)
	     nil
	   )
	dv (inters
		bv (polar bv (- (angle bv cv) (/ pi 12)) 5)
		cv (polar cv (+ (angle cv bv) (/ pi 12)) 5)
		nil
      	   ); inters
	ev (inters dv (polar dv (+ (angle d e) (* 0.5 pi)) 1e15)
		   pol (polar pol (+ (angle o1 e) (* 0.5 pi)) 1e15)
		   nil
	   )
	fv (polar pol (angle ev pol) (* (distance pol ev) (/ o1f o1e)))
	kv (inters fv (polar fv (+ (angle f k) (* 0.5 pi)) 1e15)
		   pol (polar pol (+ (angle o2 k) (* 0.5 pi)) 1e15)
		   nil
	   )
	lv (polar kv (angle kv fv) (* (distance kv fv) (/ kl kf) ))
  ;========������ ��������=======
        mvc (* muv (distance pol cv))
	mvd (* muv (distance pol dv))
	mve (* muv (distance pol ev))
	mvf (* muv (distance pol fv))
	mvk (* muv (distance pol kv))
	mvl (* muv (distance pol lv))
	
	mvcb (* muv (distance bv cv))
	mved (* muv (distance ev dv))
	mvkf (* muv (distance kv fv))
	masv (list mvc mvd mve mvf mvk mvl mvcb mved mvkf)
	masb (list "C" "D" "E" "F" "K" "L" "CB" "ED" "KF")
  ;========������� ��������====================
        ome2 (/ mvcb lbc)
	ome4 (/ mved led)
	ome5 (/ mve lo1e)
	ome6 (/ mvkf lkf)
	ome7 (/ mvk lo2k)
	masom (list ome2 ome4 ome5 ome6 ome7)
  ;=====���������� ���������=====
	macbn (/ (expt mvcb 2) lbc)
	maedn (/ (expt mved 2) led)
	maeo1n (/ (expt mve 2) lo1e)
	makfn (/ (expt mvkf 2) lkf)
	mako2n (/ (expt mvk 2) lo2k)
  ); setq
); defun plansk
;----------------------------------------------------------------------------------
;---------========������� ����� ����� ��������� � ������ ���������========---------
(defun planusk ()
  (initget 1)
  (setq pa (getint "\n������� ����� ������� ��������� ����� B [��] ")	
	polu (getpoint "\n������� ����� ����� ��������� ")
	mua (/ mab pa)
	;=====����� �������� ���������� ���������=====
	lcbn (/ macbn mua)
	ledn (/ maedn mua)
	leo1n (/ maeo1n mua)
	lkfn (/ makfn mua)
	lko2n (/ mako2n mua)
	;=====����� ����� ���������=====
	ba (polar polu (angle b a) pa)
	cbn (polar ba (angle c b) lcbn)
	ca (inters polu (polar polu 0. 1e5)
		   cbn (polar cbn (- (angle c b) (* 0.5 pi)) 5)
		   nil
	   )
	da (inters
		ba (polar ba (- (angle ba ca) (/ pi 12)) 5)
		ca (polar ca (+ (angle ca ba) (/ pi 12)) 5)
		nil
      	   ); inters
	edn (polar da (angle e d) ledn)
	eo1n (polar polu (angle e o1) leo1n)
	ea (inters edn (polar edn (+ (angle d e) (* 0.5 pi)) 1e15)
		   eo1n (polar eo1n (+ (angle o1 e) (* 0.5 pi)) 1e15)
		   nil
	   )
	fa (polar polu (angle ea polu) (* (distance polu ea) (/ o1f o1e)))
	kfn (polar fa (angle k f) lkfn)
	ko2n (polar polu (angle k o2) lko2n)
	ka (inters kfn (polar kfn (+ (angle f k) (* 0.5 pi)) 1e15)
		   ko2n (polar ko2n (+ (angle o2 k) (* 0.5 pi)) 1e15)
		   nil
	   )
	la (polar ka (angle ka fa) (* (distance ka fa) (/ kl kf) ))
	;������
	mac (* mua (distance polu ca))
	mad (* mua (distance polu da))
	mae (* mua (distance polu ea))
	maf (* mua (distance polu fa))
	mak (* mua (distance polu ka))
	mal (* mua (distance polu la))
	macb (* mua (distance ba ca))
	maed (* mua (distance ea da))
	makf (* mua (distance ka fa))

	masa (list mac mad mae maf mak mal macb maed makf)
	
	;=====������ �������������� ���������=========
	macbt (* mua (distance cbn ca))
	maedt (* mua (distance ea edn))
	maeo1t (* mua (distance ea eo1n))
	makft (* mua (distance ka kfn))
	mako2t (* mua (distance ka ko2n))
	;������� ���������
	eps2 (/ macbt lbc)
	eps4 (/ maedt led)
	eps5 (/ maeo1t lo1e)
	eps6 (/ makft lkf)
	eps7 (/ mako2t lo2k)
	masep (list eps2 eps4 eps5 eps6 eps7)
  )  
)
;=============================================================================================


(defun cherm (/ i le dmu)
  (li (list a b c d b))
  (li (list d e f))
  (li (list o2 k l))
  (duga a b b (polar b (- (angle a b) (* 0.5 pi)) 10.) 0.5 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^1;\\H2;")
  (mapcar '(lambda (t1) (add_circ 0.7 t1)) (append masivm (list o1 o2 l k)))
  (add_text o1 "\\A1;O\\H0.7x;\\S^1;")
  (add_text o2 "\\A1;O\\H0.7x;\\S^2;")
  (add_text l "L")
  (add_text k "K")
  (add_block c 0.)
  (add_opor a (+ (* 0.5 pi) fi)) (add_opor o1 (+ (angle o1 e) pi)) (add_opor o2 (+ (angle o2 k) (* 0.5 pi)))
  (setq i 65)
  (mapcar '(lambda (t1) (progn
			  (add_text t1 (chr i))
			  (setq i (1+ i))
			)
	   ) masivm)
  (muul lab ab)
); defun cherm

(defun cherv ()
  (li (list bv dv cv))
  (add_text pol "\\A1;p,a,o\\H0.7x;\\S^1;\\H1.4286x;,o\\H0.7x;\\S^2;")
  (risskor pol bv "b" "v^B")
  (risskor pol cv "c" "v^C")
  (risskor pol ev "e" "v^E")
  (risskor pol fv "f" "v^F")
  (risskor pol dv "d" "v^D")
  (risskor pol lv "l" "v^L")
  (risskor pol kv "k" "v^K")
  (risskor bv cv nil "v^CB")
  (risskor dv ev nil "v^ED")
  (risskor fv kv nil "v^KF")
  (li (list kv lv))
  (duga b c bv cv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^2;\\H2;")
  (duga d e dv ev 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^4;\\H2;")
  (duga o1 f pol fv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^5;\\H2;")
  (duga f k fv kv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^6;\\H2;")
  (duga o2 k pol kv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^7;\\H2;")
  (muuv mvb pb)
  (formv omeg lab)
  
); defun cherv

(defun chera ()
  (li (list ba ca da ba))
  (li (list ka la))
  (add_text polu "\\A1;p\\H0.7x;\\S^1;\\H1.4286x;,a,o\\H0.7x;\\S^1;\\H1.4286x;,o\\H0.7x;\\S^2;\\H1.4286x;")
  (risskor polu ba "b" "a^B") 
  (risskor polu ca "c" "a^C")
  (risskor polu da "d" "a^D")
  (risskor polu ea "e" "a^E")
  (risskor polu fa "f" "a^F")
  (risskor polu ka "k" "a^K")
  (risskor polu la "l" "a^L")
  ;����������
  (risskor ba cbn nil "an^CB")
  (risskor da edn nil "an^ED")
  (risskor polu eo1n nil "an^EO1")
  (risskor fa kfn nil "an^KF")
  (risskor polu ko2n nil "an^KO2")
  ;��������������
  (risskor cbn ca nil "a\U+03C4^CB")
  (risskor edn ea nil "a\U+03C4^ED")
  (risskor eo1n ea nil "a\U+03C4^EO1")
  (risskor kfn ka nil "a\U+03C4^KF")
  (risskor ko2n ka nil "a\U+03C4^KO2")
  ;�������������
  (risskor ba ca nil "a^CB")
  (risskor da ea nil "a^ED")
  (risskor fa ka nil "a^KF")
  (duga b c cbn ca 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^2;\\H2;")
  (duga d e edn ea 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^4;\\H2;")
  (duga o1 e eo1n ea 0.5 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^5;\\H2;")
  (duga f k kfn ka 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^6;\\H2;")
  (duga o2 k ko2n ka 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^7;\\H2;")
  (muua mab pa)
  (forma omeg lab)
  
); defun chera

(defun omep (/ i)  
  ;���������
  (add_text (getpoint "\nlBD") (strcat "\\A1;l\\H0.7x;\\S^BD;\\H1.4286x;=BD\U+00D7\U+03BC\\H0.7x;\\S^l;"
				       "\\H1.4286x;="(rtos bd 2 0)"\U+00D7"(rtos mul)"="
				       (rtos (* mul bd) 2 3) " �"
			       )
  )
  (formsk2 muv (distance pol cv) "C" "pc" t)
  (formsk2 muv (distance pol dv) "D" "pd" t)
  (formsk2 muv (distance pol ev) "E" "pe" t)
  (formsk2 muv (distance pol fv) "F" "pf" t)
  (formsk2 muv (distance pol kv) "K" "pk" t)
  (formsk2 muv (distance pol lv) "L" "pl" t)
  (formsk2 muv (distance cv bv) "CB" "bc" t)
  (formsk2 muv (distance dv ev) "ED" "ed" t)
  (formsk2 muv (distance kv fv) "KF" "kf" t)
  (add_text (getpoint "\n��������������� speed point D: ")
	    (strcat "\\A1;bd=dc=bc\U+00D7\\H0.7x;\\SBD/BC;\\H1.4286x;="
		    (rtos (distance bv cv) 2 0) "\U+00D7\\H0.7x;\\S" (rtos bd 2 0) "/" (rtos bc 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance bv cv) (/ bd bc)) 2 1) " ��"
	    )
  )
  (add_text (getpoint "\n��������������� speed point f: ")
	    (strcat "\\A1;pf=pe\U+00D7\\H0.7x;\\SO1F/O1E;\\H1.4286x;="
		    (rtos (distance pol ev) 2 0) "\U+00D7\\H0.7x;\\S" (rtos o1f 2 0) "/" (rtos o1e 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance pol ev) (/ o1f o1e)) 2 1) " ��"
	    )
  )
  (add_text (getpoint "\n��������������� speed point L: ")
	    (strcat "\\A1;kl=kf\U+00D7\\H0.7x;\\SKL/KF;\\H1.4286x;="
		    (rtos (distance kv fv) 2 0) "\U+00D7\\H0.7x;\\S" (rtos kl 2 0) "/" (rtos kf 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance kv fv) (/ kl kf)) 2 1) " ��"
	    )
  )

  ;��������������
  (formuskt2 "CB" (distance ca cbn) mua)
  (formuskt2 "ED" (distance ea edn) mua)
  (formuskt2 "EO1" (distance ea eo1n) mua)
  (formuskt2 "KF" (distance ka kfn) mua)
  (formuskt2 "KO2" (distance ka ko2n) mua)
  (formsk2 mua (distance polu ca) "C" "p\\H0.7x;\\S^1;\\H1.4286x;c" nil)
  (formsk2 mua (distance polu da) "D" "p\\H0.7x;\\S^1;\\H1.4286x;d" nil)
  (formsk2 mua (distance polu ea) "E" "p\\H0.7x;\\S^1;\\H1.4286x;e" nil)
  (formsk2 mua (distance polu fa) "F" "p\\H0.7x;\\S^1;\\H1.4286x;f" nil)
  (formsk2 mua (distance polu ka) "K" "p\\H0.7x;\\S^1;\\H1.4286x;k" nil)
  (formsk2 mua (distance polu la) "L" "p\\H0.7x;\\S^1;\\H1.4286x;l" nil)
  (formsk2 mua (distance ba ca) "CB" "bc" nil)
  (formsk2 mua (distance ea da) "ED" "ed" nil)
  (formsk2 mua (distance ka fa) "KF" "kf" nil)
  (add_text (getpoint "\n��������������� acel point D: ")
	    (strcat "\\A1;bd=dc=bc\U+00D7\\H0.7x;\\SBD/BC;\\H1.4286x;="
		    (rtos (distance ba ca) 2 0) "\U+00D7\\H0.7x;\\S" (rtos bd 2 0) "/" (rtos bc 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance ba ca) (/ bd bc)) 2 1) " ��"
	    )
  )
  (add_text (getpoint "\n��������������� acel point f: ")
	    (strcat "\\A1;p\\H0.7x;\\S^1;\\H1.4286x;f=p\\H0.7x;\\S^1;\\H1.4286x;e"
		    "\U+00D7\\H0.7x;\\SO1F/O1E;\\H1.4286x;="
		    (rtos (distance polu ea) 2 0) "\U+00D7\\H0.7x;\\S" (rtos o1f 2 0) "/" (rtos o1e 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance polu ea) (/ o1f o1e)) 2 1) " ��"
	    )
  )
  (add_text (getpoint "\n��������������� acel point L: ")
	    (strcat "\\A1;kl=kf\U+00D7\\H0.7x;\\SKL/KF;\\H1.4286x;="
		    (rtos (distance ka fa) 2 0) "\U+00D7\\H0.7x;\\S" (rtos kl 2 0) "/" (rtos kf 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance ka fa) (/ kl kf)) 2 1) " ��"
	    )
  )
  (ugsu "ome2CB" mvcb lbc)
  (ugsu "ome4ED" mved led)
  (ugsu "ome5EO1" mve lo1e)
  (ugsu "ome6KF" mvkf lkf)
  (ugsu "ome7KO2" mvk lo2k)

  (ugsu "eps2CB" macbt lbc)
  (ugsu "eps4ED" maedt led)
  (ugsu "eps5EO1" maeo1t lo1e)
  (ugsu "eps6KF" makft lkf)
  (ugsu "eps7KO2" mako2t lo2k)
  ;����������
  (formusk "aCB" mvcb lbc)
  (formusk "aED" mved led)
  (formusk "aEO1" mve lo1e)
  (formusk "aKF" mvkf lkf)
  (formusk "aKO2" mvk lo2k)
  (tablica_va masb masv masa masom masep)
); omep
;=====================================================================================================

(defun c:sem10_02 (/ osm le)
  (VL-LOAD-COM)  
  (setq osm (getvar "OSMODE"))
  (setvar "OSMODE" 20919)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "TIMESI.TTF" )))
	le (append le (list (cons 2 "�����")))	
  )
  (entmakex le)
  (setvar "textstyle" "�����")
  (load "funkcii_tmm.lsp")
  (load "tmm_semi.lsp")
  (vvod)
  (tochkim)
  (cherm)
  (plansk)
  (cherv)
  (planusk)
  (chera)
  (omep)
  (setvar "OSMODE" osm)
  (setvar "dimtxsty" "�����")
  (princ)
)
(c:sem10_02)

