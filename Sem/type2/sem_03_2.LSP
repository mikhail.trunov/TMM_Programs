;���� ��� ����������� �� ��������������� ������� � 3 ��� 2
(defun vvod (/ f )
  (initget 6)
  (setq f (getreal "\n������� ���� �� ")
        ab (getreal "\n������� �B [mm] ")
        omeg (getreal "\n������� ����� ")
        lab (getreal "\n������� AB [m] ")
        mul (atof (rtos (/ lab ab) 2 4))
	;����� �� �����
	bc (* 2.1 ab)
	cd bc
	df (* 2.22 ab)
	af (* 5. ab)
	de (* 1.34 ab)
	fe de
	fo (* 1.55 ab)
	ck (* 3.12 ab)
	ko (* 1.89 ab)
	kml ab
	;�������� �����
	lbd (atof (rtos (* lab 4.2) 2 3))
	ldf (atof (rtos (* lab 2.22) 2 3))
	lck (atof (rtos (* lab 3.12) 2 3))
	lko (atof (rtos (* lab 1.89) 2 3))
	;�������� � ���������
	mvb (* omeg lab)
	mab (atof (rtos (* omeg omeg lab) 2 4))
	fi (* f (/ pi 180.))
  );end of setq
  (add_text (getpoint "\n������� ��� ������ �������� ������: ")
	   (strcat "\\A1;\U+03C9\\H0.7x;\\S^1;\\H1.4286x;="
		   (rtos omeg)" �\\H0.7x;\\S-1^;\n\\H1.4286x;"
		   "l\\H0.7x;\\S^AB;\\H1.4286x;="
		   (rtos lab)" �\n\U+03C6\\H0.7x;\\S^1;\\H1.4286x;="
		   (rtos f)"\%%D"
	   ); strcat
  ); add_tex
);end of vvod


;;;;;;;;;;;;;;;;;;;;;�������, ������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;----------------------------------------------------------------------------------
;;;;;;---=================������� ���������� ����� ���������================-------
(defun tochkim (/ ddd)
  (setq
    a (getpoint "\n��������: ")
    b (polar a fi ab)
    f (polar a 0. af)
    o (polar f (* 1.5 pi) fo)
    d (car (vl-sort
	     (circint b (+ bc cd) f df)
	     (function (lambda (e1 e2) (> (car e1) (car e2)))) 
	   )
      )
    e (polar f (- (angle f d) (arccos (/ (* 0.5 df) fe))) fe)
    c (polar b (angle b d) (* 0.5 (distance b d)))
    k (car (vl-sort
	     (circint c ck o ko)
	     (function (lambda (e1 e2) (< (cadr e1) (cadr e2)))) 
	   )
      )
    m (polar k (angle c k) kml)
    l (polar m (+ (angle m c) (* 0.5 pi)) kml)
    masivm (list a b c d e f k l m o)
  )  
)
;----------------------------------------------------------------------------------
;--==������� ����� ����� ���������
(defun plansk (/)
  (initget 1)
  (setq pb (getdist "\n������� ����� ������� �������� Va [mm] ")
	muv (atof (rtos (/ mvb pb) 2 4))
	pol (getpoint "���� ���������: ")
  )
  ;======����� ����� ���������========  
  (setq bv (polar pol (+ (angle a b) (/ pi 2)) pb)
	dv (inters pol (polar pol (+ (angle d f) (* 0.5 pi)) 10.)
		   bv (polar bv (- (angle b d) (* pi 0.5)) 5.)
		   nil
	   )
	ev (polar pol (- (angle pol dv) (arccos (/ (* 0.5 df) fe))) (* fe (/ (distance pol dv) df)))
	cv (polar bv (angle bv dv) (* 0.5 (distance bv dv)))
	kv (inters pol (polar pol (+ (angle o k) (* 0.5 pi)) 10.)
		   cv (polar cv (- (angle k c) (* pi 0.5)) 5.)
		   nil
	   )
	sv (polar pol (angle pol kv) (* 0.5 (distance pol kv)))
	mv (polar kv (angle cv kv) (* kml (/ (distance kv cv) ck)))
	lv (polar mv (+ (angle mv cv) (* 0.5 pi)) (* kml (/ (distance kv cv) ck)))
  );end of setq
);plansk

(defun modulv ();���������� ��������� � ������ ���������
  (setq
   ;========������ ��������=======
    ;����������
    	mvd (atof (rtos (* muv (atof (rtos (distance pol dv) 2 0))) 2 3))
        mvc (atof (rtos (* muv (atof (rtos (distance pol cv) 2 0))) 2 3))	
	mve (atof (rtos (* muv (atof (rtos (distance pol ev) 2 0))) 2 3))
	mvk (atof (rtos (* muv (atof (rtos (distance pol kv) 2 0))) 2 3))
	mvl (atof (rtos (* muv (atof (rtos (distance pol lv) 2 0))) 2 3))
	mvm (atof (rtos (* muv (atof (rtos (distance pol mv) 2 0))) 2 3))
	mvs (atof (rtos (* muv (atof (rtos (distance pol sv) 2 0))) 2 3))
   ;�������������
	mvdb (atof (rtos (* muv (atof (rtos (distance bv dv) 2 0))) 2 3))
	mvkc (atof (rtos (* muv (atof (rtos (distance kv cv) 2 0))) 2 3))
  ;========������� ��������====================
        ome2 (atof (rtos (/ mvdb lbd) 2 3))
	ome3 (atof (rtos (/ mvd ldf) 2 3))
	ome4 (atof (rtos (/ mvkc lck) 2 3))
	ome5 (atof (rtos (/ mvk lko) 2 3))
	masom (list ome2 ome3 ome4 ome5)
  ;=====���������� ���������=====
	madbn (/ (expt mvdb 2) lbd)
	madfn (/ (expt mvd 2) ldf)
	makcn (/ (expt mvkc 2) lck)
	makon (/ (expt mvk 2) lko)
	masv (list mvc mvd mve mvk mvm mvl mvs mvdb mvkc)
	masb (list "C" "D" "E" "K" "M" "L" "S" "DB" "KC")
  )
);modulv

;----------------------------------------------------------------------------------
;---------========������� ����� ����� ��������� � ������ ���������========---------
(defun planusk (/)
  (initget 1)
  (setq pa (getdist "\n������� ����� ������� ��������� ����� B [mm] ")	
	polu (getpoint "\n������� ����� ����� ��������� ")
	mua (atof (rtos (/ mab pa) 2 3))
	;=====����� �������� ���������� ���������=====
	ldbn (/ madbn mua)
	ldfn (/ madfn mua)
	lkcn (/ makcn mua)
	lkon (/ makon mua)	
	;=====����� ����� ���������=====
	ba (polar polu (angle b a) pa)
	dbn (polar ba (angle d b) ldbn)
	dfn (polar polu (angle d f) ldfn)
	da (inters dfn (polar dfn (+ (angle f d) (* 0.5 pi)) 10.)
		   dbn (polar dbn (+ (angle b d) (* pi 0.5)) 10.)
		   nil
	   )
	ea (polar polu (- (angle polu da) (arccos (/ (* 0.5 df) fe))) (* fe (/ (distance polu da) df)))
	ca (polar ba (angle ba da) (* 0.5 (distance ba da)))
	kcn (polar ca (angle k c) lkcn)
	kon (polar polu (angle k o) lkon)
	ka (inters kcn (polar kcn (+ (angle k c) (* 0.5 pi)) 10.)
		   kon (polar kon (+ (angle k o) (* pi 0.5)) 10.)
		   nil
	   )
	sa (polar polu (angle polu ka) (* 0.5 (distance polu ka)))
	ma (polar ka (angle ca ka) (* kml (/ (distance ka ca) ck)))
	la (polar ma (+ (angle ma ca) (* 0.5 pi)) (* kml (/ (distance ka ca) ck)))
  );setq
);planusk
(defun modula ()
  (setq	;=====������ ���������� ���������=========
	mac (atof (rtos (* mua (distance polu ca)) 2 3))
	mae (atof (rtos (* mua (distance polu ea)) 2 3))
	mak (atof (rtos (* mua (distance polu ka)) 2 3))
	mad (atof (rtos (* mua (distance polu da)) 2 3))
	mal (atof (rtos (* mua (distance polu la)) 2 3))
	mam (atof (rtos (* mua (distance polu ma)) 2 3))
	mas (atof (rtos (* mua (distance polu sa)) 2 3))
	
        madb (atof (rtos (* mua (distance da ba)) 2 3))
	makc (atof (rtos (* mua (distance ka ca)) 2 3))
	;=====������ �������������� ���������=========
	madbt (atof (rtos (* mua (distance dbn da)) 2 3))
	madft (atof (rtos (* mua (distance dfn da)) 2 3))
	makct (atof (rtos (* mua (distance kcn ka)) 2 3))
	makot (atof (rtos (* mua (distance kon ka)) 2 3))
	;������� ���������
	eps2 (atof (rtos (/ madbt lbd) 2 3))
	eps3 (atof (rtos (/ madft ldf) 2 3))
	eps4 (atof (rtos (/ makct lck) 2 3))
	eps5 (atof (rtos (/ makot lko) 2 3))
	masep (list eps2 eps3 eps4 eps5)
	masa (list mac mad mae mak mam mal mas madb makc)
  )
)
;=============================================================================================


(defun cherm (/ i)  
  (li (list a b d e f d)) (li (list c m l c))(li (list k o)) 
  (mapcar '(lambda (t1) (add_circ 0.7 t1)) masivm)
  (add_opor a (+ (* 0.5 pi) fi))(add_opor o (+ (* 0.5 pi) (angle o k)))(add_opor f pi)
  (add_text a "A")(add_text b "B")(add_text c "C")(add_text d "D")(add_text e "E")
  (add_text f "F")(add_text k "K")(add_text l "L")(add_text m "M")(add_text o "O")
  (duga a b b (polar b (+ (angle a b) (* 0.5 pi)) 10.) 0.5 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^1;\\H2;")
  (add_text (getpoint "\n������� ��� ������ ������� ��� ����� ���������: ") 
	   (strcat "\\A1;\U+03BC\\H0.7x;\\S^l;\\H1.4286x;="
		   "l\\H0.7x;\\S^AB;\\H1.4286x;/AB="
		   "\\H0.7x;\\S"(rtos lab)"/"(rtos ab)
		   ";\\H1.4286x;="
		   (rtos mul)" \\H0.7x;\\S�/��;"
	   );end of strcat
  ); add_tex
);cherm

(defun cherv (/)
  (add_text pol "p,a,f,o")
  (li (list dv ev)) (li (list kv mv lv cv))
  ;����������
  (risskor pol bv "b" "v^B")
  (risskor pol cv "c" "v^C")
  (risskor pol kv "k" "v^K")
  (risskor pol ev "e" "v^E")
  (risskor pol dv "d" "v^D")
  (risskor pol lv "l" "v^L")
  (risskor pol mv "m" "v^M")
  (risskor pol sv "s" "v^S")
  ;�������������
  (risskor bv dv nil "v^DB")
  (risskor cv kv nil "v^KC")
  (duga b d bv dv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^2;\\H2;")
  (duga f d pol dv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^3;\\H2;")
  (duga c k cv kv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^4;\\H2;")
  (duga o k pol kv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^5;\\H2;")
  ;���������
  (add_text (getpoint "\n������� ��� ������ ������� ��� ����� ���������: ")
	   (strcat "\\A1;\\H2.2;\U+03BC"
		   "\\H0.7x;\\S^\U+03C5;"
		   "\\H2.2;=\U+03C5"
		   "\\H0.7x;\\S^B;"
		   "\\H2.2;/Pb="
		   "\\H0.8x;\\S"
		   (rtos mvb)"/"(rtos pb)
		   ";\\H2.2;="
		   (rtos (fix muv))","(substr (rtos muv) 3)
		   " \\H0.7x;\\S� �/��;"
	   );strcat
  ); add_tex
  (add_text (getpoint "\n������� ��� ������ ������� ��� ����������� ��������: ")
	   (strcat "\\A1;\\H2;\U+03C5"
	 	   "\\H0.7x;\\S^B;"
		   "\\H2;=\U+03C9"
		   "\\H0.7x;\\S^1;"
		   "\\H2;\U+00D7l"
		   "\\H0.7x;\\S^AB;"
		   "\\H2;="			    
		   (rtos omeg)"\U+00D7"(rtos lab)
		   "="(rtos mvb)" �/�"					    
	   );end of strcat
  ); add_tex
)

(defun chera (/)
  (add_text polu "\\A1;p\\H0.7x;\\S^1;\\H1.42857x;,a,f,o" )

  (li (list da ea)) (li (list ka ma la ca))
  (risskor polu ba "b" "a^B")

  (risskor polu ca "c" "a^C")
  (risskor polu ea "e" "a^E")
  (risskor polu ka "k" "a^K")
  (risskor polu da "d" "a^D")
  (risskor polu la "l" "a^L")
  (risskor polu ma "m" "a^M")
  (risskor polu sa "s" "a^S")

  (risskor ba da nil "a^DB")
  (risskor ca ka nil "a^KC")
  ;������������� ���������
  (risskor ba dbn nil "an^DB")
  (risskor polu dfn nil "an^DF")
  (risskor ca kcn nil "an^KC")
  (risskor polu kon nil "an^KO")
  (risskor dbn da nil "a\U+03C4^DB")
  (risskor dfn da nil "a\U+03C4^DF")
  (risskor kcn ka nil "a\U+03C4^KC")
  (risskor kon ka nil "a\U+03C4^KO")
  (duga b d dbn da 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^2;\\H2;")
  (duga f d dfn da 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^3;\\H2;")
  (duga c k kcn ka 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^4;\\H2;")
  (duga o k kon ka 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^5;\\H2;")
  (add_text (getpoint "\n������� ��� ������ ������� ��� ����� ���������: ")  
	   (strcat "\\A1;\\H2.2;\U+03BC";mu
		   "\\H0.7x;\\S^a;";index = a
		   "\\H2.2;=a\\H0.7x;\\S^B;"
		   "\\H2.2;/Pb="
		   "\\H0.8x;\\S"
		   (rtos mab)"/"(rtos pb)";"
		   "\\H2.2;="
		   (rtos (fix mua))","
		   (substr (rtos mua) 3)
		   " \\H0.7x;\\S� �/��;"
	   );strcat
  ); add_tex
  (add_text (getpoint "\n������� ��� ������ ������� ��� ����������� ���������: ")
	   (strcat "\\A1;\\H2;a"
		   "\\H0.7x;\\S^B;"
		   "\\H2;=\U+03C9"
		   "\\H0.7x;\\S2^1;"
		   "\\H2;\U+00D7l"
		   "\\H0.7x;\\S^AB;"
		   "\\H2;="			    
		   (rtos omeg)
		   "\\H0.7x;\\S2^;"
		   "\\H2;\U+00D7"(rtos lab)
		   "="(rtos mab)" �/�"
		   "\\H0.7x;\\S2^;"
	   );end of strcat
  ); add_tex
)

(defun omep ()
  ;���������
  ;��� ���������� ���������
  (formsk (distance pol cv) "vC" "v")
  (formsk (distance pol ev) "vE" "v")  
  (formsk (distance pol dv) "vD" "v")
  (formsk (distance pol kv) "vK" "v")
  (formsk (distance pol lv) "vL" "v")
  (formsk (distance pol mv) "vM" "v")
  (formsk (distance pol sv) "vS" "v")
  ;��� ������������� ���������
  (formsk (distance dv bv) "vDB" "v")
  (formsk (distance kv cv) "vKC" "v")
  ;���������
  (formsk (distance polu ca) "aC" "a")
  (formsk (distance polu ea) "aE" "a")
  (formsk (distance polu da) "aD" "a")
  (formsk (distance polu la) "aL" "a")
  (formsk (distance polu ka) "aK" "a")
  (formsk (distance polu ma) "aM" "a")
  (formsk (distance polu sa) "aS" "a")
  (formsk (distance da ba) "aDB" "a")
  (formsk (distance ka Ca) "aKC" "a")
  ;���������� ���������
  (formusk "aDB" mvdb lbd)
  (formusk "aDF" mvd ldf)
  (formusk "aKC" mvkc lck)
  (formusk "aKO" mvk lko)
  ;�������������� ���������
  (formuskt "aDB" (distance dbn da) madbt)
  (formuskt "aDF" (distance dfn da) madft)
  (formuskt "aKC" (distance kcn ka) makct)
  (formuskt "aKO" (distance kon ka) makot)
  
  ;�����
  (ugsu "ome2DB" mvdb lbd)
  (ugsu "ome3DF" mvd ldf)
  (ugsu "ome4KC" mvkc lck)
  (ugsu "ome5KO" mvk lko)
  ;��������
  (ugsu "eps2DB" madbt lbd)
  (ugsu "eps3DF" madft ldf)
  (ugsu "eps4KC" makct lck)
  (ugsu "eps5KO" makot lko)
  (tablica_va masb masv masa masom masep) 
)
;=====================================================================================================

(defun c:sem3_2 (/ osm le
ab omeg lab mul bc cd df af de fe fo ck ko kml lbd ldf lck lko mvb mab fi
a b f o d e c k m l masivm pb muv pol bv dv ev cv kv sv mv lv mvd mvc mve mvk mvl mvm mvs mvdb mvkc ome2 ome3 ome4 ome5 masom 
madbn madfn makcn makon masv masb pa polu mua ldbn ldfn lkcn lkon ba dbn dfn da ea ca kcn kon ka sa ma la mac mae mak mad mal 
mam mas madb makc madbt madft makct makot eps2 eps3 eps4 eps5 masep masa)
  (VL-LOAD-COM)
  (setq osm (getvar "OSMODE"))
  (setvar "OSMODE" 20919)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "TIMESI.TTF" )))
	le (append le (list (cons 2 "�����")))	
  )
  (entmakex le)
  (setvar "textstyle" "�����")
  (load "funkcii_tmm.lsp")
  (load "tmm_semi.lsp")
  (vvod)
  (tochkim)
  (cherm)
  (plansk)
  (modulv)
  (cherv)
  (planusk)
  (modula)
  (chera)  
  (omep)
  (setvar "OSMODE" osm)
  (setvar "dimtxsty" "�����")
  (princ)
)
(c:sem3_2)