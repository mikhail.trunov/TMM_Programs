;���� ��� ����������� �� ��������������� ������� � 14 ��� 2
(defun vvod (/ f le dmu)
  (initget 6)
  (setq f (getreal "\n������� ���� �� ")
        ab (getreal "\n������� �B [mm] ")
        omeg (getreal "\n������� ����� ")
        lab (getreal "\n������� AB [m] ")
        mul (/ lab ab)
	;����� �� �����
	bc (* 3.3 ab)
	cd (* 1.76 ab)
	y2 (* 1.4 ab)
	de (* 2.94 ab)
	ce (* 2.36 ab)
	x ce
	ef (* 5.5 ab)
	es4 (* 2. ab)
	bs2 (* 1.5 ab)
	y1 (* 2.83 ab)		
	;�������� �����
	lbc (* lab 3.3)
	lcd (* lab 1.76)
	lef (* lab 5.5)
	;�������� � ���������
	mvb (* omeg lab)
	mab (* omeg omeg lab)
	fi (- (* 0.5 pi) (* f (/ pi 180.)))
  );end of setq
  (ishodn omeg lab f)
  (tabl_razmer (list "\\A1;l\\H0.7x;\\S^AB;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^BC;\\H1.4286x;"
		     "\\A1;l\\H0.7x;\\S^CD;\\H1.4286x;" "\\A1;y\\H0.7x;\\S^2;\\H1.4286x;"
		     "\\A1;l\\H0.7x;\\S^DE;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^CE;\\H1.4286x;"
		     "x" "\\A1;l\\H0.7x;\\S^EF;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^ES4;\\H1.4286x;"
		     "\\A1;l\\H0.7x;\\S^BS2;\\H1.4286x;" "\\A1;y\\H0.7x;\\S^1;\\H1.4286x;"
	       )
    	       (list ab bc cd y2 de ce x ef es4 bs2 y1)
    mul
  )
);end of vvod

;;;;;;;;;;;;;;;;;;;;;�������, ������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;----------------------------------------------------------------------------------
;;;;;;---=================������� ���������� ����� ���������================-------
(defun tochkim ()
  (setq
    a (getpoint "\n��� ������� ��������: ")
    d (list (+ (car a) x) (+ (cadr a) y1))
    b (polar a fi ab)    
    c (car (vl-sort
	     (circint b bc d cd)
	     (function (lambda (e1 e2) (> (car e1) (car e2)))) 
	   )
      )  
    e (polar d (+ (angle d c) (arccos (/ (+ (* cd cd) (* de de) (* -1. ce ce)) (* 2. de cd)))) de)
    f (car (vl-sort
	     (dwgru (list -1e15 (- (cadr a) y2))
		    (list 1e15 (- (cadr a) y2))
		    e ef
	     )
	     (function (lambda (e1 e2) (< (car e1) (car e2)))) 
	   )
      )
    s2 (polar b (angle b c) bs2)
    s3 (inters d (polar c (angle c e) (* 0.5 (distance c e)))
	       c (polar d (angle d e) (* 0.5 (distance d e)))
	       nil
       )
    s4 (polar e (angle e f) es4)       
    masivm (list a b c d e f)
    mass (list s2 s3 s4)
  )
)

;----------------------------------------------------------------------------------
;--==������� ����� ����� ��������� � ���������� ��������� � ������ ���������===----

(defun plansk ()
  (initget 1)
  (setq pb (getdist "\n������� ����� ������� �������� Va [mm] ")
   	pol (getpoint "\n������� ����� ����� ��������� ")  
  	muv (/ mvb pb)
  ;======����� ����� ���������========  
  	bv (polar pol (- (angle a b) (/ pi 2)) pb)
	cv (inters pol (polar pol (+ (angle d c) (* 0.5 pi)) 100.)
		   bv (polar bv (+ (angle b c) (* 0.5 pi)) 100.)
		   nil
	    )
	ev (polar pol (+ (angle pol cv) (- (angle d e) (angle d c))) (* (distance pol cv) (/ de cd)))
	s3v (inters pol (polar cv (angle cv ev) (* 0.5 (distance cv ev)))
		    cv (polar pol (angle pol ev) (* 0.5 (distance pol ev)))
	      nil
	    )
	s2v (polar bv (angle bv cv) (* (distance bv cv) (/ bs2 bc)))
	fv (inters ev (polar ev (- (angle f e) (* pi 0.5)) 5.)
		   pol (polar pol 0. 1e5)
		   nil
	   )
	s4v (polar ev (angle ev fv) (* (distance ev fv) (/ es4 ef)))
  ;========������ ��������=======
    ;����������
        mvc (* muv (distance pol cv))
	mve (* muv (distance pol ev))
	mvf (* muv (distance pol fv))
	mvs3 (* muv (distance pol s3v))
	mvs4 (* muv (distance pol s4v))	
	mvs2 (* muv (distance pol s2v))
   ;�������������
	mvcb (* muv (distance cv bv))
	mvfe (* muv (distance fv ev))
  ;========������� ��������====================
        ome2 (/ mvcb lbc)
	ome3 (/ mvc lcd)
	ome4 (/ mvfe lef)
	masv (list mvc mve mvf mvs2 mvs3 mvs4 mvcb mvfe)
	masb (list "C" "E" "F" "\\A1;S\\H0.7x;\\S^2;"
		    "\\A1;S\\H0.7x;\\S^3;\\H1.4286x;"
		    "\\A1;S\\H0.7x;\\S^4;\\H1.4286x;" "CB" "FE")
	masom (list ome2 ome3 ome4)
  ;=====���������� ���������=====
	macbn (/ (expt mvcb 2) lbc)
	macdn (/ (expt mvc 2) lcd)
	mafen (/ (expt mvfe 2) lef)
  )
  (duga b c bv cv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^2;\\H2;")
  (duga d c pol cv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^3;\\H2;")
  (duga e f ev fv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^4;\\H2;")  
)
;----------------------------------------------------------------------------------
;---------========������� ����� ����� ��������� � ������ ���������========---------
(defun planusk (/)
  (initget 1)
  (setq pa (getdist "\n������� ����� ������� ��������� ����� B [mm] ")	
	polu (getpoint "\n������� ����� ����� ��������� ")
	mua (/ mab pa)
	;=====����� �������� ���������� ���������=====
	lcbn (/ macbn mua)
	lcdn (/ macdn mua)
	lfen (/ mafen mua)
	;=====����� ����� ���������=====
	ba (polar polu (angle b a) pa)
	cbn (polar ba (angle c b) lcbn)
	cdn (polar polu (angle c d) lcdn)	
	ca (inters cbn (polar cbn (+ (angle c b) (* 0.5 pi)) 100.)
		   cdn (polar cdn (+ (angle c d) (* 0.5 pi)) 100.)
		   nil
	    )
	ea (polar polu (+ (angle polu ca) (- (angle d e) (angle d c))) (* (distance polu ca) (/ de cd)))
	fen (polar ea (angle f e) lfen)
	fa (inters fen (polar fen (- (angle f e) (* pi 0.5)) 5.)
		   polu (polar polu 0. 5.)
		   nil
	   )
	s3a (inters polu (polar ca (angle ca ea) (* 0.5 (distance ca ea)))
		    ca (polar polu (angle polu ea) (* 0.5 (distance polu ea)))
		    nil
	    )
	s2a (polar ba (angle ba ca) (* (distance ba ca) (/ bs2 bc)))
	s4a (polar ea (angle ea fa) (* (distance ea fa) (/ es4 ef)))
	;=====������ ���������� ���������=========
	mac (* mua (distance polu ca))
	mae (* mua (distance polu ea))
	maf (* mua (distance polu fa))
	macb (* mua (distance ca ba))
	mafe (* mua (distance fa ea))
	mas2 (* mua (distance polu s2a))
	mas3 (* mua (distance polu s3a))
	mas4 (* mua (distance polu s4a))
	macb (* mua (distance ba ca))
	mafe (* mua (distance fa ea))
	;=====������ �������������� ���������=========
	macbt (* mua (distance cbn ca))
	macdt (* mua (distance cdn ca))
	mafet (* mua (distance fen fa))
	;������� ���������
	eps2 (/ macbt lbc)
	eps3 (/ macdt lcd)
	eps4 (/ mafet lef)
	masa (list mac mae maf mas2 mas3 mas4 macb mafe)
	masep (list eps2 eps3 eps4)
  )
  (duga b c cbn ca 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^2;\\H2;")
  (duga d c cdn ca 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^3;\\H2;")
  (duga e f fen fa 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^4;\\H2;")
)
;=============================================================================================


(defun cherm (/ i)
  (li (list a b c d e f)) (li (list d s3 c e s3)) 
  (mapcar '(lambda (t1) (add_circ 0.7 t1)) masivm)
  (mapcar '(lambda (t1) (add_circ 0.7 t1)) mass)
  (add_text s2 "\\A1;S\\H0.7x;\\S^2;\\H1.4286x;")
  (add_text s3 "\\A1;S\\H0.7x;\\S^3;\\H1.4286x;")
  (add_text s4 "\\A1;S\\H0.7x;\\S^4;\\H1.4286x;")
  (setq i 65)
  (mapcar '(lambda (t1) (progn
			  (add_text t1 (chr i))
			  (setq i (1+ i))
			)
	   );end of lambda
           masivm
  );end of mapcar
  (add_block f 0.)
  (add_opor a (+ (* 0.5 pi) fi))
  (add_opor d 0.)
  (duga a b b (polar b (- (angle a b) (* 0.5 pi)) 10.) 0.5 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^1;\\H2;")
  (muul lab ab)
)

(defun cherv (/ dmu le)
  (add_text pol "p,a,d")
  (li (list s3v ev cv s3v))
  ;����������
  (risskor pol bv "b" "v^B")
  (risskor pol cv "c" "v^C")
  (risskor pol ev "e" "v^E")
  (risskor pol fv "f" "v^F")
  (risskor pol s2v "s^2" "v^S2")  
  (risskor pol s3v "s^3" "v^S3")
  (risskor pol s4v "s^4" "v^S4")
  ;�������������
  (risskor bv cv nil "v^CB")
  (risskor ev fv nil "v^FE")
  (muuv mvb pb)
  (formv omeg lab)
)

(defun chera (/)
  (li (list s3a ea ca s3a))
  (add_text polu "\\A1;p\\H0.7x;\\S^1;\\H1.4286x;,a,d\\H1.4286x;")
  ;����� 1
  (risskor polu ba "b" "a^B")  
  ;����� 3
  (risskor polu ca "c" "a^C")
  (risskor polu ea "e" "a^E")
  ;����� 2
  (risskor polu fa "f" "a^F") 
  (risskor polu s2a "s^2" "a^S2")
  (risskor polu s3a "s^3" "a^S3")
  (risskor polu s4a "s^4" "a^S4")
  ;������������� ���������
  (risskor ba cbn nil "an^CB")
  (risskor polu cdn nil "an^CD")
  (risskor ea fen nil "an^FE")
  (risskor ea fa nil "a^FE")
  (risskor ba ca nil "a^CB")
  (risskor cdn ca nil "a\U+03C4^CD")
  (risskor cbn ca nil "a\U+03C4^CB")
  (risskor fen fa nil "a\U+03C4^FE")
  (muua mab pa)
  (forma omeg lab)
)

(defun omep ()
  (formsk2 muv (distance pol cv) "C" "pc" t)
  (formsk2 muv (distance pol ev) "E" "pe" t)
  (formsk2 muv (distance pol fv) "F" "pf" t)
  (formsk2 muv (distance pol s2v) "S2" "ps\\H0.7x;\\S^2;\\H1.4286x;" t)
  (formsk2 muv (distance pol s3v) "S3" "ps\\H0.7x;\\S^3;\\H1.4286x;" t)
  (formsk2 muv (distance pol s4v) "S4" "ps\\H0.7x;\\S^4;\\H1.4286x;" t)
  (formsk2 muv (distance cv bv) "CB" "CB" t)
  (formsk2 muv (distance fv ev) "FE" "FE" t)
  (add_text (getpoint "\n��������������� speed point E: ")
	    (strcat "\\A1;pe=pc\U+00D7\\H0.7x;\\SDE/DC;\\H1.4286x;="
		    (rtos (distance pol cv) 2 0) "\U+00D7\\H0.7x;\\S" (rtos de 2 0) "/" (rtos cd 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance pol cv) (/ de cd)) 2 1) " ��"
	    )
  )
  ;����������
  (formusk "aCB" mvcb lbc)
  (formusk "aCD" mvc lcd)
  (formusk "aFE" mvfe lef)
  ;��������������
  (formuskt2 "CB" (distance cbn ca) mua)
  (formuskt2 "CD" (distance cdn ca) mua)
  (formuskt2 "FE" (distance fen fa) mua)
  (formsk2 mua (distance polu ca) "C" "p\\H0.7x;\\S^1;\\H1.4286x;c" nil)
  (formsk2 mua (distance polu ea) "E" "p\\H0.7x;\\S^1;\\H1.4286x;e" nil)
  (formsk2 mua (distance polu fa) "F" "p\\H0.7x;\\S^1;\\H1.4286x;f" nil)
  (formsk2 mua (distance polu s2a) "S2" "p\\H0.7x;\\S^1;\\H1.4286x;s\\H0.7x;\\S^2;\\H1.4286x;" nil)
  (formsk2 mua (distance polu s3a) "S3" "p\\H0.7x;\\S^1;\\H1.4286x;s\\H0.7x;\\S^3;\\H1.4286x;" nil)
  (formsk2 mua (distance polu s4a) "S4" "p\\H0.7x;\\S^1;\\H1.4286x;s\\H0.7x;\\S^4;\\H1.4286x;" nil)
  (formsk2 mua (distance ca ba) "CB" "cb" nil)
  (formsk2 mua (distance fa ea) "FE" "fe" nil)
  (add_text (getpoint "\n��������������� acel point E: ")
	    (strcat "\\A1;p\\H0.7x;\\S^1;\\H1.4286x;e=p\\H0.7x;\\S^1;\\H1.4286x;c\U+00D7"
		    "\\H0.7x;\\SDE/DC;\\H1.4286x;="(rtos (distance polu ca) 2 0)
		    "\U+00D7\\H0.7x;\\S" (rtos de 2 0) "/" (rtos cd 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance polu ca) (/ de cd)) 2 1) " ��"
	    )
  )
  (ugsu "ome2CB" mvcb lbc)
  (ugsu "ome3CD" mvc lcd)
  (ugsu "ome4FE" mvfe lef)  
  (ugsu "eps2CB" macbt lbc)
  (ugsu "eps3CD" macdt lcd)
  (ugsu "eps4FE" mafet lef)
)
;=====================================================================================================

(defun c:sem14_2 (/ osm le
ab omeg lab mul bc cd y2 de ce x ef es4 bs2 y1 lbc lcd lef mvb mab fi
a d b c e f s2 s3 s4 masivm mass pb pol muv bv cv ev s3v s2v fv s4v mvc mve mvf mvs3 mvs4 mvs2 mvcb mvfe 
ome2 ome3 ome4 masv masb masom macbn macdn mafen pa polu mua lcbn lcdn lfen ba cbn cdn ca ea fen fa s3a 
s2a s4a mac mae maf macb mafe mas2 mas3 mas4 macb mafe macbt macdt mafet eps2 eps3 eps4 masa masep)
  (VL-LOAD-COM)
  (setq osm (getvar "OSMODE"))
  (setvar "OSMODE" 20919)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "TIMESI.TTF" )))
	le (append le (list (cons 2 "�����")))	
  )
  (entmakex le)
  (setvar "textstyle" "�����")
  (load "funkcii_tmm.lsp")
  (load "tmm_semi.lsp")
  (vvod)
  (tochkim)
  (cherm)
  (plansk)
  (cherv)
  (planusk)
  (chera)  
  (omep)
  (tablica_va masb masv masa masom masep)
  (setvar "OSMODE" osm)
  (setvar "dimtxsty" "�����")
)
(c:sem14_2)
