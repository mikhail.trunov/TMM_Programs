;���� ��� ����������� �� ��������������� ������� � 14 ��� 2
(defun vvod (/ f )
  (initget 6)
  (setq f (getreal "\n������� ���� �� ")
        ab (getreal "\n������� �B [mm] ")
        omeg (getreal "\n������� ����� ")
        lab (getreal "\n������� AB [m] ")
        mul (atof (rtos (/ lab ab) 2 4))
	;����� �� �����
	bc (* 2.7 ab)
	cd (* 1.07 ab)
	ke (* 2.42 ab)
	kl (* 2. ab)
	ef (* 1.8 ab)
	x ab
	x1 (* 1.35 ab)
	x2 (* 2.7 ab)
	dkec (* 1.3 ab)			
	;�������� �����
	lbc (* lab 2.7)
	lef (* lab 1.8)
	lkl (* lab 2.)
	lcd (* lab 1.07)
	;�������� � ���������
	mvb (* omeg lab)
	mab (* omeg omeg lab)
	fi (* f (/ pi 180.))
  );end of setq
  (ishodn omeg lab f)
   (tabl_razmer (list "\\A1;l\\H0.7x;\\S^AB;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^BC;\\H1.4286x;"
		      "\\A1;l\\H0.7x;\\S^CD;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^KE;\\H1.4286x;"
		      "\\A1;l\\H0.7x;\\S^KL;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^EF;\\H1.4286x;"
		      "x" "\\A1;x\\H0.7x;\\S^1;\\H1.4286x;" "\\A1;x\\H0.7x;\\S^2;\\H1.4286x;"
		      "\\A1;l\\H0.7x;\\S^KD;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^DE;\\H1.4286x;"
		)
    (list ab bc cd ke kl ef x x1 x2 dkec dkec) mul)
);end of vvod


;;;;;;;;;;;;;;;;;;;;;�������, ������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;----------------------------------------------------------------------------------
;;;;;;---=================������� ���������� ����� ���������================-------
(defun tochkim (/ ddd)
  (setq
    a (getpoint "\n��������: ")
    b (polar a fi ab)
    d (polar a pi x2)
    c (car (vl-sort
	     (circint b bc d cd)
	     (function (lambda (e1 e2) (< (cadr e1) (cadr e2)))) 
	   )
      )
    k (polar d (- (angle d c) (* 0.5 pi)) dkec)
    e (polar d (+ (angle d c) (* 0.5 pi)) dkec)
    f (car (vl-sort
	     (dwgru (setq ddd (polar a pi (- x2 x1)))
		    (polar ddd (* 0.5 pi) 10.)
		    e ef
	     )
	     (function (lambda (e1 e2) (> (cadr e1) (cadr e2)))) 
	   )
      )
    l (car (vl-sort
	     (dwgru (setq ddd (polar a pi (+ x2 x)))
		    (polar ddd (* 0.5 pi) 10.)
		    k kl
	     )
	     (function (lambda (e1 e2) (> (cadr e1) (cadr e2)))) 
	   )
      )
    masivm (list a b c d e f k l)
  )  
)
;----------------------------------------------------------------------------------
;--==������� ����� ����� ���������
(defun plansk (/)
  (initget 1)
  (setq pb (getdist "\n������� ����� ������� �������� Va [mm] ")
	muv (atof (rtos (/ mvb pb) 2 4))
	pol (getpoint "���� ���������: ")
	bv (polar pol (+ (angle a b) (/ pi 2)) pb)		
	cv (inters pol (polar pol (+ (angle d c) (* 0.5 pi)) 10.)
		   bv (polar bv (- (angle b c) (* pi 0.5)) 5.)
		   nil
	   )	
	kv (polar pol (- (angle pol cv) (* 0.5 pi)) (* dkec (/ (distance pol cv) cd)))
	ev (polar pol (+ (angle pol cv) (* 0.5 pi)) (* dkec (/ (distance pol cv) cd)))
	fv (inters pol (polar pol (* 0.5 pi) 10.)
		   ev (polar ev (- (angle f e) (* pi 0.5)) 5.)
		   nil
	   )
	lv (inters pol (polar pol (* 0.5 pi) 10.)
		   kv (polar kv (- (angle l k) (* pi 0.5)) 5.)
		   nil
	   )
  );end of setq
);plansk

(defun modulv ();���������� ��������� � ������ ���������
  (setq
   ;========������ ��������=======
    ;����������
        mvc (atof (rtos (* muv (atof (rtos (distance pol cv) 2 0))) 2 3))	
	mve (atof (rtos (* muv (atof (rtos (distance pol ev) 2 0))) 2 3))
	mvk (atof (rtos (* muv (atof (rtos (distance pol kv) 2 0))) 2 3))
	mvl (atof (rtos (* muv (atof (rtos (distance pol fv) 2 0))) 2 3))
	mvf (atof (rtos (* muv (atof (rtos (distance pol fv) 2 0))) 2 3))	
   ;�������������
	mvcb (atof (rtos (* muv (atof (rtos (distance bv cv) 2 0))) 2 3))
	mvfe (atof (rtos (* muv (atof (rtos (distance fv ev) 2 0))) 2 3))
	mvlk (atof (rtos (* muv (atof (rtos (distance lv kv) 2 0))) 2 3))
  ;========������� ��������====================
        ome2 (atof (rtos (/ mvcb lbc) 2 3))
	ome3 (atof (rtos (/ mvc lcd) 2 3))
	ome4 (atof (rtos (/ mvfe lef) 2 3))
	ome5 (atof (rtos (/ mvlk lkl) 2 3))
	masom (list ome2 ome3 ome4 ome5)
  ;=====���������� ���������=====
	macbn (/ (expt mvcb 2) lbc)
	macdn (/ (expt mvc 2) lcd)
	mafen (/ (expt mvfe 2) lef)
	malkn (/ (expt mvlk 2) lkl)
	masv (list mvc mve mvk mvf mvl mvcb mvfe mvlk)
	masb (list "C" "E" "K" "F" "L" "CB" "FE" "LK")
  )
);modulv

;----------------------------------------------------------------------------------
;---------========������� ����� ����� ��������� � ������ ���������========---------
(defun planusk (/)
  (initget 1)
  (setq pa (getdist "\n������� ����� ������� ��������� ����� B [mm] ")	
	polu (getpoint "\n������� ����� ����� ��������� ")
	mua (atof (rtos (/ mab pa) 2 3))
	;=====����� �������� ���������� ���������=====
	lcbn (/ macbn mua)
	lcdn (/ macdn mua)
	lfen (/ mafen mua)
	llkn (/ malkn mua)	
	;=====����� ����� ���������=====
	ba (polar polu (angle b a) pa)
	cbn (polar ba (angle c b) lcbn)
	cdn (polar polu (angle c d) lcdn)
	ca (inters cdn (polar cdn (+ (angle c d) (* 0.5 pi)) 10.)
		   cbn (polar cbn (+ (angle b c) (* pi 0.5)) 10.)
		   nil
	   )
	ka (polar polu (- (angle polu ca) (* 0.5 pi)) (* dkec (/ (distance polu ca) cd)))
	ea (polar polu (+ (angle polu ca) (* 0.5 pi)) (* dkec (/ (distance polu ca) cd)))
	fen (polar ea (angle f e) lfen)
	lkn (polar ka (angle l k) llkn)
	fa (inters fen (polar fen (+ (angle f e) (* 0.5 pi)) lfen)
		   polu (polar polu (* 0.5 pi) 10.)
		   nil
	   )
	la (inters lkn (polar lkn (+ (angle l k) (* 0.5 pi)) llkn)
		   polu (polar polu (* 0.5 pi) 10.)
		   nil
	   )
  );setq
);planusk
(defun modula ()
  (setq	;=====������ ���������� ���������=========
	mac (atof (rtos (* mua (distance polu ca)) 2 3))
	mae (atof (rtos (* mua (distance polu ea)) 2 3))
	mak (atof (rtos (* mua (distance polu ka)) 2 3))
	maf (atof (rtos (* mua (distance polu fa)) 2 3))
	mal (atof (rtos (* mua (distance polu la)) 2 3))
	
        macb (atof (rtos (* mua (distance ca ba)) 2 3))
	mafe (atof (rtos (* mua (distance fa ea)) 2 3))
	malk (atof (rtos (* mua (distance la ka)) 2 3))
	;=====������ �������������� ���������=========
	macbt (atof (rtos (* mua (distance cbn ca)) 2 3))
	macdt (atof (rtos (* mua (distance cdn ca)) 2 3))
	mafet (atof (rtos (* mua (distance fen fa)) 2 3))
	malkt (atof (rtos (* mua (distance lkn la)) 2 3))
	;������� ���������
	eps2 (atof (rtos (/ macbt lbc) 2 3))
	eps3 (atof (rtos (/ macdt lcd) 2 3))
	eps4 (atof (rtos (/ mafet lef) 2 3))
	eps5 (atof (rtos (/ malkt lkl) 2 3))
	masep (list eps2 eps3 eps4 eps5)
	masa (list mac mae mak maf mal macb mafe malk)
  )
)
;=============================================================================================


(defun cherm (/ i)  
  (li (list a b c k e c)) (li (list k l))(li (list e f)) 
  (mapcar '(lambda (t1) (add_circ 0.7 t1)) masivm)
  (add_block l (* 0.5 pi))(add_block f (* 0.5 pi))
  (duga a b b (polar b (- (angle a b) (* 0.5 pi)) 10.) 0.5 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^1;\\H2;")
  (add_text a "A")(add_text b "B")(add_text c "C")(add_text d "D")
  (add_text e "E")(add_text f "F")(add_text k "K")(add_text l "L")(add_text k "K")
  (add_opor a (+ (* 0.5 pi) fi)) (add_opor d (angle d e))
  (muul lab ab)
);cherm

(defun cherv (/)
  (add_text pol "p,a,d")
  (li (list kv cv ev))
  ;����������
  (risskor pol bv "b" "v^B")
  (risskor pol cv "c" "v^C")
  (risskor pol kv "k" "v^K")
  (risskor pol ev "e" "v^E")
  (risskor pol fv "f" "v^F")
  (risskor pol lv "l" "v^L")
  ;�������������
  (risskor bv cv nil "v^CB") 
  (risskor ev fv nil "v^FE")
  (risskor kv lv nil "v^LK")
  (duga b c bv cv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^2;\\H2;")
  (duga d c pol cv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^3;\\H2;")
  (duga e f ev fv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^4;\\H2;")
  (duga k l kv lv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^5;\\H2;")
  ;���������
  (muuv mvb pb)
  (formv omeg lab)
)

(defun chera (/)
  (add_text polu "\\A1;p\\H0.7x;\\S^1;\\H1.42857x;,a,d" )

  (li (list ka ca ea))
  (risskor polu ba "b" "a^B")

  (risskor polu ca "c" "a^C")
  (risskor polu ea "e" "a^E")
  (risskor polu ka "k" "a^K")
  (risskor polu fa "f" "a^F")
  (risskor polu la "l" "a^L")

  (risskor ba ca nil "a^CB")
  (risskor ea fa nil "a^FE")
  (risskor ka la nil "a^LK")
  ;������������� ���������
  (risskor ba cbn nil "an^CB")
  (risskor polu cdn nil "an^CD")
  (risskor ea fen nil "an^FE")
  (risskor ka lkn nil "an^LK")
  (risskor cbn ca nil "a\U+03C4^CB")
  (risskor cdn ca nil "a\U+03C4^CD")
  (risskor fen fa nil "a\U+03C4^FE")
  (risskor lkn la nil "a\U+03C4^LK")
  (duga b c cbn ca 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^2;\\H2;")
  (duga d c cdn ca 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^3;\\H2;")
  (duga e f fen fa 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^4;\\H2;")
  (duga k l lkn la 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^5;\\H2;")
  (muua mab pa)
  (forma omeg lab)
)

(defun omep ()
  ;���������
  ;��� ���������� ���������
  (formsk2 muv (distance pol cv) "C" "pc" t)
  (formsk2 muv (distance pol ev) "E" "pe" t)  
  (formsk2 muv (distance pol fv) "F" "pf" t)
  (formsk2 muv (distance pol kv) "K" "pk" t)
  (formsk2 muv (distance pol lv) "L" "pl" t)
  ;��� ������������� ���������
  (formsk2 muv (distance cv bv) "CB" "bc" t)
  (formsk2 muv (distance fv ev) "FE" "fe" t)
  (formsk2 muv (distance lv kv) "LK" "kl" t)
  (add_text (getpoint "\n��������������� speed point E: ")
	    (strcat "\\A1;pe=pc\U+00D7\\H0.7x;\\SDE/DC;\\H1.4286x;="
		    (rtos (distance pol cv) 2 0) "\U+00D7\\H0.7x;\\S" (rtos dkec 2 0) "/" (rtos cd 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance pol cv) (/ dkec cd)) 2 1) " ��"
	    )
  )
  (add_text (getpoint "\n��������������� speed point K: ")
	    (strcat "\\A1;pk=pc\U+00D7\\H0.7x;\\SDK/DC;\\H1.4286x;="
		    (rtos (distance pol cv) 2 0) "\U+00D7\\H0.7x;\\S" (rtos dkec 2 0) "/" (rtos cd 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance pol cv) (/ dkec cd)) 2 1) " ��"
	    )
  )
  ;���������
  (formsk2 mua (distance polu ca) "C" "p\\H0.7x;\\S^1;\\H1.4286x;c" nil)
  (formsk2 mua (distance polu ea) "E" "p\\H0.7x;\\S^1;\\H1.4286x;e" nil)
  (formsk2 mua (distance polu fa) "F" "p\\H0.7x;\\S^1;\\H1.4286x;f" nil)
  (formsk2 mua (distance polu la) "L" "p\\H0.7x;\\S^1;\\H1.4286x;l" nil)
  (formsk2 mua (distance polu ka) "K" "p\\H0.7x;\\S^1;\\H1.4286x;k" nil)
  (formsk2 mua (distance ca ba) "CB" "bc" nil)
  (formsk2 mua (distance fa ea) "FE" "fe" nil)
  (formsk2 mua (distance la ka) "LK" "kl" nil)
  (add_text (getpoint "\n��������������� acel point E: ")
	    (strcat "\\A1;p\\H0.7x;\\S^1;\\H1.4286x;e=p\\H0.7x;\\S^1;\\H1.4286x;c"
		    "\U+00D7\\H0.7x;\\SDE/DC;\\H1.4286x;="(rtos (distance polu ca) 2 0)
		    "\U+00D7\\H0.7x;\\S" (rtos dkec 2 0) "/" (rtos cd 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance polu ca) (/ dkec cd)) 2 1) " ��"
	    )
  )
  (add_text (getpoint "\n��������������� acel point K: ")
	    (strcat "\\A1;p\\H0.7x;\\S^1;\\H1.4286x;k=p\\H0.7x;\\S^1;\\H1.4286x;c"
		    "\U+00D7\\H0.7x;\\SDK/DC;\\H1.4286x;="(rtos (distance polu ca) 2 0)
		    "\U+00D7\\H0.7x;\\S" (rtos dkec 2 0) "/" (rtos cd 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance polu ca) (/ dkec cd)) 2 1) " ��"
	    )
  )
  ;���������� ���������
  (formusk "aCB" mvcb lbc)
  (formusk "aCD" mvc lcd)
  (formusk "aFE" mvfe lef)
  (formusk "aLK" mvlk lkl)
  ;�������������� ���������
  (formuskt2 "CB" (distance cbn ca) mua)
  (formuskt2 "CD" (distance cdn ca) mua)
  (formuskt2 "FE" (distance fen fa) mua)
  (formuskt2 "LK" (distance lkn la) mua)
  
  ;�����
  (ugsu "ome2CB" mvcb lbc)
  (ugsu "ome3CD" mvc lcd)
  (ugsu "ome4FE" mvfe lef)
  (ugsu "ome5LK" mvlk lkl)
  ;��������
  (ugsu "eps2CB" macbt lbc)
  (ugsu "eps3CD" macdt lcd)
  (ugsu "eps4FE" mafet lef)
  (ugsu "eps5LK" malkt lkl)
  (tablica_va masb masv masa masom masep) 
)
;=====================================================================================================

(defun c:sem_07 (/ osm le
ab omeg lab mul bc cd ke kl ef x x1 x2 dkec lbc lef lkl lcd mvb mab fi
a b d c k e f l masivm pb muv pol bv cv kv ev fv lv mvc mve mvk mvl mvf mvcb mvfe mvlk 
ome2 ome3 ome4 ome5 masom macbn macdn mafen malkn masv masb pa polu mua lcbn lcdn lfen llkn 
ba cbn cdn ca ka ea fen lkn fa la mac mae mak maf mal macb mafe malk macbt macdt mafet malkt
eps2 eps3 eps4 eps5 masep masa)
  (VL-LOAD-COM)
  (setq osm (getvar "OSMODE"))
  (setvar "OSMODE" 20919)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "TIMESI.TTF" )))
	le (append le (list (cons 2 "�����")))	
  )
  (entmakex le)
  (setvar "textstyle" "�����")
  (load "funkcii_tmm.lsp")
  (load "tmm_semi.lsp")
  (vvod)
  (tochkim)
  (cherm)
  (plansk)
  (modulv)
  (cherv)
  (planusk)
  (modula)
  (chera)  
  (omep)
  (setvar "OSMODE" osm)
  (setvar "dimtxsty" "�����")
  (princ)
)
(c:sem_07)