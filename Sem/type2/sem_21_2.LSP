;���� ��� ����������� �� ��������������� ������� � 21 ��� 2
(defun vvod (/ f)
  (initget 6)
  (setq f (getreal "\n������� ���� �� ")
        ab (getdist "\n������� �B [mm] ")
        omeg (getreal "\n������� ����� ")
        lab (getreal "\n������� AB [m] ")
        mul (/ lab ab)
	;����� �� �����
	ad ab
	bc (* 3.0 ab)
	ed bc
	bf (* 1.6 ab)
	fc bf
	dk bf
	ek bf
	;�������� �����
	lbc (* lab 3.0)
	lde lbc
	;�������� � ���������
	mvb (* omeg lab)
	mvd mvb
	mab (* omeg omeg lab)
	mad mab
	fi (* f (/ pi 180.))
  );end of setq
  (ishodn omeg lab f)
  (tabl_razmer (list "\\A1;l\\H0.7x;\\S^AB;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^AD;\\H1.4286x;"
		     "\\A1;l\\H0.7x;\\S^BC;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^ED;\\H1.4286x;"
		     "\\A1;l\\H0.7x;\\S^BF;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^FC;\\H1.4286x;"
		     "\\A1;l\\H0.7x;\\S^DK;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^EK;\\H1.4286x;"
	       )
    	       (list ab ad bc ed bf fc dk ek)
    mul
  )
);end of vvod


;;;;;;;;;;;;;;;;;;;;;�������, ������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;----------------------------------------------------------------------------------
;;;;;;---=================������� ���������� ����� ���������================-------
(defun tochkim ()
  (setq
    a (getpoint "\n��� ������� ��������: ")
    b (polar a fi ab)
    d (polar a (+ fi pi) ad)
    c (car (vl-sort
	     (dwgru a (polar a 0. 100.) b bc)
	     (function (lambda (e1 e2) (< (car e1) (car e2)))) 
	   )
      )     
    e (car (vl-sort
	     (dwgru a (polar a 0. 1e15) d ed)
	     (function (lambda (e1 e2) (> (car e1) (car e2)))) 
	   )
      )     
    f (polar c (+ (angle c b) (arccos (/ bc (* 2. fc)))) fc)
    k (polar d (+ (angle d e) (arccos (/ bc (* 2. fc)))) fc)
    s2 (inters
	 b (polar c (angle c f) (* 0.5 (distance c f)))
	 c (polar b (angle b f) (* 0.5 (distance b f)))
	 nil
       )
    s4 (inters
	 d (polar e (angle e k) (* 0.5 (distance e k)))
	 k (polar d (angle d e) (* 0.5 (distance e d)))
	 nil
       )    
    masivm (list a b c d e f)
  )
)

;----------------------------------------------------------------------------------
;--==������� ����� ����� ��������� � ���������� ��������� � ������ ���������===----
(defun plansk ()
  (initget 1)
  (setq pb (getdist "\n������� ����� ������� �������� Va [mm] ")
	pol (getpoint "\n������� ����� ����� ��������� ")  
  	muv (/ mvb pb)
  ;======����� ����� ���������========  
  	bv (polar pol (+ (angle a b) (/ pi 2.)) pb)
	dv (polar pol (+ (angle a d) (/ pi 2.)) pb)
	cv (inters
	     pol (polar pol 0. 1e15)
	     bv (polar bv (+ (angle b c) (* pi 0.5)) 5)
	     nil
	   )
	ev (inters
	     pol (polar pol 0. 1e15)
	     dv (polar dv (+ (angle d e) (* pi 0.5)) 5)
	     nil
	   )
	fv (polar cv
		  (+ (arccos (/ bc (* 2.0 bf))) (angle cv bv))
		  (* (distance bv cv) (/ bf bc))
	   )
	kv (polar dv
		  (+ (arccos (/ ed (* 2.0 ek))) (angle dv ev))
		  (* (distance ev dv) (/ dk ed))
	   )
	s2v (inters
	      bv (polar cv (angle cv fv) (* 0.5 (distance cv fv)))
	      fv (polar bv (angle bv cv) (* 0.5 (distance bv cv)))
	      nil
	    )
	s4v (inters
	      dv (polar ev (angle ev kv) (* 0.5 (distance ev kv)))
	      kv (polar ev (angle ev dv) (* 0.5 (distance ev dv)))
	      nil
	    )
	 ;========������ ��������=======
    ;����������
        mvc (* muv (distance pol cv))
	mve (* muv (distance pol ev))
	mvf (* muv (distance pol fv))
	mvk (* muv (distance pol kv))
	mvs2 (* muv (distance pol s2v))
	mvs4 (* muv (distance pol s4v))
   ;�������������
	mvcb (* muv (distance cv bv))
	mved (* muv (distance ev dv))
  ;========������� ��������====================
        ome2 (/ mvcb lbc)
	ome4 (/ mved lde)
	masom (list ome2 ome4)	
  ;=====���������� ���������=====
	macbn (/ (expt mvcb 2) lbc)
	maedn (/ (expt mved 2) lde)
	masv (list mvc mvf mve mvk mvs2 mvs4 mvcb mved)
	masb (list "C" "F" "E" "G" "\\A1;S\\H0.7x;\\S^2;" "\\A1;S\\H0.7x;\\S^4;" "CB" "ED")
   );_end of setq
  (duga b c bv cv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^2;\\H2;")
  (duga d e dv ev 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^4;\\H2;")
);_end of defun

;----------------------------------------------------------------------------------
;---------========������� ����� ����� ��������� � ������ ���������========---------
(defun planusk ()
  (initget 1)
  (setq pa (getdist "\n������� ����� ������� ��������� ����� B [mm] ")
	polu (getpoint "\n������� ����� ����� ��������� ")
	mua (/ mab pa)
	;=====����� �������� ���������� ���������=====
	lcbn (/ macbn mua)
	ledn (/ maedn mua)		
	;=====����� ����� ���������=====
	ba (polar polu (angle b a) pa)
	da (polar polu (angle d a) pa)
	cbn (polar ba (angle c b) lcbn)
	edn (polar da (angle e d) ledn)	
	ca (inters
	     polu (polar polu 0. 1e15)
	     cbn (polar cbn (+ (angle b c) (* pi 0.5)) 1e15)
	     nil
	   )
	ea (inters
	     polu (polar polu 0. 1e15)
	     edn (polar edn (+ (angle d e) (* pi 0.5)) 1e15)
	     nil
	   )
	fa (polar ca
		  (+ (arccos (/ bc (* 2.0 bf))) (angle ca ba))
		  (* (distance ba ca) (/ bf bc))
	   )
	ka (polar da
		  (+ (arccos (/ bc (* 2.0 bf))) (angle da ea))
		  (* (distance ea da) (/ ek ed))
	   )
	s2a (inters
	      ba (polar ca (angle ca fa) (* 0.5 (distance ca fa)))
	      fa (polar ba (angle ba ca) (* 0.5 (distance ba ca)))
	      nil
	    )
	s4a (inters
	      da (polar ea (angle ea ka) (* 0.5 (distance ea ka)))
	      ka (polar ea (angle ea da) (* 0.5 (distance ea da)))
	      nil
	    )
	mac (* mua (distance polu ca))
	mae (* mua (distance polu ea))
	maf (* mua (distance polu fa))
        mak (* mua (distance polu ka))
	mas4 (* mua (distance polu s4a))
	mas2 (* mua (distance polu s2a))
	;=====������ �������������� ���������=========
	macbt (* mua (distance cbn ca))
	maedt (* mua (distance edn ea))
	macb (* mua (distance ba ca))
	maed (* mua (distance da ea))
	;������� ���������
	eps2 (/ macbt lbc) 
	eps4 (/ maedt lde)
	masep (list eps2 eps4)
	masa (list mac maf mae mak mas2 mas4 macb maed)
  );_end fo setq
  (duga b c cbn ca 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^2;\\H2;")
  (duga d e edn ea 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^4;\\H2;")
);_end of defun

;=============================================================================================


(defun cherm (/ i dmu)
  (li (list c f b d e k d))
  (li (list f s2 b c s2))(li (list k s4 e))
  (mapcar '(lambda (t1) (add_circ 0.7 t1)) (append masivm (list s2 s4 k)))
  (add_text s2 "\\A1;S\\H0.7x;\\S^2;\\H1.4286x;")
  (add_text s4 "\\A1;S\\H0.7x;\\S^4;\\H1.4286x;")
  (add_text k "K")
  (setq i 65)
  (mapcar '(lambda (t1) (progn
			  (add_text t1 (chr i))
			  (setq i (1+ i))
			)
	   )
	  masivm
  );_end of mapcar
  (add_opor a (+ pi fi))
  (add_block c 0.) (add_block e 0.)
  (duga a b b (polar b (+ (angle a b) (* 0.5 pi)) 10.) 0.5 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^1;\\H2;")
  (muul lab ab)
)

(defun cherv ()
  (add_text pol "p,a")
  (li (list ev kv dv s4v ev))
  (li (list bv fv cv s2v bv)) 
  ;����������
  (risskor pol bv "b" "v^B")  
  (risskor pol cv "c" "v^C") 
  (risskor pol ev "e" "v^E")
  (risskor pol fv "f" "v^F")
  (risskor pol kv "k" "v^K")
  (risskor pol dv "d" "v^D")
  (risskor pol s2v "s^2" "v^S2")
  (risskor pol s4v "s^4" "v^S4")
  ;�������������
  (risskor bv cv nil "v^CB")
  (risskor dv ev nil "v^ED")
  (muuv mvb pb)
  (formv omeg lab)
)

(defun chera ()
  ;(new_ucs "���� ���������")
  (add_text polu "\\A1;p\\H0.7x;\\S^1;\\H1.4286x;,a")
  (li (list ba fa ca ba s2a ca))
  (li (list ea ka da ea s4a da))
  ;����� 1
  (risskor polu ba "b" "a^B")
  (risskor polu da "d" "a^D")
  ;����� 3
  (risskor polu ca "c" "a^C")
  (risskor polu fa "f" "a^F")
  (risskor polu s2a "s^2" "a^S2")
  ;����� 2
  (risskor polu ka "k" "a^K")
  (risskor polu ea "e" "a^E")
  (risskor polu s4a "s^4" "a^S4")
  ;������������� ���������
  (risskor ba cbn nil "an^CB")
  (risskor da edn nil "an^ED")  
  (risskor edn ea nil "a\U+03C4^ED")
  (risskor cbn ca nil "a\U+03C4^CB")
  (muua mab pa)
  (forma omeg lab)
)

(defun omep ()
  ;���������
  ;��� ���������� ���������
  (formsk2 muv (distance pol cv) "C" "pc" t)
  (formsk2 muv (distance pol ev) "E" "pe" t)  
  (formsk2 muv (distance pol fv) "F" "pf" t)
  (formsk2 muv (distance pol kv) "K" "pk" t) 
  (formsk2 muv (distance pol s2v) "S2" "ps\\H0.7x;\\S^2;\\H1.4286x;" t)
  (formsk2 muv (distance pol s4v) "S4" "ps\\H0.7x;\\S^4;\\H1.4286x;" t)
  (add_text (getpoint "\n��������������� speed point K: ")
	    (strcat "\\A1;dk=ke=de\U+00D7\\H0.7x;\\SDK/DE;\\H1.4286x;="
		    (rtos (distance dv ev) 2 0) "\U+00D7\\H0.7x;\\S" (rtos ek 2 0) "/" (rtos ed 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance dv ev) (/ ek ed)) 2 1) " ��"
	    )
  )
  ;��� ������������� ���������
  (formsk2 muv (distance cv bv) "CB" "cb" t)
  (formsk2 muv (distance ev dv) "ED" "ed" t)
  ;���������
  (formsk2 mua (distance polu ca) "C" "p\\H0.7x;\\S^1;\\H1.4286x;c" nil)
  (formsk2 mua (distance polu fa) "F" "p\\H0.7x;\\S^1;\\H1.4286x;f" nil)  
  (formsk2 mua (distance polu ea) "E" "p\\H0.7x;\\S^1;\\H1.4286x;e" nil)
  (formsk2 mua (distance polu ka) "K" "p\\H0.7x;\\S^1;\\H1.4286x;k" nil)
  (formsk2 mua (distance polu s2a) "S2" "p\\H0.7x;\\S^1;\\H1.4286x;s\\H0.7x;\\S^2;\\H1.4286x;" nil)
  (formsk2 mua (distance polu s4a) "S4" "p\\H0.7x;\\S^1;\\H1.4286x;s\\H0.7x;\\S^4;\\H1.4286x;" nil)
  (formsk2 mua (distance ca ba) "CB" "cb" nil)
  (formsk2 mua (distance ea da) "ED" "ed" nil)
  (add_text (getpoint "\n��������������� axel point K: ")
	    (strcat "\\A1;dk=ke=de\U+00D7\\H0.7x;\\SDK/DE;\\H1.4286x;="
		    (rtos (distance da ea) 2 0) "\U+00D7\\H0.7x;\\S" (rtos ek 2 0) "/" (rtos ed 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance da ea) (/ ek ed)) 2 1) " ��"
	    )
  )
  ;���������� ���������
  (formusk "aCB" mvcb lbc)
  (formusk "aED" mved lde)  
  ;�������������� ���������
  (formuskt2 "CB" (distance cbn ca) mua)
  (formuskt2 "ED" (distance edn ea) mua)  
  ;�����
  (ugsu "ome2CB" mvcb lbc)
  (ugsu "ome4ED" mved lde)
  ;��������
  (ugsu "eps2CB" macbt lbc)
  (ugsu "eps4CD" maedt lde)  
)
;=====================================================================================================

(defun c:sem21_2 (/ osm le ab omeg lab mul ad bc ed bf fc dk ek lbc lde mvb mvd mab mad fi a b d c e f k s2 s4 masivm
pb pol muv bv dv cv ev fv kv s2v s4v mvc mve mvf mvk mvs2 mvs4 mvcb mved ome2 ome4 masom macbn maedn masv masb
pa polu mua lcbn ledn ba da cbn edn ca ea fa ka s2a s4a mac mae maf mak mas4 mas2 macbt maedt macb maed eps2 eps4 masep masa
)
  (vl-load-com)
  (setq osm (getvar "OSMODE"))
  (setvar "OSMODE" 20919) 
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "TIMESI.TTF" )))
	le (append le (list (cons 2 "�����")))	
  )
  (entmakex le)
  (setvar "textstyle" "�����")
  (load "funkcii_tmm.lsp")
  (load "tmm_semi.lsp")
  (vvod)
  (tochkim)
  (cherm)
  (plansk)
  (cherv)
  (planusk)
  (chera)
  (omep)
  (tablica_va masb masv masa masom masep)
  (setvar "OSMODE" osm)
  (setvar "dimtxsty" "�����")
)

(c:sem21_2)
