;���� ��� ����������� �� ��������������� ������� � 9 ��� 2
(defun vvod (/ f)
  (initget 6)
  (setq f (getreal "\n������� ���� �� ")
        ab (getreal "\n������� �B [mm] ")
        omeg (getreal "\n������� ����� ")
        lab (getreal "\n������� AB [m] ")
        mul (/ lab ab)
	;����� �� �����
	bc (* 4.6 ab)
	fk (* 3.7 ab)
	ed (* 3.1 ab)
	x1 ed 
	y ed
	dc (* 1.55 ab)
	cf dc
	x (* 3.84 ab)
	;�������� �����
	lbc (* lab 4.6)
	lcf (* lab 1.55)
	lfk (* lab 3.7)
	led (* lab 3.1)
	;�������� � ���������
	mvb (* omeg lab)
	mab (* omeg omeg lab)
	fi (* f (/ pi 180.))
  );end of setq
  (ishodn omeg lab f)
  (tabl_razmer (list "\\A1;l\\H0.7x;\\S^AB;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^BC;\\H1.4286x;"
		      "\\A1;l\\H0.7x;\\S^FK;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^ED;\\H1.4286x;"
		      "\\A1;x\\H0.7x;\\S^1;\\H1.4286x;" "y" "\\A1;l\\H0.7x;\\S^DC;\\H1.4286x;"
		      "\\A1;l\\H0.7x;\\S^CF;\\H1.4286x;" "x"
		)
    (list ab bc fk ed x1 y dc cf x) mul)
);end of vvod


;;;;;;;;;;;;;;;;;;;;;�������, ������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;----------------------------------------------------------------------------------
;;;;;;---=================������� ���������� ����� ���������================-------
(defun tochkim ( /)
  (setq
    a (getpoint "\n��������: ")
    k (list (+ (car a) x1 x) (- (cadr a) y))
    b (polar a fi ab)    
    c (car (vl-sort
	     (dwgru a (polar a 0. 1e15) b bc)
	     (function (lambda (e1 e2) (> (car e1) (car e2)))) 
	   )
      )
    f (car (vl-sort
	     (circint c cf k fk)
	     (function (lambda (e1 e2) (> (cadr e1) (cadr e2)))) 
	   )
      ) 
    d (polar c (angle f c) dc)
    e (car (vl-sort
	     (dwgru (polar a 0. x) (polar (polar a 0. x) (* 0.5 pi) 1e15) d ed)
	     (function (lambda (e1 e2) (< (car e1) (car e2)))) 
	   )
      )     
    masivm (list a b c d e f)       
  )  
)

;----------------------------------------------------------------------------------
;--==������� ����� ����� ��������� � ���������� ��������� � ������ ���������===----
(defun plansk ()
  (initget 1)
  (setq pb (getint "\n������� ����� ������� �������� Va [mm] ")
 	muv (/ mvb pb)
	pol (getpoint "\n������� ����� ����� ��������� ")
  ;======����� ����� ���������========  
  	bv (polar pol (+ (angle a b) (/ pi 2)) pb)
	cv (inters
	     pol (polar pol 0. 1e5)
	     bv (polar bv (- (angle c b) (* 0.5 pi)) 5)
	     nil
	   ); inters
	fv (inters cv (polar cv (+ (angle c f) (* 0.5 pi)) 1e15)
		   pol (polar pol (+ (angle k f) (* 0.5 pi)) 1e15)
	   )
	dv (polar cv (angle fv cv) (distance fv cv))
	ev (inters
		pol (polar pol (* 0.5 pi) 1e15)
		dv (polar dv (+ (angle d e) (* pi 0.5)) 5)
		nil
      	   ); inters
  ;========������ ��������=======
        mvc (* muv (distance pol cv))
	mvd (* muv (distance pol dv))
	mve (* muv (distance pol ev))
	mvf (* muv (distance pol fv))
	
	mvcb (* muv (distance bv cv))
	mvfc (* muv (distance fv cv))
	mved (* muv (distance ev dv))
	masv (list mvc mvf mvd mve mvcb mvfc mved)
	masb (list "C" "F" "D" "E" "CB" "FC" "ED")
  ;========������� ��������====================
        ome2 (/ mvcb lbc)
	ome3 (/ mvfc lcf)
	ome4 (/ mvf lfk)
	ome5 (/ mved led)
	masom (list ome2 ome3 ome4 ome5)
  ;=====���������� ���������=====
	macbn (/ (expt mvcb 2) lbc)
	mafcn (/ (expt mvfc 2) lcf)
	mafkn (/ (expt mvf 2) lfk)
	maedn (/ (expt mved 2) led)
  ); setq
  (duga b c bv cv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^2;\\H2;")
  (duga c f cv fv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^3;\\H2;")
  (duga k f pol fv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^4;\\H2;")
  (duga d e dv ev 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^5;\\H2;")
); defun plansk
;----------------------------------------------------------------------------------
;---------========������� ����� ����� ��������� � ������ ���������========---------
(defun planusk ()
  (initget 1)
  (setq pa (getint "\n������� ����� ������� ��������� ����� B [��] ")
	polu (getpoint "\n������� ����� ����� ��������� ")
	mua (/ mab pa)
	;=====����� �������� ���������� ���������=====
	lcbn (/ macbn mua)
	lfcn (/ mafcn mua)
	lfkn (/ mafkn mua)
	ledn (/ maedn mua)
	;=====����� ����� ���������=====
	ba (polar polu (angle b a) pa)
	cbn (polar ba (angle c b) lcbn)
	ca (inters polu (polar polu 0. 1e5)
		   cbn (polar cbn (- (angle c b) (* 0.5 pi)) 5)
		   nil
	   )
	fcn (polar ca (angle f c) lfcn)
	fkn (polar polu (angle f k) lfkn)
	fa (inters fkn (polar fkn (+ (angle k f) (* 0.5 pi)) 1e15)
		   fcn (polar fcn (+ (angle c f) (* 0.5 pi)) 1e15)
		   nil
	   )
	da (polar ca (angle fa ca) (distance fa ca))
	edn (polar da (angle e d) ledn)
	ea (inters
	     polu (polar polu (* 0.5 pi) 5)
	     edn (polar edn (- (angle e d) (* 0.5 pi)) 5)
	     nil
	   )	
	mac (* mua (distance polu ca))
	mad (* mua (distance polu da))
	mae (* mua (distance polu ea))
	maf (* mua (distance polu fa))
	macb (* mua (distance ba ca))
	mafc (* mua (distance fa ca))
	maed (* mua (distance ea da))

	masa (list mac maf mad mae macb mafc maed)
	;=====������ �������������� ���������=========
	macbt (* mua (distance ca cbn))
	mafct (* mua (distance fa fcn))
	mafkt (* mua (distance fa fkn))
	maedt (* mua (distance ea edn))
	;������� ���������
	eps2 (/ macbt lbc)
	eps3 (/ mafct lcf)
	eps4 (/ mafkt lfk)
	eps5 (/ maedt led)
	masep (list eps2 eps3 eps4 eps5)
  )
  (duga b c cbn ca 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^2;\\H2;")
  (duga c f fcn fa 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^3;\\H2;")
  (duga k f fkn fa 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^4;\\H2;")
  (duga d e edn ea 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^5;\\H2;")
)
;=============================================================================================
(defun cherm (/ i le dmu)
  (li (list a b c))
  (li (list k f d e))
  (duga a b b (polar b (+ (angle a b) (* 0.5 pi)) 10.) 0.5 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^1;\\H2;")
  (mapcar '(lambda (t1) (add_circ 0.7 t1)) (list a b c d e f k))
  (add_block c 0.) (add_block e (* 0.5 pi)) (add_opor a (+ (* 0.5 pi) fi)) (add_opor k (+ (* 0.5 pi) (angle k f)))
  (add_text k "K")
  (setq i 65)
  (mapcar '(lambda (t1) (progn
			  (add_text t1 (chr i))
			  (setq i (1+ i))
			)
	   ) masivm)
  (muul lab ab)
); defun cherm

(defun cherv ()
  (li (list dv cv))
  (add_text pol "p,a,k")
  (risskor pol bv "b" "v^B")
  (risskor pol cv "c" "v^C")
  (risskor pol ev "e" "v^E")
  (risskor pol fv "f" "v^F")
  (risskor pol dv "d" "v^D")

  (risskor bv cv nil "v^CB")
  (risskor cv fv nil "v^FC")
  (risskor dv ev nil "v^ED")
  (li (list dv cv))
  (muuv mvb pb)
  (formv omeg lab)
  ;���������
  
); defun cherv

(defun chera ()
  (li (list da fa))
  (add_text polu "\\A1;p\\H0.7x;\\S^1;\\H1.4286x;,a,k")
  (risskor polu ba "b" "a^B") 
  (risskor polu ca "c" "a^C")
  (risskor polu da "d" "a^D")
  (risskor polu ea "e" "a^E")
  (risskor polu fa "f" "a^F")
  ;����������
  (risskor ba cbn nil "an^CB")
  (risskor ca fcn nil "an^FC")
  (risskor polu fkn nil "an^FK")
  (risskor da edn nil "an^ED")
  ;��������������
  (risskor cbn ca nil "a\U+03C4^CB")
  (risskor fcn fa nil "a\U+03C4^FC")
  (risskor fkn fa nil "a\U+03C4^FK")
  (risskor edn ea nil "a\U+03C4^ED")
  ;�������������
  (risskor ba ca nil "a^CB")
  (risskor ca fa nil "a^FC")
  (risskor da ea nil "a^ED")
  (muua mab pa)
  (forma omeg lab)  
); defun chera

(defun omep (/ i)
  
  (formsk2 muv (distance pol cv) "C" "pc" t)
  (formsk2 muv (distance pol dv) "D" "pd" t)
  (formsk2 muv (distance pol ev) "E" "pe" t)
  (formsk2 muv (distance pol fv) "F" "pf" t)

  (formsk2 muv (distance cv bv) "CB" "bc" t)
  (formsk2 muv (distance cv fv) "FC" "fc" t)
  (formsk2 muv (distance ev dv) "ED" "ed" t)
  (add_text (getpoint "\n��������������� speed point D: ")
	    (strcat "\\A1;cd=cf\U+00D7\\H0.7x;\\SCD/CF;\\H1.4286x;="
		    (rtos (distance fv cv) 2 0) "\U+00D7\\H0.7x;\\S" (rtos dc 2 0) "/" (rtos cf 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance fv cv) (/ dc cf)) 2 1) " ��"
	    )
  )
  ;��������������
  (formuskt2 "CB" (distance ca cbn) mua)
  (formuskt2 "FC" (distance fa fcn) mua)
  (formuskt2 "FK" (distance fa fkn) mua)
  (formuskt2 "ED" (distance ea edn) mua)
  
  (formsk2 mua (distance polu ca) "C" "p\\H0.7x;\\S^1;\\H1.4286x;c" nil)
  (formsk2 mua (distance polu da) "D" "p\\H0.7x;\\S^1;\\H1.4286x;d" nil)
  (formsk2 mua (distance polu ea) "E" "p\\H0.7x;\\S^1;\\H1.4286x;e" nil)
  (formsk2 mua (distance polu fa) "F" "p\\H0.7x;\\S^1;\\H1.4286x;f" nil)
  (formsk2 mua (distance ba ca) "CB" "cb" nil)
  (formsk2 mua (distance ea da) "ED" "ed" nil)
  ;����������
  (formusk "aCB" mvcb lbc)
  (formusk "aFC" mvfc lcf)
  (formusk "aFK" mvf lfk)
  (formusk "aED" mved led)
  (add_text (getpoint "\n��������������� acel point D: ")
	    (strcat "\\A1;cd=cf\U+00D7\\H0.7x;\\SCD/CF;\\H1.4286x;="
		    (rtos (distance fa ca) 2 0) "\U+00D7\\H0.7x;\\S" (rtos dc 2 0) "/" (rtos cf 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance fa ca) (/ dc cf)) 2 1) " ��"
	    )
  )
  (ugsu "ome2CB" mvcb lbc)
  (ugsu "ome3FC" mvfc lcf)
  (ugsu "ome4FK" mvf lfk)
  (ugsu "ome5ED" mved led)

  (ugsu "eps2CB" macbt lbc)
  (ugsu "eps3FC" mafct lcf)
  (ugsu "eps4FK" mafkt lfk)
  (ugsu "eps5ED" maedt led)  
  
  (tablica_va masb masv masa masom masep) 
); omep
;=====================================================================================================

(defun c:sem09 (/ osm le
ab omeg lab mul bc fk ed x1 y dc cf x lbc lcf lfk led mvb mab fi
a k b c f d e masivm pb muv pol bv cv fv dv ev mvc mvd mve mvf mvcb mvfc mved 
masv masb ome2 ome3 ome4 ome5 masom macbn mafcn mafkn maedn pa polu mua lcbn lfcn lfkn 
ledn ba cbn ca fcn fkn fa da edn ea mac mad mae maf macb mafc maed masa macbt mafct mafkt 
maedt eps2 eps3 eps4 eps5 masep)
  (VL-LOAD-COM)  
  (setq osm (getvar "OSMODE"))
  (setvar "OSMODE" 20919)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "TIMESI.TTF" )))
	le (append le (list (cons 2 "�����")))	
  )
  (entmakex le)
  (setvar "textstyle" "�����")
  (load "funkcii_tmm.lsp")
  (load "tmm_semi.lsp")
  (vvod)
  (tochkim)
  (cherm)
  (plansk)
  (cherv)
  (planusk)
  (chera)
  (omep)
  (setvar "OSMODE" osm)
  (setvar "dimtxsty" "�����")
  (princ)
)
(c:sem09)


