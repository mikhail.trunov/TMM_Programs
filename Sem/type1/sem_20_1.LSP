;���� ��� ����������� �� ��������������� ������� � 23 ��� 2
(defun vvod (/ f)
  (initget 6)
  (setq f (getreal "\n������� ���� �� ")
        ab (getreal "\n������� �B [mm] ")
        omeg (getreal "\n������� ����� ")
        lab (getreal "\n������� AB [m] ")
        mul (/ lab ab)
	;����� �� �����
	bd (* 4.54 ab)
	de (* 1.36 ab)
	eh (* 1.77 ab)
	ef (* 1.41 ab)
	fg (* 2.14 ab)
	x1 (* 2.82 ab)
	x2 (* 4.9 ab)
	x3 (* 6.1 ab)
	y1 (* 0.5 ab)
	y2 (* 2.23 ab)
	y3 (* 0.64 ab)
	;�������� �����
	lbd (* mul bd)
	lde (* mul de)
	leh (* mul eh)
	lef (* mul ef)
	lfg (* mul fg)
	lx1 (* mul x1)
	lx2 (* mul x2)
	lx3 (* mul x3)
	ly1 (* mul y1)
	ly2 (* mul y2)
	ly3 (* mul y3)
	;�������� � ���������
	mvb (* omeg lab)
	mab (* omeg omeg lab)
	fi (- (* 1.5 pi) (* f (/ pi 180.)))
  );end of setq
  (ishodn omeg lab f)
  (tabl_razmer (list "\\A1;l\\H0.7x;\\S^AB;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^BD;\\H1.4286x;"
		     "\\A1;l\\H0.7x;\\S^DE;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^EH;\\H1.4286x;"
		     "\\A1;l\\H0.7x;\\S^EF;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^FG;\\H1.4286x;"
		     "\\A1;x\\H0.7x;\\S^1;\\H1.4286x;" "\\A1;x\\H0.7x;\\S^2;\\H1.4286x;"
		     "\\A1;x\\H0.7x;\\S^3;\\H1.4286x;" "\\A1;y\\H0.7x;\\S^1;\\H1.4286x;"
		     "\\A1;y\\H0.7x;\\S^2;\\H1.4286x;" "\\A1;y\\H0.7x;\\S^3;\\H1.4286x;"
	       )
    	       (list ab bd de eh ef fg x1 x2 x3 y1 y2 y3)
    mul
  )
);end of vvod
;;;;;;;;;;;;;;;;;;;;;�������, ������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;----------------------------------------------------------------------------------
;;;;;;---=================������� ���������� ����� ���������================-------
(defun tochkim ()
  (setq
    a (getpoint "\n��� ������� ��������: ")
    c (list (- (car a) x1) (- (cadr a) y1))
    b (polar a fi ab)
    g (list (- (car a) x2) (- (cadr a) y2))
    h (list (- (car a) x3) (- (cadr a) y3))
    d (polar b (angle b c) bd)
    e (car (vl-sort
	     (circint d de h eh)
	     (function (lambda (e1 e2) (> (cadr e1) (cadr e2)))) 
	   )
      )
    f (car (vl-sort
	     (circint e ef g fg)
	     (function (lambda (e1 e2) (> (car e1) (car e2)))) 
	   )
      )
    bc (distance b c)
    lbc (* bc mul)
    cd (distance c d)
    masivm (list a b c d e f g h)       
  )
)
;----------------------------------------------------------------------------------
;--==������� ����� ����� ��������� � ���������� ��������� � ������ ���������===----
(defun plansk ()
  (initget 1)
  (setq pb (getdist "\n������� ����� ������� �������� Va [mm] ")
  	pol (getpoint "\n������� ����� ����� ��������� ")  
	muv (/ mvb pb)
  ;======����� ����� ���������========  
	bv (polar pol (- (angle a b) (/ pi 2.)) pb)
	c2v (inters pol (polar pol (angle b c) 100.)
		    bv (polar bv (+ (angle b c) (* 0.5 pi)) 100.)
		    nil
	    )
	dv (polar c2v (angle bv c2v) (* cd (/ (distance bv c2v) bc)))
	ev (inters pol (polar pol (+ (angle e h) (* 0.5 pi)) 100.)
		   dv (polar dv (+ (angle e d) (* 0.5 pi)) 100.)
		   nil
	   )
	fv (inters pol (polar pol (+ (angle f g) (* 0.5 pi)) 100.)
		   ev (polar ev (+ (/ pi 2.) (angle f e)) 5)
		   nil
	    )
  ;========������ ��������=======
        mvc2 (* muv (distance pol c2v))
	mvd (* muv (distance pol dv))
	mve (* muv (distance pol ev))
	mvf (* muv (distance pol fv))
	mvcb (* muv (distance c2v bv))
	mved (* muv (distance dv ev))
	mvfe (* muv (distance fv ev))
	masb (list "\\A1;C\\H0.7x;\\S^2;" "D" "E" "F"
		   "\\A1;C\\H0.7x;\\S^2;\\H1.42857x;B" "ED" "FE")
	masv (list mvc2 mvd mve mvf mvcb mved mvfe)
  ;========������� ��������====================
        ome2 (/ mvcb lbc)
	ome4 (/ mved lde)
	ome5 (/ mve leh)
	ome6 (/ mvfe lef)
	ome7 (/ mvf lfg)
	masom (list ome2 ome4 ome5 ome6 ome7)
  ;=====���������� ���������=====
	macbn (/ (expt mvcb 2) lbc)
	maedn (/ (expt mved 2) lde)
	maehn (/ (expt mve 2) leh)
	mafen (/ (expt mvfe 2) lef)
	mafgn (/ (expt mvf 2) lfg)
	mac2c3k (* 2. ome2 mvc2)
  )
  (duga b c bv c2v 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^2;\\H2;")
  (duga d e dv ev 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^4;\\H2;")
  (duga h e pol ev 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^5;\\H2;")
  (duga e f ev fv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^6;\\H2;")
  (duga g f pol fv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^7;\\H2;")
)
;----------------------------------------------------------------------------------
;---------========������� ����� ����� ��������� � ������ ���������========---------
(defun planusk ()
  (initget 1)
  (setq pa (getdist "\n������� ����� ������� ��������� ����� B [mm] ")	
	polu (getpoint "\n������� ����� ����� ��������� ")
	mua (atof (rtos (/ mab pa) 2 3))
	;=====����� �������� ���������� ���������=====
	lcbn (/ macbn mua)
	ledn (/ maedn mua)
	lehn (/ maehn mua)
	lfen (/ mafen mua)
	lfgn (/ mafgn mua)
	lc2c3k (/ mac2c3k mua)
	;=====����� ����� ���������=====
	ba (polar polu (angle b a) pa)
	c2bn (polar ba (angle c b) lcbn)
	c2c3k (polar polu (if (clockwise-p b c (polar c (angle bv c2v) 10))
			    (+ (angle pol c2v) (* 0.5 pi))
			    (- (angle pol c2v) (* 0.5 pi))
			  )
		     lc2c3k
	      )
	c2a (inters c2bn (polar c2bn (+ (angle c b) (* 0.5 pi)) 100.)
		    c2c3k (polar c2c3k (angle c b) 100.)
		    nil
	    )
	da (polar c2a (angle ba c2a) (* cd (/ (distance ba c2a) bc)))
	ehn (polar polu (angle e h) lehn)
	edn (polar da (angle e d) ledn)
	ea (inters ehn (polar ehn (+ (* 0.5 pi) (angle e h)) 100.)
		   edn (polar edn (+ (* 0.5 pi) (angle e d)) 100.)
		   nil
	   )
	fen (polar ea (angle f e) lfen)
	fgn (polar polu (angle f g) lfgn)
	fa (inters fen (polar fen (+ (* 0.5 pi) (angle e f)) 100.)
		   fgn (polar fgn (+ (* 0.5 pi) (angle f g)) 100.)
		   nil
	   )
	;=====������ ���������� ���������=========
	mac2 (* mua (distance polu c2a))
	mad (* mua (distance polu da))
	mae (* mua (distance polu ea))
	maf (* mua (distance polu fa))
	macb (* mua (distance c2a ba))
	maed (* mua (distance ea da))
	mafe (* mua (distance fa ea))
	masa (list mac2 mad mae maf macb maed mafe)
	;=====������ �������������� ���������=========
	macbt (* mua (distance c2bn ba))
	maedt (* mua (distance edn ea))
	maeht (* mua (distance ehn ea))
	mafet (* mua (distance fen fa))
	mafgt (* mua (distance fgn fa))
	;������� ���������
	eps2 (/ macbt lbc)
	eps4 (/ maedt lde)
	eps5 (/ maeht leh)
	eps6 (/ mafet lef)
	eps7 (/ mafgt lfg)
	masep (list eps2 eps4 eps5 eps6 eps7)
  )
  (duga b c c2bn c2a 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^2;\\H2;")
  (duga d e edn ea 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^4;\\H2;")
  (duga h e ehn ea 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^5;\\H2;")
  (duga e f fen fa 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^6;\\H2;")
  (duga g f fgn fa 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^7;\\H2;")
)
;=============================================================================================


(defun cherm (/ i)
  (li (list a b d e f g)) (li (list h e))
  (mapcar '(lambda (t1) (add_circ 0.7 t1)) masivm)  
  (setq i 65)
  (mapcar '(lambda (t1) (progn
			  (add_text t1 (chr i))
			  (setq i (1+ i))
			)
	   );end of lambda
           (list a b c d e f g h)
  );end of mapcar
  (add_block c (angle c b))
  (add_opor a (+ (* 0.5 pi) fi))
  (add_opor h pi) (add_opor g pi) (add_opor c pi)
  (duga a b b (polar b (- (angle a b) (* 0.5 pi)) 10.) 0.5 "\\A1;\U+03C9\\H0.7x;\\S^1;\\H1.4286x;")
  (muul lab ab)
); defun cherm

(defun cherv ()
  (add_text pol "\\A1;p,a,h,g,c\\H0.7x;\\S^3;\\H1.4286x;")
  (risskor pol bv "b" "v^B")
  (risskor pol c2v nil "v^C2")
  (risskor pol ev "e" "v^E")
  (risskor pol fv "f" "v^F")
  (risskor pol dv "d" "v^D")
  (risskor bv c2v nil "v^CB")
  (risskor dv ev nil "v^ED")
  (risskor ev fv nil "v^FE")
  (li (list c2v dv))
  (add_text c2v "\\A1;c\\H0.7x;\\S^2;\\H1.4286x;")
  (muuv mvb pb)
  (add_text (getpoint "\n������� ��� ������ ������� ��� ����������� ��������: ")
	   (strcat "\\A1;\\H2;\U+03C5"
	 	   "\\H0.7x;\\S^B;"
		   "\\H2;=\U+03C9"
		   "\\H0.7x;\\S^1;"
		   "\\H2;\U+00D7l"
		   "\\H0.7x;\\S^AB;"
		   "\\H2;="			    
		   (rtos omeg)"\U+00D7"(rtos lab)
		   "="(rtos mvb)" �/�"					    
	   );end of strcat
  ); add_tex
); defun cherv

(defun chera (/ dmu le)
  (add_text polu "\\A1;p\\H0.7x;\\S^1;\\H1.4286x;,a,h,g,c\\H0.7x;\\S^3;\\H1.4286x;")
  (risskor polu ba "b" "a^B") 
  (risskor polu c2a nil "a^C")
  (risskor polu ea "e" "a^E")
  (risskor polu fa "f" "a^F")
  (risskor polu da "d" "a^D")
  (risskor ea fa nil "a^FE")
  (risskor da ea nil "a^ED")
  (risskor ba c2a nil "a^CB")
  (li (list c2a da))
; ���������� � ���������
  (risskor ba c2bn nil "an^CB")
  (risskor polu c2c3k nil "ak^C2C3")
  (risskor da edn nil "an^ED")
  (risskor ea fen nil "an^FE")
  (risskor polu fgn nil "an^FG")
  (risskor polu ehn nil "an^EH")
; �������������� � �������������
  (risskor c2bn c2a nil "a\U+03C4^CB")
  (risskor c2c3k c2a nil "ar^C2C3")
  (risskor edn ea nil "a\U+03C4^ED")
  (risskor fen fa nil "a\U+03C4^FE")
  (risskor fgn fa nil "a\U+03C4^FG")
  (risskor ehn ea nil "a\U+03C4^EH")
  (add_text c2a "\\A1;c\\H0.7x;\\S^2;\\H1.4286x;")
  (kor_cher polu c2c3k pol c2v "v^C2C3" "ak^C2C3" "\\A1;\\H2;\U+03C9\\H0.7x;\\S^2;\\H2;")
  (muua mab pa)
  (add_text (getpoint "\n������� ��� ������ ������� ��� ����������� ���������: ")
	   (strcat "\\A1;\\H2;a"
		   "\\H0.7x;\\S^B;"
		   "\\H2;=\U+03C9"
		   "\\H0.7x;\\S2^1;"
		   "\\H2;\U+00D7l"
		   "\\H0.7x;\\S^AB;"
		   "\\H2;="			    
		   (rtos omeg)
		   "\\H0.7x;\\S2^;"
		   "\\H2;\U+00D7"(rtos lab)
		   "="(rtos mab)" �/�"
		   "\\H0.7x;\\S2^;"
	   );end of strcat
  ); add_tex
); defun chera

(defun omep ()  
  ;���������
  (formsk2 muv (distance pol c2v) "C2" "pc\\H0.7x;\\S^2;\\H1.4286x;" t)
  (formsk2 muv (distance pol dv) "D" "pd" t)
  (formsk2 muv (distance pol ev) "E" "pe" t)
  (formsk2 muv (distance pol fv) "F" "pf" t)
  (formsk2 muv (distance c2v bv) "C2B" "c\\H0.7x;\\S^2;\\H1.4286x;b" t)
  (formsk2 muv (distance ev dv) "ED" "ed" t)
  (formsk2 muv (distance fv ev) "FE" "fe" t)
  ;����������
  (formusk "aCB" mvcb lbc)
  (formusk "aED" mved lde)
  (formusk "aEH" mve leh)
  (formusk "aFE" mvfe lef)
  (formusk "aFG" mvf lfg)
  (koriolis ome2 mvc2 "C2C3" "2")
  ;��������������
  (formuskt2 "CB" (distance c2bn c2a) mua)
  (formuskt2 "ED" (distance edn ea) mua)
  (formuskt2 "EH" (distance ehn ea) mua)
  (formuskt2 "FE" (distance fen fa) mua)
  (formuskt2 "FG" (distance fgn fa) mua)
  (add_text (getpoint "\narC2C3")
	    (strcat "\\A1;a\\H0.7x;\\Sr^C2C3;\\H1.4286x;="
		    (rtos (distance c2c3k c2a) 2 0)"\U+00D7"(rtos mua)
		    "="(rtos (* (distance c2c3k c2a) mua) 2 3) "�/�\\H0.7x;\\S2^;\\H1.4286x;"
	    )
  )
  (formsk2 mua (distance polu c2a) "C2" "p\\H0.7x;\\S^1;\\H1.4286x;c\\H0.7x;\\S^2;\\H1.4286x;" nil)
  (formsk2 mua (distance polu da) "D" "p\\H0.7x;\\S^1;\\H1.4286x;d" nil)
  (formsk2 mua (distance polu ea) "E" "p\\H0.7x;\\S^1;\\H1.4286x;e" nil)
  (formsk2 mua (distance polu fa) "F" "p\\H0.7x;\\S^1;\\H1.4286x;f" nil)
  (formsk2 mua (distance c2a ba) "CB" "cb" nil)
  (formsk2 mua (distance ea da) "ED" "ed" nil)
  (formsk2 mua (distance fa ea) "FE" "fe" nil)
  (add_text (getpoint "\n��������������� speed: ")
	    (strcat "\\A1;cd\\H0.7x;\\S^2;\\H1.4286x;=bc\\H0.7x;\\S^2;\\H1.4286x;\U+00D7\\H0.7x;\\SCD/BC;\\H1.4286x;="
		    (rtos (distance c2v bv) 2 0) "\U+00D7\\H0.7x;\\S" (rtos (distance c d) 2 0) "/" (rtos (distance c b) 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance c2v bv) (/ (distance c d) (distance c b))) 2 1) " ��"
	    )
  )
  (add_text (getpoint "\n��������������� aceleration: ")
	    (strcat "\\A1;cd\\H0.7x;\\S^2;\\H1.4286x;=bc\\H0.7x;\\S^2;\\H1.4286x;\U+00D7\\H0.7x;\\SCD/BC;\\H1.4286x;="
		    (rtos (distance c2a ba) 2 0) "\U+00D7\\H0.7x;\\S" (rtos (distance c d) 2 0) "/" (rtos (distance c b) 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance c2a ba) (/ (distance c d) (distance c b))) 2 1) " ��"
	    )
  )
  (ugsu "ome2CB" mvcb lbc)
  (ugsu "ome4ED" mved lde)
  (ugsu "ome5EH" mve leh)
  (ugsu "ome6FE" mvfe lef)
  (ugsu "ome7FG" mvf lfg)
  (ugsu "eps2CB" macbt lbc)
  (ugsu "eps4ED" maedt lde)
  (ugsu "eps5EH" maeht leh)
  (ugsu "eps6FE" mafet lef)
  (ugsu "eps7FG" mafgt lfg)
)
;=====================================================================================================

(defun c:sem_20_1 (/ osm le ab omeg lab mul bd de eh ef fg x1 x2 x3 y1 y2 y3 lbd lde leh lef lfg lx1 lx2 lx3 ly1 ly2 ly3 
mvb mab fi a c b g h d e f bc lbc cd masivm pb pol muv bv c2v dv ev fv mvc2 mvd mve mvf mvcb mved mvfe ome2 ome4 ome5 ome6 ome7 
macbn maedn maehn mafen mafgn mac2c3k pa polu mua lcbn ledn lehn lfen lfgn lc2c3k ba c2bn c2c3k c2a da ehn edn ea fen fgn fa
mac2 mad mae maf macb maed mafe masa macbt maedt maeht mafet mafgt eps2 eps4 eps5 eps6 eps7 masep)
  (setq osm (getvar "OSMODE"))
  (setvar "OSMODE" 20919)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "TIMESI.TTF" )))
	le (append le (list (cons 2 "�����")))	
  )
  (entmakex le)
  (setvar "textstyle" "�����")
  (load "funkcii_tmm.lsp")
  (load "tmm_semi.lsp")
  (vvod)
  (tochkim)
  (cherm)
  (plansk)
  (cherv)
  (planusk)
  (chera)  
  (omep)
  (tablica_va masb masv masa masom masep)
  (setvar "OSMODE" osm)
  (setvar "dimtxsty" "�����")
  (princ)
)
(c:sem_20_1)
