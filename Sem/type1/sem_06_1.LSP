;���� ��� ����������� �� ��������������� ������� � 6 ��� 1
(defun vvod (/ f)
  ;(initget 6)
  (setq f (getreal "\n������� ���� �� ")
        ab (getreal "\n������� �B [mm] ")
        omeg (getreal "\n������� ����� ")
        lab (getreal "\n������� AB [m] ")
        mul (/ lab ab)
	;����� �� �����
	bc (* ab 0.78)
	ac (* ab 0.32)
	cd (* ab 1.1)
	ae (* ab 0.45)
	zvenia (list "lAB" "lBC" "lAC" "lCD" "lAE")
	razm_zv (list ab bc ac cd ae)
	;�������� �����
	lcd (atof (rtos (* lab 1.1) 2 3))
	;�������� � ���������
	mvb (* omeg lab)
	mab (atof (rtos (* omeg omeg lab) 2 4))
	fi (* f (/ pi 180.))
  );end of setq
  (ishodn omeg lab f)
  (tabl_razmer (list "\\A1;l\\H0.7x;\\S^AB;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^BC;\\H1.4286x;"
		     "\\A1;l\\H0.7x;\\S^AC;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^CD;\\H1.4286x;"
		     "\\A1;l\\H0.7x;\\S^AE;\\H1.4286x;"
	       )
    	       (list ab bc ac cd ae)
    mul
  )
);end of vvod


;;;;;;;;;;;;;;;;;;;;;�������, ������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;----------------------------------------------------------------------------------
;;;;;;---=================������� ���������� ����� ���������================-------
(defun tochkim (/)
  (setq
    a (getpoint "\n��������: ")
    alfa (arccos (/ (+ (* ac ac) (* ab ab) (* -1. bc bc))
		    (* 2. ac ab)
		 );
	 ); arccos
    b (polar a fi ab)
    c (polar a (+ fi alfa) ac)
    e (polar a pi ae)
    d (cadr (vl-sort
	     (dwgru e (polar e (+ (angle e b) (/ pi 3.)) 100.) c cd)
	     (function (lambda (e1 e2) (< (cadr e1) (cadr e2)))) 
	   )
      )
    eb (distance b e)
    leb (* eb mul)
    de (distance d e)
    lde (* de mul)
    masivm (list a b c d e)
  )  
)
;----------------------------------------------------------------------------------
;--==������� ����� ����� ���������
(defun plansk (/)
  (initget 1)
  (setq pb (getint "\n������� ����� ������� �������� Va [mm] "))
  (setq	muv (atof (rtos (/ mvb pb) 2 4))
	pol (getpoint "\n������� ����� ����� ��������� ")
  )
  ;======����� ����� ���������========  
  (setq b12v (polar pol (+ (angle a b) (/ pi 2)) pb)
	b3v (inters
	      b12v (polar b12v (angle e b) 10.)
	      pol (polar pol (+ (angle e b) (* 0.5 pi)) 10.)
	      nil
	    )
	cv (polar pol (+ (angle pol b12v) alfa) (* ac (/ pb ab)))
	d3v (polar pol (+ (angle pol b3v) (/ pi 3.)) (* de (/ (distance pol b3v) eb)))
	d4v (inters
	      cv (polar cv (+ (angle c d) (* 0.5 pi)) 10.)
	      d3v (polar d3v (angle e d) 10.)
	      nil
	    )
  );end of setq
);plansk

(defun modulv ();���������� ��������� � ������ ���������
  (setq
  ;========������ ��������=======
    ;����������
        mvb3 (atof (rtos (* muv (atof (rtos (distance pol b3v) 2 0))) 2 3))	
	mvc (atof (rtos (* muv (atof (rtos (distance pol cv) 2 0))) 2 3))
	mvd3 (atof (rtos (* muv (atof (rtos (distance pol d3v) 2 0))) 2 3))
	mvd4 (atof (rtos (* muv (atof (rtos (distance pol d4v) 2 0))) 2 3))
   ;�������������
	mvb3b2 (atof (rtos (* muv (atof (rtos (distance b3v b12v) 2 0))) 2 3))
	mvd4d3 (atof (rtos (* muv (atof (rtos (distance d3v d4v) 2 0))) 2 3))
	mvd4c (atof (rtos (* muv (atof (rtos (distance cv d4v) 2 0))) 2 3))
  ;========������� ��������====================
        ome3 (atof (rtos (/ mvb3 leb) 2 3))
	ome5 (atof (rtos (/ mvd4c lde) 2 3))
	masom (list ome3 ome5)
  ;=====���������� ���������=====
	mab3n (/ (expt mvb3 2) leb)
	madcn (/ (expt mvd4c 2) lcd)
	mab3b2k (* 2. ome3 mvb3b2)
	mad4d3k (* 2. ome3 mvd4d3)
	masv (list mvb3 mvc mvd3 mvd4 mvb3b2 mvd4d3 mvd4c)
	masb (list "\\A1;B\\H0.7x;\\S^3;" "C" "\\A1;D\\H0.7x;\\S^3;" "\\A1;D\\H0.7x;\\S^4;"
		   "\\A1;B\\H0.7x;\\S^3;\\H1.42857x;B\\H0.7x;\\S^2;" "\\A1;D\\H0.7x;\\S^4;\\H1.42857x;D\\H0.7x;\\S^3;"
		   "DC")
  )
);modulv

;----------------------------------------------------------------------------------
;---------========������� ����� ����� ��������� � ������ ���������========---------
(defun planusk (/)
  (initget 1)
  (setq pa (getint "\n������� ����� ������� ��������� ����� B [mm] ")	
	polu (getpoint "\n������� ����� ����� ��������� ")
	mua (atof (rtos (/ mab pa) 2 3))
	;=====����� �������� ���������� ���������=====
	lb3n (/ mab3n mua)
	ld4cn (/ madcn mua)
	lb3b2k (/ mab3b2k mua)
	ld4d3k (/ mad4d3k mua)
	;=====����� ����� ���������=====
	b12a (polar polu (angle b a) pa)
	ca (polar polu (+ (angle polu b12a) alfa) (* ac (/ pa ab)))
	b3en (polar polu (angle b e) lb3n)
	b3b2k (polar b12a
		    (if (clockwise-p e b (polar b (angle pol b3v) 10.))
		      (+ (angle b12v b3v) (* 0.5 pi))
		      (- (angle b12v b3v) (* 0.5 pi))
		    )
		    lb3b2k
	     )
	b3a (inters b3en (polar b3en (+ (angle e b) (* 0.5 pi)) 10)
		    b3b2k (polar b3b2k (angle b e) 10.)
		    nil
	    )
	d3a (polar polu (+ (angle polu b3a) (/ pi 3.)) (* de (/ (distance polu b3a) eb)))
	d4cn (polar ca (angle d c) ld4cn)
	d4d3k (polar d3a
		    (if (clockwise-p e d (polar d (angle pol d4v) 10.))
		      (+ (angle d3v d4v) (* 0.5 pi))
		      (- (angle d3v d4v) (* 0.5 pi))
		    )
		    ld4d3k
	     )
	d4a (inters d4d3k (polar d4d3k (angle d e) 10)
		    d4cn (polar d4cn (+ (angle d c) (* 0.5 pi)) 10.)
		    nil
	    )		
  );setq
);planusk

(defun modula ()
  (setq	;=====������ ���������� ���������=========
	mab3 (* mua (distance polu b3a))
	mac (* mua (distance polu ca))
	mad3 (* mua (distance polu d3a))
	mad4 (* mua (distance polu d4a))
	;=====������ �������������� ���������=========
	mab3t (* mua (distance b3en b3a))
	madct (* mua (distance d4cn d4a))
	mab3b2r (* mua (distance b3b2k b3a))
	mad4d3r (* mua (distance d4d3k d4a))
	madc (* mua (distance ca d4a))
	mab3b2 (* mua (distance b12a b3a))
	mad4d3 (* mua (distance d4a d3a))
	;������� ���������
	eps3 (/ mab3t leb)
	eps5 (/ madct lcd)
	masep (list eps3 eps5)
	masa (list mab3 mac mad3 mad4 mab3b2 mad4d3 madc)
  )
  (duga e b b3en b3a 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^3;\\H2;")
  (duga c d d4cn d4a 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^5;\\H2;")
)
;=============================================================================================


(defun cherm (/ i)  
  (li (list a b c a))(li (list c d e b))
  (add_block b (angle e b))(add_block d (angle e d))
  (add_opor a (+ (* 0.5 pi) fi)) (add_opor e (+ (* 0.67 pi) (angle e b)))
  (mapcar '(lambda (t1) (add_circ 0.7 t1)) masivm)
  (setq i 65)
  (mapcar '(lambda (t1) (progn
			  (add_text t1 (chr i))
			  (setq i (1+ i))
			)
	   );end of lambda
           masivm
  );end of mapcar
  (duga a b b (polar b (+ (angle a b) (* 0.5 pi)) 10.) 0.5 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^1;\\H2;")
  (muul lab ab)
);cherm

(defun cherv (/)
  (add_text pol "p,a,e")  
  ;����������
  (risskor pol b12v nil "v^B1")
  (risskor pol cv "c" "v^C")
  (li (list cv b12v))
  (risskor pol d3v nil "v^D3")
  (risskor pol b3v nil "v^B3")
  (risskor pol d4v nil "v^D4")
  (add_text b12v "\\A1;b\\H0.7x;\\S^2;\\H1.4286x;")
  (add_text b3v "\\A1;b\\H0.7x;\\S^3;\\H1.4286x;")
  (add_text d4v "\\A1;d\\H0.7x;\\S^4;\\H1.4286x;")
  (add_text d3v "\\A1;d\\H0.7x;\\S^3;\\H1.4286x;")
  ;�������������
  (risskor d3v d4v nil "v^D4D3")
  (risskor cv d4v nil "v^D4C")
  (risskor b12v b3v nil "v^B3B2")
  (duga e d pol d3v 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^3;\\H2;")
  (duga c d cv d4v 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^5;\\H2;")
  ;���������
  (muuv mvb pb)
  (formv omeg lab)
)

(defun chera (/ dmu le)
  (add_text polu "\\A1;p\\H0.7x;\\S^1;\\H1.42857x;,a,e")
  ;����� 1
  (risskor polu b12a nil "a^B1")
  ;����� 3
  (risskor polu ca "c" "a^C")
  (risskor polu d3a nil "a^D3")
  (risskor polu b3a nil "a^B3")
  (risskor polu d4a nil "a^D4")
  (add_text b12a "\\A1;b\\H0.7x;\\S^1;\\H1.42857x;")
  (add_text b3a "\\A1;b\\H0.7x;\\S^3;")
  (add_text d3a "\\A1;d\\H0.7x;\\S^3;")
  (add_text d4a "\\A1;d\\H0.7x;\\S^4;")
  (risskor b12a b3a nil "a^B3B2")
  (risskor d3a d4a nil "a^D4D3")
  (risskor ca d4a nil "a^DC")
  ;������������� ���������
  (risskor polu b3en nil "an^BE")
  (risskor b3en b3a nil "a\U+03C4^BE")
  (risskor b12a b3b2k nil "ak^B3B2")
  (risskor b3b2k b3a nil "ar^B3B2")
  (risskor ca d4cn nil "an^DC") 
  (risskor d4cn d4a nil "a\U+03C4^DC")  
  (risskor d3a d4d3k nil "ak^D4D3")
  (risskor d4d3k d4a nil "ar^D4D3")
  (kor_cher b12a b3b2k b12v b3v "v^B3B2" "ak^B3B2" "\\A1;\\H2;\U+03C9\\H0.7x;\\S^3;\\H2;")
  (kor_cher d3a d4d3k d3v d4v "v^D4D3" "ak^D4D3" "\\A1;\\H2;\U+03C9\\H0.7x;\\S^3;\\H2;")
  (muua mab pa)
  (forma omeg lab)
)

(defun omep ()
  (add_text (getpoint "\nlDE: ")
	    (strcat "\\A1;l\\H0.7x;\\S^DE;\\H1.4286x;=DE\U+00D7\U+03BC\\H0.7x;"
		    "\\S^l;\\H1.4286x;="(rtos de 2 0)"\U+00D7"(rtos mul)
		    "="(rtos lde 2 3)" �"
		    
	    )
  )
  (add_text (getpoint "\nlEB: ")
	    (strcat "\\A1;l\\H0.7x;\\S^EB;\\H1.4286x;=EB\U+00D7\U+03BC\\H0.7x;"
		    "\\S^l;\\H1.4286x;="(rtos eb 2 0)"\U+00D7"(rtos mul)
		    "="(rtos leb 2 3)" �"
		    
	    )
  )
  ;���������
  ;��� ���������� ���������
  (formsk2 muv (distance pol b3v) "B3" "pb\\H0.7x;\\S^3;\\H1.4286x;" t)
  (formsk2 muv (distance pol d3v) "D3" "pd\\H0.7x;\\S^3;\\H1.4286x;" t)
  (formsk2 muv (distance pol d4v) "D4" "pd\\H0.7x;\\S^4;\\H1.4286x;" t)
  (formsk2 muv (distance pol cv) "C" "pc" t)
  ;��� ������������� ���������
  (formsk2 muv (distance d4v cv) "D4C" "cd\\H0.7x;\\S^4;\\H1.4286x;" t)
  (formsk2 muv (distance b12v b3v) "B3B2" "b\\H0.7x;\\S^3;\\H1.4286x;b\\H0.7x;\\S^2;\\H1.4286x;" t)
  (formsk2 muv (distance d4v d3v) "D4D3" "d\\H0.7x;\\S^4;\\H1.4286x;d\\H0.7x;\\S^3;\\H1.4286x;" t)
  (add_text (getpoint "\n��������������� speed point D: ")
	    (strcat "\\A1;pd\\H0.7x;\\S^3;\\H1.4286x;=pb\\H0.7x;\\S^3;\\H1.4286x;"
		    "\U+00D7\\H0.7x;\\SEB/ED;\\H1.4286x;="
		    (rtos (distance pol b3v) 2 0) "\U+00D7\\H0.7x;\\S" (rtos de 2 0) "/"
		    (rtos eb 2 0)";\\H1.4286x;=" (rtos (* (distance pol b3v) (/ de eb)) 2 1) " ��"
	    )
  )

  ;���������
  (formsk2 mua (distance polu d3a) "D3" "p\\H0.7x;\\S^1;\\H1.4286x;d\\H0.7x;\\S^3;\\H1.4286x;" nil)
  (formsk2 mua (distance polu d4a) "D4" "p\\H0.7x;\\S^1;\\H1.4286x;d\\H0.7x;\\S^4;\\H1.4286x;" nil)
  (formsk2 mua (distance polu ca) "C" "p\\H0.7x;\\S^1;\\H1.4286x;c" nil)
  (formsk2 mua (distance polu b3a) "B3" "p\\H0.7x;\\S^1;\\H1.4286x;b\\H0.7x;\\S^3;\\H1.4286x;" nil)
  (formsk2 mua (distance b12a b3a) "B3B2" "b\\H0.7x;\\S^3;\\H1.4286x;b\\H0.7x;\\S^2;\\H1.4286x;" nil)
  (formsk2 mua (distance d3a d4a) "D4D3" "d\\H0.7x;\\S^4;\\H1.4286x;d\\H0.7x;\\S^3;\\H1.4286x;" nil)
  (formsk2 mua (distance ca d4a) "D4C" "d\\H0.7x;\\S^4;\\H1.4286x;c" nil)
  (add_text (getpoint "\n��������������� speed point D: ")
	    (strcat "\\A1;p\\H0.7x;\\S^1;\\H1.4286x;d\\H0.7x;\\S^3;\\H1.4286x;"
		    "=p\\H0.7x;\\S^1;\\H1.4286x;b\\H0.7x;\\S^3;\\H1.4286x;"
		    "\U+00D7\\H0.7x;\\SEB/ED;\\H1.4286x;="
		    (rtos (distance polu b3a) 2 0) "\U+00D7\\H0.7x;\\S" (rtos de 2 0) "/"
		    (rtos eb 2 0)";\\H1.4286x;=" (rtos (* (distance polu b3a) (/ de eb)) 2 1) " ��"
	    )
  )
  (add_text (getpoint "\narB3B2")
	    (strcat "\\A1;a\\H0.7x;\\Sr^B3B2;\\H1.4286x;="
		    (rtos (distance b3b2k b3a) 2 0)"\U+00D7"(rtos mua)
		    "="(rtos (* (distance b3b2k b3a) mua) 2 3) "�/�\\H0.7x;\\S2^;\\H1.4286x;"
	    )
  )
  (add_text (getpoint "\narD4D3")
	    (strcat "\\A1;a\\H0.7x;\\Sr^D4D3;\\H1.4286x;="
		    (rtos (distance d4d3k d4a) 2 0)"\U+00D7"(rtos mua)
		    "="(rtos (* (distance d4d3k d4a) mua) 2 3) "�/�\\H0.7x;\\S2^;\\H1.4286x;"
	    )
  )

  ;���������� ���������
  (formusk "aB3E" mvb3 leb)
  (formusk "aDC" mvd4c lcd)
  (formuskt2 "B3E" (distance b3en b3a) mua)
  (formuskt2 "DC" (distance d4cn d4a) mua)
  (koriolis ome3 mvb3b2 "B3B2" "3")
  (koriolis ome3 mvd4d3 "D4D3" "3")
  ;�����
  (ugsu "ome3B3E" mvb3 leb)
  (ugsu "ome5DC" mvd4c lcd)
  ;��������
  (ugsu "eps3BE" mab3t leb)
  (ugsu "eps5DC" madct lcd)
)
;=====================================================================================================

(defun c:sem6_1 (/ osm le ab omeg lab mul bc ac cd ae zvenia razm_zv lcd mvb mab fi  
a alfa b c e d eb leb de lde masivm pb muv pol b12v b3v cv d3v d4v mvb3 mvc mvd3 mvd4 
mvb3b2 mvd4d3 mvd4c ome3 ome5 masom mab3n madcn mab3b2k mad4d3k masv masb pa polu mua lb3n 
ld4cn lb3b2k ld4d3k b12a ca b3en b3b2k b3a d3a d4cn d4d3k d4a mab3 mac mad3 mad4 mab3t madct 
mab3b2r mad4d3r madc mab3b2 mad4d3 eps3 eps5 masep masa)
  (VL-LOAD-COM)
  (setq osm (getvar "OSMODE"))
  (setvar "OSMODE" 20919)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "TIMESI.TTF" )))
	le (append le (list (cons 2 "�����")))	
  )
  (entmakex le)
  (setvar "textstyle" "�����")
  (load (strcat tmm_folder "funkcii_tmm.lsp"))
  (load (strcat tmm_folder "tmm_semi.lsp"))
  (vvod)
  (tochkim)
  (cherm)
  (plansk)
  (modulv)
  (cherv)
  (planusk)
  (modula)
  (chera)  
  (omep)
  (tablica_va masb masv masa masom masep)
  (setvar "OSMODE" osm)
  (setvar "dimtxsty" "�����")
  (princ)
)
(c:sem6_1)
