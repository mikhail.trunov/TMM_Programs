;���� ��� ����������� �� ��������������� ������� � 23 ��� 2
(defun vvod (/ f)
  (initget 6)
  (setq f (getreal "\n������� ���� �� ")
        ab (getreal "\n������� �B [mm] ")
        omeg (getreal "\n������� ����� ")
        lab (getreal "\n������� AB [m] ")
        mul (/ lab ab)
	;����� �� �����
	ac (* 3. ab)
	be (* 5. ab)
	ef (* 0.5 ab)
	cd ef
	;�������� �����
	lcd (* 0.5 lab)
	;�������� � ���������
	mvb (* omeg lab)
	mab (* omeg omeg lab)
	fi (- pi (* f (/ pi 180.)))
  );end of setq
  (ishodn omeg lab f)
  (tabl_razmer (list "\\A1;l\\H0.7x;\\S^AB;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^AC;\\H1.4286x;"
		     "\\A1;l\\H0.7x;\\S^BE;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^EF;\\H1.4286x;"
		     "\\A1;l\\H0.7x;\\S^CD;\\H1.4286x;"
	       )
    	       (list ab ac be ef cd)
    mul
  )
);end of vvod
;;;;;;;;;;;;;;;;;;;;;�������, ������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;----------------------------------------------------------------------------------
;;;;;;---=================������� ���������� ����� ���������================-------
(defun tochkim ()
  (setq
    a (getpoint "\n��� ������� ��������: ")
    c (polar a 0. ac)
    b (polar a fi ab)
    e (polar b (angle b c) be)
    f (polar e (+ (angle e b) (* 0.5 pi)) ef)
    d (polar c (- (angle e b) (* 0.5 pi)) cd)   
    bc (distance b c)
    lbc (* bc mul)
    masivm (list a b c d e f)       
  )
)
;----------------------------------------------------------------------------------
;--==������� ����� ����� ��������� � ���������� ��������� � ������ ���������===----
(defun plansk ()
  (initget 1)
  (setq pb (getdist "\n������� ����� ������� �������� Va [mm] ")
  	pol (getpoint "\n������� ����� ����� ��������� ")  
	muv (/ mvb pb)
  ;======����� ����� ���������========  
	bv (polar pol (- (angle a b) (/ pi 2.)) pb)
	c2v (inters pol (polar pol (angle b c) 100.)
		    bv (polar bv (+ (angle b c) (* 0.5 pi)) 100.)
		    nil
	    )
	ev (polar bv (angle bv c2v) (* be (/ (distance bv c2v) bc)))
	fv (polar ev (+ (angle ev bv) (* 0.5 pi)) (* ef (/ (distance bv c2v) bc)))
	
  ;========������ ��������=======
        mvc2 (* muv (distance pol c2v))	
	mve (* muv (distance pol ev))
	mvf (* muv (distance pol fv))
	mvcb (* muv (distance c2v bv))	
  ;========������� ��������====================
        ome2 (/ mvcb lbc)
	masom (list ome2)
  ;=====���������� ���������=====
	macbn (/ (expt mvcb 2) lbc)
	mac2c3k (* 2. ome2 mvc2)
	dv (if (clockwise-p b c (polar c (angle bv c2v) 1e15))
	     (polar pol (+ (angle c d) (* 0.5 pi)) (* cd (/ (distance bv c2v) bc)))
	     (polar pol (- (angle c d) (* 0.5 pi)) (* cd (/ (distance bv c2v) bc)))
           )
	     
	mvd (* muv (distance pol dv))
	madcn (/ (expt mvd 2) lcd)
	masb (list "\\A1;C\\H0.7x;\\S^2;" "D" "E" "F"
		   "\\A1;C\\H0.7x;\\S^2;\\H1.42857x;B")
	masv (list mvc2 mvd mve mvf mvcb mved mvfe)
  )
  (duga b c bv c2v 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^2;\\H2;")  
)
;----------------------------------------------------------------------------------
;---------========������� ����� ����� ��������� � ������ ���������========---------
(defun planusk ()
  (initget 1)
  (setq pa (getdist "\n������� ����� ������� ��������� ����� B [mm] ")	
	polu (getpoint "\n������� ����� ����� ��������� ")
	mua (atof (rtos (/ mab pa) 2 3))
	;=====����� �������� ���������� ���������=====
	lcbn (/ macbn mua)
	ldcn (/ madcn mua)
	lc2c3k (/ mac2c3k mua)
	;=====����� ����� ���������=====
	ba (polar polu (angle b a) pa)
	c2bn (polar ba (angle c b) lcbn)
	c2c3k (polar polu (if (clockwise-p b c (polar c (angle bv c2v) 10))
			    (+ (angle pol c2v) (* 0.5 pi))
			    (- (angle pol c2v) (* 0.5 pi))
			  )
		     lc2c3k
	      )
	c2a (inters c2bn (polar c2bn (+ (angle c b) (* 0.5 pi)) 100.)
		    c2c3k (polar c2c3k (angle c b) 100.)
		    nil
	    )
	ea (polar ba (angle ba c2a) (* be (/ (distance ba c2a) bc)))
	fa (polar ea (+ (angle ea ba) (* 0.5 pi)) (* ef (/ (distance ba c2a) bc)))
	dcn (polar polu (angle d c) ldcn)
	 
	;=====������ ���������� ���������=========
	mac2 (* mua (distance polu c2a))
	;mad (* lcd ome2 ome2)
	mae (* mua (distance polu ea))
	maf (* mua (distance polu fa))
	macb (* mua (distance c2a ba))
	;da (polar polu (angle d c) (/ mad mua))

	;=====������ �������������� ���������=========
	macbt (* mua (distance c2bn ba))
	;������� ���������
	eps2 (/ macbt lbc)
	masep (list eps2)
	madct (* eps2 lcd)
	ladct (/ madct mua)
	da (if (clockwise-p b c (polar c (angle c2bn c2a) 1e15))
	     (polar dcn (+ (angle c d) (* 0.5 pi)) ladct)
	     (polar dcn (- (angle c d) (* 0.5 pi)) ladct)
           )
	mad (* mua (distance polu da))
	masa (list mac2 mad mae maf macb)
  )
  (duga b c c2bn c2a 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^2;\\H2;")
)
;=============================================================================================


(defun cherm (/ i)
  (li (list a b e f)) (li (list c d))
  (mapcar '(lambda (t1) (add_circ 0.7 t1)) masivm)  
  (setq i 65)
  (mapcar '(lambda (t1) (progn
			  (add_text t1 (chr i))
			  (setq i (1+ i))
			)
	   );end of lambda
           (list a b c d e f)
  );end of mapcar
  (add_block c (angle c b))
  (add_opor a (+ (* 0.5 pi) fi))
  (add_opor c pi)
  (duga a b b (polar b (- (angle a b) (* 0.5 pi)) 10.) 0.5 "\\A1;\U+03C9\\H0.7x;\\S^1;\\H1.4286x;")
  (muul lab ab)
); defun cherm

(defun cherv ()
  (add_text pol "\\A1;p,a,c\\H0.7x;\\S^3;\\H1.4286x;")
  (risskor pol bv "b" "v^B")
  (risskor pol c2v nil "v^C2")
  (risskor pol ev "e" "v^E")
  (risskor pol fv "f" "v^F")
  (risskor pol dv "d" "v^D")
  (risskor bv c2v nil "v^CB")
  (li (list c2v ev fv))
  (add_text c2v "\\A1;c\\H0.7x;\\S^2;\\H1.4286x;")
  (muuv mvb pb)
  (formv omeg lab)
); defun cherv

(defun chera (/ dmu le)
  (add_text polu "\\A1;p\\H0.7x;\\S^1;\\H1.4286x;,a,c\\H0.7x;\\S^3;\\H1.4286x;")
  (risskor polu ba "b" "a^B") 
  (risskor polu c2a nil "a^C")
  (risskor polu ea "e" "a^E")
  (risskor polu fa "f" "a^F")
  (risskor polu da "d" "a^D")
  (risskor ba c2a nil "a^CB")
  (li (list c2a ea fa))
; ���������� � ���������
  (risskor ba c2bn nil "an^CB")
  (risskor polu dcn nil "an^DC")
  (risskor dcn da nil "a\U+03C4^DC")
  (risskor polu c2c3k nil "ak^C2C3")
; �������������� � �������������
  (risskor c2bn c2a nil "a\U+03C4^CB")
  (risskor c2c3k c2a nil "ar^C2C3") 
  (add_text c2a "\\A1;c\\H0.7x;\\S^2;\\H1.4286x;")
  (kor_cher polu c2c3k pol c2v "v^C2C3" "ak^C2C3" "\\A1;\\H2;\U+03C9\\H0.7x;\\S^2;\\H2;")
  (muua mab pa)
  (forma omeg lab)
); defun chera

(defun omep ()  
  ;���������
  (formsk2 muv (distance pol c2v) "C2" "pc\\H0.7x;\\S^2;\\H1.4286x;" t)
  (formsk2 muv (distance pol dv) "D" "pd" t)
  (formsk2 muv (distance pol ev) "E" "pe" t)
  (formsk2 muv (distance pol fv) "F" "pf" t)
  (formsk2 muv (distance c2v bv) "C2B" "c\\H0.7x;\\S^2;\\H1.4286x;b" t)
  ;����������
  (formusk "aCB" mvcb lbc)  
  (koriolis ome2 mvc2 "C2C3" "2")
  ;��������������
  (formuskt2 "CB" (distance c2bn c2a) mua)
  (add_text (getpoint "\narC2C3")
	    (strcat "\\A1;a\\H0.7x;\\Sr^C2C3;\\H1.4286x;="
		    (rtos (distance c2c3k c2a) 2 0)"\U+00D7"(rtos mua)
		    "="(rtos (* (distance c2c3k c2a) mua) 2 3) "�/�\\H0.7x;\\S2^;\\H1.4286x;"
	    )
  )
  (formsk2 mua (distance polu c2a) "C2" "p\\H0.7x;\\S^1;\\H1.4286x;c\\H0.7x;\\S^2;\\H1.4286x;" nil)
  (formsk2 mua (distance polu da) "D" "p\\H0.7x;\\S^1;\\H1.4286x;d" nil)
  (formsk2 mua (distance polu ea) "E" "p\\H0.7x;\\S^1;\\H1.4286x;e" nil)
  (formsk2 mua (distance polu fa) "F" "p\\H0.7x;\\S^1;\\H1.4286x;f" nil)
  (formsk2 mua (distance c2a ba) "CB" "cb" nil)
  (add_text (getpoint "\n��������������� speed: ")
	    (strcat "\\A1;cd\\H0.7x;\\S^2;\\H1.4286x;=bc\\H0.7x;\\S^2;\\H1.4286x;\U+00D7\\H0.7x;\\SCD/BC;\\H1.4286x;="
		    (rtos (distance c2v bv) 2 0) "\U+00D7\\H0.7x;\\S" (rtos (distance c d) 2 0) "/" (rtos (distance c b) 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance c2v bv) (/ (distance c d) (distance c b))) 2 1) " ��"
	    )
  )
  (add_text (getpoint "\n��������������� aceleration: ")
	    (strcat "\\A1;cd\\H0.7x;\\S^2;\\H1.4286x;=bc\\H0.7x;\\S^2;\\H1.4286x;\U+00D7\\H0.7x;\\SCD/BC;\\H1.4286x;="
		    (rtos (distance c2a ba) 2 0) "\U+00D7\\H0.7x;\\S" (rtos (distance c d) 2 0) "/" (rtos (distance c b) 2 0)
		    ";\\H1.4286x;=" (rtos (* (distance c2a ba) (/ (distance c d) (distance c b))) 2 1) " ��"
	    )
  )
  (ugsu "ome2CB" mvcb lbc) 
  (ugsu "eps2CB" macbt lbc)  
)
;=====================================================================================================

(defun c:sem_24_1 (/ osm le ab omeg lab mul )
  (setq osm (getvar "OSMODE"))
  (setvar "OSMODE" 20919)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "TIMESI.TTF" )))
	le (append le (list (cons 2 "�����")))	
  )
  (entmakex le)
  (setvar "textstyle" "�����")
  (load "funkcii_tmm.lsp")
  (load "tmm_semi.lsp")
  (vvod)
  (tochkim)
  (cherm)
  (plansk)
  (cherv)
  (planusk)
  (chera)  
  (omep)
  (tablica_va masb masv masa masom masep)
  (setvar "OSMODE" osm)
  (setvar "dimtxsty" "�����")
)
(c:sem_24_1)
