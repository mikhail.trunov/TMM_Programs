;���� ��� ����������� �� ��������������� ������� � 25 ��� 1
(defun vvod (/ f)
  (initget 6)
  (setq f (getreal "\n������� ���� �� ")
        ab (getdist "\n������� �B [mm] ")
        omeg (getreal "\n������� ����� ")
        lab (getreal "\n������� AB [m] ")	
        mul (/ lab ab)
	mvb (* omeg lab)
	mab (* omeg omeg lab)
	fi (* f (/ pi 180.))
  );end of setq
  (ishodn omeg lab f)
  (tabl_razmer (list "\\A1;l\\H0.7x;\\S^AB;\\H1.4286x;")
    	       (list ab)
    mul
  )
);end of vvod


;;;;;;;;;;;;;;;;;;;;;�������, ������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;----------------------------------------------------------------------------------
;;;;;;---=================������� ���������� ����� ���������================-------
(defun tochkim ()
  (setq
    a (getpoint "\n��� ������� ���������: ")        
    b (polar a fi ab)    
    masivm (list a b)    
  )  
)
;----------------------------------------------------------------------------------
;--==������� ����� ����� ��������� � ���������� ��������� � ������ ���������===----
(defun plansk (/)
  (initget 1)
  (setq pb (getdist "\n������� ����� ������� �������� Va [mm] ")
  	pol (getpoint "\n������� ����� ����� ��������� ")  
	muv (/ mvb pb)
  ;======����� ����� ���������========  
  	b2v (polar pol (+ (angle a b) (/ pi 2.)) pb)
	;vC2=vB+vC2B vC2//BC vC2B_|_BC
	b3v (inters pol (polar pol 0. 10.)
		    b2v (polar b2v (* 0.5 pi) 10.)
		    nil
	    )	
  ;========������ ��������=======
        mvb3 (* muv (distance pol b3v))
	mvb3b2 (* muv (distance b2v b3v))       
  ;=====���������� ���������=====
	masv (list mvb3 mvb3b2)
	masom (list omeg)
	masb (list "\\A1;B\\H0.7x;\\S^3;\\H1.4286x;"
		   "\\A1;B\\H0.7x;\\S^3;\\H1.4286x;B\\H0.7x;\\S^2;\\H1.4286x;")
  )
)
;----------------------------------------------------------------------------------
;---------========������� ����� ����� ��������� � ������ ���������========---------
(defun planusk ()
  (initget 1)
  (setq pa (getdist "\n������� ����� ������� ��������� ����� B [mm] ")	
	polu (getpoint "\n������� ����� ����� ��������� ")
	mua (/ mab pa)	
	;=====����� ����� ���������=====
	b2a (polar polu (angle b a) pa)
	b3a (inters polu (polar polu 0. 10.)
		    b2a (polar b2a (* 0.5 pi) 10.)
		    nil
	    )
	;=====������ ���������=========
        mab3 (* mua (distance polu b3a))
	mab3b2 (* mua (distance b2a b3a))
	masa (list mab3 mab3b2)
	masep (list 0.)
  )  
)
;=============================================================================================


(defun cherm (/ i)
  (li (list a b)) (li (list a (polar a 0. 50.)))
  (mapcar '(lambda (t1) (add_circ 0.7 t1)) masivm)
  (setq i 65)
  (mapcar '(lambda (t1) (progn
			  (add_text t1 (chr i))
			  (setq i (1+ i))
			)
	   ) masivm
  ); mapcar
  (add_opor a (+ (* 0.5 pi) fi))
  (add_block b (* 0.5 pi))
  (duga a b b (polar b (+ (angle a b) (* 0.5 pi)) 10.) 0.5 "\\A1;\U+03C9\\H0.7x;\\S^1;\\H1.4286x;")
  (muul lab ab)
)

(defun cherv ()
  (risskor pol b2v nil "v^B2") 
  (risskor pol b3v nil "v^B3")
  (risskor b2v b3v nil "v^B3B2") 
  (add_text pol "p,a")
  (add_text b2v "\\A1;b\\H0.7x;\\S^2;\\H1.4286x;")
  (add_text b3v "\\A1;b\\H0.7x;\\S^3;\\H1.4286x;")
  (muuv mvb pb)
  (formv omeg lab)
  (formsk2 muv (distance pol b3v) "B3" "pb\\H0.7x;\\S^3;\\H1.4286x;" t)
  (formsk2 muv (distance b2v b3v) "B3B2" "b\\H0.7x;\\S^3;\\H1.4286x;b\\H0.7x;\\S^2;\\H1.4286x;" t)
)

(defun chera ()
  (add_text polu "\\A1;p\\H0.7x;\\S^1;\\H1.4286x;,a")
  (risskor polu b2a nil "a^B2")
  (risskor polu b3a nil "a^B3")
  (risskor b2a b3a nil "a^B3B2")
  (add_text b2a "\\A1;b\\H0.7x;\\S^2;\\H1.4286x;")
  (add_text b3a "\\A1;b\\H0.7x;\\S^3;\\H1.4286x;")
  (muua mab pa)
  (forma omeg lab)
  (formsk2 mua (distance polu b3a) "B3" "p\\H0.7x;\\S^p;\\H1.4286x;b\\H0.7x;\\S^3;\\H1.4286x;" nil)
  (formsk2 mua (distance b2a b3a) "B3B2" "b\\H0.7x;\\S^3;\\H1.4286x;b\\H0.7x;\\S^2;\\H1.4286x;" nil)
)
;=================================================================================================

;=====================================================================================================

(defun c:sem_25_1 (/ osm le ab omeg lab mul mvb mab fi


		   )
  (VL-LOAD-COM)
  (setq osm (getvar "OSMODE"))
  (setvar "OSMODE" 20919)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "TIMESI.TTF" )))
	le (append le (list (cons 2 "�����")))	
  )
  (entmakex le)
  (setvar "textstyle" "�����")
  (load "funkcii_tmm.lsp")
  (load "tmm_semi.lsp")
  (vvod)
  (tochkim)
  (cherm)
  (plansk)
  (cherv)
  (planusk)
  (chera)
  (tablica_va masb masv masa masom masep)
  (setvar "OSMODE" osm)
  (setvar "dimtxsty" "�����")
)
(c:sem_25_1)