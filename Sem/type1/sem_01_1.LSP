;���� ��� ����������� �� ��������������� ������� � 1 ��� 1
(defun vvod (/ f)
  (initget 6)
  (setq f (getreal "\n������� ���� �� ")
        ab (getreal "\n������� �B [mm] ")
        omeg (getreal "\n������� ����� ")
        lab (getreal "\n������� AB [m] ")	
        mul (atof (rtos (/ lab ab)2 4))
	;�������� �����
	lbc (* 13. lab)
	lad lbc
	lcd (* 1.3 lab)
	ldf (* 1.5 lab)
	lef (* 1.4 lab)
	;����� �� �����
	bc (/ lbc mul)
	ad bc
	cd (/ lcd mul)
	df (/ ldf mul)
	ef (/ lef mul)
	;�������� � ���������
	mvb (* omeg lab)
	mab (atof (rtos (* omeg omeg lab) 2 4))
	fi (- pi (* f (/ pi 180.)))
  );end of setq
  (ishodn omeg lab f)
  (tabl_razmer (list "\\A1;l\\H0.7x;\\S^AB;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^BC;\\H1.4286x;"
		     "\\A1;l\\H0.7x;\\S^AD;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^CD;\\H1.4286x;"
		     "\\A1;l\\H0.7x;\\S^DF;\\H1.4286x;" "\\A1;l\\H0.7x;\\S^EF;\\H1.4286x;"		     
	       )
    	       (list ab bc ad cd df ef)
    mul
  )
);end of vvod

;;;;;;;;;;;;;;;;;;;;;�������, ������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;----------------------------------------------------------------------------------
;;;;;;---=================������� ���������� ����� ���������================-------
(defun tochkim ()
  (setq
    a (getpoint "\n��� ������� ���������: ")
    d (polar a 0. ad)
    f (polar d 0. df)
    b (polar a fi ab)    
    c (car (vl-sort
	     (circint b bc d cd)
	     (function (lambda (e1 e2) (> (cadr e1) (cadr e2)))) 
	   )
      )
    e (cadr (vl-sort
	     (dwgru d (polar d (- (angle d c) (* 0.5 pi)) 100.) f ef)
	     (function (lambda (e1 e2) (> (cadr e1) (cadr e2)))) 
	   )
      )
    de (distance d e)
    lde (* de mul)
    masivm (list a b c d e f) 
  )  
)
;----------------------------------------------------------------------------------
;--==������� ����� ����� ��������� � ���������� ��������� � ������ ���������===----
(defun plansk (/)
  (initget 1)
  (setq pb (getint "\n������� ����� ������� �������� Va [mm] ")
  	pol (getpoint "\n������� ����� ����� ��������� ")
  	muv (atof (rtos (/ mvb pb) 2 4))
  ;======����� ����� ���������========  
  	bv (polar pol (- (angle a b) (/ pi 2)) pb)
	;vC2=vB+vC2B vC2//BC vC2B_|_BC
	cv (inters pol (polar pol (- (angle d c) (* 0.5 pi)) 10.)
		    bv (polar bv (- (angle b c) (* 0.5 pi)) 10.)
		    nil
	    )
	e3v (polar pol (- (angle pol cv) (/ pi 2.)) (* (distance d e) (/ (distance cv pol) cd)))
	e5v (inters e3v (polar e3v (angle e d) 10.)
		    pol (polar pol (- (angle f e) (* 0.5 pi)) 10.)
		    nil
	    )
  ;========������ ��������=======
        mvc (* muv (distance pol cv))
	mve3 (* muv (distance pol e3v))
	mve5 (* muv (distance pol e5v))
	mvcb (* muv (distance cv bv))
	mve5e3 (* muv (distance e5v e3v))	
  ;========������� ��������====================
        ome2 (/ mvcb lbc)
	ome3 (/ mvc lcd)
	ome5 (/ mve5 lef)
	masom (list ome2 ome3 ome5)
  ;=====���������� ���������=====
	macbn (/ (expt mvcb 2) lbc)
	mae5e3k (* ome3 mve5e3 2)
	macdn (/ (expt mvc 2) lcd)
	mae5fn (/ (expt mve5 2) lef)
	masv (list mvc mve3 mve5 mvcb mve5e3)
	masb (list "C" "\\A1;E\\H0.7x;\\S^3;\\H1.4286x;" "\\A1;E\\H0.7x;\\S^5;\\H1.4286x;" "CB"
		   "\\A1;E\\H0.7x;\\S^5;\\H1.4286x;E\\H0.7x;\\S^3;\\H1.4286x;")
  )
  (duga b c bv cv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^2;\\H2;")
  (duga d c pol cv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^3;\\H2;")
  (duga f e pol e5v 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^5;\\H2;")
)
;----------------------------------------------------------------------------------
;---------========������� ����� ����� ��������� � ������ ���������========---------
(defun planusk (/)
  (initget 1)
  (setq pa (getint "\n������� ����� ������� ��������� ����� B [mm] ")	
	polu (getpoint "\n������� ����� ����� ��������� ")
	mua (atof (rtos (/ mab pa) 2 3))
	;=====����� �������� ���������� ���������=====
	lcbn (/ macbn mua)
	lcdn (/ macdn mua)
	le5fn (/ mae5fn mua)
	le5e3k (/ mae5e3k mua)	
	;=====����� ����� ���������=====
	ba (polar polu (angle b a) pa)
	cbn (polar ba (angle c b) lcbn)
	cdn (polar polu (angle c d) lcdn)
	ca (inters cbn (polar cbn (+ (angle b c) (* 0.5 pi)) 10.)
		   cdn (polar cdn (+ (angle c d) (* 0.5 pi)) 10.)
		   nil
	   )
	e5fn (polar polu (angle e f) le5fn)
	e3a (polar polu (- (angle polu ca) (/ pi 2.)) (* (distance d e) (/ (distance ca polu) cd)))
	e5e3k (polar e3a
		     (if (clockwise-p d e (polar e (angle pol e5v) 10))
		       (+ (angle e3v e5v) (* 0.5 pi))
		       (- (angle e3v e5v) (* 0.5 pi))
		     )
		     le5e3k)
	e5a (inters e5e3k (polar e5e3k (angle d e) 10)
		    e5fn (polar e5fn (+ (angle f e) (* 0.5 pi)) 10)
		    nil
	    )	
	;=====������ ���������=========
        mac (* mua (distance polu ca))
	mae3 (* mua (distance polu e3a))
	mae5 (* mua (distance polu e5a))
	mae5e3 (* mua (distance e5a e3a))
	macb (* mua (distance ca ba))
	masa (list mac mae3 mae5 macb mae5e3)	
	macbt (* mua (distance cbn ca))
	macdt (* mua (distance cdn ca))
	maeft (* mua (distance e5fn e5a))
	mae5e3r (* mua (distance e5e3k e5a))
	;������� ���������
	eps2 (atof (rtos (/ macbt lbc) 2 3))
	eps3 (atof (rtos (/ macdt lcd) 2 3))
	eps5 (atof (rtos (/ maeft lef) 2 3))
	masep (list eps2 eps3 eps5)
  )
  (duga b c cbn ca 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^2;\\H2;")
  (duga d c cdn ca 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^3;\\H2;")
  (duga f e e5fn e5a 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^5;\\H2;")
)
;=============================================================================================


(defun cherm (/ i)
  (li (list a b c d e f))
  (add_opor a (+ (* 0.5 pi) fi))(add_opor d (+ (* 0.5 pi) (angle d e)))
  (add_opor f (+ (* 0.5 pi) (angle f e)))
  (add_block e (angle d e))
  (duga a b b (polar b (- (angle a b) (* 0.5 pi)) 10.) 0.5 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^1;\\H2;")
  (mapcar '(lambda (t1) (add_circ 0.7 t1)) masivm)
  (setq i 65)
  (mapcar '(lambda (t1) (progn
			  (add_text t1 (chr i))
			  (setq i (1+ i))
			)
	   ) masivm
  ); mapcar
  (muul lab ab)
)

(defun cherv ()
  (risskor pol bv "b" "v^B") 
  (risskor pol cv "c" "v^C")
  (risskor pol e3v nil "v^E3")
  (risskor pol e5v nil "v^E5")
  (risskor bv cv nil "v^CB")
  (risskor e3v e5v nil "v^E5E3")
  (add_text pol "p,a,d")
  ;(li (list bv dv))
  (add_text e3v "\\A1;e\\S^3;") (add_text e5v "\\A1;e\\S^5;")
  (muuv mvb pb)
  (formv omeg lab)
  ;���������  
)

(defun chera ()
  (add_text polu "\\A1;p\\H0.7x;\\S^1;\\H1.4286x;,a,d,f")
  (risskor polu ba "b" "a^B")
  (risskor polu ca "c" "a^C")
  (risskor polu e5a nil "a^E5")
  (risskor polu e3a nil "a^E3")
  ;���������� � ���������
  (risskor ba cbn nil "an^CB")
  (risskor polu cdn nil "an^CD")
  (risskor e3a e5e3k nil "ak^E5E3")
  (risskor polu e5fn nil "an^E5F")
  ;�������������� � �������������
  (risskor cbn ca nil "a\U+03C4^CB")
  (risskor e5e3k e5a nil "ar^E5E3")
  (risskor cdn ca nil "a\U+03C4^CD")
  (risskor e5fn e5a nil "a\U+03C4^E5F")
  (risskor ba ca nil "a^CB")
  (risskor e3a e5a nil "a^E5E3")
  (kor_cher e3a e5e3k e3v e5v "v^E5E3" "ak^E5E3" "\\A1;\\H2;\U+03C9\\H0.7x;\\S^3;\\H2;")
  ;(li (list ba d2a))
  (add_text e5a "\\A1;e\\S^5")(add_text e3a "\\A1;e\\S^3")
  (muua mab pa)
  (forma omeg lab)  
)
;=================================================================================================
(defun omep ()
  (add_text (getpoint "\nlDE: ")
	    (strcat "\\A1;l\\H0.7x;\\S^DE;\\H1.4286x;=DE\U+00D7\U+03BC\\H0.7x;"
		    "\\S^l;\\H1.4286x;="(rtos de 2 0)"\U+00D7"(rtos mul)
		    "="(rtos lde 2 3)" �"
		    
	    )
  )
  (formsk2 muv (distance pol cv) "C" "pc" t)
  (formsk2 muv (distance pol e3v) "E3" "pe\\H0.7x;\\S^3;\\H1.4286x;" t)
  (formsk2 muv (distance pol e5v) "E5" "pe\\H0.7x;\\S^5;\\H1.4286x;" t)
  (formsk2 muv (distance bv cv) "CB" "bc" t)
  (formsk2 muv (distance e3v e5v) "E5E3" "e\\H0.7x;\\S^5;\\H1.4286x;e\\H0.7x;\\S^3;\\H1.4286x;" t)
  (add_text (getpoint "\n��������������� speed point E: ")
	    (strcat "\\A1;pe\\H0.7x;\\S^3;\\H1.4286x;=pc"
		    "\U+00D7\\H0.7x;\\SED/DC;\\H1.4286x;="
		    (rtos (distance pol cv) 2 0) "\U+00D7\\H0.7x;\\S" (rtos de 2 0) "/"
		    (rtos cd 2 0)";\\H1.4286x;=" (rtos (* (distance pol cv) (/ de cd)) 2 1) " ��"
	    )
  )
  (formusk "aCB" mvcb lbc)
  (formusk "aCD" mvc lcd)
  (formusk "aE5F" mve5 (* mul (distance e f)))  
  (formuskt2 "CB" (distance cbn ca) mua)
  (formuskt2 "CD" (distance cdn ca) mua)
  (formuskt2 "E5F" (distance e5fn e5a) mua)
  (koriolis ome3 mve5e3 "E5E3" "3")
  (formsk2 mua (distance polu ca) "C" "p\\H0.7x;\\S^1;\\H1.4286x;c" nil)
  (formsk2 mua (distance polu e5a) "E5" "p\\H0.7x;\\S^1;\\H1.4286x;e\\H0.7x;\\S^5;\\H1.4286x;" nil)
  (formsk2 mua (distance polu e3a) "E3" "p\\H0.7x;\\S^1;\\H1.4286x;e\\H0.7x;\\S^3;\\H1.4286x;" nil)
  (formsk2 mua (distance ba ca) "CB" "bc" nil)
  (formsk2 mua (distance e3a e5a) "E5E3" "e\\H0.7x;\\S^5;\\H1.4286x;e\\H0.7x;\\S^3;\\H1.4286x;" nil)
  (add_text (getpoint "\n��������������� axel point E: ")
	    (strcat "\\A1;p\\H0.7x;\\S^1;\\H1.4286x;e\\H0.7x;\\S^3;\\H1.4286x;=p\\H0.7x;\\S^1"
		    ";\\H1.4286x;c\U+00D7\\H0.7x;\\SED/DC;\\H1.4286x;="
		    (rtos (distance polu ca) 2 0) "\U+00D7\\H0.7x;\\S" (rtos de 2 0) "/"
		    (rtos cd 2 0)";\\H1.4286x;=" (rtos (* (distance polu ca) (/ de cd)) 2 1) " ��"
	    )
  )
  (ugsu "ome2CB" mvcb lbc)
  (ugsu "ome3CD" mvc lcd)
  (ugsu "ome5E5F" mve5 (* mul (distance f e)))
  (ugsu "eps2CB" macbt lbc)
  (ugsu "eps3CD" macdt lcd)
  (ugsu "eps5E5F" maeft (* mul (distance f e)))
)
;=====================================================================================================

(defun c:sem1_1 (/ osm le
ab omeg lab mul lbc lad lcd ldf lef bc ad cd df ef mvb mab fi
a d f b c e masivm pb pol muv bv cv e3v e5v mvc mve3 mve5 mvcb mve5e3
ome2 ome3 ome5 masom macbn mae5e3k macdn mae5fn masv masb pa polu mua lcbn 
lcdn le5fn le5e3k ba cbn cdn ca e5fn e3a e5e3k e5a mac mae3 mae5 mae5e3 macb 
masa macbt macdt maeft mae5e3r eps2 eps3 eps5 masep)
  (VL-LOAD-COM)
  (setq osm (getvar "OSMODE"))
  (setvar "OSMODE" 20919)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "TIMESI.TTF" )))
	le (append le (list (cons 2 "�����")))	
  )
  (entmakex le)
  (setvar "textstyle" "�����")
  (load "funkcii_tmm.lsp")
  (load "tmm_semi.lsp")
  (vvod)
  (tochkim)
  (cherm)
  (plansk)
  (cherv)
  (planusk)
  (chera)
  (omep)
  (tablica_va masb masv masa masom masep)
  (setvar "OSMODE" osm)
  (setvar "dimtxsty" "�����")
)
(c:sem1_1)