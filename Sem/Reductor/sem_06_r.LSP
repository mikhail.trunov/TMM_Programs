(defun vvod ()
  (setq z1 (getreal "\nZ1: ")
	z2 (getreal "\nZ2: ")
	z3 (+ z1 z2 z2) ;(getreal "\nZ3: ")
	z4 (getreal "\nZ4: ")
	z5 (getreal "\nZ5: ")
	z6 (+ z4 z5 z5) ;(getreal "\nZ6: ")
	z7 (getreal "\nZ7: ")
	z8 (getreal "\nZ8: ")
	z9 (+ z7 z8 z8);(getreal "\nZ9: ")
	dz1 (getdist (strcat "\nDlina Z1=("(rtos z1)") [mm]: "))
	mul (/ (* 0.01 z1) dz1)
	dz2 (* 0.01 z2 (/ 1. mul))
	dz3 (* 0.01 z3 (/ 1. mul))
	dz4 (* 0.01 z4 (/ 1. mul))
	dz5 (* 0.01 z5 (/ 1. mul))
	dz6 (* 0.01 z6 (/ 1. mul))
	dz7 (* 0.01 z7 (/ 1. mul))
	dz8 (* 0.01 z8 (/ 1. mul))
	dz9 (* 0.01 z9 (/ 1. mul))
	u1h (1+ (/ z3 z1))
	u4h (1+ (/ z6 z4))
	u7h (1+ (/ z9 z7))
	u12 (/ u1h (- 1. (/ z3 z2)))
	
	ured (* u1h u4h u7h)
  )
)


(defun redpoints ()
  (setq poiz1 (getpoint "\n��������: ")
	poiz2 (polar poiz1 (* 0.5 pi) (* 0.5 0.01 (/ 1. mul) (+ z1 z2)))
	poiz3 (polar poiz1 pi 10.)
	poiz4 (polar poiz1 0. 20.)
	poiz5 (polar poiz4 (* 0.5 pi) (* 0.5 0.01 (/ 1. mul) (+ z4 z5)))
	poiz6 (polar poiz1 0. 15.)
	poiz7 (polar poiz4 0. 20.)
	poiz8 (polar poiz7 (* 0.5 pi) (* 0.5 0.01 (/ 1. mul) (+ z7 z8)))
	poiz9 (polar poiz4 0. 15.)
  )
)

(defun redcher (/ temp)
  (redopora (polar poiz1 pi 6.2) 1 9. 3.5 t)
  (zubch poiz1 0 0 nil nil dz1)
  (zubch poiz2 0 7 4 nil dz2)
  (li (list poiz1 (polar poiz1 pi 15.)))
  (li (list (polar poiz2 pi 5.)
	    (polar poiz2 0 6.)
	    (polar poiz4 pi 14.)
	    poiz4
      )
  )
  (redopora (polar poiz1 0. 8.5) -1 9. 3.5 t)
  (zubch poiz4 0 0 nil nil dz4)
  (zubch poiz5 0 7 4 nil dz5)
  (li (list (polar poiz5 pi 5.)
	    (polar poiz5 0 6.)
	    (polar poiz7 pi 14.)
	    poiz7
      )
  )
  (redopora (polar poiz4 0. 8.5) -1 9. 3.5 t)
  (zubch poiz7 0 0 nil nil dz7)
  (zubch poiz8 0 7 4 nil dz8)
  (li (list (polar poiz8 pi 5.)
	    (polar poiz8 0 6.)
	    (polar poiz7 0 6.)
	    (polar poiz7 0 20.)
      )
  )
  (redopora (polar poiz7 0. 8.5) -1 9. 3.5 t)
  (li (list (setq temp (list (- (car poiz1) 6.2) (+ (cadr poiz1) 1.75)))
	    (setq temp (polar temp (* 0.5 pi) (+ 5 (max (+ dz2 (* 0.5 dz1)) (+ dz5 (* 0.5 dz4)) (+ dz8 (* 0.5 dz7))))))
	    (polar temp 0. 54.7)
	    (list (+ (car poiz7) 8.5) (+ (cadr poiz7) 1.75))
      )
  )
  (li (list (setq temp (list (- (car poiz1) 6.2) (- (cadr poiz1) 1.75)))
	    (setq temp (polar temp (* 1.5 pi) (+ 5 (max (+ dz2 (* 0.5 dz1)) (+ dz5 (* 0.5 dz4)) (+ dz8 (* 0.5 dz7))))))
	    (polar temp 0. 54.7)
	    (list (+ (car poiz7) 8.5) (- (cadr poiz7) 1.75))
      )
  )
  (muul (* 0.01 z1) dz1)
)

(defun speeds (/ dis_red max_speed pointo)
  (setq dis_red (getdist "\nDistance from Reductor: ")
	max_speed (getdist "\nMax speed: ")
	muspeed (/ (* 0.001 z1 0.5 10.) max_speed)
	pointo (polar poiz9 0. (+ dis_red 20))
	n12 (polar pointo (* 0.5 pi) (* 0.5 dz1))
	po12 (list (+ (car pointo) max_speed) (cadr n12))
	n45 (list (car pointo) (- (cadr poiz5) (* 0.5 dz5)))
	n78 (list (car pointo) (- (cadr poiz8) (* 0.5 dz8)))
	po23 (polar pointo (* 0.5 pi) (* 0.5 dz3))
	po56 (polar pointo (* 0.5 pi) (* 0.5 dz6))
	po89 (polar pointo (* 0.5 pi) (* 0.5 dz9))
	os2 (list (car pointo) (cadr poiz2))
	os5 (list (car pointo) (cadr poiz5))
	os8 (list (car pointo) (cadr poiz8))
	poh1v (inters po12 po23 poiz2 os2 nil)
	po45 (inters pointo poh1v n45 (polar n45 0. 10) nil)
	poh2v (inters po45 po56 poiz5 os5 nil)
	po78 (inters pointo poh2v n78 (polar n78 0. 10) nil)
	poh3v (inters po78 po89 poiz8 os8 nil)
	max_main_l (list (car pointo) (+ 7 (cadr pointo) (max (* 0.5 dz3) (* 0.5 dz6) (* 0.5 dz9))))
	min_main_l (list (car pointo) (- (cadr pointo) (max (* 0.5 dz3) (* 0.5 dz6) (* 0.5 dz9)) 50.))
	
	max_frequ (getdist "\nFrequency oust: ")
	polus_plus (polar min_main_l (* 0.5 pi) max_frequ)
	hor1 (polar polus_plus pi 150)
	hor2 (polar polus_plus 0. 150)
	frh1 (inters min_main_l (polar min_main_l (angle pointo poh1v) 100.)
		    hor1 hor2
	      nil)
	frh2 (inters min_main_l (polar min_main_l (angle pointo poh2v) 100.)
		    hor1 hor2
	      nil)
	frh3 (inters min_main_l (polar min_main_l (angle pointo poh3v) 100.)
		    hor1 hor2
	      nil)
	fr1 (inters min_main_l (polar min_main_l (angle pointo po12) 100.)
		    hor1 hor2
	      nil)
	fr2 (inters min_main_l (polar min_main_l (angle po12 po23) 100.)
		    hor1 hor2
	      nil)
	fr4 (inters min_main_l (polar min_main_l (angle pointo po45) 100.)
		    hor1 hor2
	      nil)
	fr5 (inters min_main_l (polar min_main_l (angle po45 po56) 100.)
		    hor1 hor2
	      nil)
	fr7 (inters min_main_l (polar min_main_l (angle pointo po78) 100.)
		    hor1 hor2
	      nil)
	fr8 (inters min_main_l (polar min_main_l (angle po78 po89) 100.)
		    hor1 hor2
	      nil)

	muomeg (/ 10. (distance fr1 polus_plus))
  )
  (drawspeed)
  (muu "v" muspeed "� c/��")
  (muu "\U+03C9" muomeg "c/��")
)

(defun drawspeed ()
  (li (list pointo po12 po23))
  (li (list pointo poh1v))
  (li (list po45 po56))
  (li (list po78 po89))
  (li (list poh2v pointo poh3v))  
  (risskor os2 poh1v "H1" nil)
  (risskor os5 poh2v "H2" nil)
  (risskor os8 poh3v "H3" nil)
  (risskor n12 po12 "1,2" nil)
  (risskor n45 po45 "4,5" nil)
  (risskor n78 po78 "7,8" nil)  
  (li (list max_main_l min_main_l))  
  (li (list min_main_l frh1))
  (li (list min_main_l frh2))
  (li (list min_main_l frh3))  
  (li (list min_main_l fr1))
  (li (list min_main_l fr2))
  (li (list min_main_l fr4))
  (li (list min_main_l fr5))
  (li (list min_main_l fr7))
  (li (list min_main_l fr8))
  (li (list (list (- (min (car frh1) (car frh2) (car frh3) (car fr1) (car fr2) (car fr4) (car fr7) (car fr8)) 7) (cadr fr2))
	    (list (+ (max (car frh1) (car frh2) (car frh3) (car fr1) (car fr2) (car fr4) (car fr7) (car fr8)) 7) (cadr fr2))
      )
  )
  (add_text (polar frH1 (* 109. (/ pi 180.)) 4.1) "H1,4")
  (add_text (polar frH2 (* 109. (/ pi 180.)) 4.1) "H2,7")
  (add_text (polar frH3 (* 109. (/ pi 180.)) 4.1) "H3")
  (add_text (polar fr1 (* 109. (/ pi 180.)) 4.1) "1")
  (add_text (polar fr2 (* 109. (/ pi 180.)) 4.1) "2")
  (add_text (polar fr5 (* 109. (/ pi 180.)) 4.1) "5")
  (add_text (polar fr8 (* 109. (/ pi 180.)) 4.1) "8")

  
  (add_text (getpoint "U1H3=") (strcat "\\A1;U\\H0.7x;\\S^1H3;\\H1.4286x;="
				      "n\\H0.7x;\\S^1;\\H1.4286x;/n\\H0.7x;\\S^H3;\\H1.4286x;=\\H0.7x;\\S01/0H3;\\H1.4286x;="
				      "\\H0.7x;\\S"(rtos (distance polus_plus fr1) 2 0)"/"
				      (rtos (distance polus_plus frh3) 2 0)";\\H1.4286x;="
				      (rtos (/ (distance polus_plus fr1) (distance polus_plus frh3)) 2 3)
				      
			      )
	    )
)


(defun c:red6 (/)
  (vl-load-com)
  (setq osm (getvar "OSMODE"))
  (setvar "OSMODE" 20919)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "TIMESI.TTF" )))
	le (append le (list (cons 2 "�����")))	
  )
  (entmakex le)
  (setvar "textstyle" "�����")
  (load "funkcii_tmm.lsp")
  (load "tmm_semi.lsp")
  (vvod)
  (redpoints)
  (redcher)
  (speeds)
 
); defun
(c:red6)