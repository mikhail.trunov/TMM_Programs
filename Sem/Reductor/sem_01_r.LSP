(defun vvod ()
  (setq z1 34.;(getreal "\nZ1: ")
	z1s 34.;(getreal "\nZ1s: ")
	z2 30.;(getreal "\nZ3: ")
	z2s 25.;(getreal "\nZ3s: ")
	z3 32.;(getreal "\nZ4: ")
	
	z4 30.;(getreal "\nZ5: ")
	z5 32.;(getreal "\nZ6: ")
	z6 18.;(getreal "\nZ6s: ")
	z7 28.;(getreal "\nZ7: ")
	z2s (- z3 z1 (* -1. z2))
	z4s (- z5 z1s (* -1. z4))
	
	dz1 (getdist (strcat "\nDlina Z1=("(rtos z1)") [mm]: "))
	mul (/ (* 0.01 z1) dz1)
	dz1s (* 0.01 z1s (/ 1. mul))
	dz2 (* 0.01 z2 (/ 1. mul))
	dz2s (* 0.01 z2s (/ 1. mul))
	dz3 (* 0.01 z3 (/ 1. mul))
	dz4 (* 0.01 z4 (/ 1. mul))
	dz4s (* 0.01 z4s (/ 1. mul))
	dz5 (* 0.01 z5 (/ 1. mul))
	dz5 (* 0.01 z5 (/ 1. mul))
	dz6 (* 0.01 z6 (/ 1. mul))
	
	dz7 (* 0.01 z7 (/ 1. mul))
	
;;;	u1h (1+ (/ z3 z1))
;;;	u4h (1+ (/ z6 z4))
;;;	u7h (1+ (/ z9 z7))
;;;	u12 (/ u1h (- 1. (/ z3 z2)))
;;;	
;;;	ured (* u1h u4h u7h)
  )
)


(defun redpoints ()
  (setq poiz1 (getpoint "\n��������: ")
	poiz2 (list (+ (car poiz1) 15) (+ (cadr poiz1) (* 0.5 (- dz1 dz2))))	
	poiz2s (polar poiz2 0. 12.)
	poiz3 (polar poiz1 0. 39.)
	poiz4 (list (+ (car poiz3) 10) (+ (cadr poiz3) (* 0.5 (- dz1s dz4))))
	poiz4s (polar poiz4 0. 12.)
	poiz5 (polar poiz3 0. 34.)
	poiz6 (polar poiz5 0. 15.)
	poiz7 (polar poiz6 (* 1.5 pi) (* 0.5 (+ dz6 dz7)))
  )
)

(defun redcher (/ temp)
  (redopora poiz1 0 7. 3.5 t)
  (redopora (polar poiz3 pi 5) 0 6. 3.5 t)
  (redopora (polar poiz6 pi 7.5) 0 7. 3.5 t)
  (redopora (polar poiz7 0 7.5) 0 7. 3.5 t)
;;;  (redopora (polar poiz4s 0 7.5) 0 7. 3.5 nil)
;;;  (redopora (polar poiz6s pi 6) 0 7. 3.5 t)
;;;  (redopora (polar poiz7 0 6) 0 7. 3.5 t)

  
  (zubch poiz2 -1 12 3.5 nil dz2)
  (zubch poiz2s 1 1 3.5 nil dz2s)
  (zubch poiz4 -1 12 3.5 nil dz4)
  (zubch poiz4s 1 1 3.5 nil dz4s)
  (zubch poiz6 0 0 nil nil dz6)
  (zubch poiz7 0 0 nil nil dz7)
   
  (zubchvn poiz3 0 0 nil nil dz3 6. nil)
  (zubchvn poiz5 0 0 nil nil dz5 6. nil)
  

;;;  (li (list (polar poiz4 pi 11) (polar poiz4s 0 11)))
  (li (list poiz5 poiz6))

  (li (list (polar poiz7 0 12) poiz7))
  (li (list (polar poiz3 pi 10) poiz3))
  (li (list (polar poiz4s pi 22) poiz4s))
;;;  (li (list (polar poiz2 pi 12) poiz2))
;;;  (li (list (polar poiz6s pi 13) poiz6s))
 
  (muul (* 0.001 z1) dz1)
  (add_text (getpoint "\nd1: ")
	    (strcat "\\A1;d\\H0.7x;\\S^1;\\H1.4286x;=z\\H0.7x;\\S^1;\\H1.4286x;*m="
		    (rtos z1) "*0.001 �=" (rtos (* z1 0.001))" �" 
	    )
  )
)

(defun speeds (/ dis_red max_speed pointo)
  (setq dis_red (getdist "\nDistance from Reductor: ")
	max_speed (getdist "\nMax speed: ")
	muspeed (/ (* 0.001 z1 0.5 10.) max_speed)
	pointo (polar poiz6 0. (+ dis_red 30))
	
	os2 (list (car pointo) (cadr poiz2))
	os4 (list (car pointo) (cadr poiz4))
	os7 (list (car pointo) (cadr poiz7))
	
	n12 (polar pointo (* 0.5 pi) (* 0.5 dz1))
	n2s3 (polar pointo (* 0.5 pi) (* 0.5 dz3))
	n1s4 (polar pointo (* 0.5 pi) (* 0.5 dz1s))
	n4s5 (polar pointo (* 0.5 pi) (* 0.5 dz5))
	n67 (polar pointo (* 1.5 pi) (* 0.5 dz6))
	
	
	poh (list (+ (car pointo) max_speed) (cadr os2))
	po2s3 (inters poh n12 n2s3 (polar n2s3 0 100.) nil)
	poh1 (inters po2s3 pointo os4 poiz4 nil)
	po4s5 (inters poh1 n1s4 n4s5 (polar n4s5 0 100.) nil)
	
	po67 (inters po4s5 pointo n67 (polar n67 0 100.) nil)

	max_main_l (list (car pointo) (+ 10 (cadr pointo) (* 0.5 dz1)))
	min_main_l (list (car pointo) (- (cadr pointo) (* 0.5 dz1) 60.))	
	;max_frequ
	angles (list
		 (angle pointo poh)
		 (angle pointo poh1)		 
		 (angle n12 poh)
		 (angle n1s4 poh1)
		 (angle n67 pointo)
		 (angle n67 os7)
	       )
	names (list "H" "3,H1" "2,2'" "4,4;" "5,6" "7")
	
  )
  (drawspeed)
  (muu "v" muspeed "� c/��")
)

(defun drawspeed ()
  (li (list max_main_l min_main_l))
  (li (list pointo poh n12 po2s3 pointo))
  (li (list poh1 n1s4 po4s5 po67 os7))
  
  
  (risskor os2 poh "H" nil)
  (risskor os4 poh1 "3,H1" nil)
  (risskor n2s3 po2s3 "2',3" nil)
  (risskor n4s5 po4s5 "4',5" nil)
 
  (risskor n67 po67 "6,7" nil)  
  (chastota min_main_l (getdist "\nFrequency oust: ") angles names) 
)


(defun c:red16 (/)
  (vl-load-com)
  (setq osm (getvar "OSMODE"))
  (setvar "OSMODE" 20919)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "TIMESI.TTF" )))
	le (append le (list (cons 2 "�����")))	
  )
  (entmakex le)
  (setvar "textstyle" "�����")
  (load (strcat tmm_folder "funkcii_tmm.lsp"))
  (load (strcat tmm_folder "tmm_semi.lsp"))
  (vvod)
  (redpoints)
  (redcher)
  (speeds)
 
); defun
(c:red16)