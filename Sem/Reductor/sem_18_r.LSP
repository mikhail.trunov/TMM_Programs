(defun vvod ()
  (setq z1 (getreal "\nZ1: ")
	z2 (getreal "\nZ2: ")
	z2s (getreal "\nZ2s: ")
	z3 (getreal "\nZ3: ")
	z3s (getreal "\nZ3s: ")
	z3ss (getreal "\nZ3ss: ")
	z4 (getreal "\nZ4: ")	
	z5 (getreal "\nZ5: ")
	z6 (getreal "\nZ6: ")
	z7 (getreal "\nZ7: ")
	dz1 (getdist (strcat "\nDlina Z1=("(rtos z1)") [mm]: "))
	mul (/ (* 0.01 z1) dz1)
	dz2 (* 0.01 z2 (/ 1. mul))
	dz2s (* 0.01 z2s (/ 1. mul))
	dz3 (* 0.01 z3 (/ 1. mul))
	dz3s (* 0.01 z3s (/ 1. mul))
	dz3ss (* 0.01 z3ss (/ 1. mul))
	dz4 (* 0.01 z4 (/ 1. mul))
	dz5 (* 0.01 z5 (/ 1. mul))
	dz6 (* 0.01 z6 (/ 1. mul))
	dz7 (* 0.01 z7 (/ 1. mul))
  )
)


(defun redpoints ()
  (setq poiz1 (getpoint "\n��������: ")	
	poiz2 (polar poiz1 (* 1.5 pi) (* 0.5 0.01 (/ 1. mul) (+ z1 z2)))
	poiz2s (polar poiz2 0 22.)	
	poiz3 (polar poiz2s (* 0.5 pi) (* 0.5 0.01 (/ 1. mul) (+ z2s z3)))
	poiz3s (polar poiz3 0 10.)
	poiz3ss (polar poiz3s 0 10.)
	poiz4 (polar poiz2 0 10.)
	poiz5 (polar poiz2s 0 10.)	
	poiz6 (polar poiz5 0 10.)
	poiz7 (polar poiz2 (* 1.5 pi) (* 0.5 0.01 (/ 1. mul) (+ z7 z2)))
  )
)

(defun redcher (/ a)
  (zubch poiz1 0 0 nil nil dz1)
  (zubch poiz2 0 0 nil nil dz2)
  (zubch poiz2s 0 0 nil nil dz2s)
  (zubch poiz3 0 0 nil nil dz3)
  (zubch poiz3s 0 0 nil nil dz3s)
  (zubch poiz3ss 0 0 nil nil dz3ss)
  (zubchvn poiz4 1 7 3.5 t dz4 6. t)  
  (zubch poiz5 0 0 nil nil dz5)
  (zubch poiz6 -1 10 3.5 nil dz6)
  (zubch poiz7 0 0 nil nil dz7)
  
  (li (list (polar poiz1 pi 9) poiz1))
  (li (list (polar poiz7 pi 9) poiz7))
  (li (list poiz2 poiz2s))
  (li (list (polar poiz3 pi 6) poiz3ss))
  (li (list poiz5 (polar poiz5 0 20.)))  
  (redopora (polar poiz1 pi 2) 1 7. 3.5 t)
  (redopora (polar poiz7 pi 2) 1 7. 3.5 t)
  (redopora (polar poiz4 0. 5) 0 7. 3.5 nil)
  (redopora (polar poiz3 pi 5) 0 7. 3.5 nil)
  (redopora (polar poiz6 0 2.5) -1 7. 6.5 t)

  (muu "l" mul "�/��")
)

(defun speeds (/ dis_red max_speed)
  (setq dis_red (getdist "\nDistance from Reductor: ")
	max_speed (getdist "\nMax speed: ")
	muspeed (/ (* 0.01 z1 0.5 10.) max_speed)
	pointo (polar poiz6 0. (+ dis_red 20))
	n12 (list (car pointo) (+ (cadr poiz2) (* 0.5 0.01 (/ 1. mul) z2)))
	n27 (list (car pointo) (- (cadr poiz2) (* 0.5 0.01 (/ 1. mul) z2)))
	n2s3 (list (car pointo) (+ (cadr poiz2) (* 0.5 0.01 (/ 1. mul) z2s)))	
	n34 (list (car pointo) (+ (cadr poiz3) (* 0.5 0.01 (/ 1. mul) z3)))
	n3s5 (list (car pointo) (- (cadr poiz3s) (* 0.5 0.01 (/ 1. mul) z3s)))
	n3ss6 (list (car pointo) (- (cadr poiz3ss) (* 0.5 0.01 (/ 1. mul) z3ss)))
	
	os1 (list (car pointo) (cadr poiz1))
	os3 (list (car pointo) (cadr poiz3))
	os7 (list (car pointo) (cadr poiz7))
	
	po12 (polar n12 0. max_speed)
	po2s3 (inters pointo po12 n2s3 (polar n2s3 0. 100) nil)
	poh (inters po2s3 n34 poiz3 os3 nil)
	po3s5 (inters n34 po2s3 n3s5 (polar n3s5 0. 100) nil)
	po3ss6 (inters n34 po2s3 n3ss6 (polar n3ss6 0. 100) nil)
	po27 (inters pointo po12 n27 (polar n27 0. 100) nil)
	
	max_main_l (list (car pointo) (+ 7 (cadr poiz3) (* 0.5 0.01 (/ 1. mul) (max z3 z3s z3ss)) 10))
	min_main_l (list (car pointo) (- (cadr pointo) (* 0.5 0.01 (/ 1. mul) z4) 60.))
	max_frequ (getdist "\nFrequency oust: ")
	polus_plus (polar min_main_l (* 0.5 pi) max_frequ)
	hor1 (polar polus_plus pi 150)
	hor2 (polar polus_plus 0. 150)
	frh (inters min_main_l (polar min_main_l (angle pointo poh) 100.)
		    hor1 hor2
	      nil)
	fr1 (inters min_main_l (polar min_main_l (angle os1 po12) 100.)
		    hor1 hor2
	      nil)
	fr2 (inters min_main_l (polar min_main_l (angle pointo po12) 100.)
		    hor1 hor2
	      nil)
	fr7 (inters min_main_l (polar min_main_l (angle os7 po27) 100.)
		    hor1 hor2
	      nil)
	fr3 (inters min_main_l (polar min_main_l (angle n34 po2s3) 100.)
		    hor1 hor2
	      nil)
	fr5 (inters min_main_l (polar min_main_l (angle pointo po3s5) 100.)
		    hor1 hor2
	      nil)
	fr6 (inters min_main_l (polar min_main_l (angle pointo po3ss6) 100.)
		    hor1 hor2
	      nil)
	muomeg (/ 10. (distance fr1 polus_plus))
  )  
)

(defun drawspeed ()
  (li (list poiz1 os1 po12))
  (li (list po2s3 po27 os7 poiz7))
  (li (list n34 po3s5 po3ss6))
  (li (list os3 poiz3ss))
  (li (list pointo poh))
  (li (list poiz2s pointo po3s5))
  (li (list pointo po3ss6))
  (li (list max_main_l min_main_l))
  (risskor n12 po12 "1,2" nil)  
  (risskor n27 po27 "2,7" nil)

  (risskor os3 poh "H" nil) 
  (risskor n2s3 po2s3 "2',3" nil)
  (risskor n3s5 po3s5 "3',5" nil)
  (risskor n3ss6 po3ss6 "3'',6" nil)
  (li (list min_main_l frh))
  (li (list min_main_l fr1))
  (li (list min_main_l fr2))
  (li (list min_main_l fr3))  
  (li (list min_main_l fr5))
  (li (list min_main_l fr6))
  (li (list min_main_l fr7))
  (li (list (list (- (min (car frh) (car fr1) (car fr2) (car fr3) (car fr7) (car fr5) (car fr6)) 7) (cadr fr1))
	    (list (+ (max (car frh) (car fr1) (car fr2) (car fr3) (car fr7) (car fr5) (car fr6)) 7) (cadr fr2))
      )
  )
  (add_text (polar frH (* 109. (/ pi 180.)) 4.1) "H")
  (add_text (polar fr1 (* 109. (/ pi 180.)) 4.1) "1")
  (add_text (polar fr2 (* 109. (/ pi 180.)) 4.1) "2,2'")
  (add_text (polar fr3 (* 144. (/ pi 180.)) 6.1) "3,3',3''")
  (add_text (polar fr7 (* 109. (/ pi 180.)) 4.1) "7")
  (add_text (polar fr5 (* 109. (/ pi 180.)) 4.1) "5")
  (add_text (polar fr6 (* 109. (/ pi 180.)) 4.1) "6")
  
  (add_text (getpoint "U15=") (strcat "\\A1;U\\H0.7x;\\S^15;\\H1.4286x;="
				      "n\\H0.7x;\\S^1;\\H1.4286x;/n\\H0.7x;\\S^5;\\H1.4286x;=\\H0.7x;\\S01/05;\\H1.4286x;="
				      "\\H0.7x;\\S"(rtos (distance polus_plus fr1) 2 0)"/"
				      (rtos (distance polus_plus fr5) 2 0)";\\H1.4286x;="
				      (rtos (/ (distance polus_plus fr1) (distance polus_plus fr5)) 2 3)
				      
			      )
	    )
  (add_text (getpoint "U76=") (strcat "\\A1;U\\H0.7x;\\S^76;\\H1.4286x;="
				      "n\\H0.7x;\\S^7;\\H1.4286x;/n\\H0.7x;\\S^6;\\H1.4286x;=\\H0.7x;\\S07/06;\\H1.4286x;="
				      "\\H0.7x;\\S"(rtos (distance polus_plus fr7) 2 0)"/"
				      (rtos (distance polus_plus fr6) 2 0)";\\H1.4286x;="
				      (rtos (/ (distance polus_plus fr7) (distance polus_plus fr6)) 2 3)
				      
			      )
	    )
  (muu "v" muspeed "� c/��")
  (muu "\U+03C9" muomeg "c/��")
)


(defun c:red18 (/)
  (vl-load-com)
  (setq osm (getvar "OSMODE"))
  (setvar "OSMODE" 20919)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "TIMESI.TTF" )))
	le (append le (list (cons 2 "�����")))	
  )
  (entmakex le)
  (setvar "textstyle" "�����")
  (load "funkcii_tmm.lsp")
  (load "tmm_semi.lsp")
  (vvod)
  (redpoints)
  (redcher)
  (speeds)
  (drawspeed) 
); defun
(c:red18)