(defun vvod ()
  (setq z1 36.;(getreal "\nZ1: ")
	
	z2s 37. ;(getreal "\nZ2s: ")
	z3 120.;(getreal "\nZ3: ")
	z4 115.;(getreal "\nZ4: ")
	z4s 65.;(getreal "\nZ4s: ")
	
	z6 151.;(getreal "\nZ6: ")
	z6s 30.;(getreal "\nZ6s: ")
	z7 57.;(getreal "\nZ7: ")
	
	z2 (* 0.5 (- z3 z1))
	z5 (* 0.5 (- z6 z4s))
	
	dz1 (getdist (strcat "\nDlina Z1=("(rtos z1)") [mm]: "))
	mul (/ (* 0.01 z1) dz1)
	dz2 (* 0.01 z2 (/ 1. mul))
	dz2s (* 0.01 z2s (/ 1. mul))
	dz3 (* 0.01 z3 (/ 1. mul))
	dz4 (* 0.01 z4 (/ 1. mul))
	dz4s (* 0.01 z4s (/ 1. mul))
	dz5 (* 0.01 z5 (/ 1. mul))	
	dz6 (* 0.01 z6 (/ 1. mul))
	dz6s (* 0.01 z6s (/ 1. mul))
	dz7 (* 0.01 z7 (/ 1. mul))
	
	
;;;	u1h (1+ (/ z3 z1))
;;;	u4h (1+ (/ z6 z4))
;;;	u7h (1+ (/ z9 z7))
;;;	u12 (/ u1h (- 1. (/ z3 z2)))
;;;	
;;;	ured (* u1h u4h u7h)
  )
)


(defun redpoints ()
  (setq poiz1 (getpoint "\n��������: ")
	poiz2 (polar poiz1 (* 0.5 pi) (* 0.5 (+ dz2 dz1)))
	poiz3 (polar poiz1 pi 10.)
	poiz2s (polar poiz2 0 12.)
	poiz4 (polar poiz1 0 22.)
	poiz4s (polar poiz4 0 12.)
	poiz5 (polar poiz4s (* 1.5 pi) (* 0.5 (+ dz4s dz5)))
	poiz6 (polar poiz4s 0 10.)
	poiz6s (polar poiz6 0 12.)
	poiz7 (polar poiz6s (* 1.5 pi) (* 0.5 (+ dz6s dz7)))
  )
)

(defun redcher (/ temp)
  (redopora (polar poiz6 0 6) 0 7. 3.5 t)
  (redopora (polar poiz7 pi 6) 0 7. 3.5 t)
  (redopora (polar poiz5 pi 6) 0 7. 3.5 t)
  (redopora (polar poiz4s pi 6) 0 7. 3.5 t)
  (redopora (polar poiz1 0 6) 0 7. 3.5 nil)
  (redopora (polar poiz2 0 6) 0 7. 3.5 nil)  
  (zubch poiz1 0 0 nil nil dz1)
  (zubch poiz2 0 0 nil nil dz2)
  (zubch poiz2s 0 0 nil nil dz2s)
  (zubch poiz4s 0 0 nil nil dz4s)
  (zubch poiz5 0 0 nil nil dz5)
  (zubch poiz6s 0 0 nil nil dz6s)
  (zubch poiz7 0 0 nil nil dz7) 

  (zubchvn poiz3 1 7 3.5 t dz3 5 t)
  (zubchvn poiz4 0 0 nil nil dz4 5 nil)
  (zubchvn poiz6 0 0 nil nil dz6 5 nil)   
 

  (li (list (polar poiz1 pi 21) poiz1))
  (li (list (polar poiz7 pi 10) poiz7))
  (li (list (polar poiz5 pi 10) poiz5))
  (li (list poiz2 poiz2s))
  (li (list poiz4 poiz4s))
  (li (list poiz6 poiz6s))
  
 
  (muul (* 0.001 z1) dz1)
  (add_text (getpoint "\nd1: ")
	    (strcat "\\A1;d\\H0.7x;\\S^1;\\H1.4286x;=z\\H0.7x;\\S^1;\\H1.4286x;*m="
		    (rtos z1) "*0.001 �=" (rtos (* z1 0.001))" �" 
	    )
  )
)

(defun speeds (/ dis_red max_speed pointo)
  (setq dis_red (getdist "\nDistance from Reductor: ")
	max_speed (getdist "\nMax speed: ")
	muspeed (/ (* 0.001 z1 0.5 10.) max_speed)
	pointo (polar poiz6s 0. (+ dis_red 30))

	os2 (list (car pointo) (cadr poiz2))
	os5 (list (car pointo) (cadr poiz5))
	os7 (list (car pointo) (cadr poiz7))
		
	n12 (polar pointo (* 0.5 pi) (* 0.5 dz1))
	n23 (polar pointo (* 0.5 pi) (* 0.5 dz3))
	n2s4 (polar pointo (* 0.5 pi) (* 0.5 dz4))
	n4s5 (polar pointo (* 1.5 pi) (* 0.5 dz4s))	
	n56 (polar pointo (* 1.5 pi) (* 0.5 dz6))
	n6s7 (polar pointo (* 1.5 pi) (* 0.5 dz6s))
	
	po12 (list (+ (car pointo) max_speed) (cadr n12))
	poh (inters po12 n23 poiz2 poiz2s nil)
	po2s4 (inters n23 po12 n2s4 (polar n2s4 0 100.) nil)
	po4s5 (inters pointo po2s4 n4s5 (polar n4s5 0 100.) nil)
	po56 (inters os5 po4s5 n56 (polar n56 0 100.) nil)
	po6s7 (inters po56 pointo n6s7 (polar n6s7 0 100.) nil)	
	
		
	max_main_l (list
		     (car pointo)
		     (+
		       7
		       (cadr pointo)
		       (* 0.5 (max
			 dz3 dz4 dz6
		       ))
		     ); +
		    )
	min_main_l (list (car pointo) (- (cadr pointo) (* 0.5 (min dz3 dz4 dz6)) 50.))	
	;max_frequ
	angles (list
		 (angle pointo po12)
		 (angle po12 n23)		 
		 (angle poh pointo)
		 (angle pointo po4s5)
		 (angle os5 po4s5)
		 (angle pointo po56)
		 (angle os7 po6s7)
	       )
	names (list "1" "2,2'" "H" "4,4'" "5" "6,6'" "7")
	
  )
  (drawspeed)
  (muu "v" muspeed "� c/��")
)

(defun drawspeed ()
  (li (list max_main_l min_main_l))
  (li (list pointo po12 n23 po2s4 po4s5 po56 po6s7 os7))
  (li (list pointo poh))  
  
  (risskor os2 poh "H" nil)
  (risskor n12 po12 "1,2" nil)
  (risskor n2s4 po2s4 "2',4" nil)
  (risskor n4s5 po4s5 "4',5" nil)
  (risskor n56 po56 "5,6" nil)
  (risskor n6s7 po6s7 "6',7" nil)  
  (chastota min_main_l (getdist "\nFrequency oust: ") angles names) 
)


(defun c:red15 (/)
  (vl-load-com)
  (setq osm (getvar "OSMODE"))
  (setvar "OSMODE" 20919)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "TIMESI.TTF" )))
	le (append le (list (cons 2 "�����")))	
  )
  (entmakex le)
  (setvar "textstyle" "�����")
  (load (strcat tmm_folder "funkcii_tmm.lsp"))
  (load (strcat tmm_folder "tmm_semi.lsp"))
  (vvod)
  (redpoints)
  (redcher)
  (speeds)
 
); defun
(c:red15)