(defun main_funkc (/ ugolok pol ugol0
		   polus_mexan polus_skor polus_uskor vektorva vektoraa
  

		   )
  (initget 65)
  (setq	polus_mexan (getpoint "\n������� ����� O ����� ���������: ")
	ugol0 (+ pi (asin (/ oa ob)))
	ugolok (- ugol0 fi)
  ); setq
  (tochkim polus_mexan ugol0)
  (cherm polus_mexan)
  (tochkim polus_mexan ugolok)
  (cherm polus_mexan)
  (add_opor polus_mexan pi)
  (add_opor b pi)
  (add_circ 0.7 b)(add_circ 0.7 polus_mexan)
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���������: ") mul "l" "�/��")
  (setq vektorva (getreal "\n������� ����� ������� �������� ����� A: ")
	muv (/ va vektorva)  
  )
  (plansk (setq pol (getpoint "\n������� ��� ������� ���� ���������: ")) vektorva ugolok)
  (cherv pol)
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���������: ") muv "\U+03C5" "� �/��")
  (setq vektoraa (getreal "\n������� ����� ������� ��������� ����� A [mm]: ")
	mua (/ aan vektoraa)
  )
  (initget 65)
  (setq	polus_uskor (getpoint "\n������� ��� ������� ���� ���������: "))
  (planusk polus_uskor vektoraa ugolok)
  (chera polus_uskor)
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���������: ") mua "a" "� �/��")
  (sili ugolok)  
  (grup45 (getpoint "\nGruppa 45: ") ugolok)
  (grup23 (getpoint "\nGruppa 23: ") ugolok)
  (grup01 (getpoint "\nGruppa 01: ") ugolok)
  (guk (getpoint "\nGuk: ") ugolok vektorva)
  (alert (strcat "����������� �����\n"
		 (rtos (* (/ (- (max (abs fuu) (abs fur)) (min (abs fuu) (abs fur))) (max (abs fuu) (abs fur))) 100))
		 "\n"(rtos fuu)
		 "\n"(rtos fur)
	 );_end of strcat
  );_end of alert
  (initget 1 "Yes No")
  (if (equal "Yes" (getkword "\n������� � excel? [Yes/No]: "))
    (excel_export ugolok)
  )
)

(defun sili (ugol /
b a c d s3 s5 left5 righ5

av12 av3 cv dv s3v mva3 mvc mvd mva3a2 mvdc mvs3 omeg3 omeg4 aa3bn adcn aa3a2k

daa3bn ddcn daa3a2k aa12 aa3n aa3k aa3 ac adn ad as3 ma3 ma3a2r ma3bt mac mad mas3 madct eps3 eps4)
  (planusk '(0 0) 100 ugol)
  (setq sils (if (zerop (angle '(0 0) dv))
	       sils
	       0
	     )
	g3 (* 9.8 m3)
	g5 (* 9.8 m5)	
	msf3 (* eps3 is3)	
	sf3 (* m3 mas3)
	sf5 (* m5 mad)
	h3 (/ msf3 sf3)
	mh3 (/ h3 mul)
  );_end of setq
);_end of defun

(defun excel_export (ugol / g_oex g_wkbs g_wb_put g_shs_put g_sheet1 g_sheet2 g_sheet3
		     fii i polus_skor)
  (setq g_oex (vlax-get-or-create-object "Excel.Application"))
  (vlax-put-property g_oex "Visible" :vlax-false)  
  (setq g_wkbs (vlax-get-property g_oex "Workbooks")        
        g_wb_put (vla-open g_wkbs (strcat total_adress "blanks\\first_list_5d.xls"))        
        g_shs_put (vlax-get-property g_wb_put "Worksheets")
	g_sheet1 (vlax-get-property g_shs_put "Item" 1)
	g_sheet2 (vlax-get-property g_shs_put "Item" 2)
	g_sheet3 (vlax-get-property g_shs_put "Item" 3)
	polus_skor '(0 0)
	polus_uskor polus_skor
  )
  (ex_put loa 2 "B" g_sheet1)
  (ex_put oa 3 "B" g_sheet1)
  (ex_put n1 4 "B" g_sheet1)
  (ex_put (* fi (/ 180. pi)) 8 "B" g_sheet1)
  (ex_put lob 2 "C" g_sheet1)
  (ex_put lbc 2 "D" g_sheet1)
  (ex_put lcd 2 "E" g_sheet1)
  (ex_put la 2 "G" g_sheet1)
  (ex_put lb 2 "H" g_sheet1)
  (ex_put ly1 2 "I" g_sheet1)
  (ex_put ly2 2 "J" g_sheet1)
  (ex_put muv 3 "O" g_sheet2)
  (plansk polus_skor vektorva ugol)
    (ex_put (distance polus_skor av3) 3 "C" g_sheet2)
    (ex_put (distance av12 av3) 4 "C" g_sheet2)
    (ex_put (distance polus_skor cv) 5 "C" g_sheet2)
    (ex_put (distance polus_skor s4v) 6 "C" g_sheet2)
    (ex_put (distance polus_skor dv) 7 "C" g_sheet2)
    (ex_put (distance cv dv) 8 "C" g_sheet2)   
    
  (planusk polus_uskor vektoraa ugol)
  (ex_put (distance a b) 5 "O" g_sheet2)
  (ex_put mua 20 "R" g_sheet2)
  
  (ex_put aa3bn 23 "B" g_sheet2)
  (ex_put ma3bt 23 "C" g_sheet2)
  (ex_put aa3a2k 23 "D" g_sheet2)
  (ex_put ma3a2r 23 "E" g_sheet2)
  (ex_put adcn 23 "F" g_sheet2)
  (ex_put madct 23 "G" g_sheet2)
  
  (ex_put ma3 20 "I" g_sheet2)
  (ex_put mad 20 "J" g_sheet2)
  (ex_put mas4 20 "K" g_sheet2)
  (ex_put mac 20 "L" g_sheet2)

  (ex_put m3 3 "F" g_sheet3)
  (ex_put m4 3 "G" g_sheet3)
  (ex_put m5 3 "H" g_sheet3)
  (ex_put i4 5 "G" g_sheet3)
  (ex_put i3 5 "F" g_sheet3)
  (ex_put sils 7 "F" g_sheet3)

  (ex_put r05 11 "C" g_sheet3)
  (ex_put (abs r34t) 11 "D" g_sheet3)
  (ex_put r34 11 "B" g_sheet3)
  (ex_put muf45 13 "C" g_sheet3)
  (ex_put muf23 13 "I" g_sheet3)
  (ex_put muf01 13 "K" g_sheet3)
  (ex_put r12 11 "I" g_sheet3)
  (ex_put r03 11 "H" g_sheet3)
  (ex_put fuu 11 "K" g_sheet3)
  (ex_put r01 11 "J" g_sheet3)
  (vlax-invoke-method g_wb_put "SaveCopyAs" (getfiled "������� ��� ��������� ���� � ��������" "c:\\" "xls" 129))
  (vlax-invoke-method g_wb_put "Close" :vlax-false :vlax-false)
  (vlax-invoke-method g_oex "Quit")
  (mapcar
    (function (lambda (x)
      (if
        (and x (not (vlax-object-released-p x)))
        (vlax-release-object x)
      );_ if
              );_ lambda
    );_ function
    (list g_shs_put
	  g_wb_put
	  g_wkbs g_sheet1 g_sheet2 g_sheet3
	  g_oex)
  )  
  (gc) 
)

(defun guk (polus ugol dlina_v / wib polus uggg
	   qav12 qav3 qcv qdv qs3v qs3shv
	   rimom)
  ;������ ����� ������
  (setq wib (getint "\n���� ������� ������������ ������? �� ����� [1] ������ [2] "))
  (if (= wib 1)
      (setq uggg (/ pi 2.))
      (setq uggg (/ pi -2.))
  )
  (planusk polus 80 ugol)
  (plansk polus dlina_v ugol)
  (setq qav12 (polar polus (+ (angle polus av12) uggg) (distance polus av12))
	qav3 (polar polus (+ (angle polus av3) uggg) (distance polus av3))
	qcv (polar polus (+ (angle polus cv) uggg) (distance polus cv))	   
	qdv (polar polus (+ (angle polus dv) uggg) (distance polus dv))	    
	qs3v (polar polus (+ (angle polus s3v) uggg) (distance polus s3v))	    
	qs3shv (polar polus (angle polus qcv) (* (distance polus qcv) (/ dlia_guka bc)))    

  )
  ;���������� ������ ����������
  
  (li (list polus qav12 qav3 polus qcv qdv polus qs3v))
  (add_text qav12 "\\A1;\\H2.2;a\\H0.7x;\\S^1;\\H2.2;a\\H0.7x;\\S^2;")
  (add_text qcv "c")
  (add_text qdv "d")
  (add_text qav3 "\\A1;\\H2.2;a\\H0.7x;\\S^3;")  
  (add_text qs3v "\\A1;\\H2.2;s\\H0.7x;\\S^3;")
  (add_text polus "p,o,b")
  ;������� ����� ��� �� ������
  (setq g3ver  (polar polus (* -0.5 pi) 20)
	g5ver  (polar qdv (* -0.5 pi) 20)
	;g4ver  (polar qs4v (* -0.5 pi) 20)
	sf3ver (polar qs3shv (angle as3 polus) 20)	
	sf5ver (polar qdv (angle ad polus) 20)
	sil_ver (polar qdv pi 20)
  );_end of setq
  ;-----====�������� ���, ����������� �� ��������=====-----
  (risskor polus g3ver  nil  "G^3")
  (risskor qs3v g3ver  nil  "G^4")
  (risskor qdv  g5ver  nil  "G^5")  
  (risskor qs3shv sf3ver nil  "�'^3")
  (risskor qdv  sf5ver nil  "�^5")
  (risskor qdv  sil_ver nil  "F^C")
  ;------==========������� �������� �� ������============--------
  (setq momg5 (moment g5 qdv  g5ver polus)
	momg3 (moment g3 qs3v g3ver polus)
	momsf3 (moment sf3 qs3shv sf3ver polus)
	momsf5 (moment sf5 qdv sf5ver polus)
	momsils (moment sils qdv sil_ver polus)
  );_end of setq
  ;-----=============������� ���������������� ����========--------
  (setq fur (/ (+ momg5 momg3 momsf3 momsf5 momsils
		   )
	       (distance polus qav12)
	    );_end of /
  );_end of setq
   (if (< fur 0)
    (risskor qav12 (polar qav12 (+ (angle qav12 polus) (/ pi 2)) 20) nil "F^y")
    (risskor qav12 (polar qav12 (- (angle qav12 polus) (/ pi 2)) 20) nil "F^y")
  );_end of if
  (add_text (getpoint "\nFy: ")
	    (strcat "\\A1;F\\H0.7x;\\S^Y;\\H1.4286x;=("
		    (if (minusp momg5)
		      "-"
		      ""
		    )
		    "G\\H0.7x;\\S^5;\\H1.4286x;*"
		    (rtos (/ (abs momg5) g5) 2 1)
		    (if (minusp momg3)
		      "-"
		      "+"
		    )
		    "G\\H0.7x;\\S^3;\\H1.4286x;*"
		    (rtos (/ (abs momg3) g3) 2 1)
		    (if (minusp momsf3)
		      "-"
		      "+"
		    )
		    "�\\H0.7x;\\S^4;\\H1.4286x;*"
		    (rtos (/ (abs momsf3) sf3) 2 1)
		    (if (minusp momsf5)
		      "-"
		      "+"
		    )
		    "�\\H0.7x;\\S^5;\\H1.4286x;*"
		    (rtos (/ (abs momsf5) sf5) 2 1)
		    (if (minusp momsils)
		      "-"
		      "+"
		    )
		    "F\\H0.7x;\\S^C;\\H1.4286x;*"
		    (rtos (/ (abs momsils) sils) 2 1)
		    ")/pa=("
		    (rtos momg5 2 2)
		    (if (minusp momg3)
		      ""
		      "+"
		    )
		    (rtos momg3 2 2)
		    (if (minusp momsf3)
		      ""
		      "+"
		    )
		    (rtos momsf3 2 2)
		    (if (minusp momsf5)
		      ""
		      "+"
		    )
		    (rtos momsf5 2 2)
		    (if (minusp momsils)
		      ""
		      "+"
		    )
		    (rtos momsils 2 2)
		    ")/"(rtos (distance polus qav12) 2 0)"="(rtos fur 2 2)" H"
	    )
  )
);end of defun guk





(defun c:din2 (/ oldos oldt

loa n1 mul lob ob lbc bc lcd cd lbs3 bs3 lds5 ds5 lh1 h1 lh2 h2 ll1 l1 ll2 l2 f fi omeg1 va aan

m3 m5 is3 sils

b a c d s3 s5 left5 righ5

av12 av3 cv dv s3v mva3 mvc mvd mva3a2 mvdc mvs3 omeg3 omeg4 aa3bn adcn aa3a2k

daa3bn ddcn daa3a2k aa12 aa3n aa3k aa3 ac adn ad as3 ma3 ma3a2r ma3bt mac mad mas3 madct eps3 eps4
	       )
  (vl-load-com)
  (load "tmm\\mex_2\\for_2.LSP")
  (load "funkcii_tmm.LSP")
  (load "tmm_semi.lsp")
  (load "tmm\\mex_2\\for_sili2.LSP")
 
  (setq oldos (getvar "OSMODE"))
  (setvar "OSMODE" 20903)
  (setq oldt (entget (tblobjname "style" "standard"))
	oldt (vl-remove (assoc 3 oldt) oldt)
	oldt (append oldt (list (cons 3 "TIMESI.TTF" )))
  );_end of setq
  (entmod oldt)
  (vvod)
  (main_funkc)
  (setvar "OSMODE" oldos)
  (vla-ZoomExtents (vlax-get-acad-object))
 
);_end of defun

(c:din2)