(defun grup45 (polus ugol / r05_niz r05_ver
	       r34n_ver r34t_ver
	       g5_ver sf5_ver
	       g4_ver sf4_ver sf4sh_n sf4sh_ver

b a c d s4 op_up5 op_down5 up5 down5 fs_ug fs_kon

av12 av3 cv dv s4v mvc mva3 mva3a2 mvdc omeg3

mua aa3bn adcn aa3a2k daa3bn ddcn daa3a2k aa12 aa3n
aa3k aa3 ac adn ad as4 ma3 ma3a2r ma3bt mac mad mas4 madct eps3 eps4
       
	       )
  (planusk polus 100 ugol)
  (setq ;����� ��� �������� ������ 4-5
	r05_niz (polar op_down5 pi 20)
	r05_ver (polar op_up5 pi 20)
	r34n_ver (polar c (angle d c) 20)
	g5_ver (polar down5 (/ pi -2) 20)
	sf5_ver (polar down5 (angle ad polus) 20)
	g4_ver (polar s4 (/ pi -2) 20)
	sf4_ver (polar s4 (angle as4 polus) 20)
	sf4sh_n (if (clockwise-p c d (polar d (angle adn ad) 20));���� ��������� ��������� ������ �����
		  (polar s4 (+ (angle s4 sf4_ver) (/ pi 2)) mh4)
		  (polar s4 (- (angle s4 sf4_ver) (/ pi 2)) mh4)
		);_end of if
	sf4sh_ver (polar sf4sh_n (angle as4 polus) 20)
	dlia_guka (distance d (inters d c sf4sh_n sf4sh_ver nil))
	r34t (/ (+ (moment g4 s4 g4_ver d)
		   (moment sf4 sf4sh_n sf4sh_ver d)
	        ); +
		cd
	     ); /
	r34t_ver (if (minusp r34t)
		   (polar c (+ (* 0.5 pi) (angle d c)) 20)
		   (polar c (- (angle d c) (* 0.5 pi)) 20)
		 ); if
  );_end of setq
;;;  (ex_put (moment g4 s4 g4_ver d) 11 "B" 1)
;;;  (ex_put (moment sf4 sf4sh_n sf4sh_ver d) 11 "C" 1)
  (chergrup45)  
);_end of defun

(defun chergrup45 (/ )  
  (li (list c d (polar d pi dla)))
  (li (list up5 down5 fs_ug fs_kon))
  (duga s4 c sf4sh_ver sf4sh_n 0.4 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^4;\\H2;")
  (duga s4 c sf4sh_n sf4sh_ver 0.8 "\\A1;\\H2;M\\H0.7x;\\S^�4;\\H2;")
  (mapcar '(lambda (x) (add_circ 0.7 x)) (list c s4 d))
  (add_text c "C")(add_text d "D")(add_text s4 "\\A1;S\\H0.7x;\\S^4;\\H1.42857x;")
  (risskor r05_niz op_down5 nil "RI^05")
  (risskor op_up5 r05_ver nil "RII^05")
  (risskor down5 g5_ver nil "G^5")
  (risskor s4 g4_ver nil "G^4")
  (risskor c r34n_ver nil "Rn^34")
  (risskor down5 sf5_ver nil "�^5")
  (risskor s4 sf4_ver nil "�^4")
  (risskor sf4sh_n sf4sh_ver nil "�'^4")  
  (risskor c r34t_ver nil "R\U+03C4^34")
  (if (not (zerop sils))
    (risskor (polar fs_kon (* -0.5 pi) 20) fs_kon nil "F^C")
  ); if
  (add_text (getpoint "\nR34t: ")
	    (strcat "\\A1;R\\H0.7x;\\S\U+03C4^34;\\H1.4286x;=("
		    (if (minusp (moment g4 s4 g4_ver d))
		      "-"
		      ""
		    )
		    "G\\H0.7x;\\S^4;\\H1.4286x;*"
		    (rtos (/ (abs (moment g4 s4 g4_ver d)) g4) 2 1)
		    (if (minusp (moment sf4 sf4sh_n sf4sh_ver d))
		      "-"
		      "+"
		    )
		    "�\\H0.7x;\\S^4;\\H1.4286x;*"
		    (rtos (/ (abs (moment sf4 sf4sh_n sf4sh_ver d)) sf4) 2 1)
		    ")/CD=("
		    (rtos (moment g4 s4 g4_ver d) 2 2)
		    (if (minusp (moment sf4 sf4sh_n sf4sh_ver d))
		      ""
		      "+"
		    )
		    (rtos (moment sf4 sf4sh_n sf4sh_ver d) 2 2)
		    ")/"(rtos cd 2 0)"="(rtos r34t 2 2)" H"
	    )
  )
  (plan45)
);_end of defun

(defun plan45 (/ as45 bs45 cs45 ds45 es45 fs45 gs45 hs45
	       ;r05i r05ii
	       )
  (setq muf45 (/ (max sils g5 sf5 g4 sf4 (abs r34t))
		 (getreal "\n������� ���������� ���� �� ����� [��] ")
	      )
	as45 (getpoint "\nPlansil45")
  );_end of setq
  (if (zerop sils)
    ;G5+�5+�'4+G4+R34t+R34n+R05=0
    (progn
    (setq bs45 (polar as45 (/ pi -2) (/ g5 muf45))       	    ;G5
	  cs45 (polar bs45 (angle ad polus) (/ sf5 muf45))	    ;�5
	  ds45 (polar cs45 (angle s4 sf4_ver) (/ sf4 muf45));�'4
	  es45 (polar ds45 (* pi -0.5) (/ g4 muf45))	    ;G4
	  fs45 (polar es45 (angle c r34t_ver) (/ (abs r34t) muf45));R34t
	  gs45 (inters
		 fs45 (polar fs45 (angle d c) 5);R34n
		 as45 (polar as45 0. 5)               ;R05
		 nil
	       ); inters
	  ugol_r43 (angle gs45 es45)
	  r05 (* muf45 (distance gs45 as45))
	  r34 (* muf45 (distance gs45 es45))
	  r45 (* muf45 (distance cs45 gs45))	  
    );_end of setq
    ); progn
    ;Fc+G5+�5+�'4+G4+R34t+R34n+R05=0
    (progn
    (setq bs45 (polar as45 (* 0.5 pi) (/ sils muf45))          ;Fc
	  cs45 (polar bs45 (/ pi -2) (/ g5 muf45))     	    ;G5
	  ds45 (polar cs45 (angle ad polus) (/ sf5 muf45))	    ;�5
	  es45 (polar ds45 (angle s4 sf4_ver) (/ sf4 muf45));�'4
	  fs45 (polar es45 (* pi -0.5) (/ g4 muf45))	    ;G4
	  gs45 (polar fs45 (angle c r34t_ver) (/ (abs r34t) muf45));R34t
	  hs45 (inters
		 gs45 (polar gs45 (angle d c) 5);R34n
		 as45 (polar as45 0. 5)               ;R05
		 nil
	       ); inters
	  ugol_r43 (angle hs45 fs45)
	  r05 (* muf45 (distance hs45 as45))
	  r34 (* muf45 (distance fs45 hs45))
	  r45 (* muf45 (distance ds45 hs45))	  
    );_end of setq
    ); progn
  );_end of if
  (cherplan45)
);_end of defun

(defun cherplan45 (/ gde)  
  (if (zerop sils)
    (progn
      (risskor as45 bs45 "b" "G^5")
      (risskor bs45 cs45 "c" "�^5")
      (risskor cs45 ds45 "d" "�'^4")
      (risskor ds45 es45 "e" "G^4")
      (risskor es45 fs45 "f" "R\U+03C4^34")
      (risskor fs45 gs45 "g" "Rn^34")
      (risskor gs45 as45 "a" "R^05")
      (risskor es45 gs45 nil "R^34")
      
    );_end of progn
    (progn
      (risskor as45 bs45 "b" "F^C")
      (risskor bs45 cs45 "c" "G^5")
      (risskor cs45 ds45 "d" "�^5")
      (risskor ds45 es45 "e" "�'^4")
      (risskor es45 fs45 "f" "G^4")
      (risskor fs45 gs45 "g" "R\U+03C4^34")
      (risskor gs45 hs45 "h" "Rn^34")
      (risskor hs45 as45 "a" "R^05")
      (risskor fs45 hs45 nil "R^34")      
    );_end of progn
  );_end of if
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���: ") muf45 "F" "H/��")
);_end of defun


(defun grup23 (polus ugol / g3_ver r43_ver r12_ver
	       
b a c d s4 op_up5 op_down5 up5 down5 fs_ug fs_kon

av12 av3 cv dv s4v mvc mva3 mva3a2 mvdc omeg3

mua aa3bn adcn aa3a2k daa3bn ddcn daa3a2k aa12 aa3n
aa3k aa3 ac adn ad as4 ma3 ma3a2r ma3bt mac mad mas4 madct eps3 eps4



	       )
  (planusk polus 100 ugol)
  (setq ;����� ��� ����������� ���
	g3_ver (polar b (/ pi -2) 20)
	r43_ver (polar c ugol_r43 20)
	;����������� �������� ���� �������
	r12 (/
	      (+ (moment r34 c r43_ver b)
		 ;(/ msf3 mul)
	      ); +
	      (distance a b)
	    ); /
	r12_ver (if (minusp r12)
		  (polar a (- (angle a b) (* pi 0.5)) 20)
		  (polar a (+ (* pi 0.5) (angle a b)) 20)
		); if
  );_end of setq
  (chergrup23)
);_end of defun

(defun chergrup23 (/ gde)  
  (li (list (polar a (angle c a) 11) c))
  (command "._rectang" (polar a (+ (angle b a) (/ pi 6)) 8) "_r" (angtos (angle b a) 1 10) (polar a (+ (angle a b) (/ pi 6)) 8))
  (risskor b g3_ver nil "G^3")
  (risskor c r43_ver nil "R^43")
  (risskor a r12_ver nil "R^12")
  (mapcar '(lambda (x) (add_circ 0.7 x)) (list a b c))
  (add_text c "C")(add_text a "A")(add_text b "B")
  (add_text (getpoint "\nR34t: ")
	    (strcat "\\A1;R\\H0.7x;\\S^12;\\H1.4286x;="
		    (if (minusp (moment r34 c r43_ver b))
		      "-"
		      ""
		    )
		    "R\\H0.7x;\\S^34;\\H1.4286x;*"
		    (rtos (abs (/ (moment r34 c r43_ver b) r34)) 2 1)
		    "/AB="
		    (rtos (moment r34 c r43_ver b) 2 2)
		    "/"(rtos (distance a b) 2 1)"="(rtos r12 2 2)" H"
	    )
  )
  (plan23)  
);_end of defun

(defun plan23 (/)
  (setq muf23 (/ (max r34 g3 (abs r12)) (getreal "\n������� ����� ����� ������� ���� �� ����� ������ 2-3 "))
	as23 (getpoint "\nPlan sil 23: ")
	;���������: G3+R12+R43+R03=0
	bs23 (polar as23 (/ pi -2) (/ g3 muf23))                    ;G3
	cs23 (polar bs23 (angle a r12_ver) (/ (abs r12) muf23))  ;R12
	ds23 (polar cs23 ugol_r43 (/ r34 muf23))                 ;R43
	r03 (* muf23 (distance ds23 as23))
	ugol_r21 (angle cs23 bs23)
  );_end of setq
  (cherplan23)
);_end of defun

(defun cherplan23 (/ gde le)  
  (risskor as23 bs23 "b" "G^3")
  (risskor bs23 cs23 "c" "R^12")
  (risskor cs23 ds23 "d" "R^43")
  (risskor ds23 as23 "a" "R^03")
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���: ") muf23 "F" "H/��")
);_end of defun


(defun grup01 (polus ugol / r21_ver fy_ver

b a c d s4 op_up5 op_down5 up5 down5 fs_ug fs_kon

av12 av3 cv dv s4v mvc mva3 mva3a2 mvdc omeg3

mua aa3bn adcn aa3a2k daa3bn ddcn daa3a2k aa12 aa3n
aa3k aa3 ac adn ad as4 ma3 ma3a2r ma3bt mac mad mas4 madct eps3 eps4
	       
	       )
  (planusk polus 100 ugol)
  (setq ;����� ��� ������������ ������ � ���
	r21_ver (polar a ugol_r21 20)
	fuu (/ (moment r12 a r21_ver polus) oa)	
	fy_ver (if (minusp fuu)
	           (polar a (+ (angle polus a) (/ pi 2)) 20)
		   (polar a (- (angle polus a) (/ pi 2)) 20)
	       );_end of if
  );_end of setq
  (chergrup01)  
);_end of defun

(defun chergrup01 (/ gde)
  (mapcar '(lambda (x) (add_circ 0.7 x)) (list polus a))
  (add_opor polus (+ (* pi 0.5) ugol))
  (li (list polus a))
  (add_text polus "O") (add_text a "A")
  (risskor a r21_ver nil "R^21")
  (risskor a fy_ver nil "F^y")
  (duga polus a a fy_ver 0.6 "\\A1;\\H2;M\\H0.7x;\\S^Y;\\H2;")
  (add_text (getpoint "\nFy: ")
	    (strcat "\\A1;F\\H0.7x;\\S^Y;\\H1.4286x;="
		    
		    "R\\H0.7x;\\S^12;\\H1.4286x;*"
		    (rtos (abs (/ (moment r12 a r21_ver polus) r12)) 2 1)
		    "/OA="
		    (rtos (abs (moment r12 a r21_ver polus)) 2 2)
		    "/"(rtos oa 2 1)"="(rtos (abs fuu) 2 2)" H"
	    )
  )
  (plan01)  
);_end of defun

(defun plan01 (/ max01
	      as01 bs01 cs01 )
  (setq max01 (getreal "\n������� ������������ ����� ���� �� ����� ��� 0-1 ")
	muf01 (/ (abs r12) max01)
	;R21+Fy+R01=0
	as01 (getpoint "\nPlan 01")
	bs01 (polar as01 ugol_r21 (/ (abs r12) muf01))
	cs01 (polar bs01 (angle a fy_ver) (/ (abs fuu) muf01))
	r01 (* muf01 (distance as01 bs01))
  );_end of setq
  (cherplan01)  
);_end of defun

(defun cherplan01 (/ gde)
  (risskor as01 bs01 "b" "R^21")
  (risskor bs01 cs01 "c" "F^y")
  (risskor cs01 as01 "a" "R^01")  
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���: ") muf01 "F" "H/��")
);_end of defun