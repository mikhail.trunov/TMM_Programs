;�������� �������������� ��� ������� �3 �������� �������������� ��������
(defun vvod (/ nom_var)
  (initget 5)
  (setq nom_var (1+ (getint "\n������� ����� ��������: ")))
  (initget 7)
  (setq oa (getreal "\n������� O� [��]: "))
  (exel_get)
);end of vvod

(defun exel_get (/ g_oex g_wkbs g_wb_get g_shs_get g_sheet f)
  (setq g_oex (vlax-get-or-create-object "Excel.Application"))
  (vlax-put-property g_oex "Visible" :vlax-false)
  (setq g_wkbs (vlax-get-property g_oex "Workbooks")        
        g_wb_get (vla-open g_wkbs "c:\\Program files\\AutoCAD 2008\\Support\\variants\\variants_3.xls")        
        g_shs_get (vlax-get-property g_wb_get "Worksheets")
	g_sheet (vlax-get-property g_shs_get "Item" 1)
	n1 (ex_get 18 nom_var)
        loa (ex_get 4 nom_var)
        mul (/ loa oa)
        lab (ex_get 5 nom_var)
        ab (/ lab mul)
        lbcd (ex_get 6 nom_var)
        bcd (/ lbcd mul)       
	la (ex_get 7 nom_var)
        al (/ la mul)
	lb (ex_get 8 nom_var)
        bl (/ lb mul)
	f (ex_get 23 nom_var)	
        fi (* f (/ pi 180.))
	omeg1 (/ (* pi n1) 30.)
	va (* omeg1 loa)
	aan (* omeg1 omeg1 loa)	
	m2 (ex_get 11 nom_var)
	m3 (ex_get 12 nom_var)
	m4 m3
	m5 (ex_get 13 nom_var)
	g2 (* 9.81 m2)
	g3 (* 9.81 m3)
	g4 g3
	g5 (* 9.81 m5)
	is2 (ex_get 16 nom_var)
	is3 (ex_get 17 nom_var)
	is4 is3
  )
  (vlax-invoke-method g_wb_get "Close" :vlax-false :vlax-false)
  (vlax-invoke-method g_oex "Quit")
  (mapcar
    (function (lambda (x)
      (if
        (and x (not (vlax-object-released-p x)))
        (vlax-release-object x)
      );_ if
              );_ lambda
    );_ function
    (list g_shs_get
	  g_wb_get
	  g_wkbs g_sheet
	  g_oex)
  )  
  (gc)  
)

(defun tochkim (o ugol)
  (setq a (polar o ugol oa)
	c (list (+ (car o) al) (+ (cadr o) bl))
	b (perokr a c ab bcd nil)
        d (cadr (dwgru (list 0 (+ (cadr o) bl)) (list 100 (+ (cadr o) bl)) b bcd))
	s2 (polar a (angle a b) (* 0.5 (distance a b)))
	s3 (polar c (angle c b) (* 0.5 (distance c b)))
	s4 (polar d (angle d b) (* 0.5 (distance d b)))
	masivm (list o a b c d)
	mass (list s2 s3 s4)
  );_end of setq
)

(defun main_funkc (/ ugolok pol ugol0
		   polus_mexan polus_skor polus_uskor vektorva vektoraa)
  (initget 65)
  (setq ugol0 (- (angle '(0 0) (list (- la lbcd) lb))
		 (arccos (/ (+ (* lb lb) (* loa loa) (* -1. lab lab)) (* 2. lb loa)))
	      )
	polus_mexan (getpoint "\n������� ����� O ����� ���������: ")
	ugolok ugol0
  ); setq
  (repeat 12
    (tochkim polus_mexan ugolok)
    (cherm)
    (setq ugolok (+ ugolok (/ pi 6.)))
  ); repeat
  (initget 65)
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���������: ") mul "m")
  (initget 7)
  (setq ugolok ugol0
	vektorva (getreal "\n������� ����� ������� �������� ����� A: ")
	muv (/ va vektorva)  
  )
  (repeat 12
    (tochkim polus_mexan ugolok)
    (initget 64)
    (plansk (setq pol (getpoint "\n������� ��� ������� ���� ���������: ")) vektorva)
    (cherv pol)
    (if (equal fi (- ugolok ugol0) 1.e-5)
	(setq polus_skor pol)
    )
    (setq ugolok (+ ugolok (/ pi 6.)))
  ); repeat
  (initget 65)
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���������: ") muv "v")
  (tochkim polus_mexan (+ fi ugol0))
  (plansk polus_skor vektorva)
  (modulv)
  (initget 7)
  (setq vektoraa (getreal "\n������� ����� ������� ��������� ����� A [mm]: ")
	mua (/ aan vektoraa)
  )
  (initget 65)
  (setq	polus_uskor (getpoint "\n������� ��� ������� ���� ���������: ")
  )
  (planusk vektoraa polus_uskor)
  (chera polus_uskor)
  (excel_export)
)

(defun excel_export (/ g_oex g_wkbs g_wb_put g_shs_put g_sheet1 g_sheet2
		     fii i)
  (setq g_oex (vlax-get-or-create-object "Excel.Application"))
  (vlax-put-property g_oex "Visible" :vlax-false)  
  (setq g_wkbs (vlax-get-property g_oex "Workbooks")        
        g_wb_put (vla-open g_wkbs "c:\\Program files\\AutoCAD 2008\\Support\\blanks\\first_list_3.xls")        
        g_shs_put (vlax-get-property g_wb_put "Worksheets")
	g_sheet1 (vlax-get-property g_shs_put "Item" 1)
	g_sheet2 (vlax-get-property g_shs_put "Item" 2)
  )
  (ex_put loa 2 "B" g_sheet1)
  (ex_put oa 3 "B" g_sheet1)
  (ex_put n1 4 "B" g_sheet1)
  (ex_put (* fi (/ 180. pi)) 8 "B" g_sheet1)
  (ex_put lab 2 "C" g_sheet1)
  (ex_put lbcd 2 "D" g_sheet1)
  (ex_put la 2 "E" g_sheet1)
  (ex_put lb 2 "F" g_sheet1)
  (ex_put muv 3 "O" g_sheet2)
  (setq fii ugol0
	i 2
  )
  (repeat 12
    (tochkim polus_mexan fii)
    (plansk polus_skor vektorva)
    (ex_put (distance polus_skor bv) 3 i g_sheet2)
    (ex_put (distance av bv) 4 i g_sheet2)
    (ex_put (distance polus_skor dv) 5 i g_sheet2)
    (ex_put (distance dv bv) 6 i g_sheet2)
    (ex_put (distance polus_skor s2v) 7 i g_sheet2)
    (ex_put (distance polus_skor s3v) 8 i g_sheet2)
    (ex_put (distance polus_skor s4v) 9 i g_sheet2)
    (setq fii (+ fii (/ pi 6.))
	  i (1+ i)
    )
  ); repeat
  (tochkim polus_mexan (- ugol0 fi))
  (plansk polus_skor vektorva)
  (ex_put mad 23 "K" g_sheet2)
  (ex_put mas2 23 "M" g_sheet2)
  (ex_put mas3 23 "N" g_sheet2)
  (ex_put mas4 23 "O" g_sheet2)
  (ex_put aban 26 "C" g_sheet2)
  (ex_put abcn 26 "D" g_sheet2)
  (ex_put adbn 26 "E" g_sheet2)
  (ex_put mabat 26 "F" g_sheet2)
  (ex_put mabct 26 "G" g_sheet2)
  (ex_put madbt 26 "H" g_sheet2)
  (ex_put mua 26 "I" g_sheet2)
  (vlax-invoke-method g_wb_put "SaveCopyAs" (getfiled "������� ��� ��������� ���� � ��������" "c:\\" "xls" 129))
  (vlax-invoke-method g_wb_put "Close" :vlax-false :vlax-false)
  (vlax-invoke-method g_oex "Quit")
  (mapcar
    (function (lambda (x)
      (if
        (and x (not (vlax-object-released-p x)))
        (vlax-release-object x)
      );_ if
              );_ lambda
    );_ function
    (list g_shs_put
	  g_wb_put
	  g_wkbs g_sheet1 g_sheet2
	  g_oex)
  )  
  (gc) 
)

(defun chera (p /)    
    (risskor p aa "a" "a^A")  
    (risskor aa ban nil "an^BA")
    (risskor p bcn nil "an^BC")
    (risskor bcn ba nil "a\U+03C4^BC")
    (risskor ban ba nil "a\U+03C4^BA")
    (risskor p ba "b" "a^B")
    (risskor p da "d" "a^D")
    (risskor ba dbn nil "an^DB")
    (risskor dbn da nil "a\U+03C4^DB")
    (risskor p s2a "s^2" "a^S2")
    (risskor p s3a "s^3" "a^S3")
    (risskor p s4a "s^4" "a^S4")
    (li (list aa ba da))
    (add_tex p "p,o,c" nil)
    (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���������: ") mua "a")
)

(defun planusk (pa p /)  
  (setq 
    ;=====����� �������� ���������� ���������=====
	dban (/ aban mua)
	dbcn (/ abcn mua)
	ddbn (/ adbn mua)	
	;=====����� ����� ���������=====
	aa (polar p (angle a polus_mexan) pa)
	ban (polar aa (angle b a) dban)
	bcn (polar p (angle b c) dbcn)
	ba (peres a b c bcn ban)
	dbn (polar ba (angle d b) ddbn)
	da (inters p (polar p 0. 100) dbn (polar dbn (- (angle dbn ba) (/ pi 2)) 100) nil)
	s2a (polar aa (angle aa ba) (* 0.5 (distance aa ba)))
	s3a (polar p (angle p ba) (* 0.5 (distance p ba)))
	s4a (polar ba (angle ba da) (* 0.5 (distance ba da)))
	;=====������ ���������=========
    ;����������
    mab (* mua (distance p ba))
    mad (* mua (distance p da))
    mas2 (* mua (distance p s2a))
    mas3 (* mua (distance p s3a))
    mas4 (* mua (distance p s4a))
    ;��������������
    mabat (* mua (distance ban ba))
    mabct (* mua (distance bcn ba))
    madbt (* mua (distance dbn da))
    ;��������
    eps2 (/ mabat lab)
    eps3 (/ mabct lbcd)
    eps4 (/ madbt lbcd)
  )
)

(defun modulv (/ ome2 ome3 ome4)
 (setq mvb (* muv (distance polus_skor bv))
	mvd (* muv (distance polus_skor dv))
	mvs2 (* muv (distance polus_skor s2v))
	mvs3 (* muv (distance polus_skor s3v))
	mvs4 (* muv (distance polus_skor s4v))
	mvba (* muv (distance av bv))
	mvdb (* muv (distance dv bv))
	;���������� ���������
	abcn (/ (expt mvb 2) lbcd)
	aban (/ (expt mvba 2) lab)
	adbn (/ (expt mvdb 2) lbcd)
	;�����
	ome2 (/ mvba lab)
	ome3 (/ mvb lbcd)
	ome4 (/ mvdb lbcd)
 )
)

(defun cherv (p / )    
    (add_tex p "p,o,c" nil)
    (risskor p av "a" "v^A")
    (risskor av bv nil "v^BA")
    (risskor p bv "b" "v^B")
    (risskor bv dv nil "v^DB")
    (risskor p dv "d" "v^D")
    (risskor p s2v "s^2" "v^S2")
    (risskor p s3v "s^3" "v^S3")
    (risskor p s4v "s^4" "v^S4")    
)

(defun plansk (polus pav);������� ����� ����� ���������
  (setq av  (polar polus (+ (/ pi 2.) (angle polus_mexan a)) pav)
        bv  (peres a b c polus av)
        dv  (inters polus (polar polus 0. 100)
		    bv (polar bv (- (angle d b) (* 0.5 pi)) 100)	      
	      nil
	    )
	s2v (polar av (angle av bv) (* 0.5 (distance av bv)))
	s3v (polar polus (angle polus bv) (* 0.5 (distance polus bv)))
	s4v (polar bv (angle bv dv) (* 0.5 (distance dv bv)))
  );_end of setq
);_end of defun

(defun cherm (/ i)  
  (li (list polus_mexan a b c))
  (li (list b d))
  (setq i 79)
  (mapcar '(lambda (x) (add_circ 0.7 x)) masivm)
  (mapcar '(lambda (x) (add_circ 0.7 x)) mass)
  (mapcar '(lambda (x)
	      (add_tex x (chr i) nil)
	      (if (= i 79)
		(setq i 65)
	        (setq i (1+ i))
	      )
	   );_end of lambda
  masivm)  
)



(defun c:mex3 (/ old le eps2 eps3 eps4 lbcd lab
a b c d s2 s3 s4 av bv dv s2v s3v s4v aa ban bcn ba dbn da s2a s3a s4a muv mul mua
mvb mvd mvs2 mvs3 mvs4 mvba mvdb abcn aban adbn dban dbcn ddbn	
mab mad mas2 mas3 mas4 mabat mabct madbt)
  (vl-load-com)
  (setq old (getvar "osmode"))
  (setvar "osmode" 20903)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "timesi.TTF" )))	
  )
  (entmod le)
  (setq old (getvar "OSMODE"))
  (setvar "OSMODE" 20919)
  (load "funkcii_tmm.LSP")
  (vvod)
  (main_funkc)
  (setvar "OSMODE" old)
  (vla-ZoomExtents (vlax-get-acad-object))
)
(c:mex3)