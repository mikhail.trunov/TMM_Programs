(defun tochkim (o ugol)
  (setq a (polar o ugol oa)
	c (list (+ (car o) al) (+ (cadr o) bl))
	b (perokr a c ab bcd nil)
        d (cadr (dwgru (list 0 (+ (cadr o) bl)) (list 100 (+ (cadr o) bl)) b bcd))
	s2 (polar a (angle a b) (* 0.5 (distance a b)))
	s3 (polar c (angle c b) (* 0.5 (distance c b)))
	s4 (polar d (angle d b) (* 0.5 (distance d b)))
	masivm (list o a b c d)
	mass (list s2 s3 s4)
  );_end of setq
)

(defun plansk (polus pav ugols / ;a c b d s2 s3 s4 mass masivm
	       );������� ����� ����� ���������
  (tochkim polus ugols)
  (setq av  (polar polus (+ (/ pi 2.) (angle polus a)) pav)
        bv  (peres a b c polus av)
        dv  (inters polus (polar polus 0. 100)
		    bv (polar bv (- (angle d b) (* 0.5 pi)) 100)	      
	      nil
	    )
	s2v (polar av (angle av bv) (* 0.5 (distance av bv)))
	s3v (polar polus (angle polus bv) (* 0.5 (distance polus bv)))
	s4v (polar bv (angle bv dv) (* 0.5 (distance dv bv)))
  );_end of setq
);_end of defun

(defun planusk (polus pa ugolu / ;a c b d s2 s3 s4 mass masivm mvb mvd mvs2 mvs3 mvs4 mvba mvdb
		)
  (plansk polus 70. ugolu)
  (setq mvb (* muv (distance polus bv))        
	mvba (* muv (distance av bv))
	mvdb (* muv (distance dv bv))
	;���������� ���������
	abcn (/ (expt mvb 2) lbcd)
	aban (/ (expt mvba 2) lab)
	adbn (/ (expt mvdb 2) lbcd)
    ;=====����� �������� ���������� ���������=====
	dban (/ aban mua)
	dbcn (/ abcn mua)
	ddbn (/ adbn mua)	
	;=====����� ����� ���������=====
	aa (polar polus (angle a polus) pa)
	ban (polar aa (angle b a) dban)
	bcn (polar polus (angle b c) dbcn)
	ba (peres a b c bcn ban)
	dbn (polar ba (angle d b) ddbn)
	da (inters polus (polar polus 0. 100) dbn (polar dbn (- (angle dbn ba) (/ pi 2)) 100) nil)
	s2a (polar aa (angle aa ba) (* 0.5 (distance aa ba)))
	s3a (polar polus (angle polus ba) (* 0.5 (distance polus ba)))
	s4a (polar ba (angle ba da) (* 0.5 (distance ba da)))
	;=====������ ���������=========
    ;����������
    mab (* mua (distance polus ba))
    mad (* mua (distance polus da))
    mas2 (* mua (distance polus s2a))
    mas3 (* mua (distance polus s3a))
    mas4 (* mua (distance polus s4a))
    ;��������������
    mabat (* mua (distance ban ba))
    mabct (* mua (distance bcn ba))
    madbt (* mua (distance dbn da))
    ;��������
    eps2 (/ mabat lab)
    eps3 (/ mabct lbcd)
    eps4 (/ madbt lbcd)
  )
)

(defun cherm (/ i)  
  (li (list polus_mexan a b c))
  (li (list b d))
  (setq i 79)
  (mapcar '(lambda (x) (add_circ 0.7 x)) masivm)
  (mapcar '(lambda (x) (add_circ 0.7 x)) mass)
  (mapcar '(lambda (x)
	      (add_tex x (chr i) nil nil)
	      (if (= i 79)
		(setq i 65)
	        (setq i (1+ i))
	      )
	   );_end of lambda
  masivm)  
)

(defun cherv (p / )    
    (add_tex p "p,o,c" nil nil)
    (risskor p av "a" "v^A")
    (risskor av bv nil "v^BA")
    (risskor p bv "b" "v^B")
    (risskor bv dv nil "v^DB")
    (risskor p dv "d" "v^D")
    (risskor p s2v "s^2" "v^S2")
    (risskor p s3v "s^3" "v^S3")
    (risskor p s4v "s^4" "v^S4")    
)

(defun chera (p /)    
    (risskor p aa "a" "a^A")  
    (risskor aa ban nil "an^BA")
    (risskor p bcn nil "an^BC")
    (risskor bcn ba nil "a\U+03C4^BC")
    (risskor ban ba nil "a\U+03C4^BA")
    (risskor p ba "b" "a^B")
    (risskor p da "d" "a^D")
    (risskor ba dbn nil "an^DB")
    (risskor dbn da nil "a\U+03C4^DB")
    (risskor p s2a "s^2" "a^S2")
    (risskor p s3a "s^3" "a^S3")
    (risskor p s4a "s^4" "a^S4")
    (li (list aa ba da))
    (add_tex p "p,o,c" nil nil)
    (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���������: ") mua "a" "� �/��")
)

;�������� �������������� ��� ������� �3 �������� �������������� ��������
(defun vvod (/ nom_var)
  (initget 5)
  (setq nom_var (1+ (getint "\n������� ����� ��������: ")))
  (initget 7)
  (setq oa (getreal "\n������� O� [��]: "))
  (exel_get)
  (setq fi (+ fi (* (/ pi 180.) (getreal "\n������� ��������� � ���������� ����: "))))
);end of vvod

(defun exel_get (/ g_oex g_wkbs g_wb_get g_shs_get g_sheet f)
  (setq g_oex (vlax-get-or-create-object "Excel.Application"))
  (vlax-put-property g_oex "Visible" :vlax-false)
  (setq g_wkbs (vlax-get-property g_oex "Workbooks")        
        g_wb_get (vla-open g_wkbs (strcat total_adress "variants\\variants_3.xls"))        
        g_shs_get (vlax-get-property g_wb_get "Worksheets")
	g_sheet (vlax-get-property g_shs_get "Item" 1)
	n1 (ex_get 18 nom_var)
        loa (ex_get 4 nom_var)
        mul (/ loa oa)
        lab (ex_get 5 nom_var)
        ab (/ lab mul)
        lbcd (ex_get 6 nom_var)
        bcd (/ lbcd mul)       
	la (ex_get 7 nom_var)
        al (/ la mul)
	lb (ex_get 8 nom_var)
        bl (/ lb mul)
	f (ex_get 23 nom_var)	
        fi (* f (/ pi 180.))
	omeg1 (/ (* pi n1) 30.)
	va (* omeg1 loa)
	aan (* omeg1 omeg1 loa)	
	m2 (ex_get 11 nom_var)
	m3 (ex_get 12 nom_var)
	m4 m3
	m5 (ex_get 13 nom_var)
	g2 (* 9.81 m2)
	g3 (* 9.81 m3)
	g4 g3
	g5 (* 9.81 m5)
	is2 (ex_get 16 nom_var)
	is3 (ex_get 17 nom_var)
	is4 is3
	f_max (ex_get 21 nom_var)
  )
  (vlax-invoke-method g_wb_get "Close" :vlax-false :vlax-false)
  (vlax-invoke-method g_oex "Quit")
  (mapcar
    (function (lambda (x)
      (if
        (and x (not (vlax-object-released-p x)))
        (vlax-release-object x)
      );_ if
              );_ lambda
    );_ function
    (list g_shs_get
	  g_wb_get
	  g_wkbs g_sheet
	  g_oex)
  )  
  (gc)  
)

(defun cherdiagr (polus / b ugolbcd m maxf p1 p2 p3 p4 p5 p6 p7 p8)
  (setq maxf (getdist "\n������� ������������ ����� ��� ������� �������� [��]: ")
	p2 (list (- (car polus) (* 2. bcd) (* -1. al))
		 (+ (cadr polus) bl)
	   ); list
	b (polar '(0 0) (+ pi ugol0) (- ab oa))
	ugolbcd (- pi (angle (list al bl) b))
	m (- (* 2. bcd (cos ugolbcd)) al)
	p1 (list (- (car polus)
		    m
		 )
		 (+ (cadr polus) bl)
	   ); list
	p2 (polar p2 (* 0.5 pi) 15.)
	p1 (polar p1 (* 0.5 pi) 15.)
  )
  (risskor p1 (polar p2 pi 10) "H" nil)
  (risskor p1 (polar p1 (* 0.5 pi) (+ maxf 10.)) nil "F^C")
  (setq p3 (polar p1 pi (* 0.3 (distance p1 p2)))
	p4 (polar (polar p1 pi (* 0.4 (distance p1 p2)))
		  (* 0.5 pi)
		  (* 0.3 maxf)
	   )
	p5 (polar p1 pi (* 0.5 (distance p1 p2)))
	p6 (polar p1 pi (* 0.7 (distance p1 p2)))
	p7 (polar (polar p1 pi (* 0.95 (distance p1 p2)))
		  (* 0.5 pi)
		  maxf
	   )
	p8 (polar p2 (* 0.5 pi) (* 0.1 maxf))
  )
  (li (list p3 p4 p5))
  (li (list p6 p7 p8 p2))
  (initget 65)
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ��������� ���: ") (/ f_max maxf) "F" "H/��")
)