;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;�������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun c:max10 (/ old le ugol0
a alfa bet gamma ob b d c s2 s4 muv av bv dv cv s2v s4v mvb mvd mvba 
mvc mvcd mvs2 mvs4 omeg2 omeg4 aban acdn mua dban dcdn aa ban ba da
cdn ca s2a s4a mab mabat mad mac macdt mas2 mas4
n1 loa mul lab ab lad ad lbd bd ldc dc las2 as2 lds4 ds4 tetgr tet fi 
omeg1 va aan m2 m3 m4 m5 i2 i4 pmax k_nerav g2 g3 g4 g5 d_cilindr)
  (vl-load-com)
  (load "funkcii_tmm.LSP")
  (load "tmm\\mex_10\\for_10.LSP")
  (load "tmm\\mex_10\\for_d10.LSP")
  (load "tmm_maxovik.lsp")
  (setq old (getvar "osmode"))
  (setvar "osmode" 20903)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "timesi.TTF" )))	
  ); setq
  (entmod le)
  (vvod)
  (max_cc1 (* (+ tet pi) 0.5))
  (setq ugol0 (* 0.5 (+ pi tet))
	muv (/ va 70.)
  )
  (maxovik)
  (setvar "osmode" old)
  (initget 1 "Yes No")
  (if (equal (getkword "\n������� � excel? [Yes/No]: ") "Yes")
    (excel_expmax)
  ) 
  (vla-ZoomExtents (vlax-get-acad-object))  
);_end of defun

(defun mpr1 (ugol / p2 fdv2)
  (plansk polus 70. ugol)
   (setq p2 (* pmax (znach_4_b ugol))
	 fdv2 (* p2 pi 0.25 d_cilindr d_cilindr)

   ); setq
  (* fdv2 -1. (cosinus polus bv polus (polar polus ugol0 5)) loa (/ (distance polus bv) 70.))
)

(defun mpr2 (ugol / p4 fdv4)
  (plansk polus 70. ugol)
   (setq p4 (* pmax (znach_4_c ugol))
	 fdv4 (* p4 pi 0.25 d_cilindr d_cilindr)

   ); setq
  (* fdv4 -1. (cosinus polus cv
		       polus (polar polus (* 0.5 (- pi tet)) 5)
	      )
     loa
     (/ (distance polus cv) 70.)
  )
)

(defun mpr (kolvo / polus shag_ugl ugol0 fi polog1 polog2
	    p1 p2 sils1 sils2 mdfs1 mdfs2 md_sum mom_mas)
  (setq polus '(0 0)
	shag_ugl (/ pi kolvo 0.5)
	ugol0 (* (+ tet pi) 0.5)
	fi ugol0
	;tekush_ug 0.
  );_ setq
  (repeat (1+ kolvo)
    (plansk polus 70. fi)
    (setq p1 (* pmax (znach_4_b fi))
	  p2 (* pmax (znach_4_c fi))
	  sils1 (* p1 pi 0.25 d_cilindr d_cilindr)
	  sils2 (* p2 pi 0.25 d_cilindr d_cilindr)
	  mdfs1 (* sils1 (/ (distance polus bv) 70.) loa
		  (if (equal (angle polus bv) (* (+ tet pi) 0.5) 1e-6)
		    1.
		    -1.
		  )
		 )
	  mdfs2 (* sils2 (/ (distance polus cv) 70.) loa
		   (if (equal (angle polus cv) (* (- pi tet) 0.5) 1e-6)
		    1.
		    -1.
		  )
		)
	  md_sum (+ mdfs1 mdfs2)
		   ;mdg3)
	  mom_mas (cons md_sum mom_mas)
	  fi (- fi shag_ugl)
    ); setq
  ); repeat
  (setq mom_mas (reverse mom_mas))
)

(defun ipr (kolvo / polus shag_ugl ugol0 fi
	    i_sumar mas_i)
  (setq polus '(0 0)
	shag_ugl (/ pi kolvo 0.5)
	ugol0 (* (+ tet pi) 0.5)
	fi ugol0
  );_ setq
  (repeat (1+ kolvo)
    (plansk polus 70. fi)
    (setq i_sumar (+ (* i2 (expt (/ omeg2 omeg1) 2))
		     (* i4 (expt (/ omeg4 omeg1) 2))
		     (* m2 (expt (/ mvs2 omeg1) 2))
		     (* m3 (expt (/ mvb omeg1) 2))
		     (* m4 (expt (/ mvs4 omeg1) 2))
		     (* m5 (expt (/ mvc omeg1) 2))
		  ); +
	  mas_i (cons i_sumar mas_i)
	  fi (- fi shag_ugl)
    ); setq
  ); repeat
  (setq mas_i (reverse mas_i))
)

(c:max10)