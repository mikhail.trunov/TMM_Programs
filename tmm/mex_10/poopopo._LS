(defun c:qw ()
  (setq oa 50.
	ab 120.
	ad 30.
	bd 90.
	tet (* pi 0.5)
	fii (* 0.5 (+ pi tet))
  )
  (repeat 12
;;;    (setq gamma (- (* 0.5 (+ pi tet)) fii)
;;;	  xa (* oa (cos gamma))
;;;	  ya (* oa (sin gamma))
;;;	  alfa (asin (* (/ oa ab) (sin (- (* 2. fii) (* 0.5 (+ pi tet))))))
;;;	  betta (- pi alfa fii)
;;;	  ob (* ab (/ (sin betta) (sin (- (* 2. fii) (* 0.5 (+ pi tet))))))
;;;	  xb (* ob (cos (* 0.5 (- pi tet))))
;;;	  yb (* ob (sin (* 0.5 (- pi tet))))
;;;	  fii (- fii (/ pi 6.))
;;;    )
    (setq a (polar '(0 0) fii oa)
	  alfa (- (* 0.5 (+ pi tet)) fii)
	  bet (asin (* (sin alfa) (/ oa ab)))
	  gamma (- pi alfa bet)
	  ob (cond
	       ((equal alfa pi 1e-10) (- ab oa))
	       ((equal alfa 0.) (+ ab oa))	       
	       (t (* ab (/ (sin gamma) (sin alfa))))
	     )
	  b (polar '(0 0) (* 0.5 (+ pi tet)) ob)
	  d (polar a (- (angle a b) (arccos (/ (+ (* ab ab) (* ad ad) (* bd bd -1.)) (* 2. ab ad)))) ad)
	  fii (- fii (/ pi 6.))
    )
    (li (list '(0 0) a b d))
  )
)


  (defun asin (sine / cosine)
    (setq cosine (sqrt (- 1.0 (expt sine 2))))
    (if (zerop cosine)
      (setq cosine 1e-30)
    );if
    (atan (/ sine cosine))
  );defun asin
(defun li (list_of / i list_new)
  (if (> (length list_of) 1)
    (progn
      (entmakex (list '(0 . "line")		  
		'(48 . 0.59)
		(cons 8 (getvar "CLAYER"))
		(cons 10 (trans (nth 0 list_of) 1 0))
		(cons 11 (trans (nth 1 list_of) 1 0))
	    ); list
      ); entmakex
      (setq i 1)
      (repeat (1- (length list_of))
	(setq list_new (cons (nth i list_of) list_new)
	      i (1+ i)
	); setq
      ); repeat
      (li list_new)
    ); progn
  ); if
); defun

(defun arccos (cosalf)
   (cond
     ((> (abs cosalf) 1) (alert (strcat "\t������!\n� ������� ����������� �������� �������� "(rtos cosalf)
					"\n ��� >1 !")))
     ((= cosalf 0) (/ pi 2))
     ((>= cosalf 0) (atan (/ (sqrt (- 1 (* cosalf cosalf))) cosalf)))
     ((< cosalf 0) (+ pi (atan (/ (sqrt (- 1 (* cosalf cosalf))) cosalf)))) 
   )
)