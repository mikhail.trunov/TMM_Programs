;�������� �������������� ��� ������� �1 �������� ��������� ������
(defun vvod (/ f n1)
  (initget 5)  
  (setq nom_var (getint "\n������� ����� ��������: ")
    	f (ex_get 21 (1+ nom_var))
        oa (getreal "\n������� O� [mm] ")
        n1 (ex_get 15 (1+ nom_var))
        loa (ex_get 4 (1+ nom_var))
        mul (/ loa oa)
        lab (ex_get 5 (1+ nom_var))
        ab (/ lab mul)
        lbc (ex_get 6 (1+ nom_var))
        bc (/ lbc mul)
	omeg1 (/ (* pi n1) 30)
	va (* omeg1 loa)
	aan (* omeg1 omeg1 loa)
	f (if (getint "\n+180?")
	    (+ f 180)
	    f
	  ); if
	fi (+ pi (* f (/ pi 180)))
  );end of setq
  (ex_put oa 3 "B" 1)
  (ex_put (rtos ab 2 0) 3 "C" 1)
  (ex_put (rtos bc 2 0) 3 "D" 1)
  (ex_put loa 2 1 1)
  (ex_put lab 2 "C" 1)
  (ex_put lbc 2 "D" 1)
  (ex_put n1 4 "B" 1) 
  (princ)
);end of vvod


;_end of defun

;===� � � � �   � � � � � � �   � � � � � � � � � � � � � � �    � � � � � � �=====
;0000000000000000000000000000000000000000000000000000000000000000000000000000000000
;1111111111111111111111111111111111111111111111111111111111111111111111111111111111
;2222222222222222222222222222222222222222222222222222222222222222222222222222222222
;3333333333333333333333333333333333333333333333333333333333333333333333333333333333
;**********************************************************************************
;----------------------------------------------------------------------------------



;**********************************************************************************
;----------------------------------------------------------------------------------
;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
;=============� � � � � � �    � � � � � �   � � � � � � � � �============---------


;----------------------------------------------------------------------------------
;=============� � � �    � � � �   �   � � � � � � � �   � � � � � � �=============
(defun sili (/ 
	      eps2 eps3
	     msf2 msf3 h2 h3
	     failsil)

  (setq sils (if (and (>= fi (+ pi (/ pi 4))) (<= fi (+ pi (/ pi 2))))
		   (* (ex_get 19 (1+ nom_var)) (1- (/ fi (/ pi 4))))
		   0
	       );_ if
	g2 (* 9.8 m2)
	g3 (* 9.8 m3)
	g4 (* 9.8 m4)
	g5 (* 9.8 m5)
	eps2 (/ mabat lab)
	eps3 (/ mabct lbc)
	msf2 (* eps2 i2)
	msf3 (* eps3 i3)
	sf2 (* m2 mad)
	sf3 (* m3 mas3)
	sf4 (* m4 mad)
	sf5 (* m5 mazv5a)
	h2 (/ msf2 sf2)
	h3 (/ msf3 sf3)
	mh2 (/ h2 mul)
	mh3 (/ h3 mul)

  );_end of setq
 
);_end of defun

;**********************************************************************************
;----------------------------------------------------------------------------------
;                            � � � � �   � � � � � �                               
;------------===========� � � � �    � � � � � � � � � �==============-------------
;**********************************************************************************
;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

(defun guk (/ wib polus qav   qbv    qdv    qfv    qs2v qs3v  silmoment
	    momg5 momg3 momg2 momsf5 momsf3 momsf2 momsils
	    g2ver g3ver g5ver sf2ver sf3ver sf4ver sf5ver
	    plg5 plg3 plg2 plsf2 plsf3)
  ;������ ����� ������
  (setq wib (getint "\n���� ������� ������������ ������? �� ����� [1] ������ [2] ")
  );end of setq
  (if (= wib 1)
      (setq qav (list (cadr av) (* (car av) -1))
	    qbv (list (cadr bv) (* (car bv) -1))
	    qdv (list (cadr dv) (* (car dv) -1))	  
	    qs3v (list (cadr s3v) (* (car s3v) -1))
	    qzv5v (list (cadr zv5v) (* (car zv5v) -1))
      )
      (setq qav (list (* (cadr av) -1) (car av))
	    qbv (list (* (cadr bv) -1) (car bv))
	    qdv (list (* (cadr dv) -1) (car dv))	   
	    qs3v (list (* (cadr s3v) -1) (car s3v))
	    qzv5v (list (* (cadr zv5v) -1) (car zv5v))
      )
  );end of if

  
  ;���������� ������ ����������
  (setq polus (getpoint "\n�������� ����� ��� ������ ����������"))
  (command "._ucs" polus "")
  (li (list p qav qbv p qdv qzv5v p)) 
  (add_tex qav "a")
  (add_tex qbv "b")
  (add_tex qdv "d")
  (add_tex qs3v "\\A1;s\\H0.7x;\\S^3;")
  (add_tex p "P,o,c")
  ;������� ����� ��� �� ������
  (setq g2ver  (list (car qdv) (- (cadr qdv) 20))
	g4ver  (list (car qdv) (- (cadr qdv) 30))
	g3ver  (list (car qs3v) (- (cadr qs3v) 20))
	g5ver  (list (car qzv5v) (- (cadr qzv5v) 20))
	sf2_niz (polar qbv (angle qbv qav) (* (distance b toch_s2) (/ (distance qav qbv) ab)))
	sf3_niz (polar p (angle p qbv) (* (distance c toch_s3) (/ (distance p qbv) bc)))
	sf3_verg (polar sf3_niz (angle s3a p) 20)
	sf2_verg (polar sf2_niz (angle da p) 20)	
	sf5ver (polar qzv5v (angle zv5a p) 20)
	sf4ver (polar qdv (angle da p) 20)
	fsopver (polar qdv (/ pi 2) 30)
  );_end of setq
  ;-----====�������� ���, ����������� �� ��������=====-----
  (risskor qdv g2ver  nil  "G^2")
  (risskor qs3v g3ver  nil  "G^3")
  (risskor qdv g4ver  nil  "G^4")
  (risskor qzv5v  g5ver  nil  "G^5")
  (risskor sf2_niz sf2_verg nil  "�'^2")
  (risskor sf3_niz sf3_verg nil  "�'^3")
  (risskor qdv sf4ver nil  "�^4") 
  (risskor qzv5v  sf5ver nil  "�^5")
  (if (not (zerop sils))
     (risskor qdv fsopver nil  "F^C")
  )
  ;------==========������� �������� �� ������============--------
  (setq momg5 (moment g5 qzv5v  g5ver p)
	momg3 (moment g3 qs3v g3ver p)
	momg4 (moment g4 qdv g4ver p)
	momg2 (moment g2 qdv g2ver p)
	momsf3 (moment sf3 sf3_niz sf3_verg p)
	momsf2 (moment sf2 sf2_niz sf2_verg p)
	momsf5 (moment sf5 qzv5v sf5ver p)
	momsf4 (moment sf4 qdv sf4ver p)
	momsils (moment sils qdv fsopver p)	
  );_end of setq
  (ex_put (strcat (if (minusp momg5) "-" "+") (rtos (rastpr2 p qzv5v  g5ver))"*"(rtos g5)) 16 1 3)
  (ex_put momg5 17 1 3)
  (ex_put (strcat (if (minusp momg4) "-" "+") (rtos (rastpr2 p qdv g4ver))"*"(rtos g4)) 16 2 3)
  (ex_put momg4 17 2 3)
  (ex_put (strcat (if (minusp momg3) "-" "+") (rtos (rastpr2 p qs3v g3ver))"*"(rtos g3)) 16 3 3)
  (ex_put momg3 17 3 3)
  (ex_put (strcat (if (minusp momg2) "-" "+") (rtos (rastpr2 p qdv g2ver))"*"(rtos g2)) 16 4 3)
  (ex_put momg2 17 4 3)
  (ex_put (strcat (if (minusp momsf2) "-" "+") (rtos (rastpr2 p sf2_niz sf2_verg))"*"(rtos sf2 2 2)) 16 5 3)
  (ex_put momsf2 17 5 3)
  (ex_put (strcat (if (minusp momsf3) "-" "+") (rtos (rastpr2 p sf3_niz sf3_verg))"*"(rtos sf3 2 2)) 16 6 3)
  (ex_put momsf3 17 6 3)
  (ex_put (strcat (if (minusp momsf4) "-" "+") (rtos (rastpr2 p qdv sf4ver))"*"(rtos sf4 2 2)) 16 7 3)
  (ex_put momsf4 17 7 3)
  (ex_put (strcat (if (minusp momsf5) "-" "+") (rtos (rastpr2 p qzv5v sf5ver))"*"(rtos sf5 2 2)) 16 8 3)
  (ex_put momsf5 17 8 3)
  (ex_put (strcat (if (minusp momsils) "-" "+") (rtos (rastpr2 p qdv fsopver))"*"(rtos sils)) 16 9 3)
  (ex_put momsils 17 9 3)
  ;-----=============������� ���������������� ����========--------
  (setq fur (/ (+ momg5 momg3 momg4 momg2 momsf3 momsf2 momsf5 momsf4 momsils)
	       (distance p qav)
	    );_end of /
  );_end of setq
  (if (< fur 0)
    (risskor qav (polar qav (- (angle qav p) (/ pi 2)) 20) nil "F^y")
    (risskor qav (polar qav (+ (angle qav p) (/ pi 2)) 20) nil "F^y")
  );_end of if
  (princ)
);end of defun guk




;                           � � � � �     � � � � � �                             ;
;    � � � � � � � � � �     � � � � �    � � � � � �    �    � � � � � �   � � � ;
;**********************************************************************************
;++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


;----------------------------------------------------------------------------------
;                             � � � � � �      4 - 5                              ;
;==================================================================================
(defun grup45 (/ max45 ugol
	      d_new r34n_ver r34t_ver r05_ver
	      momg5 momsf5 momsfc;|������ � ���� ��������� �������������|;
	      reakcii45)
  ;������ 5 �������� 4
  (setq
    ;����� ���������� ��� � ����� ���
    g4_ver (polar p (/ pi -2) 20)
    g5_ver (polar p (/ pi -2) 32)
    sf4_ver (polar p (angle da p) 20)
    sf5_ver (polar p (angle zv5a p) 20)
    fsop_ver (polar p (/ pi 2) 30)
    r05_ver (polar p (/ pi 2) 20)
    ;������� R05
    r05 g5
    r45 sf5
  );_end of setq
  (chergrup45)
  (princ)  
);_end of defun

;-----=========������ �������������� ���� ����� DF � ������ F===========-----------
(defun chergrup45 (/ gde) 
  (setq gde (getpoint "\n������� ��� ������� ���� DF ��� �������� ������� "))
  (command "._ucs" gde "")  
  (risskor p g4_ver nil "G^4")
  (risskor p g5_ver nil "G^5")
  (risskor p sf4_ver nil "�^4")
  (risskor p sf5_ver nil "�^5")
  (risskor p r05_ver nil "R^05")
  (if (not (zerop sils))
    (risskor p fsop_ver nil "F^C")
  );_ if
  (plan45)
  (princ)
);_end of defun
;             � � � �    � � �   � � �    � � � � � �    4  - 5              ;
;                                � � � � � � �                               ;
(defun plan45 (/ max45
	      bs45 cs45 ds45 es45)
  ;------====================���� ��� ��� ������ 4-5===================------------
  (setq max45 (getreal "\n������� ����� ����� ������� ���� �� ����� [��] ")
	muf45 (/ (max sils sf5 g5 g4 sf4) max45)
  );_end of setq
  (if (zerop sils)
    ;��������� ����� ��� ���� ���� ������������� ����� 0: G5+Sf5+R05+Sf4+G4+R34=0
    (setq bs45 (polar p (/ pi -2) (/ g5 muf45))                   ;G5
	  cs45 (polar bs45 (angle zv5a p) (/ sf5 muf45))          ;Sf5
	  ds45 (polar cs45 (/ pi 2) (/ r05 muf45))		  ;R05
	  es45 (polar ds45 (angle da p) (/ sf4 muf45))		  ;Sf4
	  fs45 (polar es45 (/ pi -2) (/ g4 muf45))		  ;G4
	  ugolr34 (angle p fs45)
	  r43 (* muf45 (distance p fs45))
    );_end of setq
    ;��������� ����� ��� ���� ���� ������������� ������� �� 0: G5+Sf5+R05+Sf4+G4++Fc+R34=0
    (setq bs45 (polar p (/ pi -2) (/ g5 muf45))                   ;G5
	  cs45 (polar bs45 (angle zv5a p) (/ sf5 muf45))          ;Sf5
	  ds45 (polar cs45 (/ pi 2) (/ r05 muf45))		  ;R05
	  es45 (polar ds45 (angle da p) (/ sf4 muf45))		  ;Sf4
	  fs45 (polar es45 (/ pi -2) (/ g4 muf45))		  ;G4
	  gs45 (polar fs45 (/ pi 2) (/ sils muf45))		  ;Fc
	  ugolr34 (angle p gs45)
	  r43 (* muf45 (distance p gs45))
    );_end of setq
  );_end of if
  (cherplan45)  
  (princ)
);_end of defun
;            � � � �     � � �   � � �    � � � � � �    4  - 5              ;
;                                � � � � � � � �                             ;
(defun cherplan45 (/ gde)
  ;------=========������ ���� ��� ��� ������ 4-5=====--------
  (setq gde (getpoint "\n������� ��� ������� ���� ��� ���� 4-5 "))
  (command "._ucs" gde "")
  (if (zerop sils)
    (progn        
        (risskor p bs45 "b" "G^5")
  	(risskor bs45 cs45 "c" "�^5")
  	(risskor cs45 ds45 "d" "R^05")
        (risskor ds45 es45 "e" "�^4")
        (risskor es45 fs45 "f" "G^4")
        (risskor fs45 p "a" "R^24")     
    );_end of progn
    (progn
        (risskor p bs45 "b" "G^5")
  	(risskor bs45 cs45 "c" "�^5")
  	(risskor cs45 ds45 "d" "R^05")
        (risskor ds45 es45 "e" "�^4")
        (risskor es45 fs45 "f" "G^4")
        (risskor fs45 gs45 "g" "F^C")
        (risskor gs45 p "a" "R^24")
        
    );_end of progn
  );_end of if
  (ex_put r43 14 1 3)
  (ex_put g5 14 2 3)
  (ex_put Sf5 14 3 3)  
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���: ")
	   (strcat "\\A1;\\H2.6;\U+03BC";mu
		   "\\H0.7x;\\S^F;";index = F
		   "\\H2.6;="
		   (rtos muf45)					    
		   " \\H0.7x;\\S�/��;"
	   );strcat
  );_ add_tex
  (princ)
);_end of defun

;----------------------------------------------------------------------------------
;                             � � � � � �      2 - 3                              ;
;==================================================================================

(defun grup23 (/ sf2_ver sf3_ver reakcii23
	       r03n_ver r12n_ver
	       sf2sh_n sf3sh_n sf2sh_ver sf3sh_ver
	       r12t_ver r03t_ver)
  ;������ 3 � 2
  (setq
    ;���� �������
    sf3_ver (polar s3 (angle s3a p) 20)
    sf2_ver (polar d (angle da p) 20)
    ;�������
    r03n_ver (polar c (angle b c) 20)
    r12n_ver (polar a (angle b a) 20)
  );_end of setq
  ;����������� ����� ���������� ��� �� '
  (if
    (clockwise-p c b (polar b (angle bcn ba) 20))
    (setq ugol_sh3 (+ (angle s3a p) (/ pi 2)))
    (setq ugol_sh3 (- (angle s3a p) (/ pi 2)))
  );_end of if
  (setq sf3sh_n (polar s3 ugol_sh3 mh3)  
 	sf3sh_ver (polar sf3sh_n (angle s3a p) 20)
	toch_s3 (inters
		  c b
		  sf3sh_n sf3sh_ver
		  nil
		);_end of inters
  );_end of setq
  
  (if
    (clockwise-p a b (polar b (angle ban ba) 20))
    (setq ugol_sh2 (+ (angle da p) (/ pi 2)))
    (setq ugol_sh2 (- (angle da p) (/ pi 2)))
  );_end of if
  (setq sf2sh_n (polar d ugol_sh2 mh2)  
  	sf2sh_ver (polar sf2sh_n (angle da p) 20)
	toch_s2 (inters
		  a b
		  sf2sh_n sf2sh_ver
		  nil
		);_end of inters
  );_end of setq
  
  ;����������� �������������� ������������
  ;--------==========R03t========--------- 
  (setq r03t (/ (+ (moment g3 s3 (polar s3 (/ pi -2) 20) b)
		   (moment sf3 sf3Sh_n sf3sh_ver b)
		)
	        (distance b c)
	     );_end of /
  );_end of setq
  (if (< r03t 0)
    (setq r03t_ver (polar c (- (angle c b) (/ pi 2)) 20))
    (setq r03t_ver (polar c (+ (angle c b) (/ pi 2)) 20))
  );_end of if
  ;--------==========R12t========---------
  (setq r12t (/ (+
		  (moment g2 d (polar d (/ pi -2) 20) b)
		  (moment sf2 sf2sh_n sf2sh_ver b)
		  (moment r43 d (polar d ugolr34 20) b)
		);_end of +
	        (distance a b)
	     );_end of /

  );_end of setq
  (if (< r12t 0)
    (setq r12t_ver (polar a (- (angle a b) (/ pi 2)) 20))
    (setq r12t_ver (polar a (+ (angle a b) (/ pi 2)) 20))
  );_end of if

  
  (chergrup23)

  (princ)
);_end of defun grup23

(defun chergrup23 (/ gde)  
  ;----=======���� ����� CD(3) � ����� AB(2)==========-----
  (setq gde (getpoint "\n������� ��� ������� ���� DC BA ��� �������� ������� "))
  (command "._ucs" gde "")
  (li (list a b c))  
  ;���� �������
  (risskor d sf2_ver nil "�^2")
  (risskor s3 sf3_ver nil "�^3")
  ;���������� �������
  (risskor c r03n_ver nil "Rn^03")
  (risskor a r12n_ver nil "Rn^12")
  ;���� �������
  (risskor d (polar d (/ pi -2) 20) nil "G^2")
  (risskor s3 (polar s3 (/ pi -2) 20) nil "G^3")
  ;������� R43
  (risskor d (polar d ugolr34 20) nil "R^42") 
  ;���� �� '
  (risskor sf3sh_n sf3sh_ver nil "�'^3")
  (risskor sf2sh_n sf2sh_ver nil "�'^2") 
  ;������������� �������
  (risskor c r03t_ver nil "R\U+03C4^03")
  (risskor a r12t_ver nil "R\U+03C4^12")
  (plan23)
  (princ)
);_end of defun

(defun plan23 (/ max23
	       bs23 cs23 ds23 es23 fs23 gs23 is23 hs23)  
  ;-----======���� ��� 2======------------�������� �� ��������� �3+G3+�2+G2+R43+R12t+R12n+R03n+R03t=0
  (setq max23 (getreal "\n������� ����� ����� ������� ���� �� ����� ��� 2-3 ")
        muf23 (/ (max (abs r12t) (abs r03t) g2 g3 sf3 sf2 r43) max23) 
        bs23 (polar p (angle s3a p) (/ sf3 muf23))               ;�'3
	cs23 (polar bs23 (/ pi -2) (/ g3 muf23))                 ;G3
	ds23 (polar cs23 (angle da p) (/ sf2 muf23))             ;�'2
	es23 (polar ds23 (/ pi -2) (/ g2 muf23))                 ;G2
	fs23 (polar es23 ugolr34 (/ r43 muf23))                  ;R43
	gs23 (polar fs23 (angle a r12t_ver) (/ (abs r12t) muf23));R12t
	is23 (polar p (angle r03t_ver c) (/ (abs r03t) muf23))   ;R03t
	hs23 (inters is23
		     (polar is23 (+ (angle p is23) (/ pi 2)) 20)
		     gs23
		     (polar gs23 (+ (angle fs23 gs23) (/ pi 2)) 20)
	  nil);_end of inters
	r03 (*
	      (sqrt
		(+
		  (expt (distance is23 p) 2)
		  (expt (distance is23 hs23) 2)
		);_end of +
	      );_end of sqrt
	      muf23
	    );_end of *
	r12 (*
	      (sqrt
		(+
		  (expt (distance fs23 gs23) 2)
		  (expt (distance gs23 hs23) 2)
		);_end of +
	      );_end of sqrt
	      muf23
	    );_end of *
	Fuu (/ (moment r12 a (polar a (angle hs23 fs23) 20) p) oa)
  );_end of setq
  (cherplan23)
  (princ)
);_end of defun

;            � � � �     � � �   � � �    � � � � � �    2  - 3              ;
;                                � � � � � � � �                             ;
(defun cherplan23 (/ gde
		   ab_sf2 bc_g3 cd_sf3 de_g2 ef_r43 fg_r03t gh_r03n hi_r12n ip_r12t hp_r12 fh_r03)
  ;���� ��� � 2
  (setq gde (getpoint "\n������� ��� ������� ���� ��� ���� 2-3 "))
  (command "._ucs" gde "")
  (risskor p    bs23 "b" "�'^3"    )   ;�'3
  (risskor bs23 cs23 "c" "G^3"     )   ;G3
  (risskor cs23 ds23 "d" "�'^2"    )   ;�'2
  (risskor ds23 es23 "e" "G^2"     )   ;G2
  (risskor es23 fs23 "f" "R^42"    )   ;R42
  (risskor is23 p "a" "R\U+03C4^03")   ;R03t
  (risskor fs23 gs23 "g" "R\U+03C4^12");R12t
  (risskor gs23 hs23 "h" "Rn^12")      ;R12n
  (risskor hs23 is23 "i" "Rn^03")      ;R03n
  (risskor hs23 p nil "R^03")          ;R03
  (risskor fs23 hs23 nil "R^12")       ;R12
  (setq ugolr21 (angle hs23 fs23))

  (ex_put (abs r12t) 14 4 3)
  (ex_put (abs r03t) 14 5 3)
  
  (ex_put (* (distance hs23 gs23) muf23) 14 6 3);R12n
  (ex_put (* (distance hs23 is23) muf23) 14 7 3);R03n

  (ex_put (* (distance hs23 fs23) muf23) 14 8 3);R12
  (ex_put (* (distance hs23 p) muf23) 14 9 3);R03

  (ex_put (* (distance hs23 cs23) muf23) 14 10 3);R23
  
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���: ")
	   (strcat "\\A1;\\H2.6;\U+03BC";mu
		   "\\H0.7x;\\S^F;";index = F
		   "\\H2.6;="
		   (rtos muf23)			    
		   " \\H0.7x;\\S�/��;"
	   );strcat
  );_ add_tex
  (princ)
);_end of defun cherplan23

;----------------------------------------------------------------------------------
;                             � � � � � �      0 - 1                              ;
;==================================================================================

(defun grup01 (/ gde fu_ver)
  ;��������� �����
  (setq gde (getpoint "\n������� ��� ������� ������� ����� "))
  (command "._ucs" gde "")
  (li (list p a))
  (risskor a (polar a ugolr21 20) nil "R^21")
  (if (< (moment 10 a (polar a ugolr21 20) p) 0)
    (progn
      (setq fu_ver (polar a (- (angle a p) (/ pi 2)) 20))
      (risskor a (polar a (- (angle a p) (/ pi 2)) 20) nil "F^y")
    );_end of progn
    (progn
      (risskor a (polar a (+ (angle a p) (/ pi 2)) 20) nil "F^y")
      (setq fu_ver (polar a (+ (angle a p) (/ pi 2)) 20))
    );_end of progn
  );_end of if
  (plan01)
  (princ)
);_end of defun silivse

;���� ��� ��� ������ 0-1
(defun plan01 (/ gde maximum r21v fuuv reakcii01)
  (setq maximum (getreal "\n������� ����� ������������ ���� �� ����� 0-1: ")
	muf01 (/ (max (abs fuu) (abs r12)) maximum)
	r21v (polar p ugolr21 (/ (abs r12) muf01))
	fuuv (polar r21v (angle a fu_ver) (/ (abs fuu) muf01))
  );_end of setq
  (cherplan01)
  ;(close reakcii01)
  (princ)
);_end of defun

(defun cherplan01 (/ gde le)
  (setq gde (getpoint "\n������� ��� ������� ���� ��� ��� ������ 0-1: "))
  (command "._ucs" gde "")
  (risskor p r21v "b" "R^21")
  (risskor r21v fuuv "c" "F^y")
  (risskor fuuv p "a" "R^01")

  (ex_put (* muf01 (distance p fuuv)) 14 11 3);R01
  (ex_put (abs fuu) 14 12 3);Fy
  (ex_put (abs (* fuu loa)) 14 13 3);R01
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���: ") 
  	   (strcat "\\A1;\\H2.6;\U+03BC";mu
		   "\\H0.7x;\\S^F;";index = F
		   "\\H2.6;="
		   (rtos muf01)					    
		   " \\H0.7x;\\S�/��;"
	   );strcat
  );_ add_tex
  (princ)
);_end of defun
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;����� �������, ������� ������;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;�������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun c:qw (/ old le pogresh)
  (defun activex_start ()
    (vl-load-com)
    (setq document (vla-get-ActiveDocument (vlax-get-acad-object))
	  model (vla-get-ModelSpace document)
	  paper (vla-get-PaperSpace document)
    );_end of setq
  );_end of defun
  (activex_start)
  (ex_set_connect)
  (setq old (getvar "osmode"))
  (setvar "osmode" 20903)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "timesi.TTF" )))	
  )
  (entmod le)  
  (vvod)
  (tablic)
  (tochkim fi)  
  (plansk)
  (modulv)
  (planusk)  
  (sili)
  (grup45)
  (grup23)
  (grup01)
  (guk)  
  (setq pogresh (* 100 (/ (- (max (abs fuu) (abs fur))
			     (min (abs fuu) (abs fur))
			  );_end of -
		          (max (abs fuu) (abs fur))
		       );_end of /
		);_end of *
  );_nd of setq
  (alert (strcat "����������� �����\n"(rtos pogresh)))
  (ex_break_connect)
  (setvar "osmode" old)
  (vla-ZoomExtents (vlax-get-acad-object))
);_end of defun

(defun cosinus (t1 t2 t3 t4)
    (cos (- (angle t1 t2)
	    (angle t3 t4)
	 )
    );_ cos
);_ defun

(defun momentvit (/ polo os_x n_x max_mom fi
		  fs i)
  (setq p '(0 0)
	oa (ex_get 4 (1+ nomer_varianta))
	loa oa
	va (* omeg1 loa)
	MUV (/ va 70.)
	ab (ex_get 5 (1+ nomer_varianta))
	lab ab
	bc (ex_get 6 (1+ nomer_varianta))
	lbc bc
	fsop (ex_get 19 (1+ nomer_varianta))
	mom_mas nil
	os_x (getdist "\n����� ��� �����: ")
	mufi (/ pi os_x 0.5)
	n_x (getreal "\n���������� �������� [12]: ")
	shag_ugl (/ pi n_x 0.5)
	shag_os (/ os_x n_x)
	fi pi
	tekush_ug 0.
	fsop (ex_get 19 (1+ nomer_varianta))
	i 1
  );_ setq
  (li (list '(0 50) '(0 -500)))
  (repeat 13
    (tochkim fi)
    (plansk)
    (setq polo (- fi pi)
      	  sils (if (and (>= polo (/ pi 4)) (<= polo (* 0.5 pi)))
		 (- (* fi (/ (* 4. fsop) pi)) fsop)
		 0.
	       )
	  mdfs (* sils (/ (distance p dv) 70.) oa
		  (cosinus p (polar p (* 0.5 pi) 5) p dv)
	       )
	  mdg5 (* g5 oa (/ (distance p zv5v) 70.) (cosinus p zv5v p (polar p (/ pi -2) 5)))
	  mdg4 (* g4 oa (/ (distance p dv) 70.) (cosinus p dv p (polar p (/ pi -2) 5)))
	  mdg3 (* g3 oa (/ (distance p s3v) 70.) (cosinus p s3v p (polar p (/ pi -2) 5)))
	  mdg2 (* g2 oa (/ (distance p dv) 70.) (cosinus p dv p (polar p (/ pi -2) 5)))	  
		 
    ); setq
    (ex_put sils 3 i 1)
    (ex_put (/ (distance p dv) 70.) 4 i 1)
    (ex_put mdfs 5 i 1)
    (ex_put mdg2 6 i 1)
    (ex_put mdg3 7 i 1)
    (ex_put mdg4 8 i 1)
    (ex_put mdg5 9 i 1)
    (setq i (1+ i)
	  fi (+ fi (/ pi 6.))
    )
  ); repeat
  (setq fi pi)
  (repeat (fix (1+ n_x))
    (tochkim fi)
    (plansk)
    (setq polo (- fi pi)
      	  sils (if (and (>= polo (/ pi 4)) (<= polo (* 0.5 pi)))
		 (- (* fi (/ (* 4. fsop) pi)) fsop)
		 0.
	       )
	  mdfs (* sils (/ (distance p dv) 70.) oa
		  (cosinus p (polar p (* 0.5 pi) 5) p dv)
	       )
	  md_sum (+ mdfs)
		   ;mdg3)
	  mom_mas (cons (list tekush_ug md_sum) mom_mas)
	  tekush_ug (+ tekush_ug shag_os)
	  fi (+ fi shag_ugl)
    ); setq
  );_ repeat
  (setq	max_mom (car (vl-sort (mapcar '(lambda (p) (abs (cadr p))) mom_mas) '>))
	mumoment (/ max_mom (getdist "\n������� Ymax: "))
	mom_mas (mapcar '(lambda (lis)
			   	 (list (car lis) (/ (cadr lis) mumoment))
			 ) mom_mas)
  ); setq
)

;;;=========================================
;;;��������� ��� ������������ ��������������
;;;=========================================
;;;graf_list - ������ � ������������ �������
;;;��� ��������������
;;;r1, r2, r3 - ����� ��� ���������� ����� ��������������
;;;graf_int - ����� ������������������� �������
;;;s - ����� ��� ����������������� �������
;;;i - �������
(defun grafint (graf_list / r1 r2 r3
		i s mas_uglov linii)
  (setq h (getdist "\n������� H: ")
	r1 (polar (car graf_list) pi h)
	s (list (car (car graf_list)) (* -1 (getdist "\n������� �� ������� �������� ���� ������: ")))
	i 0
	graf_int (list s)
  ); setq
  (repeat (1- (LENGTH graf_list))
    (setq r2 (list (car (car graf_list))
		   (* (+ (cadr (nth i graf_list))
			 (cadr (nth (1+ i) graf_list))
		      ); +
		      0.5
		   ); *
	     ); list
	  r3 (polar r2 0 (* (+ (car (nth i graf_list))
			       (car (nth (1+ i) graf_list))
			    ); +
			    0.5
			 ); *
	     ); polar
	  s (inters (polar s (angle r1 r2) 100) s
		    (list (car (nth (1+ i) graf_list)) 0)
		    (list (car (nth (1+ i) graf_list)) 10)
		    nil
	    ); inters
	  graf_int (cons s graf_int)
	  linii (cons (list r1 r2 r3) linii)
	  i (1+ i)
    ); setq
  ); repeat
  (if (eq "y" (getstring "\n���������� ���������� ����� ��������������? [y/n]: "))
    (mapcar '(lambda (a) (li a))
	    linii
    )
  )
  (add_pl graf_int)
  (princ)
); grafint

(defun deltat (listik)
  (setq dett nil)
  (vl-every
    '(lambda (tochka)
       (setq
	 y (- (cadr tochka)
	      (+ (/ (* (- (car tochka) (car (car listik))); x-x1
		       (- (cadr (last listik)) (cadr (car listik))); y2-y1
		    ); *
		    (- (car (last listik)) (car (car listik))); x2-x1
	 	 ); /
		 (cadr (car listik))
	      ); +
	   ); -
	 dett (cons (list (car tochka) y) dett)	 
       );_ setq
     );_ lambda
    listik
  );_ mapcar
  (vl-cmdf "._ucs" (getpoint "\n������ ���������� �������: ") "")
  (add_pl dett)
)

(defun inercia (/ masshtab fi cv dv i
		omeg2 omeg3 mvd mvs3 mvz5v)
  (setq masshtab 1.;(getreal "\n��������� ����������� I: ")
	fi pi
	tekush_ug 0.
	mas_i nil
	i 1
  )
  (repeat 13
    (tochkim fi)
    (plansk)
    (modulv)
    (ex_put (+ (* i2 (expt (/ omeg2 omeg1) 2))
	       (* m2 (expt (/ mvd omeg1) 2))
	    )
	    11 i 1)
    (ex_put (+ (* i3 (expt (/ omeg3 omeg1) 2))
	       (* m3 (expt (/ mvs3 omeg1) 2))
	    )
	    12 i 1)
    (ex_put (* m4 (expt (/ mvd omeg1) 2))
	    13 i 1)
    (ex_put (* m5 (expt (/ mvz5v omeg1) 2))
	    14 i 1)
    (setq i (1+ i)
	  fi (+ fi (/ pi 6.))
    ); setq
  ); repeat
  (ex_put i1 18 "F" 1)
  (setq fi pi)  
  (repeat (length graf_int)
    (tochkim fi)
    (plansk)
    (modulv)
    (setq i_sumar (+ (* i2 (expt (/ omeg2 omeg1) 2))
		     i1
		     (* i3 (expt (/ omeg3 omeg1) 2))
		     (* m2 (expt (/ mvd omeg1) 2))
		     (* m3 (expt (/ mvs3 omeg1) 2))
		     (* m4 (expt (* oa (/ mvd 70.)) 2))
		     (* m5 (expt (/ mvz5v omeg1) 2))
		  ); +
	  i_sumar (* i_sumar masshtab)
	  mas_i (cons (list i_sumar tekush_ug) mas_i)
	  tekush_ug (+ tekush_ug shag_os)
	  fi (+ fi shag_ugl)
    ); setq
  ); repeat
  ;(add_pl mas_i)
); inercia

(defun tzvezda (listik / x y)
  (setq tzv nil)
  (vl-every '(lambda (tochka)
	       (setq x (cadr tochka)
		     y (* 0.5 omeg1 omeg1 (car tochka) (/ 1. muat))
		     tzv (cons (list x y) tzv)
	       )
	    )
	    listik
  ); vl-every
  (vl-cmdf "._ucs" (getpoint "\n������ T*: ") "")
  (add_pl tzv)
); defun tzvezda

(defun detzvez (listik1 listik2 / x y i)
  (setq detzv nil
	i 0
  )
  (repeat (length listik2)
    (setq x (car (nth i listik2))
	  y (- (cadr (nth i listik2)) (cadr (nth i listik1)))
	  detzv (cons (list x y) detzv)
	  i (1+ i)
    )
  ); repeat
  
)

(defun ex_maxovik ()
  (vl-load-com)
  (setq g_oex (vlax-get-or-create-object "Excel.Application"))
  (vlax-put-property g_oex "Visible" :vlax-false)
  (setq g_wkbs (vlax-get-property g_oex "Workbooks")
        g_wb_put (vla-open g_wkbs "D:\\�����������\\���\\���\\��������\\��������\\7\\Maxovik_.xlsx")
        g_wb_get (vla-open g_wkbs "D:\\�����������\\���\\���\\��������\\��������\\7\\Variants_7.xlsx")
        g_shs (vlax-get-property g_wb_put "Worksheets")
        g_shs_get (vlax-get-property g_wb_get "Worksheets")
  )
);defun ex_set_connect

(defun ex_break_maxovik ( / )  
    (vlax-invoke-method g_wb_put "SaveCopyAs" (strcat "D:\\�����������\\���\\���\\��������\\��������\\7\\Maxovik_"
					            (getstring "\n������� ��� ����� ��� ���������� " ) 
					            ".xlsx"
				           );_ strcat
    ); vlax-invoke-method
  (vlax-invoke-method g_wb_put "Close" :vlax-false :vlax-false)
  (vlax-invoke-method g_wb_get "Close" :vlax-false :vlax-false)
  (vlax-invoke-method g_oex "Quit")
; ����������� �������, ��������� � Excel,
; ��� ���������� �������� Excel �� ������
  (mapcar
    (function (lambda (x)
      (if
        (and x (not (vlax-object-released-p x)))
        (vlax-release-object x)
      );_ if
              );_ lambda
    );_ function
    (list g_cell	  
	  g_shs g_shs_get
	  g_wb_put g_wb_get
	  g_wkbs
	  g_oex)
  )
  ; ������ ������
  (setq g_cell nil	
        g_shs nil g_shs_get nil
        g_wb_get nil g_wb_put nil
        g_wkbs nil
        g_oex nil
  )
  (gc)
);defun ex_break_connect

(defun c:as (/ ymax ymin ff1 i0 i3 m3 m5 g2 g3 g4 g5 pv
	     omeg1 oa ab bc lab lbc muv va)  
  (setq pv 70.
	
        nomer_varianta (getint "\n������� ����� ��������: ")
  )
  (ex_maxovik)
  (ex_put nomer_varianta 2 "O" 1)
  (setq	i2 (ex_get 13 (1+ nomer_varianta))
	i1 (ex_get 17 (1+ nomer_varianta))
	i3 (ex_get 14 (1+ nomer_varianta))
	m2 (ex_get 8 (1+ nomer_varianta))
	g2 (* m2 9.8)
	m3 (ex_get 9 (1+ nomer_varianta))
	g3 (* m3 9.8)
	m4 (ex_get 10 (1+ nomer_varianta))
	g4 (* m4 9.8)
	m5 (ex_get 11 (1+ nomer_varianta))
	g5 (* m5 9.8)
	omeg1 (/ (* pi (ex_get 15 (1+ nomer_varianta))) 30)	
  )  
  (momentvit)
  (add_pl mom_mas)
  (add_tex (getpoint "\n������� ��������: ")
	   (strcat "\\A1;\\H3.0;\U+03BC"
		   "\\H0.7x;\\S^M;"
		   "\\H3.0;="
		   (rtos mumoment)
		   " \\H0.7x;\\S��/��;"
	   );strcat
  )
  (add_tex (getpoint "\n������� �����: ")
	   (strcat "\\A1;\\H3.0;\U+03BC"
		   "\\H0.7x;\\S^\U+03C6;";index = fi
		   "\\H3.0;="
		   (rtos mufi)
		   " \\H0.7x;\\S���/��;"
	   );strcat
  )
  (grafint (reverse mom_mas))
  (setq muat (* h mufi mumoment))
  (add_tex (getpoint "\n������� �����: ")
	   (strcat "\\A1;\\H3.0;\U+03BC"
		   "\\H0.7x;\\S^A;";index = A
		   "\\H3.0;="
		   (rtos muat)
		   " \\H0.7x;\\S��/��;"
	   );strcat
  )
  (deltat (reverse graf_int))
  (inercia)
  (tzvezda mas_i)
  (detzvez tzv (reverse dett))
  (add_pl detzv)
  (setq ymax (car (vl-sort (mapcar '(lambda (p) (cadr p)) detzv) '>))
	ymin (car (vl-sort (mapcar '(lambda (p) (cadr p)) detzv) '<))
	ff1 (abs (- ymax ymin))
	imaxov (/ (* ff1 muat)
		  (* omeg1 omeg1 (ex_get 20 (1+ nomer_varianta)))
	       )
  )
  (ex_put ff1 18 "C" 1)
  (ex_put muat 18 "D" 1)
  (ex_put omeg1 18 "B" 1)
  (ex_put (ex_get 20 (1+ nomer_varianta)) 18 "A" 1)
  (alert (strcat "Imax= " (rtos imaxov)))
  (ex_break_maxovik)
  ;(vitten)
)