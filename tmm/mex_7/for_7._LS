;----------------------------------------------------------------------------------
;==============� � � � � � � � � � � � � �    � � � � � �==========================
;--==������� ����� ����� ��������� � ���������� ��������� � ������ ���������===----
(defun plansk (p pv ugol / omeg2 omeg3 omeg4 omeg5)
  (tochkim p ugol)
  ;======����� ����� ���������========  
  (setq av (polar p (+ (angle p a) (/ pi 2)) pv)
	bv (peres a b c p av)
	dv (polar av (angle av bv) (* (distance bv av) 0.5))
	s3v (polar p (angle p bv) (* (distance bv p) 0.5))
	zv5v (inters
	       p (polar p 0 100)
	       dv (polar dv (/ pi 2) 5)
	       nil
	     )
	mvb (* muv (distance p bv))
	mvd (* muv (distance p dv))
	mvba (* muv (distance av bv))
	mvs3 (* muv (distance p s3v))
	mvz5v (* muv (distance p zv5v))
	mvd4d5 (* muv (distance dv zv5v))
	;������� ��������
	omeg2 (/ mvba lab)
	omeg3 (/ mvb lbc)	
	;=====���������� ���������=====
	aban (/ (expt (* (distance av bv) muv) 2) lab)
	abcn (/ (expt (* (distance p bv) muv) 2) lbc)
  ); setq
);_end of defun

;;;;;;---=================������� ���������� ����� ���������================-------
(defun tochkim (o ugol)
  (setq
    c (list (- (car o) ab) (+ (cadr o) (sqrt (- (* bc bc) (* oa oa)))))    
    a (polar o ugol oa)
    b (perokr a c ab bc nil)
    d (polar a (angle a b) (/ ab 2.0))    
    s3 (polar c (angle c b) (/ bc 2))
  )
)

;----------------------------------------------------------------------------------
;---------========������� ����� ����� ��������� � ������ ���������========---------
(defun planusk (p pa ugol)
  (plansk p 70. ugol)
  (setq ;=====����� �������� ���������� ���������=====
	dban (/ aban mua)
	dbcn (/ abcn mua)	
	;=====����� ����� ���������=====
	aa (polar p (angle a p) pa)
	ban (polar aa (angle b a) dban)
	bcn (polar p (angle b c) dbcn)
	ba (peres a b c bcn ban)
	da (polar aa (angle aa ba) (* (distance aa ba) 0.5))
	s3a (polar ba (angle ba p) (* (distance ba p) 0.5))
	zv5a (inters
	       p (polar p 0 100)
	       da (polar da (/ pi 2) 5)
	       nil
	     )
	mab (* mua (distance p ba))
	mabat (* mua (distance ban ba))
	mabct (* mua (distance bcn ba))
	mad (* mua (distance p da))
	mas3 (* mua (distance p s3a))
	mazv5a (* mua (distance p zv5a))
  );_end of setq
);_end of defun

;�������� �������������� ��� ������� �1 �������� ��������� ������
(defun vvod (/ nom_var)
  (initget 5)
  (setq nom_var (1+ (getint "\n������� ����� ��������: ")))
  (initget 7)
  (setq oa (getdist "\n������� O� [��]: "))
  (exel_get)
  (initget 1)
  (setq fi (+ (* (getreal "\n������� ��������� � ����: ") (/ pi 180.)) fi))
);end of vvod

(defun exel_get (/ g_oex g_wkbs g_wb_get g_shs_get g_sheet f)
  (setq g_oex (vlax-get-or-create-object "Excel.Application"))
  (vlax-put-property g_oex "Visible" :vlax-false)
  (setq g_wkbs (vlax-get-property g_oex "Workbooks")        
        g_wb_get (vla-open g_wkbs (strcat total_adress "variants\\variants_7.xls"))        
        g_shs_get (vlax-get-property g_wb_get "Worksheets")
	g_sheet (vlax-get-property g_shs_get "Item" 1)
	f (ex_get 21 nom_var)
        n1 (ex_get 15 nom_var)
        loa (ex_get 4 nom_var)
        mul (/ loa oa)
        lab (ex_get 5 nom_var)
        ab (/ lab mul)	
        lbc (ex_get 6 nom_var)
        bc (/ lbc mul)	



	m2 (ex_get 8 nom_var)
	m3 (ex_get 9 nom_var)
	m4 (ex_get 10 nom_var)
	m5 (ex_get 11 nom_var)
	sils (ex_get 19 nom_var)
	i2 (/ (* m2 lab lab) 12.0)
	i3 (/ (* m3 lbc lbc) 12.0)
		
        fi (* f (/ pi 180.))
	omeg1 (/ (* pi n1) 30)	
	va (* omeg1 loa)	
	aan (* omeg1 omeg1 loa)
	;k_nerav (ex_get 22 nom_var)
  )
  (vlax-invoke-method g_wb_get "Close" :vlax-false :vlax-false)
  (vlax-invoke-method g_oex "Quit")
  (mapcar
    (function (lambda (x)
      (if
        (and x (not (vlax-object-released-p x)))
        (vlax-release-object x)
      );_ if
              );_ lambda
    );_ function
    (list g_shs_get
	  g_wb_get
	  g_wkbs g_sheet
	  g_oex)
  )  
  (gc)  
)


(defun cherm (o /)
  (li (list o a b c))
  (add_text a "A")(add_text b "B")(add_text d "D")
  (mapcar '(lambda (x) (add_circ 0.7 x))(list o a b c d s3))
)

;---==========================������ ���� ���������=======================---------
(defun cherv (p /)
  (risskor p av "a" "v^A")
  (risskor av bv nil "v^BA")
  (risskor p bv "b" "v^B")
  (risskor p dv "d" "v^D")
  (risskor p s3v "s^3" "v^S3")
  (risskor p zv5v "d^5" "v^D5")
  (risskor zv5v dv nil "v^D4D5")
  (add_text p "p,o,c")
);_end of defun

;-----=========================������ ���� ���������=======================--------
(defun chera (p / i)
  (risskor p aa "a" "a^A")
  (risskor aa ban nil "an^BA")
  (risskor p bcn nil "an^BC")
  (risskor bcn ba nil "a\U+03C4^BC")
  (risskor ban ba nil "a\U+03C4^BA")
  (risskor p ba "b" "a^B")
  (risskor p da "d" "a^D") 
  (risskor p zv5a "d^5" "a^D5")
  (risskor zv5a da nil "a^D4D5")
  (risskor p s3a "s^3" "a^S3")
  (li (list ba aa))
  (add_tex p "\\A1;p\\H0.7x;\\S^1;\\H1.286x;,o,c" nil nil) 
)

(defun excel_export (/ g_oex g_wkbs g_wb_put g_shs_put g_sheet1 g_sheet2
		     fii i)
  (setq g_oex (vlax-get-or-create-object "Excel.Application"))
  (vlax-put-property g_oex "Visible" :vlax-false)  
  (setq g_wkbs (vlax-get-property g_oex "Workbooks")        
        g_wb_put (vla-open g_wkbs (strcat total_adress "blanks\\first_list_7.xls"))        
        g_shs_put (vlax-get-property g_wb_put "Worksheets")
	g_sheet1 (vlax-get-property g_shs_put "Item" 1)
	g_sheet2 (vlax-get-property g_shs_put "Item" 2)
  )
  (ex_put loa 2 "B" g_sheet1)
  (ex_put lab 2 "C" g_sheet1)
  (ex_put lbc 2 "D" g_sheet1)
  
  (ex_put oa 3 "B" g_sheet1)
  (ex_put n1 4 "B" g_sheet1)
  (ex_put (* fi (/ 180. pi)) 8 "B" g_sheet1)
  (setq fii pi
	i 2
  )
  (ex_put muv 3 "O" g_sheet2)
  (repeat 12    
    (plansk '(0 0) vektorva fii)
    (ex_put (distance av bv) 3 i g_sheet2)
    (ex_put (distance '(0 0) bv) 4 i g_sheet2)
    (ex_put (distance '(0 0) dv) 5 i g_sheet2)
    (ex_put (distance '(0 0) s3v) 6 i g_sheet2)
    (ex_put (distance '(0 0) zv5v) 7 i g_sheet2)
    (ex_put (distance zv5v dv) 8 i g_sheet2)

    (setq fii (+ fii (/ pi 6.))
	  i (1+ i)
    )
  ); repeat
;;;  (planusk (setq polyc '(0 0)) vektoraa (- (* 0.5 pi) fi))
;;;  (ex_put mua 24 "A" g_sheet2)
;;;  (ex_put (distance polyc ba) 22 "B" g_sheet2)
;;;  (ex_put (distance polyc da) 22 "C" g_sheet2)
;;;  (ex_put (distance polyc fa) 22 "D" g_sheet2)
;;;  (ex_put (distance polyc ea) 22 "E" g_sheet2)
;;;  (ex_put (distance aa ba) 22 "F" g_sheet2)
;;;  (ex_put (distance ba ea) 22 "H" g_sheet2)  
;;;  (ex_put (/ mabat lab) 24 "B" g_sheet2)
;;;  (ex_put (/ mabct lbc) 24 "C" g_sheet2)
;;;  (ex_put (/ maebt lbe) 24 "D" g_sheet2)
;;;  (ex_put (/ maekt lek) 24 "E" g_sheet2)
  (vlax-invoke-method g_wb_put "SaveCopyAs" (getfiled "������� ��� ��������� ���� � ��������" "c:\\" "xls" 129))
  (vlax-invoke-method g_wb_put "Close" :vlax-false :vlax-false)
  (vlax-invoke-method g_oex "Quit")
  (mapcar
    (function (lambda (x)
      (if
        (and x (not (vlax-object-released-p x)))
        (vlax-release-object x)
      );_ if
              );_ lambda
    );_ function
    (list g_shs_put
	  g_wb_put
	  g_wkbs g_sheet1 g_sheet2
	  g_oex)
  )  
  (gc) 
)
