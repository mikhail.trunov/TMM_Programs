;----------------------------------------------------------------------------------
;=============� � � �    � � � �   �   � � � � � � � �   � � � � � � �=============
(defun sili (/ msf2 msf4
	     h2 h4  polog1 polog2
	    )  
  (setq polog1 (/ (distance (list 0. (+ oa ab)) b) (* 2. oa))
	polog2 (/ (distance (list 0. (+ oa ab)) d) (* 2. oa))
    	p1 (if (equal (* 1.5 pi) (angle '(0 0) bv))
	     (if (< polog1 0.2)
	       (* p1max (* (/ 1. (- 1. (expt 500. -0.2))) (- (expt 500. (* -1 polog1)) (expt 500. -0.2))))
	       0.
	     )
	     (if (> polog1 0.2)
	       (* p1max  (/ 1. (- (expt 50. -0.2) 0.02)) (- (expt 50. (* -1. polog1)) 0.02))
	       p1max
	     )
	   )
	p2 (if (equal (* 1.5 pi) (angle '(0 0) dv))
	     (if (< polog2 0.2)
	       (* p2max (expt (/ p1max p2max) (* 5. polog2)))
	       p1max
	     )
	     (if (> polog2 0.2)
	       (* p2max (+ (* 1.5625 (- 1. (/ p1max p2max)) (expt polog2 2.)) (* polog2 3.125 (- (/ p1max p2max) 1.)) (- 1.5625 (* polog2 0.5625))))
	       p2max
	     )
	   )
    	sils1 (* p1 1e6 pi 0.25 ld1 ld1)
	sils2 (* p2 1e6 pi 0.25 ld2 ld2)
	msf2 (* eps2 i24)
	msf4 (* eps4 i24)
	sf3 (* m3 mab)
	sf5 (* m5 mad)
	sf2 (* m24 mas2)
	sf4 (* m24 mas4)
	h2 (/ msf2 sf2)
	mh2 (/ h2 mul)
	h4 (/ msf4 sf4)
	mh4 (/ h4 mul)
  );_end of setq
);_end of defun

(defun new_funcia_din (/ sils1 sils2)  
  (setq fi (- (* pi 0.5) fi (* (/ pi 180.) (getreal "\n������� ��������� � ���������� ���� [����]: ")))
	muv (/ va 70.)
	mua (/ aan 100.)
	polus '(0 0)
  )
  (planusk polus 100. fi)
  (sili)
  (grup45)
  (grup23)
  (grup01)
  (guk)  
)

(defun excel_export ()
  (setq g_oex (vlax-get-or-create-object "Excel.Application"))
  (vlax-put-property g_oex "Visible" :vlax-false)  
  (setq g_wkbs (vlax-get-property g_oex "Workbooks")        
        g_wb_put (vla-open g_wkbs (strcat total_adress "blanks\\din_list_8.xls"))        
        g_shs_put (vlax-get-property g_wb_put "Worksheets")
	g_sheet1 (vlax-get-property g_shs_put "Item" 1)	
  )
  (ex_put ld1 2 "L" g_sheet1)
  (ex_put ld2 2 "M" g_sheet1)
  (ex_put p1 2 "J" g_sheet1)
  (ex_put p2 2 "K" g_sheet1)
  (ex_put m24 2 "F" g_sheet1)  
  (ex_put m3 2 "G" g_sheet1)
  (ex_put m5 2 "I" g_sheet1)  
  (ex_put mas2 3 "F" g_sheet1)
  (ex_put mab 3 "G" g_sheet1)
  (ex_put mas4 3 "H" g_sheet1)
  (ex_put mad 3 "I" g_sheet1)
  (ex_put muf45 8 "G" g_sheet1)
  (ex_put muf23 8 "M" g_sheet1)
  (ex_put muf01 8 "Q" g_sheet1)
  (ex_put eps2 2 "P" g_sheet1)
  (ex_put i24 4 "N" g_sheet1)
  (ex_put loa 10 "P" g_sheet1)
  (ex_put (abs r14t) 8 "E" g_sheet1)
  (ex_put r05 8 "B" g_sheet1)
  (ex_put r14n 8 "D" g_sheet1)
  (ex_put r54 8 "F" g_sheet1)
  (ex_put (abs r12t) 8 "K" g_sheet1)
  (ex_put r12n 8 "J" g_sheet1)
  (ex_put r03 8 "H" g_sheet1)
  (ex_put r01 8 "N" g_sheet1)
  (ex_put r32 8 "L" g_sheet1)
  (ex_put (abs fuu) 8 "O" g_sheet1)
  
  (ex_put momg2g 12 "B" g_sheet1)
  (ex_put momg3g 12 "C" g_sheet1)
  (ex_put momg4g 12 "D" g_sheet1)
  (ex_put momg5g 12 "E" g_sheet1)

  (ex_put momsf2g 12 "F" g_sheet1)  
  (ex_put momsf3g 12 "G" g_sheet1)
  (ex_put momsf4g 12 "H" g_sheet1)
  (ex_put momsf5g 12 "I" g_sheet1)
  
  (ex_put momsils1g 12 "J" g_sheet1)
  (ex_put momsils2g 12 "K" g_sheet1)
  (ex_put pa_guk 13 "M" g_sheet1)
  (vlax-invoke-method g_wb_put "SaveCopyAs" (getfiled "������� ��� ��������� ���� � ��������" "c:\\" "xls" 129))
  (vlax-invoke-method g_wb_put "Close" :vlax-false :vlax-false)
  (vlax-invoke-method g_oex "Quit")
  (mapcar
    (function (lambda (x)
      (if
        (and x (not (vlax-object-released-p x)))
        (vlax-release-object x)
      );_ if
              );_ lambda
    );_ function
    (list g_shs_put
	  g_wb_put
	  g_wkbs g_sheet1 g_sheet2
	  g_oex)
  )  
  (gc) 		      
)

(defun c:din8 (/ le old polus r05 r14n r14t r12t r12n r03 r01 r54 r32 fuu
ld1 ld2 p1 p2 m24 m3 m5 mas2 mab mad mas4 i24 eps2 eps4 loa lab
momg5g momg4g momg3g momg2g momsf5g momsf4g momsf3g momsf2g momsils1g momsils2g pa_guk)
  (vl-load-com)
  (load "funkcii_tmm.LSP")
  (load "tmm\\mex_8\\for_8.LSP")
  (setq old (getvar "osmode"))
  (setvar "osmode" 20903)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "timesi.TTF" )))	
  ); setq
  (entmod le)
  (vvod)
  (new_funcia_din)  
  (setvar "osmode" old)
  (vla-ZoomExtents (vlax-get-acad-object))
  (alert (strcat "�����������:\n"
		 (rtos (* (/ (- (max (abs fur) (abs fuu)) (min (abs fur) (abs fuu)))
			     (max (abs fur) (abs fuu))) 100))
	 )
  )
  (initget 1 "Yes No")  
  (if (equal "Yes" (getkword "\n������� � Excel? [Yes/No]: "))
    (excel_export)
  )
);_end of defun

;-----------------------------------------------------------------------------------
;                       � � � � � �    � � � � � �   4 - 5                         ;
;***********************************************************************************
;===================================================================================
(defun grup45 (/ r05_ver r14n_ver g5_ver g4_ver sf5_ver sf4_ver sf4sh_n sf4sh_ver)
  (setq ;����� ��� �������� ������ 4-5
	r05_ver (polar d 0 20)
	r14n_ver (polar c (angle d c) 20)
	g5_ver (polar d (/ pi -2.) 20)
	g4_ver (polar s4 (* pi -0.5) 20)
	sf5_ver (polar d (angle da polus) 20)
	sf4_ver (polar s4 (angle as4 polus) 20)
	f2_ver (polar d (* pi -0.5) 20) 	       
	;���� �� �������
    sf4sh_n (if (clockwise-p c d (polar d (angle dcn da) 5))
	      (polar s4 (+ (angle s4 sf4_ver) (/ pi 2)) mh4)
	      (polar s4 (- (angle s4 sf4_ver) (/ pi 2)) mh4)
	    );_end of if
    sf4sh_ver (polar sf4sh_n (angle as4 polus) 20)
    sf4_rich (inters
	       sf4sh_n sf4sh_ver
	       d c
	       nil
	     );_end of inters
    	mom_sf4 (moment sf4 sf4sh_n sf4sh_ver d)
    	momg4 (moment g24 s4 (polar s4 (/ pi -2) 20) d)    	
	r14t (/ (+ momg4 mom_sf4) ab)
	r14t_ver (if (minusp r14t)
	       (polar c (- (angle c d) (/ pi 2)) 20)
	       (polar c (+ (angle c d) (/ pi 2)) 20)
	     );_end of if
  );_end of setq
  (chergrup45)
);_end of defun

(defun chergrup45 (/ gde)
  (setq gde (getpoint "\n������� ��� ������� ������ 4-5: "))
  (command "._ucs" gde "")
  (li (list c d cil2))
  (add_circ 0.7 c)
  (add_circ 0.7 d)
  (add_tex c "C" nil nil)
  (add_tex d "D" nil nil)
  (risskor d r05_ver nil "R^05")
  (risskor s4 g4_ver nil "G^4")
  (risskor d g5_ver nil "G^5")
  (risskor c r14n_ver nil "Rn^14")
  (risskor c r14t_ver nil "R\U+03C4^14")
  (risskor d sf5_ver nil "�^5")
  (risskor s4 sf4_ver nil "�^4")
  (risskor sf4sh_n sf4sh_ver nil "�'^4")
  (risskor (polar d (* pi 0.5) 20) d nil "F^5")
  (plan45)
);_end of defun

(defun plan45 (/ max45
	       bs45 cs45 ds45 es45 fs45 gs45 hs45
	       )
;------====================���� ��� ��� ������ 4-5===================------------
  (setq max45 (getreal "\n������� ����� ����� ������� ���� �� ����� [��] ")
	muf45 (/ (max sils2 (abs r14t) sf5 g5 sf4 g24) max45)	
	;G5+�5+Fc+�'4+G4+R34t+R34n+R05=0
	bs45 (polar polus (/ pi -2) (/ g5 muf45))	     	 ;G5
	cs45 (polar bs45 (angle da polus) (/ sf5 muf45)) 	 ;�5
	ds45 (polar cs45 (* -0.5 pi) (/ sils2 muf45))		 ;Fc
	es45 (polar ds45 (angle as4 polus) (/ sf4 muf45))	 ;�'4
	fs45 (polar es45 (/ pi -2) (/ g24 muf45))		 ;G4
	gs45 (polar fs45 (angle c r14t_ver) (/ (abs r14t) muf45));R14t
	hs45 (inters
	       gs45 (polar gs45 (angle c d) 5);R14n
	       polus (polar polus (angle d r05_ver) 5)
	       nil
	     );_end of inters
	r14 (* muf45 (distance fs45 hs45))
	ang_r41 (angle hs45 fs45)
	r05 (* muf45 (distance hs45 polus))
	r14n (* muf45 (distance hs45 gs45))
	r54 (* muf45 (distance hs45 ds45))
  );_end of setq
  (cherplan45)
);_end of defun

(defun cherplan45 (/ gde)
  (setq gde (getpoint "\n������� ��� ������� ���� ��� ������ 4-5: "))
  (command "._ucs" gde "")
  (risskor polus bs45 "b" "G^5")
  (risskor bs45 cs45 "c" "�^5")
  (risskor cs45 ds45 "d" "F^C")
  (risskor ds45 es45 "e" "�'^4")
  (risskor es45 fs45 "f" "G^4")
  (risskor fs45 gs45 "g" "R\U+03C4^14")
  (risskor gs45 hs45 "h" "Rn^14")
  (risskor fs45 hs45 nil "R^14")
  (risskor hs45 polus "a" "R^05")
  (risskor hs45 ds45 nil "R^54")
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���: ") muf45 "F" "�/��")
);_end of defun
;-----------------------------------------------------------------------------------
;                       � � � � � �    � � � � � �   2 - 3                         ;
;***********************************************************************************
;===================================================================================
(defun grup23 (/ r05_niz r05_ver r34n_ver g5_ver sf5_ver)  
  (setq ;����� ��� �������� ������ 2-3
	r03_ver (polar b 0 20)
	r12n_ver (polar a (angle b a) 20)
	g3_ver (polar b (/ pi -2) 20)
	g2_ver (polar s2 (* pi -0.5) 20)
	sf3_ver (polar b (angle ba polus) 20)
	sf2_ver (polar s2 (angle as2 polus) 20)
	f1_ver (polar b (* pi -0.5) 20) 	       
	;���� �� �������
    sf2sh_n (if (clockwise-p a b (polar b (angle ban ba) 5))
	      (polar s2 (+ (angle s2 sf2_ver) (/ pi 2)) mh2)
	      (polar s2 (- (angle s2 sf2_ver) (/ pi 2)) mh2)
	    );_end of if
    sf2sh_ver (polar sf2sh_n (angle as2 polus) 20)
    sf2_rich (inters
	       sf2sh_n sf2sh_ver
	       b a
	       nil
	     );_end of inters

    	mom_sf2 (moment sf2 sf2sh_n sf2sh_ver b)
    	momg2 (moment g24 s2 (polar s2 (/ pi -2) 20) b)
    	;momsils2 (moment sils2 d f2_ver c)
	r12t (/ (+ momg2 mom_sf2) ab)
	r12t_ver (if (minusp r12t)
	       (polar a (- (angle a b) (/ pi 2)) 20)
	       (polar a (+ (angle a b) (/ pi 2)) 20)
	     );_end of if
  );_end of setq
  (chergrup23)
);_end of defun

(defun chergrup23 (/ gde)
  (setq gde (getpoint "\n������� ��� ������� ������ 2-3: "))
  (command "._ucs" gde "")
  (li (list a b cil1))
  (add_tex a "A" nil nil)
  (add_tex b "B" nil nil)
  (add_circ 0.7 a)
  (add_circ 0.7 b)
  (risskor b r03_ver nil "R^03")
  (risskor s2 g2_ver nil "G^2")
  (risskor b g3_ver nil "G^3")
  (risskor a r12n_ver nil "Rn^12")
  (risskor a r12t_ver nil "R\U+03C4^12")
  (risskor b sf3_ver nil "�^3")
  (risskor s2 sf2_ver nil "�^2")
  (risskor sf2sh_n sf2sh_ver nil "�'^2")
  (risskor (polar b (* pi 0.5) 20) b nil "F^3")
  (plan23)
);_end of defun

(defun plan23 (/ bs23 cs23 ds23 es23 fs23 gs23 hs23
	       )
;------====================���� ��� ��� ������ 4-5===================------------
  (setq muf23 (/ (max sils1 (abs r12t) sf3 g3 sf2 g24)
		 (getreal "\n������� ����� ����� ������� ���� �� ����� [��] ")
	      )	
	;R12t+�'2+G2+�3+G3+Fc+R03+R12n=0
	as23 (getpoint "\n��� ������� ���� ��� ��� ������ 2-3: ")
	bs23 (polar as23 (angle a r12t_ver) (/ (abs r12t) muf23));R12t
	cs23 (polar bs23 (angle as2 '(0 0)) (/ sf2 muf23))	 ;�'2
	ds23 (polar cs23 (/ pi -2.) (/ g24 muf23))		 ;G2
	es23 (polar ds23 (angle ba '(0 0)) (/ sf3 muf23)) 	 ;�3
	fs23 (polar es23 (/ pi -2.) (/ g3 muf23))	     	 ;G3
  )
  (if (zerop sils1)
    (setq gs23 (inters fs23 (polar fs23 0. 100.)                 ;R03
		       as23 (polar as23 (angle a b) 100.)        ;R12n
		       nil
	       ); inters
	  r12 (* muf23 (distance bs23 gs23))
	  r03 (* muf23 (distance fs23 gs23))
	  r12n (* muf23 (distance as23 gs23))
	  ang_r21 (angle bs23 gs23)
	  r32 (* muf23 (distance ds23 gs23))
    ); setq
    (setq gs23 (polar fs23 (/ pi -2.) (/ sils1 muf23))	     	 ;Fc1
	  hs23 (inters gs23 (polar gs23 0. 100.)                 ;R03
		       as23 (polar as23 (angle a b) 100.)        ;R12n
		       nil
	       ); inters
	  r12 (* muf23 (distance bs23 hs23))
	  r03 (* muf23 (distance gs23 hs23))
	  r12n (* muf23 (distance as23 hs23))
	  ang_r21 (angle bs23 hs23)
	  r32 (* muf23 (distance ds23 hs23))
    ); setq
  ); if
  (cherplan23)
);_end of defun

(defun cherplan23 (/)
  ;R12t+�'2+G2+�3+G3+Fc+R03+R12n=0
  (risskor as23 bs23 "b" "R\U+03C4^12")
  (risskor bs23 cs23 "c" "�'^2")
  (risskor cs23 ds23 "d" "G^2")
  (risskor ds23 es23 "e" "�^3")
  (risskor es23 fs23 "f" "G^3")
  (if (zerop sils1)
    (progn
      (risskor fs23 gs23 "g" "R^03")
      (risskor gs23 as23 "a" "Rn^12")
      (risskor gs23 bs23 nil "R^12")
      (risskor ds23 gs23 nil "R^32")
    ); progn
    (progn
      (risskor fs23 gs23 "g" "F^C3")
      (risskor gs23 hs23 "h" "R^03")
      (risskor hs23 as23 "a" "Rn^12")
      (risskor hs23 bs23 nil "R^12")
      (risskor ds23 hs23 nil "R^32")
    ); progn
  ); if
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���: ") muf23 "F" "�/��")
);_end of defun

;-----------------------------------------------------------------------------------
;                       � � � � � �    � � � � � �   0 - 1                         ;
;***********************************************************************************
;===================================================================================
(defun grup01 (/ 
	       r21_ver fy_ver)
  (setq ;����� ��� ������������ ������ � ���
	r21_ver (polar a ang_r21 20)
	r41_ver (polar c ang_r41 20)
	fuu (/ (+ (moment r12 a r21_ver polus)
		  (moment r14 c r41_ver polus)
	       )
	       oa
	    ); /
	fy_ver (if (minusp fuu)
	           (polar a (+ (angle (polar c (angle c a) oa) a) (/ pi 2)) 20)
		   (polar a (- (angle (polar c (angle c a) oa) a) (/ pi 2)) 20)
	       );_end of if
  );_end of setq
  (chergrup01)  
); defun

(defun chergrup01 (/ gde)
  (setq gde (getpoint "\n������� ��� ������� ������� ����� "))
  (command "._ucs" gde "")
  (li (list c a))
  (add_tex c "C" nil nil)
  (add_tex a "A" nil nil)
  (add_tex (polar a (angle a c) (* 0.5 (distance a c))) "O" nil nil)
  (add_circ 0.7 c)
  (add_circ 0.7 a)
  (add_circ 0.7 (polar a (angle a c) (* 0.5 (distance a c))))
  (add_opor (polar a (angle a c) (* 0.5 (distance a c))) pi)
  (risskor a r21_ver nil "R^21")
  (risskor c r41_ver nil "R^41")
  (risskor a fy_ver nil "F^y")
  (plan01)
);_end of defun

(defun plan01 (/ 
	       bs01 cs01 ds01)
  (setq muf01 (/ (max r12 r14) (getreal "\n������� ������������ ����� ���� �� ����� ��� 0-1 "))
	;R21+Fy+R41+R01=0
	bs01 (polar polus ang_r21 (/ r12 muf01))
	cs01 (polar bs01 (angle a fy_ver) (/ (abs fuu) muf01))
	ds01 (polar cs01 ang_r41 (/ r14 muf01))
	r01 (* muf01 (distance ds01 polus))
  );_end of setq
  (cherplan01)  
);_end of defun

(defun cherplan01 (/ gde le)
  (setq gde (getpoint "\n������� ��� ������� ���� ��� ��� ������ 0-1 "))
  (command "._ucs" gde "")
  (risskor polus bs01 "b" "R^21")
  (risskor bs01 cs01 "c" "F^y")
  (risskor cs01 ds01 "d" "R^41")
  (risskor ds01 polus "a" "R^01")
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���: ") muf01 "F" "�/��")
);_end of defun

;-----------------------------------------------------------------------------------
;                       � � � � �     � � � � � � � � � �                          ;
;***********************************************************************************
;===================================================================================
(defun guk (/ gde
	    qav12 qav3 qcv qdv qs3v qs3shv
	    p)
  ;������ ����� ������
  (initget 1 "Yes No") 
  (setq p '(0 0))
  (if (equal "Yes" (getkword "\n������� ������ �����? [Yes/No]: "))
      (setq qav (polar p (+ (angle p av) (* 0.5 pi)) (distance p av))
	    qcv (polar p (+ (angle p cv) (* 0.5 pi)) (distance p cv))
	    qbv (polar p (+ (angle p bv) (* 0.5 pi)) (distance p bv))
	    qdv (polar p (+ (angle p dv) (* 0.5 pi)) (distance p dv))
	    qs2v (polar p (+ (angle p s2v) (* 0.5 pi)) (distance p s2v))
	    qs4v (polar p (+ (angle p s4v) (* 0.5 pi)) (distance p s4v))
	    qs2shv (polar qbv (angle qbv qav) (* (distance qbv qav) (/ (distance b sf2_rich) ab)))
	    qs4shv (polar qdv (angle qdv qcv) (* (distance qdv qcv) (/ (distance d sf4_rich) ab)))
      )
      (setq qav (polar p (- (angle p av) (* 0.5 pi)) (distance p av))
	    qcv (polar p (- (angle p cv) (* 0.5 pi)) (distance p cv))
	    qbv (polar p (- (angle p bv) (* 0.5 pi)) (distance p bv))
	    qdv (polar p (- (angle p dv) (* 0.5 pi)) (distance p dv))
	    qs2v (polar p (- (angle p s2v) (* 0.5 pi)) (distance p s2v))
	    qs4v (polar p (- (angle p s4v) (* 0.5 pi)) (distance p s4v))
	    qs2shv (polar qbv (angle qbv qav) (* (distance qbv qav) (/ (distance b sf2_rich) ab)))
	    qs4shv (polar qdv (angle qdv qcv) (* (distance qdv qcv) (/ (distance d sf4_rich) ab)))
      )
  );end of if
  ;���������� ������ ����������
  (initget 65)
  (setq gde (getpoint "\n������� ����� ��� ������ ���������� "))
  (command "._ucs" gde "")
  (li (list qbv qdv qcv qav qbv))
  (li (list qs2v polus qs4v))  
  (add_tex qav "a" nil nil)
  (add_tex qbv "b" nil nil)
  (add_tex qcv "c" nil nil)
  (add_tex qdv "d" nil nil)
  (add_tex qs2v "\\A1;\\H2.2;s\\H0.7x;\\S^2;" nil nil)  
  (add_tex qs4v "\\A1;\\H2.2;s\\H0.7x;\\S^4;" nil nil)
  (add_tex polus "p,o,b" nil nil)
  ;������� ����� ��� �� ������
  (setq g2ver  (polar qs2v (* pi -0.5) 20)
	g3ver  (polar qbv (* pi -0.5) 20)
	g4ver  (polar qs4v (* pi -0.5) 20)
	g5ver  (polar qdv (* pi -0.5) 20)	
	sf2_verg (polar qs2shv (angle as2 polus) 20)
	sf4_verg (polar qs4shv (angle as4 polus) 20)		
	sf5ver (polar qdv (angle da polus) 20)
	sf3ver (polar qbv (angle ba polus) 20)
	fsop1_n (polar qbv (* pi 0.5) 20)
	fsop2_n (polar qdv (* pi 0.5) 20)
  );_end of setq
  ;-----====�������� ���, ����������� �� ��������=====-----
  (risskor qs2v g2ver  nil  "G^2")
  (risskor qs4v g4ver  nil  "G^4")
  (risskor qbv  g3ver  nil  "G^3")
  (risskor qdv  g5ver  nil  "G^5")
  (risskor qs2shv sf2_verg nil  "�'^2")
  (risskor qs4shv sf4_verg nil  "�'^4")
  (risskor qdv  sf5ver nil  "�^5")
  (risskor qbv  sf3ver nil  "�^3")
  (risskor fsop2_n qdv nil  "F^C5")
  (risskor fsop1_n qbv nil  "F^C3")
  ;------==========������� �������� �� ������============--------
  (setq momg5g (moment g5 qdv  g5ver polus)
	momg4g (moment g24 qs4v g4ver polus)
	momg3g (moment g3 qbv g3ver polus)
	momg2g (moment g24 qs2v g2ver polus)
	momsf3g (moment sf3 qbv sf3ver polus)
	momsf2g (moment sf2 qs2shv sf2_verg polus)
	momsf4g (moment sf4 qs4shv sf4_verg polus)
	momsf5g (moment sf5 qdv sf5ver polus)
	momsils1g (moment sils1 fsop1_n qbv polus)
	momsils2g (moment sils2 fsop2_n qdv polus)
  );_end of setq
  ;-----=============������� ���������������� ����========--------
  (setq fur (/ (+ momg4g momg5g
		  momg3g momg2g
		  momsf5g momsf4g
		  momsf3g momsf2g
		  momsils1g
		  momsils2g
		  )
	       (setq pa_guk (distance polus qav))
	    );_end of /
  );_end of setq
  (if (< fur 0)
    (risskor qav (polar qav (- (angle qav polus) (/ pi 2)) 20) nil "F^y")
    (risskor qav (polar qav (+ (angle qav polus) (/ pi 2)) 20) nil "F^y")
  );_end of if
);end of defun guk
(c:din8)