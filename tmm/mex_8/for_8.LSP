(defun tochkim (o ugol)
  (setq   
    a (polar o ugol oa)
    c (polar o (angle a o) oa)
    b (car (dwgru o (polar o (* 0.5 pi) 5) a ab))
    d (car (dwgru o (polar o (* 0.5 pi) 5) c ab))
    s2 (polar a (angle a b) (/ ab 3.))
    s4 (polar c (angle c d) (/ ab 3.))
    cil1 (polar b pi d1)
    cil2 (polar d 0 d2)
  )
); tochkim

;----------------------------------------------------------------------------------
;--==������� ����� ����� ��������� � ���������� ��������� � ������ ���������===----
(defun plansk (p pv ugols)
  (tochkim p ugols)
  (setq	av (polar p (- (angle p a) (/ pi 2)) pv)
	cv (polar p (+ (angle p a) (/ pi 2)) pv)
	bv (inters
	     av (polar av (+ (angle b a) (/ pi 2)) 5)
	     p (polar p (* 0.5 pi) 5)
	     nil
	   )
	dv (inters
	     cv (polar cv (+ (angle d c) (/ pi 2)) 5)
	     p (polar p (* 0.5 pi) 5)
	     nil
	   )	
	s2v (polar av (angle av bv) (/ (distance av bv) 3.))
	s4v (polar cv (angle cv dv) (/ (distance cv dv) 3.))

	mvb (* muv (distance p bv))	
	mvd (* muv (distance p dv))
	mvba (* muv (distance av bv))
	mvdc (* muv (distance dv cv))
	mvs2 (* muv (distance p s2v))
	mvs4 (* muv (distance p s4v))
	;========������� ��������====================
 	omeg2 (/ mvba lab)
	omeg4 (/ mvdc lab)

	aban (/ (* mvba mvba) lab)
	adcn (/ (* mvdc mvdc) lab)
  );_end of setq
);_end of defun

(defun planusk (p pa ugolu)
  (plansk p 70. ugolu)
  (setq ;=====����� �������� ���������� ���������=====
       	mua (/ aan pa)
	daban (/ aban mua)
	dadcn (/ adcn mua)	
	;=====����� ����� ���������=====
	aa (polar p (angle a p) pa)
	ca (polar p (angle c p) pa)
	ban (polar aa (angle b a) daban)
	dcn (polar ca (angle d c) dadcn)
	ba (inters
	     ban (polar ban (+ (angle b a) (* 0.5 pi)) 5)
	     p (polar p (* 0.5 pi) 5)
	     nil
	   )
	da (inters
	     dcn (polar dcn (+ (angle d c) (* 0.5 pi)) 5)
	     p (polar p (* 0.5 pi) 5)
	     nil
	   )
	as2 (polar aa (angle aa ba) (/ (distance aa ba) 3.))
	as4 (polar ca (angle ca da) (/ (distance ca da) 3.))
	mab (* mua (distance p ba))
	mad (* mua (distance p da))
	mabat (* mua (distance ban ba))
	madct (* mua (distance dcn da))
	mas2 (* mua (distance p as2))
	mas4 (* mua (distance p as4))
	;������� ���������
	eps2 (/ mabat lab)
	eps4 (/ madct lab)
  ); setq
);_end of defun

;�������� �������������� ��� �������
(defun vvod (/ nom_var)
  (initget 5)
  (setq nom_var (1+ (getint "\n������� ����� ��������: ")))
  (initget 7)
  (setq oa (getdist "\n������� O� [��]: "))
  (exel_get)
);end of vvod

(defun exel_get (/ g_oex g_wkbs g_wb_get g_shs_get g_sheet)
  (setq g_oex (vlax-get-or-create-object "Excel.Application"))
  (vlax-put-property g_oex "Visible" :vlax-false)
  (setq g_wkbs (vlax-get-property g_oex "Workbooks")        
        g_wb_get (vla-open g_wkbs  (strcat total_adress "variants\\variants_8.xls"))        
        g_shs_get (vlax-get-property g_wb_get "Worksheets")
	g_sheet (vlax-get-property g_shs_get "Item" 1)
	loa (ex_get 4 nom_var)
	n1 (ex_get 15 nom_var)	
        mul (/ loa oa)
	lab (ex_get 5 nom_var)
        ab (/ lab mul)	
	f (ex_get 22 nom_var)
	ld1 (ex_get 13 nom_var)
	d1 (/ ld1 mul)
	ld2 (ex_get 14 nom_var)
	d2 (/ ld2 mul)
        fi (* f (/ pi 180.))	     
	omeg1 (/ (* pi n1) 30)
	va (* omeg1 loa)
	aan (* omeg1 omeg1 loa)
	m3 (ex_get 8 nom_var)
	m24 (ex_get 7 nom_var)
	m5 (ex_get 9 nom_var)
	i1 (ex_get 11 nom_var)
	i24 (ex_get 12 nom_var)
	p1max (ex_get 19 nom_var)
	p2max (ex_get 20 nom_var)
	g24 (* 9.8 m24)
	g3 (* 9.8 m3)
	g5 (* 9.8 m5)
	k_nerav (ex_get 21 nom_var)
  )
  (vlax-invoke-method g_wb_get "Close" :vlax-false :vlax-false)
  (vlax-invoke-method g_oex "Quit")
  (mapcar
    (function (lambda (x)
      (if
        (and x (not (vlax-object-released-p x)))
        (vlax-release-object x)
      );_ if
              );_ lambda
    );_ function
    (list g_shs_get
	  g_wb_get
	  g_wkbs g_sheet
	  g_oex)
  )  
  (gc)  
)


;;;;;;;;;;;;;;;;;;;;;;;;;�������, ������� ������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;----=========================������ ��������=================================-----
(defun cherm ()
  (li (list cil1 b a c d cil2))  
  (mapcar '(lambda (x) (add_circ 1. x)) (list s2 s4 b a c d))
);_end of defun

;---==========================������ ���� ���������=======================---------
(defun cherv (p / )  
  (risskor p av "a" "v^A")
  (risskor p bv "b" "v^B")
  (risskor p cv "c" "v^C")
  (risskor p dv "d" "v^D")
  (risskor cv dv nil "v^DC")
  (risskor av bv nil "v^BA")
  (risskor p s2v nil "v^S2")
  (risskor p s4v nil "v^S4")
  (add_tex s2v "\\A1;\\H2.5;s\\H1.5;\\S^2;\\H2.5" nil nil)
  (add_tex s4v "\\A1;\\H2.5;s\\H1.5;\\S^4;\\H2.5" nil nil)
  (add_tex p "p,o,b" nil nil) 
);_end of defun
;-----=========================������ ���� ���������=======================--------
(defun chera (p /)   
  (risskor p aa "a" "a^A")
  (risskor p ca "c" "a^C")
  (risskor p ba "b" "a^B")
  (risskor p da "d" "a^D")
  (risskor aa ban nil "an^BA")
  (risskor ca dcn nil "an^DC")
  (risskor ban ba nil "a\U+03C4^BA")
  (risskor dcn da nil "a\U+03C4^DC")
  (risskor p as2 nil "a^S2")
  (add_tex as2 "\\A1;\\H2.5;s\\H1.5;\\S^2;\\H2.5" nil nil)
  (add_tex as4 "\\A1;\\H2.5;s\\H1.5;\\S^4;\\H2.5" nil nil)
  (risskor p as4 nil "a^S4")
  (risskor aa ba nil "a^BA")
  (risskor ca da nil "a^DC")
  (add_tex p "\\A1;\\H2.5;p\\H1.5;\\S^1;\\H2.5;,o,b" nil nil)
);_end of defun