(defun momentvit (/ polo os_x n_x max_mom fi polus)
  (setq polus '(0 0)
	mom_mas nil
	os_x (getdist "\n����� ��� �����: ")
	mufi (/ pi os_x 0.5)
	n_x (getreal "\n���������� �������� [12]: ")
	shag_ugl (/ pi n_x 0.5)
	shag_os (/ os_x n_x)
	ugol0 (* pi 0.5)
	fi ugol0
	tekush_ug 0.
	
  );_ setq
  (repeat (fix (1+ n_x))    
    (plansk polus 70. fi)
    (setq polog1 (/ (distance (list 0. (+ oa ab)) b) (* 2. oa))
	  polog2 (/ (distance (list 0. (+ oa ab)) d) (* 2. oa))
    )
    (if (equal (* 0.5 pi) (angle '(0 0) bv) 1e-14)
      (if (<= polog1 0.2)
	(setq p1 p1max)
	(setq p1 (* p1max (/ 1. (- (expt 50. -0.2) 0.02)) (- (expt 50. (* -1. polog1)) 0.02)))
      )
      (if (<= polog1 0.2)
	(setq p1 (* p1max (/ 1. (- 1. (expt 500. -0.2))) (- (expt 500. (* -1 polog1)) (expt 500. -0.2))))
	(setq p1 0.)	    
      )
    ); if (equal)...
    (if (equal (* 0.5 pi) (angle '(0 0) dv) 1e-14)
      (if (<= polog2 0.2)
	(setq p2 p2max)
	(setq p2 (+ (* 1.5625 (- 1. (/ p1max p2max)) (expt polog2 2.))
			     (* polog2 3.125 (- (/ p1max p2max) 1.))
			     (- 1.5625 (* (/ p1max p2max) 0.5625))
	         );+
	); setq
      ); if
      (if (<= polog2 0.2)
	(setq p2 (* p2max (expt (/ p1max p2max) (* 5. polog2))))
	(setq p2 p1max)
      )
    ); if (equal)...
    (setq	  
;;;    	p1 (if (equal (* 1.5 pi) (angle '(0 0) bv))
;;;	     (if (< polog1 0.5)
;;;	       (* p1max (1+ (/ polog1 -0.5)))
;;;	       0.
;;;	     )
;;;	     (if (> polog1 0.25)
;;;	       (* p1max (- (/ 4. 3.) (/ polog1 0.75)))
;;;	       p1max
;;;	     )
;;;	   )
;;;	p2 (if (equal (* 1.5 pi) (angle '(0 0) dv))
;;;	     (if (< polog2 0.5)
;;;	       (- p2max (* 2. polog2 (- p2max p1max)))
;;;	       p1max
;;;	     )
;;;	     (if (> polog2 0.25)
;;;	       (- (/ (- (* 4 p2max) p1max) 3.)
;;;		  (* polog2 (/ (- p2max p1max) 0.75))
;;;	       )
;;;	       p2max
;;;	     )
;;;	   )
    	  sils1 0.;(* p1 1e6 pi 0.25 ld1 ld1)
	  sils2 (* p2 1e6 pi 0.25 ld2 ld2)
	  mdfs1 (* sils1 (/ (distance polus bv) 70.) loa
		  (if (equal (angle polus bv) (* pi 0.5))
		    -1.
		    1.
		  )
		 )
	  mdfs2 (* sils2 (/ (distance polus dv) 70.) loa
		   (if (equal (angle polus dv) (* pi 0.5))
		    -1.
		    1.
		  )
		)
	  md_sum (+ mdfs1 mdfs2)
		   ;mdg3)
	  mom_mas (cons (list tekush_ug md_sum) mom_mas)
	  tekush_ug (+ tekush_ug shag_os)
	  fi (- fi shag_ugl)
  )
  );_ repeat
  (initget 7)
  (setq	max_mom (car (vl-sort (mapcar '(lambda (p) (abs (cadr p))) mom_mas) '>))
	mumoment (/ max_mom (getdist "\n������� Ymax: "))
	mom_mas (mapcar '(lambda (lis)
			   	 (list (car lis) (/ (cadr lis) mumoment))
			 ) mom_mas)
  ); setq
  (add_pl mom_mas)
  (add_tex (getpoint "\n��� ������ ���������� ����������� ��� ������� �������: ") mumoment "M" "�*�/��")
)


(defun inercia (/ masshtab fi polus max_ii mu_ii
		omeg4 omeg2 mvd mvb)
  (setq ;masshtab (getreal "\n��������� ����������� I: ")
	fi ugol0
	tekush_ug 0.
	mas_i nil
	polus '(0 0)
  )
  (repeat (length graf_int)    
    (plansk polus 70. fi)
    (setq i_sumar (+ i1	     
		     (* i24 (expt (/ omeg2 omeg1) 2))
		     (* m24 (expt (/ mvs2 omeg1) 2))
		     (* i24 (expt (/ omeg4 omeg1) 2))
		     (* m24 (expt (/ mvs4 omeg1) 2))
		     (* m3 (expt (/ mvb omeg1) 2))
		     (* m5 (expt (/ mvd omeg1) 2))
		  ); +
	  mas_i (cons (list i_sumar tekush_ug) mas_i)
	  tekush_ug (+ tekush_ug shag_os)
	  fi (- fi shag_ugl)
    ); setq
  ); repeat
  (initget 7)
  (setq	max_ii (car (vl-sort (mapcar '(lambda (p) (abs (car p))) mas_i) '>))
	mu_ii (/ max_ii (getdist "\n������� Ymax ��� ������� �������: "))
	mas_i (mapcar '(lambda (lis)
			       (list (/ (car lis) mu_ii) (cadr lis))
			 ) mas_i)
  ); setq
  (vl-cmdf "._ucs" '(-170. 200.) "")  
  (add_pl mas_i)
  (add_tex (getpoint "\n��� ������ ���������� ����������� ��� ������� �������: ") mu_ii "I" "�� �/��")
); inercia

(defun c:as ()
  (vl-load-com)
  (load "funkcii_tmm.LSP")
  (load "tmm\\mex_8\\for_8.LSP")
  (setq old (getvar "osmode"))
  (setvar "osmode" 20903)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "timesi.TTF" )))	
  ); setq
  (entmod le)
  (vvod)
  (setq muv (/ va 70.))
  (momentvit)  
  (grafint (reverse mom_mas) -100.)  
  (deltat (reverse graf_int) '(0. -200.))
  (add_tex (getpoint "\n��� ������ ���������� ����������� ��� ������: ") (* mufi h mumoment) "A" "��/��")
  (inercia)
  (vitten '(0 -200) 200 170)
  (initget 1 "Yes No")
  (if (equal "Yes" (getkword "\n������� � excel? [Yes/No]: "))
    (exel_expo_max)
  )  
)

(defun exel_expo_max (/ fi polus i omeg3 omeg2 mvf fsop polo g_oex g_wkbs g_wb_put g_shs_put g_sheet1
		      fkr_v fkr_n dis_fkr)
  (setq fi (* 0.5 pi)	
	mas_i nil
	polus '(0 0)
	g_oex (vlax-get-or-create-object "Excel.Application")
	i 1
  )
  (vlax-put-property g_oex "Visible" :vlax-false)  
  (setq g_wkbs (vlax-get-property g_oex "Workbooks")        
        g_wb_put (vla-open g_wkbs "c:\\Program files\\AutoCAD 2008\\Support\\blanks\\maxovik_8.xls")        
        g_shs_put (vlax-get-property g_wb_put "Worksheets")
	g_sheet1 (vlax-get-property g_shs_put "Item" 1)
  )
  (ex_put i1 3 "B" g_sheet1)
  (ex_put k_nerav 21 "A" g_sheet1)
  (ex_put omeg1 21 "B" g_sheet1)
  (ex_put (* h mumoment mufi) 21 "D" g_sheet1)  
  (repeat 13
    (plansk polus 70. fi)
    (ex_put (+ (* i24 (expt (/ omeg2 omeg1) 2))
	       (* m24 (expt (/ mvs2 omeg1) 2))
	    ) 4 i g_sheet1); I2
    (ex_put (* m3 (expt (/ mvb omeg1) 2)) 5 i g_sheet1); I3
    (ex_put (+ (* i24 (expt (/ omeg4 omeg1) 2))
	       (* m24 (expt (/ mvs4 omeg1) 2))
	    ) 6 i g_sheet1); I2
    (ex_put (* m5 (expt (/ mvd omeg1) 2)) 7 i g_sheet1); I3
    (setq i (1+ i)
	  fi (- fi (/ pi 6.))
    ); setq
  ); repeat
  (setq fi (* 0.5 pi)
	i 1
  )
  (repeat 13
    (plansk polus 70. fi)
    (setq polog1 (/ (distance (list 0. (+ oa ab)) b) (* 2. oa))
	  polog2 (/ (distance (list 0. (+ oa ab)) d) (* 2. oa))
    	  p1 (if (equal (* 1.5 pi) (angle '(0 0) bv))
	         (if (< polog1 0.5)
	             (* p1max (1+ (/ polog1 -0.5)))
	             0.
	         )
	         (if (> polog1 0.25)
	             (* p1max (- (/ 4. 3.) (/ polog1 0.75)))
	             p1max
	         )
	     )
	  p2 (if (equal (* 1.5 pi) (angle '(0 0) dv))
	         (if (< polog2 0.5)
	             (- p2max (* 2. polog2 (- p2max p1max)))
	             p1max
	         )
	         (if (> polog2 0.25)
	             (- (/ (- (* 4 p2max) p1max) 3.)
		        (* polog2 (/ (- p2max p1max) 0.75))
	             )
	             p2max
	         )
	     )
    	  sils1 (* p1 1e6 pi 0.25 ld1 ld1)
	  sils2 (* p2 1e6 pi 0.25 ld2 ld2)
	  mdfs1 (* sils1 (/ (distance polus bv) 70.) loa
		  (if (equal (angle polus bv) (* pi 0.5))
		    -1.
		    1.
		  )
		 )
	  mdfs2 (* sils2 (/ (distance polus dv) 70.) loa
		   (if (equal (angle polus dv) (* pi 0.5))
		    -1.
		    1.
		  )
		)
	  mdg2 (* g24 (/ (distance polus s2v) 70.) loa
		 (cosinus polus (polar polus (* 1.5 pi) 5) polus s2v))
	  mdg3 (* g3 (/ (distance polus bv) 70.) loa
		 (cosinus polus (polar polus (* 1.5 pi) 5) polus bv))
	  mdg4 (* g24 (/ (distance polus s4v) 70.) loa
		 (cosinus polus (polar polus (* 1.5 pi) 5) polus s4v))
	  mdg5 (* g5 (/ (distance polus dv) 70.) loa
		 (cosinus polus (polar polus (* 1.5 pi) 5) polus dv))
    ); setq
    (ex_put sils1 9 i g_sheet1)
    (ex_put sils2 10 i g_sheet1)
    (ex_put mdfs1 11 i g_sheet1)
    (ex_put mdfs2 12 i g_sheet1)
    (ex_put mdg2 13 i g_sheet1)
    (ex_put mdg3 14 i g_sheet1)
    (ex_put mdg4 15 i g_sheet1)
    (ex_put mdg5 16 i g_sheet1)
    (setq i (1+ i)
	  fi (- fi (/ pi 6.))
    )
  ); repeat
  (vlax-invoke-method g_wb_put "SaveCopyAs" (getfiled "������� ��� ��������� ���� � ��������" "c:\\" "xls" 129))
  (vlax-invoke-method g_wb_put "Close" :vlax-false :vlax-false)
  (vlax-invoke-method g_oex "Quit")
  (mapcar
    (function (lambda (x)
      (if
        (and x (not (vlax-object-released-p x)))
        (vlax-release-object x)
      );_ if
              );_ lambda
    );_ function
    (list g_shs_put
	  g_wb_put
	  g_wkbs g_sheet1
	  g_oex)
  )  
  (gc)
)