;----------------------------------------------------------------------------------
;=============� � � �    � � � �   �   � � � � � � � �   � � � � � � �=============
(defun sili (/ a b c d s2 s3 s4 av bv dv s2v s3v s4v aa ba da s2a s3a s4a)
  (planusk polus 100. (- ugol0 fi))
   (setq sf5 (* m5 mad)
	 sf4 (* m4 mas4)
	 sf3 (* m3 mas3)
	 sf2 (* m2 mas2)  
	;������� �� ��� �������
	msf2 (if (clockwise-p a b (polar b (angle ban ba) 10))
	       (* -1 eps2 is2)
	       (* eps2 is2)
	     )
	msf3 (if (clockwise-p c b (polar b (angle bo1n ba) 10))
	       (* -1 eps3 is3)
	       (* eps3 is3)
	     )
	msf4 (if (clockwise-p c d (polar d (angle dcn da) 10))
	       (* -1 eps4 is4)
	       (* eps4 is4)
	     )
	;����� ��� ��� �� '
	lh2 (/ msf2 sf2)   ;[m]
	lh3 (/ msf3 sf3)
	lh4 (/ msf4 sf4)
	h2 (/ lh2 mul)  ;[mm]
	h3 (/ lh3 mul)
	h4 (/ lh4 mul) 
  );_end of setq
);_end of defun

(defun new_funcia_din (/ )  
  (setq ;fi (- ugol0 fi)
	muv (/ va 70.)
	mua (/ aan 100.)
	polus '(0 0)
  )  
  (sili)
  (grup45)
  (grup23)
  (grup01)
  (guk)
  (initget 1 "Yes No")
  (if (equal (getkword "\n�������������� ������ � excel [Yes/No]: ") "Yes")
    (excel_export)
  )
)

(defun c:din4 (/ polus n1 omeg1 loa mul va aan lab ab lbc bc lbd bd lx x
ly y fi m2 m3 m4 m5 m_mat is2 is3 is4 fsshtr fsobr g2 g3 g4 g5 g_mat)
  (vl-load-com)
  (load "funkcii_tmm.LSP")
  (load "tmm\\mex_4\\for_4_g.LSP")
  (setq old (getvar "osmode"))
  (setvar "osmode" 20903)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "timesi.TTF" )))	
  ); setq
  (entmod le)
  (vvod)
  (new_funcia_din)  
  (setvar "osmode" old)
  (vla-ZoomExtents (vlax-get-acad-object))
  (alert (strcat "�����������:\n"
		 (rtos (* (/ (- (max (abs fursil) (abs furguk)) (min (abs fursil) (abs furguk)))
			     (max (abs fursil) (abs furguk))) 100))
	 )
  )
);_end of defun

;-----------------------------------------------------------------------------------
;                       � � � � � �    � � � � � �   4 - 5                         ;
;***********************************************************************************
;===================================================================================
(defun grup45 (/ pol sf5v g5v fsoprv sf4v g4v r24nv sf4shn sf4shv r05v r24tv
a b c d s2 s3 s4 av bv dv s2v s3v s4v aa ba da s2a s3a s4a)
  (planusk (setq pol (getpoint "\n������� �����, ��� ������� ������ 4-5: ")) 100 (- ugol0 fi))
  (setq sf5v (polar d (angle da pol) 20)
	g5v (polar d (/ pi -2) 20)
	Gmaterial (if (equal (angle pol dv) 0. 1e-6)
		    Gmaterial
		    0.
		  )
	sila_sopr (if (equal (angle pol dv) 0. 1e-6)
		    frabhod
		    fholhod
		  )
	fsoprv (if (equal (angle pol dv) 0. 1e-6)
		 (polar d pi 20)
		 (polar d 0 20)
	       )
	sf4v (polar s4 (angle s4a pol) 20)	
	g4v (polar s4 (/ pi -2) 20)
	r34nv (polar c (angle d c) 20)
	sf4shn (if (> msf4 0)		  
		   (polar s4 (+ (angle s4 sf4v) (/ pi 2)) (abs h4))
		   (polar s4 (- (angle s4 sf4v) (/ pi 2)) (abs h4))
	       )		
	sf4shv (polar sf4shn (angle s4a pol) 20)
	s4shtrih_guk (inters
		       c d
		       sf4shn sf4shv
		       nil
		     )
	guk_otn_bs4shkbd (/ (distance c s4shtrih_guk) cd)
	paramgukbd (equal (angle c s4shtrih_guk) (angle c d) 1e-10)
	r05v (polar d (* 0.5 pi) 20)
	r34t (/ (+ (moment g4 s4 g4v d)
		   (moment sf4 sf4shn sf4shv d)
		)
	        cd
	     );_end of /
	r34tv (if (minusp r34t)
		(polar c (+ (angle d c) (/ pi 2)) 20)
		(polar c (- (angle d c) (/ pi 2)) 20)
	      )
  );_end of setq
  (chergrup54)
  (plansil45)  
);_end of defun

(defun chergrup54 ()
  (li (list d c))
  (mapcar '(lambda (x) (add_circ 0.7 x)) (list d s4 c))
  (risskor s4 sf4v  nil "�^4")
  (risskor s4 g4v nil "G^4")
  (risskor sf4shn sf4shv  nil "�'^4")
  (risskor d sf5v  nil "�^5")
  (risskor d g5v  nil "G^5")
  (risskor d r05v  nil "R^05")
  (risskor c r34nv  nil "Rn^24")
  (risskor c r34tv  nil "R\U+03C4^24")
  (risskor d fsoprv  nil "F^C")
  (if (not (minusp Gmaterial))
    (risskor d (polar d (* -0.5 pi) 40)  nil "G^MAT")
  )
)

;===\|/========� � � �   � � �   � � �   � � � � � �   4-5======\|/
(defun plansil45 (/ maxsil45 
abr24t bcg4 cdsf4 deg5 efsf5 fgfsopr ghgmat
asil4 bsil4 csil4 dsil4 esil4 fsil4 gsil4 hsil4 isil4)
  (setq asil4 (getpoint "\n������� ��� ������� ���� ��� ��� ������ 4-5: ")
	maxsil45 (getreal "\n������� � �� ����� ������� ���� �� �����: ")	
	muf45 (/ (max g4 g5 sila_sopr sf5 sf4 (abs r34t) Gmaterial) maxsil45)
	abr24t (/ (abs r34t) muf45)
	bcg4 (/ g4 muf45)
	cdsf4 (/ sf4 muf45)
	deg5 (/ g5 muf45)
	efsf5 (/ sf5 muf45)
	fgfsopr (/ sila_sopr muf45)
	ghgmat (/ Gmaterial muf45)
	;����� ����� ��� R24t+G4+�4+G5+�5+Fc+Gmat+R05+R24n=0
	bsil4 (polar asil4 (angle c r34tv) abr24t);R24t
	csil4 (polar bsil4 (* pi -0.5) bcg4)	  ;G4
	dsil4 (polar csil4 (angle s4a pol) cdsf4) ;�4
	esil4 (polar dsil4 (* pi -0.5) deg5)	  ;G5
	fsil4 (polar esil4 (angle da pol) efsf5)  ;�5
	gsil4 (polar fsil4 (angle dv pol) fgfsopr);Fc
  );_end of setq
  (if (zerop Gmaterial)
    (setq hsil4 (inters gsil4 (polar gsil4 (* 0.5 pi) 100.)
			asil4 (polar asil4 (angle c d) 100.)
			nil
		)
	  ;������� ���������� ������� R24n
	  r34n (* muf45 (distance hsil4 asil4))
	  ;������� ������ ������� R24
	  r34 (sqrt (+ (* r34n r34n) (* r34t r34t)))
	  r05 (* muf45 (distance hsil4 gsil4))
	  ugolr42 (angle bsil4 hsil4)
    );_end of setq
    (setq hsil4 (polar gsil4 (* pi -0.5) ghgmat)
	  isil4 (inters hsil4 (polar hsil4 (* 0.5 pi) 100.)
			asil4 (polar asil4 (angle c d) 100.)
			nil
		)
	  ;������� ���������� ������� R24n
	  r34n (* muf45 (distance isil4 asil4))
	  ;������� ������ ������� R24
	  r34 (sqrt (+ (* r34n r34n) (* r34t r34t)))
	  r05 (* muf45 (distance hsil4 isil4))
	  ugolr42 (angle bsil4 isil4)
    );_end of setq
  );_end of if
  (cherplan45)
);_end of defun
;===/|\========� � � �   � � �   � � �   � � � � � �   4-5======/|\
(defun cherplan45 (/ gde);R24t+G4+�4+G5+�5+Fc+Gmat+R05+R24n=0
  (risskor asil4 bsil4 "b" "R\U+03C4^34")
  (risskor bsil4 csil4 "c" "G^4")
  (risskor csil4 dsil4 "d" "�^4")
  (risskor dsil4 esil4 "e" "G^5")
  (risskor esil4 fsil4 "f" "�^5")
  (risskor fsil4 gsil4 "g" "F^C")
  (if (zerop Gmaterial)
    (progn
      (risskor gsil4 hsil4 "h" "R^05")
      (risskor hsil4 asil4 "a" "Rn^34")
      (risskor hsil4 bsil4 nil "R^34")
      (setq r45 (* muf45 (distance dsil4 hsil4)))
    )
    (progn
      (risskor gsil4 hsil4 "h" "G^MAT")
      (risskor hsil4 isil4 "i" "R^05")
      (risskor isil4 asil4 "a" "Rn^34")
      (risskor isil4 bsil4 nil "R^34")
      (setq r45 (* muf45 (distance dsil4 isil4)))
    )
  )
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���: ") muf45 "F" "�/��") 
)


;======\|/===� � � � � �   � � �   � � � � � �   2-3=====\|/========
(defun grup23 (/ pol g3v g2v sf3v sf2v r03nv r12nv sf3shn sf3shv sf2shn sf2shv r42v r03tv
a b c d s2 s3 s4 bv dv s2v s3v s4v aa ba da s2a s3a s4a)
  (planusk (setq pol (getpoint "\n������� �����, ��� ������� ������ 2-3: ")) 100 (- ugol0 fi))
  (setq g3v (polar s3 (/ pi -2) 20)
	g2v (polar s2 (/ pi -2) 20)
	sf3v (polar s3 (angle s3a pol) 20)
	sf2v (polar s2 (angle s2a pol) 20)
	r03nv (polar o1 (angle b o1) 20)
	r12nv (polar a (angle b a) 20)
	sf3shn (if (minusp msf3)
		 (polar s3 (+ (angle s3 sf3v) (/ pi 2)) (abs h3))
		 (polar s3 (- (angle s3 sf3v) (/ pi 2)) (abs h3))
	       );_end of if
	sf3shv (polar sf3shn (angle s3 sf3v) 20)
	sf2shn (if (minusp msf2)
		 (polar s2 (+ (angle s2 sf2v) (/ pi 2)) (abs h2))
		 (polar s2 (- (angle s2 sf2v) (/ pi 2)) (abs h2))
	       );_end of if
	sf2shv (polar sf2shn (angle s2 sf2v) 20)
	s2shtrih_guk (inters
		       a b
		       sf2shn sf2shv
		       nil
		     )
	guk_otn_bs2shkba (/ (distance b s2shtrih_guk) ab)
	paramgukba (equal (angle b s2shtrih_guk) (angle b a) 1e-10)
	s3shtrih_guk (inters
		       b o1
		       sf3shn sf3shv
		       nil
		     );_end of inters
	guk_otn_bs3shkbc (/ (distance b s3shtrih_guk) o1b)
	paramgukbc (equal (angle b s3shtrih_guk) (angle b o1) 1e-10)
	r43v (polar c ugolr42 20)
	r03t (/ (+ (moment g3 s3 g3v b)
		   (moment r34 c r43v b)
		   (moment sf3 sf3shn sf3shv b)
		);_end of +
		o1b
	     );_end of /
	r12t (/ (+ (moment g2 s2 g2v b)
		   (moment sf2 sf2shn sf2shv b)
		)
		ab
	     );_end of /
	r03tv (if (minusp r03t)
		(polar o1 (+ (angle b o1) (/ pi 2)) 20)
		(polar o1 (- (angle b o1) (/ pi 2)) 20)
	      );_end of if
	r12tv (if (minusp r12t)
		(polar a (+ (angle b a) (/ pi 2)) 20)
		(polar a (- (angle b a) (/ pi 2)) 20)
	      );_end of if
  );_end of setq
  (chergrup23)
  (plansil23)
);_end of defun
;======/|\===� � � � � �   � � �   � � � � � �   2-3=====/|\========
(defun chergrup23 ()
  (li (list a b c o1))
  (mapcar '(lambda (x) (add_circ 0.7 x)) (list b a c s3 s2 o1))
  (risskor s2 g2v  nil "G^2")
  (risskor s3 g3v nil "G^3")
  (risskor s2 sf2v nil "�^2")
  (risskor s3 sf3v nil "�^3")
  (risskor o1 r03nv nil "Rn^03")
  (risskor a r12nv nil "Rn^12")
  (risskor sf3shn sf3shv nil "�'^3")
  (risskor sf2shn sf2shv nil "�'^2")
  (risskor c r43v nil "R^43")
  (risskor a r12tv nil "R\U+03C4^12")
  (risskor o1 r03tv nil "R\U+03C4^03")
)

;===\|/========� � � �   � � �   � � �   � � � � � �   2-3======\|/
(defun plansil23 (/ maxsil23 bcg2 cdsf2 der42 efsf3 fgg3 ghr03t
asil2 bsil2 csil2 dsil2 esil2 fsil2 gsil2 hsil2 isil2)
  ;���� ��� �������� �� ��������: R12t+G2+�'2+R42+�'3+G3+R03t+R03n+R12n=0
  (setq asil2 (getpoint "\n������� ��� ������� ���� ��� ��� ������ 2-3: ")
	maxsil23 (getreal "\n������� � �� ����� ������� ���� �� �������� 2-3: ")
	muf23 (/ (max g2 g3 sf2 sf3 r34 (abs r03t) (abs r12t)) maxsil23)
	abr12t (abs (/ r12t muf23));R12t
	bcg2 (/ g2 muf23)	;G2
	cdsf2 (/ sf2 muf23);�'2
	der42 (/ r34 muf23);R42
	efsf3 (/ sf3 muf23);�'3
	fgg3 (/ g3 muf23)	 ;G3
	ghr03t (abs (/ r03t muf23));R03t
	;����� ��� ����� ���
	bsil2 (polar asil2 (angle a r12tv) abr12t)
	csil2 (polar bsil2 (/ pi -2) bcg2)
	dsil2 (polar csil2 (angle s2 sf2v) cdsf2)
	esil2 (polar dsil2 (angle c r43v) der42)
	fsil2 (polar esil2 (angle s3 sf3v) efsf3)
	gsil2 (polar fsil2 (/ pi -2) fgg3)
	hsil2 (polar gsil2 (angle o1 r03tv) ghr03t)
	isil2 (inters hsil2 (polar hsil2 (angle o1 b) 100)
		      asil2 (polar asil2 (angle a b) 100)
		      nil
	      )
	;���������� ���������� �������
	r12n (* (distance asil2 isil2) muf23)
	r03n (* (distance hsil2 isil2) muf23)
	;���������� ������ �������
	r12 (sqrt (+ (* r12n r12n) (* r12t r12t)))
	r03 (sqrt (+ (* r03n r03n) (* r03t r03t)))
	r23 (* muf23 (distance esil2 isil2))
	ugolr21 (angle bsil2 isil2)
  );_end of setq
  (cherplan23)
)
(defun cherplan23 (/)
  (risskor asil2 bsil2 "b" "R\U+03C4^12")
  (risskor bsil2 csil2 "c" "G^2")
  (risskor csil2 dsil2 "d" "�'^2")
  (risskor dsil2 esil2 "e" "R^43")
  (risskor esil2 fsil2 "f" "�'^3")
  (risskor fsil2 gsil2 "g" "G^3")
  (risskor gsil2 hsil2 "h" "R\U+03C4^03")
  (risskor hsil2 isil2 "i" "Rn^03")
  (risskor gsil2 isil2 nil "R^03")
  (risskor isil2 asil2 "a" "Rn^12")
  (risskor isil2 bsil2 nil "R^12")
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���: ") muf23 "F" "�/��")
)

;===/|\========� � � �   � � �   � � �   � � � � � �   2-3======/|\

;======\|/===� � � � � �   � � �   � � � � � �   0-1=====\|/========
(defun grup01 (/ pol a b c d s2 s3 s4 maxsils1
asil1 bsil1 csil1 fu_ver r21v)
  (tochkim (setq pol (getpoint "\n������� ��� ������� ������� �����: ")) (- ugol0 fi)) 
  (setq	r21v (polar a ugolr21 20)
	fursil (/ (moment r12 a r21v pol) oa)
  );_end of setq
  (li (list pol a))
  (mapcar '(lambda (x) (add_circ 0.7 x)) (list pol a))
  (risskor a r21v nil "R^21")
  (if (< (moment 10 a (polar a ugolr21 20) pol) 0)
      (setq fu_ver (polar a (- (angle a pol) (/ pi 2)) 20))
      (setq fu_ver (polar a (+ (angle a pol) (/ pi 2)) 20))
  );_end of if
  (risskor a fu_ver nil "F^y")
  (setq asil1 (getpoint "\n������� ��� ������� ���� ��� ��� ������ 0-1: ")
	maxsils1 (getdist "\n����� ������������ ����: ")
	muf01 (/ r12 maxsils1)
	bsil1 (polar asil1 (angle a r21v) maxsils1)
	csil1 (polar bsil1 (angle a fu_ver) (/ (abs fursil) muf01))
  )
  (risskor asil1 bsil1 "b" "R^21")
  (risskor bsil1 csil1 "c" "F^Y")
  (risskor csil1 asil1 "a" "R^01")
  (setq r01 (* muf01 (distance csil1 asil1)))
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ��� ������ 0-1: ") muf01 "F" "�/��")
)

;==========================================================================
;*********************� � � � �   � � � � � � � � � �**********************
;==========================================================================
;==========================================================================
(defun guk (/ cl chasi p a b c d s2 s3 s4
ag bg dg s2g s3g s4g s2_shtrih s3_shtrih s4_shtrih bv dv s2v s3v s4v aa ba da s2a s3a s4a)
  (setq chasi (getint "\n��� �������? [1-�� �����] "))
  (if (= chasi 1)
    (setq cl (/ pi 2))
    (setq cl (/ pi -2))
  );_end of if
  (planusk (setq p (getpoint "\n��� ������� ����� ����������: "))
	   100.;(getdist "\n������� ����� ������� Va: ")
	  (- ugol0 fi))
  (plansk p (getdist "\n������� ����� ������� Va: ") (- ugol0 fi))
  (setq ag (polar p (+ (angle p av) cl) (distance p av))
        bg (polar p (+ (angle p bv) cl) (distance p bv))
	dg (polar p (+ (angle p dv) cl) (distance p dv))
	cg (polar p (+ (angle p cv) cl) (distance p cv))
	s2g (polar p (+ (angle p s2v) cl) (distance p s2v))
	s3g (polar p (+ (angle p s3v) cl) (distance p s3v))
	s4g (polar p (+ (angle p s4v) cl) (distance p s4v))
	s2_shtrih (polar bg
			 (if paramgukba
			   (angle bg ag)
			   (angle ag bg)
			 )
			 (* (distance ag bg) guk_otn_bs2shkba))
	s3_shtrih (polar bg
			 (if paramgukbc
			   (angle bg p)
			   (angle p bg)
			 )
			 (* (distance bg p) guk_otn_bs3shkbc))
	s4_shtrih (polar cg
			 (if paramgukbd
			   (angle cg dg)
			   (angle dg cg)
			 )
			 (* (distance dg cg) guk_otn_bs4shkbd)
		  )
	 ;(polar dg (angle dg bg) (* (distance dg bg) (/ (distance p s4shtrih_guk) bd)))
	;������� ���������������� ����
	furguk (/ (+ ;���� �������
		     (setq momgug5 (moment (+ g5 Gmaterial) dg (polar dg (/ pi -2) 5) p))
		     (setq momgug4 (moment g4 s4g (polar s4g (/ pi -2) 5) p))
		     (setq momgug3 (moment g3 s3g (polar s3g (/ pi -2) 5) p))
		     (setq momgug2 (moment g2 s2g (polar s2g (/ pi -2) 5) p))
		     ;���� �������
		     (setq momgusf5 (moment sf5 dg (polar dg (angle da p) 5) p))
		     (setq momgusf4 (moment sf4 s4_shtrih (polar s4_shtrih (angle s4a p) 5) p))
		     (setq momgusf3 (moment sf3 s3_shtrih (polar s3_shtrih (angle s3a p) 5) p))
		     (setq momgusf2 (moment sf2 s2_shtrih (polar s2_shtrih (angle s2a p) 5) p))
		     ;���� ��������� �������������
		     (setq momgufsop (moment sila_sopr dg
					     (if (equal 0. (angle p dv) 1e-6)
					       (polar dg pi 5)
					       (polar dg 0 5)
					     ); if
					     p
				     ); moment
		     ); setq
		  );_end of +
		 (distance p ag)
	       );_end of /
  );_end of setq
  (risguk)
)

(defun risguk (/ )  
  (li (list p ag bg p cg dg p))
  (add_tex p "p,o,o^1" nil nil)
  (add_tex ag "a" nil nil)
  (add_tex bg "b" nil nil)
  (add_tex cg "c" nil nil)
  (add_tex dg "d" nil nil)
  (add_tex s2g "\\A1;s\\H0.7x;\\S^2;" nil nil)
  (add_tex s3g "\\A1;s\\H0.7x;\\S^3;" nil nil)
  (add_tex s4g "\\A1;s\\H0.7x;\\S^4;" nil nil)
  (li (list p s2g))
  (li (list p s3g))
  (li (list p s4g))
  (risskor s2g (polar s2g (/ pi -2) 20) nil "G^2")
  (risskor s3g (polar s3g (/ pi -2) 20) nil "G^3")
  (risskor s4g (polar s4g (/ pi -2) 20) nil "G^4")
  (risskor dg (polar dg (/ pi -2) 20) nil "G^5")
  (if (not (minusp Gmaterial))
    (risskor dg (polar dg (* -0.5 pi) 40)  nil "G^MAT")
  )  
  (risskor dg (polar dg (angle da p) 20) nil "�^5")
  (risskor s4_shtrih (polar s4_shtrih (angle s4a p) 20) nil "�'^4")
  (risskor s3_shtrih (polar s3_shtrih (angle s3a p) 20) nil "�'^3")
  (risskor s2_shtrih (polar s2_shtrih (angle s2a p) 20) nil "�'^2")
  (risskor dg (polar dg (angle dv p) 20) nil "F^C")  
)


(defun excel_export (/ g_oex g_wkbs g_wb_put g_shs_put g_sheet1 g_sheet2
		     fii i polus_sko polus)
  (setq polus '(0. 0.))
  (planusk polus 100. (- ugol0 fi))
  (setq g_oex (vlax-get-or-create-object "Excel.Application"))
  (vlax-put-property g_oex "Visible" :vlax-false)  
  (setq g_wkbs (vlax-get-property g_oex "Workbooks")        
        g_wb_put (vla-open g_wkbs (strcat total_adress "blanks\\din_list_4g.xlsx")) 
        g_shs_put (vlax-get-property g_wb_put "Worksheets")
	g_sheet1 (vlax-get-property g_shs_put "Item" 1)
  )
  (ex_put m2 2 "F" g_sheet1)
  (ex_put m3 2 "G" g_sheet1)
  (ex_put m4 2 "H" g_sheet1)
  (ex_put m5 2 "I" g_sheet1)
  (ex_put mas2 3 "F" g_sheet1)
  (ex_put mas3 3 "G" g_sheet1)
  (ex_put mas4 3 "H" g_sheet1)
  (ex_put mad 3 "I" g_sheet1)
  (ex_put eps2 2 "M" g_sheet1)
  (ex_put eps3 2 "N" g_sheet1)
  (ex_put eps4 2 "O" g_sheet1)
  (ex_put is2 4 "J" g_sheet1)
  (ex_put is3 4 "K" g_sheet1)
  (ex_put is4 4 "L" g_sheet1)
  (ex_put Gmaterial 2 "P" g_sheet1)
  (ex_put sila_sopr 2 "S" g_sheet1)
  (ex_put frabhod 2 "R" g_sheet1)
  (ex_put fholhod 2 "Q" g_sheet1)
  
  ;Grup 4-5
  (ex_put r05 8 "B" g_sheet1)
  (ex_put r34n 8 "D" g_sheet1)
  (ex_put (abs r34t) 8 "E" g_sheet1)
  (ex_put r45 8 "F" g_sheet1)
  (ex_put muf45 8 "G" g_sheet1)
  ;Grup 2-3
  (ex_put r12n 8 "L" g_sheet1)
  (ex_put (abs r12t) 8 "M" g_sheet1)
  (ex_put r03n 8 "J" g_sheet1)
  (ex_put (abs r03t) 8 "K" g_sheet1)
  (ex_put r23 8 "N" g_sheet1)
  (ex_put muf23 8 "O" g_sheet1)
  
  ;Grup 0-1
  (ex_put (abs fursil) 8 "Q" g_sheet1)
  (ex_put r01 8 "P" g_sheet1)
  (ex_put muf01 8 "S" g_sheet1)
  (ex_put loa 10 "R" g_sheet1)
  
 
;;;  (ex_put momg5g 16 "B" g_sheet1)
;;;  (ex_put momg4g 16 "C" g_sheet1)
;;;  (ex_put momg3g 16 "D" g_sheet1)
;;;  (ex_put momg2g 16 "E" g_sheet1)
;;;  (ex_put momsf5g 16 "F" g_sheet1)
;;;  (ex_put momsf4g 16 "G" g_sheet1)
;;;  (ex_put momsf3g 16 "H" g_sheet1)
;;;  (ex_put momsf2g 16 "I" g_sheet1)
;;;  (ex_put momsils3g 16 "K" g_sheet1)
;;;  (ex_put momsils5g 16 "J" g_sheet1)  
;;;  (ex_put vektor_va_din 17 "M" g_sheet1)
  (vlax-invoke-method g_wb_put "SaveCopyAs" (getfiled "������� ��� ��������� ���� � ��������" "c:\\" "xlsx" 129))
  (vlax-invoke-method g_wb_put "Close" :vlax-false :vlax-false)
  (vlax-invoke-method g_oex "Quit")
  (mapcar
    (function (lambda (x)
      (if
        (and x (not (vlax-object-released-p x)))
        (vlax-release-object x)
      );_ if
              );_ lambda
    );_ function
    (list g_shs_put
	  g_wb_put
	  g_wkbs g_sheet1
	  g_oex)
  )  
  (gc)
)

(c:din4)