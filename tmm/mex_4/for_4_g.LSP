;----------------------------------------------------------------------------------
;==============� � � � � � � � � � � � � �    � � � � � �==========================
;--==������� ����� ����� ��������� � ���������� ��������� � ������ ���������===----
(defun plansk (p pv ugol / ome2 ome3 ome4 muv);������� ����� ����� ���������
  (tochkim p ugol)
  (setq muv (/ va pv)
	av  (polar p (- (angle p a) (/ pi 2)) pv)
        bv  (peres a b o1 p av)
	cv  (polar p (angle p bv) (* (distance p bv) (/ o1c o1b)))
        dv  (inters p (polar p 0. 10.)
		    cv (polar cv (- (angle d c) (* 0.5 pi)) 10.)
		    nil
	    ); inters
	s2v (polar av (angle av bv) (* 0.5 (distance av bv)))
	s3v (polar p (angle p bv) (* 0.5 (distance p cv)))
	s4v (polar cv (angle cv dv) (* 0.5 (distance dv cv)))
	mvb (* muv (distance p bv))
	mvd (* muv (distance p dv))
	mvc (* muv (distance p cv))
	mvs2 (* muv (distance p s2v))
	mvs3 (* muv (distance p s3v))
	mvs4 (* muv (distance p s4v))
	mvba (* muv (distance av bv))
	mvdc (* muv (distance dv cv))
;���������� ���������
	abo1n (/ (expt mvb 2) lo1b)
	aban (/ (expt mvba 2) lab)
	adcn (/ (expt mvdc 2) lcd)
;�����
	ome2 (/ mvba lab)
	ome3 (/ mvb lo1b)
	ome4 (/ mvdc lcd)
  );_end of setq
);_end of defun

;;;;;;---=================������� ���������� ����� ���������================-------
(defun tochkim (o ugol)
  (setq a (polar o ugol oa)
	o1 (list (+ (car o) x) (+ (cadr o) y))
	b (car (vl-sort (2d_inters_circle a ab o1 o1b) '(lambda (e1 e2) (< (cadr e1) (cadr e2)))))
	c (polar o1 (angle o1 b) o1c)
        d (car (dwgru (list 0 (cadr o)) (list 100 (cadr o)) c cd))
	s2 (polar a (angle a b) (* 0.5 (distance a b)))
	s3 (polar o1 (angle o1 b) (* 0.5 (distance o1 c)))
	s4 (polar d (angle d c) (* 0.5 (distance d c)))
	masivm (list o o1 a b c d)
	mass (list s2 s3 s4)
  );_end of setq
)
;----------------------------------------------------------------------------------
;---------========������� ����� ����� ��������� � ������ ���������========---------
(defun planusk (p pa ugol /)
  (plansk p 70. ugol)
  (setq ;=====����� �������� ���������� ���������=====
	dban (/ aban mua)
	dbo1n (/ abo1n mua)
	ddcn (/ adcn mua)	
	;=====����� ����� ���������=====
	aa (polar p (angle a p) pa)
	ban (polar aa (angle b a) dban)
	bo1n (polar p (angle b o1) dbo1n)
	ba (peres a b o1 bo1n ban)
	ca (polar p (angle p ba) (* (distance p ba) (/ o1c o1b)))
	dcn (polar ca (angle d c) ddcn)
	da (inters p (polar p 0 10.) dcn
		   (polar dcn (- (angle dcn ca) (/ pi 2)) 100)
		   nil
	   )
	s2a (polar aa (angle aa ba) (* 0.5 (distance aa ba)))
	s3a (polar p (angle p ba) (* 0.5 (distance p ca)))
	s4a (polar ca (angle ca da) (* 0.5 (distance ca da)))
	;=====������ ���������=========
    ;����������
    	mab (* mua (distance p ba))
  	mad (* mua (distance p da))
	mac (* mua (distance p ca))
    	mas2 (* mua (distance p s2a))
   	mas3 (* mua (distance p s3a))
    	mas4 (* mua (distance p s4a))
    ;��������������
    	mabat (* mua (distance ban ba))
    	mabo1t (* mua (distance bo1n ba))
    	madct (* mua (distance dcn da))
;;;	maba (* mua (distance aa ba))
;;;	madb (* mua (distance da ba))
    ;��������
    	eps2 (/ mabat lab)
    	eps3 (/ mabo1t lo1b)
    	eps4 (/ madct lcd)
  )  
)

;�������� �������������� ��� ������� �4
(defun vvod (/ nom_var)
  (initget 5)
  (setq nom_var (1+ (getint "\n������� ����� ��������: ")))
  (initget 7)
  (setq oa (getreal "\n������� O� [��]: "))
  (exel_get)  
  (setq fi (+ fi (* (/ pi 180.) (getreal "\n������� ��������� � ���������� ���� [����]: "))))
);end of vvod

(defun exel_get (/ g_oex g_wkbs g_wb_get g_shs_get g_sheet f)
  (setq g_oex (vlax-get-or-create-object "Excel.Application"))
  (vlax-put-property g_oex "Visible" :vlax-false)
  (setq g_wkbs (vlax-get-property g_oex "Workbooks")        
        g_wb_get (vla-open g_wkbs (strcat total_adress "variants\\gvariants_4.xls"))        
        g_shs_get (vlax-get-property g_wb_get "Worksheets")
	g_sheet (vlax-get-property g_shs_get "Item" 1)
	f (ex_get 30 nom_var)
	n1 (ex_get 29 nom_var)
	omeg1 (/ (* pi n1) 30)
  	loa (ex_get 15 nom_var)
        mul (/ loa oa)
	va (* omeg1 loa)
	aan (* omeg1 omeg1 loa)
	lab (ex_get 16 nom_var)
        ab (/ lab mul)
	lo1b (ex_get 13 nom_var)
        o1b (/ lo1b mul)
	lo1c (ex_get 12 nom_var)
        o1c (/ lo1c mul)
	lcd (ex_get 14 nom_var)
        cd (/ lcd mul)
	lx (ex_get 5 nom_var)
        x (/ lx mul)
	ly (ex_get 6 nom_var)
        y (/ ly mul)
        fi (* f (/ pi 180.))
	m2 (ex_get 18 nom_var)
	m3 (ex_get 19 nom_var)
	m4 (ex_get 20 nom_var)
	m5 (ex_get 21 nom_var)
	;m_mat (ex_get 15 nom_var)
	is2 (ex_get 23 nom_var)
	is3 (ex_get 24 nom_var)
  	is4 (ex_get 25 nom_var)
	Gmaterial (ex_get 26 nom_var)
	frabhod (ex_get 27 nom_var)
	fholhod (ex_get 28 nom_var)
	;���� ������� �������
	g2 (* 9.81 m2)
	g3 (* 9.81 m3)
	g4 (* 9.81 m4)
	g5 (* 9.81 m5)
	;g_mat (* 9.81 m_mat)
	ugol0 (- pi
		 (- (arccos (/ (+ (expt (- oa ab) 2) (* x x) (* y y) (* -1 o1b o1b))
			       (* 2 (- ab oa) (sqrt (+ (* x x) (* y y))))
			    )
		    )
		    (atan (/ y x))
		 )		 
	      )
  );_end of setq
  (vlax-invoke-method g_wb_get "Close" :vlax-false :vlax-false)
  (vlax-invoke-method g_oex "Quit")
  (mapcar
    (function (lambda (x)
      (if
        (and x (not (vlax-object-released-p x)))
        (vlax-release-object x)
      );_ if
              );_ lambda
    );_ function
    (list g_shs_get
	  g_wb_get
	  g_wkbs g_sheet
	  g_oex)
  )  
  (gc)
)

(defun cherm (/ i)  
  (li (list polus_mexan a b))
  (li (list o1 c d))
  (setq i 0)
  (mapcar '(lambda (x) (add_circ 0.7 x)) masivm)
  (mapcar '(lambda (x) (add_circ 0.7 x)) mass)
  (mapcar '(lambda (x)
	      (add_tex x (nth i (list "O" "\\A1;O\\H0.7x;\\S^1;" "A" "B" "C" "D")) nil nil)
	     (setq i (1+ i))
	   );_end of lambda
   masivm
  )
)

(defun cherv (p / )    
    (add_tex p "p,o,o1" nil nil)
    (risskor p av "a" "v^A")
    (risskor av bv nil "v^BA")
    (risskor p bv "b" "v^B")
    (risskor p cv "c" "v^C")
    (risskor cv dv nil "v^DC")
    (risskor p dv "d" "v^D")
    (risskor p s2v "s^2" "v^S2")
    (risskor p s3v "s^3" "v^S3")
    (risskor p s4v "s^4" "v^S4")   
)

(defun chera (p / gde)
    (risskor p aa "a" "a^A")  
    (risskor aa ban nil "an^BA")
    (risskor p bo1n nil "an^BO1")
    (risskor bo1n ba nil "a\U+03C4^BO1")
    (risskor ban ba nil "a\U+03C4^BA")
    (risskor p ba "b" "a^B")
    (risskor p da "d" "a^D")
    (risskor p ca "c" "a^C")
    (risskor ca dcn nil "an^DC")
    (risskor dcn da nil "a\U+03C4^DC")
    (risskor p s2a "s^2" "a^S2")
    (risskor p s3a "s^3" "a^S3")
    (risskor p s4a "s^4" "a^S4")
    (li (list aa ba)) (li (list da ca))
    (add_tex p "p^1,o,o^1" nil nil)
)  