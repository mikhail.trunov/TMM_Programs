;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;�������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun c:max6 (/ old le n1 loa mul lab ab lbc bc lbd bd lbe be lek ek lef ef lx x ly y lxy1 xy1 gf gsht gjid fi omeg1 va aan
k c a b e d f shtang av bv ev fv dv shtv mvb mvd mvba mvf mveb mve mvsht aban abcn aebn aekn
dban dbcn debn dekn aa ban bcn ba ekn ebn ea fa da shta mab mabat mabct mad maf maebt maekt mae masht
key_cher ;dett	tzv
	       )
  (vl-load-com)
  (load "funkcii_tmm.LSP")
  (load "tmm\\mex_6\\for_6.LSP")
  (setq old (getvar "osmode"))
  (setvar "osmode" 20903)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "timesi.TTF" )))	
  ); setq
  (entmod le)
  (vvod)
  (new_funcia_max)
  (setvar "osmode" old)
  (vla-ZoomExtents (vlax-get-acad-object))  
);_end of defun

(defun new_funcia_max (/ polus_mexan i)
  (setq polus_mexan '(0 0)
	muv (/ va 70.)
  )
  (momentvit)
  (initget 1 "Merc Vitt")
  (setq key_cher (getkword "\n������ �� ��������� ��� ������������ [M/V]: "))
  (add_pl mom_mas)
  (add_tex (getpoint "\n������� ��������: ") mumoment "M" "�/��")
  (add_tex (getpoint "\n������� �����: ") mufi "\U+03C6" "���/��")
  (grafint (reverse mom_mas) -150)
  (setq muat (* h mufi mumoment)) 
  (inercia)
  (if (equal key_cher "Merc")
    (progn
      (deltat graf_int '(0. -200))
      (tzvezda mas_i -150 muat)
      (add_tex (getpoint "\n������� �����: ") muat "A" "��/��")
      (detzvez dett tzv -100)
    )
    (progn
  	(vl-cmdf "._ucs" '(-120 -100) "")
  	(add_pl mas_i)
  	(deltat graf_int '(120 -150))
  	(vitten '(-120 0) 150. 120.)
     )
  )
  (initget 1 "Yes No")
  (if (equal (getkword "\n�������������� ������ � excel [Yes/No]: ") "Yes")
    (exel_expo_max)
  )  
)

(defun momentvit (/ polo os_x n_x max_mom fi
		  fs i)
  (initget 7)
  (setq mom_mas nil
	os_x (getdist "\n����� ��� �����: ")
	mufi (/ pi os_x 0.5)
  )
  (initget 7)
  (setq	n_x (getreal "\n���������� �������� [12]: ")
	shag_ugl (/ pi n_x 0.5)
	shag_os (/ os_x n_x)
	fi (* pi 0.5)
	tekush_ug 0.	
	i 1
  );_ setq
  (li (list '(0 50) '(0 -500)))  
  (repeat (fix (1+ n_x))
    ;(tochkim fi polus_mexan)
    (plansk polus_mexan 70. fi)
    (setq mdprot (* gf
		    (cosinus polus_mexan fv polus_mexan (polar polus_mexan (* -0.5 pi) 5))
		    loa
		    (/ (distance polus_mexan fv) 70.)
		 )
	  mdshtang (* gsht
		      (cosinus polus_mexan shtv polus_mexan (polar polus_mexan (* -0.5 pi) 5))
		      loa
		      (/ (distance polus_mexan shtv) 70.)
		   )
	  mdjidk (* gjid
		    (if (equal (angle polus_mexan shtv) (* pi 1.5) 1e-6)
		      0.
		      (cosinus polus_mexan shtv polus_mexan (polar polus_mexan (* -0.5 pi) 5))
		    )		    
		    loa
		    (/ (distance polus_mexan shtv) 70.)
		 )
	  md_sum (+ mdprot mdshtang
		    mdjidk)
	  mom_mas (cons (list tekush_ug md_sum) mom_mas)
	  tekush_ug (+ tekush_ug shag_os)
	  fi (- fi shag_ugl)
    ); setq
  );_ repeat
  (initget 7)
  (setq	max_mom (car (vl-sort (mapcar '(lambda (p) (abs (cadr p))) mom_mas) '>))
	mumoment (/ max_mom (getdist "\n������� Ymax ��� ��������: "))
	mom_mas (mapcar '(lambda (lis)
			   	 (list (car lis) (/ (cadr lis) mumoment))
			 ) mom_mas)
  ); setq
)

(defun inercia (/ masshtab fi cv dv i)
  (initget 7)
  (setq ;masshtab (getreal "\n��������� ����������� I: ")
	fi (* pi 0.5)
	tekush_ug 0.
	mas_i nil
	i 1
  )
  (setq fi (* pi 0.5))
  (repeat (length graf_int) 
    (plansk polus_mexan 70. fi)
    (setq i_sumar (+ ikriv idvig
		     (* (/ gf 9.8) (expt loa 2) (expt (/ (distance polus_mexan fv) 70.) 2))
		     (* (/ gsht 9.8) (expt loa 2) (expt (/ (distance polus_mexan shtv) 70.) 2))
		     (if (equal (angle polus_mexan shtv) (* pi 1.5) 1e-6)
		       0.
		       (* (/ gjid 9.8) (expt loa 2) (expt (/ (distance polus_mexan shtv) 70.) 2))
		     )
		  ); +
	  ;i_sumar (* i_sumar masshtab)
	  mas_i (cons (list i_sumar tekush_ug) mas_i)
	  tekush_ug (+ tekush_ug shag_os)
	  fi (- fi shag_ugl)
    ); setq
  ); repeat
  (initget 7)
  (setq	max_ii (car (vl-sort (mapcar '(lambda (p) (car p)) mas_i) '>))
	muii (/ max_ii (if (equal key_cher "Merc")
			 max_ii
			 (getdist "\n������� Ymax ��� ������� �������: ")
		       )
	     )
	mas_i (mapcar '(lambda (lis)
			   	 (list (/ (car lis) muii) (cadr lis))
			 ) mas_i)
  ); setq
); inercia

(defun exel_expo_max (/ fi polus i g_oex g_wkbs g_wb_put g_shs_put g_sheet1
		      k c a b e d f shtang)
  (setq fi (* 0.5 pi)	
	mas_i nil
	polus '(0 0)
	g_oex (vlax-get-or-create-object "Excel.Application")
	i 1
  )
  (vlax-put-property g_oex "Visible" :vlax-false)  
  (setq g_wkbs (vlax-get-property g_oex "Workbooks")        
        g_wb_put (vla-open g_wkbs (strcat total_adress "blanks\\maxovik_6.xls"))        
        g_shs_put (vlax-get-property g_wb_put "Worksheets")
	g_sheet1 (vlax-get-property g_shs_put "Item" 1)
  )
  (ex_put (+ ikriv idvig) 8 "B" g_sheet1)
  (ex_put k_nerav 16 "A" g_sheet1)
  (ex_put omeg1 16 "B" g_sheet1)
  (ex_put (* h mumoment mufi) 16 "D" g_sheet1)
  (ex_put muii 16 "H" g_sheet1)  
  (repeat 13
    (plansk polus 70. fi)
    (setq mdprot (* gf
		    (cosinus polus_mexan fv polus_mexan (polar polus_mexan (* -0.5 pi) 5))
		    loa
		    (/ (distance polus_mexan fv) 70.)
		 )
	  mdshtang (* gsht
		      (cosinus polus_mexan shtv polus_mexan (polar polus_mexan (* -0.5 pi) 5))
		      loa
		      (/ (distance polus_mexan shtv) 70.)
		   )
	  mdjidk (* gjid
		    (if (equal (angle polus_mexan shtv) (* pi 1.5) 1e-6)
		      0.
		      (cosinus polus_mexan shtv polus_mexan (polar polus_mexan (* -0.5 pi) 5))
		    )		    
		    loa
		    (/ (distance polus_mexan shtv) 70.)
		 )
	  fgjid (if (equal (angle polus_mexan shtv) (* pi 1.5) 1e-6)
		      0.
		      gjid
		    )
    ); setq
    (ex_put fgjid 3 i g_sheet1)
    (ex_put mdjidk 4 i g_sheet1)
    (ex_put mdprot 5 i g_sheet1)
    (ex_put mdshtang 6 i g_sheet1)   
    (setq i (1+ i)
	  fi (- fi (/ pi 6.))
    )
  ); repeat
  (setq fi (* 0.5 pi)
	i 1
  )
  (repeat 13
    (plansk polus 70. fi)
    (ex_put (* (/ gf 9.8) (expt loa 2) (expt (/ (distance polus fv) 70.) 2)) 9 i g_sheet1); IF
    (ex_put (* (/ gsht 9.8) (expt loa 2) (expt (/ (distance polus shtv) 70.) 2)) 10 i g_sheet1); I��
    (ex_put (if (equal (angle polus shtv) (* pi 1.5) 1e-6)
		       0.
		       (* (/ gjid 9.8) (expt loa 2) (expt (/ (distance polus shtv) 70.) 2))
		     )
      11 i g_sheet1); I���
    (setq i (1+ i)
	  fi (- fi (/ pi 6.))
    ); setq
  ); repeat
  (vlax-invoke-method g_wb_put "SaveCopyAs" (getfiled "������� ��� ��������� ���� � ��������" "c:\\" "xls" 129))
  (vlax-invoke-method g_wb_put "Close" :vlax-false :vlax-false)
  (vlax-invoke-method g_oex "Quit")
  (mapcar
    (function (lambda (x)
      (if
        (and x (not (vlax-object-released-p x)))
        (vlax-release-object x)
      );_ if
              );_ lambda
    );_ function
    (list g_shs_put
	  g_wb_put
	  g_wkbs g_sheet1
	  g_oex)
  )  
  (gc)
)
(c:max6)