(defun new_funcia_kin (/ polus_mexan fii vektorva vektoraa
		   	 polus_skor polus_uskor ugol0 ugolf pol)
  (initget 1 "Yes No")
  (if (equal "Yes" (getkword "\n��������� ��� 12 ������ ���������? [Yes/No]: "))
    (progn
      (initget 65)  
       (setq fii (* pi 0.5)
	     polus_mexan (getpoint "\n������� ��� ������� ��������: ")
       )
       (repeat 12
         (tochkim polus_mexan fii)
         (cherm polus_mexan)
         (setq fii (- fii (/ pi 6.)))
       )
       (initget 65)
       (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���������: ") mul "l" "�/��")
    ); progn
    (setq polus_mexan '(0 0))
  )  
  (initget 7)
  (setq vektorva (getdist "\n������� ����� ������� �������� ����� A: ")
	fii (* 0.5 pi)
	muv (/ va vektorva)
  )
  (initget 1 "Yes No")
  (if (equal "Yes" (getkword "\n��������� ��� 12 ������ ���������? [Yes/No]: "))
    (progn
      (repeat 12        
        (initget 65)    
        (plansk (setq pol (getpoint "\n������� ��� ������� ���� ���������: ")) vektorva fii)
        (cherv pol)
        (setq fii (- fii (/ pi 6)))
      ); repeat
      (initget 65)
      (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���������: ") muv "\U+03C5" "� �/��")
    ); progn
  ); if
  (initget 7)
  (setq vektoraa (getdist "\n������� ����� ������� ��������� ����� A: ")
	mua (/ aan vektoraa)
  )
  (initget 65)
  (setq polus_uskor (getpoint "\n������� ��� ������� ���� ���������: "))  
  (planusk polus_uskor vektoraa (- (* 0.5 pi) fi))
  (chera polus_uskor)
  (initget 65)
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���������: ") mua "A" "� �/��")  
  (initget 1 "Yes No")
  (if (equal (getkword "\n�������������� ������ � excel [Yes/No]: ") "Yes")
    (excel_export)
  )
)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;����� ������� ��������������� �������;;;;;;;;;;;;;;;;;;;;;;;;;
;----=========================������ ��������=================================-----
(defun cherm (o /)
  (li (list o a b c)) (li (list f d shtang))(li (list k e))
  (mapcar '(lambda (x) (add_circ 0.7 x))(list o a b c k e d))
)

;---==========================������ ���� ���������=======================---------
(defun cherv (p /)  
  (risskor p av "a" "v^A")
  (risskor av bv nil "v^BA")
  (risskor p bv "b" "v^B")
  (risskor p dv "d" "v^D")
  (risskor p fv "f" "v^F")
  (risskor p ev "e" "v^E")
  (risskor bv ev nil "v^EB")
  (risskor p shtv nil "v^��")
  (li (list dv shtv))
  (li (list fv dv))
  (add_tex p "p,o,c,k" nil nil)  
);_end of defun

;-----=========================������ ���� ���������=======================--------
(defun chera (p / i) 
  (risskor p aa "a" "a^A")
  (risskor aa ban nil "an^BA")
  (risskor aa ba nil "a^BA")
  (risskor ba ea nil "a^EB")
  (risskor p bcn nil "an^BC")
  (risskor bcn ba nil "a\U+03C4^BC")
  (risskor ban ba nil "a\U+03C4^BA")
  (risskor p ba "b" "a^B")
  (risskor p da "d" "a^D")
  (risskor p fa "f" "a^F")
  (risskor ba ebn nil "an^EB")
  (risskor ebn ea nil "a\U+03C4^EB")
  (risskor p ekn nil "an^EK")
  (risskor ekn ea nil "a\U+03C4^EK")
  (risskor p ea "e" "a^E")
  (risskor p shta nil "a^��")
  (li (list shta da fa))
  (add_tex p "\\A1;p\\H0.7x;\\S^1;\\H1.286x;,o,c,k" nil nil) 
)
;===� � � � �   � � � � � � �   � � � � � � � � � � � � � � �    � � � � � � �=====

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;�������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun c:kin6 (/ old le f n1 loa mul lab ab lbc bc lbd bd lbe be lek ek lef ef lx x ly y lxy1 xy1 gf gsht gjid fi omeg1 va aan
k c a b e d f shtang av bv ev fv dv shtv mvb mvd mvba mvf mveb mve mvsht aban abcn aebn aekn
dban dbcn debn dekn aa ban bcn ba ekn ebn ea fa da shta mab mabat mabct mad maf maebt maekt mae masht)
  (vl-load-com)
  (load "funkcii_tmm.LSP")
  (load "tmm\\mex_6\\for_6.LSP")
  (setq old (getvar "osmode"))
  (setvar "osmode" 20903)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "timesi.TTF" )))	
  ); setq
  (entmod le)
  (vvod)
  (new_funcia_kin)
  (setvar "osmode" old)
  (vla-ZoomExtents (vlax-get-acad-object))  
);_end of defun

(defun excel_export (/ g_oex g_wkbs g_wb_put g_shs_put g_sheet1 g_sheet2
		     fii i)
  (setq g_oex (vlax-get-or-create-object "Excel.Application"))
  (vlax-put-property g_oex "Visible" :vlax-false)  
  (setq g_wkbs (vlax-get-property g_oex "Workbooks")        
        g_wb_put (vla-open g_wkbs "c:\\Program files\\AutoCAD 2008\\Support\\blanks\\first_list_6.xls")        
        g_shs_put (vlax-get-property g_wb_put "Worksheets")
	g_sheet1 (vlax-get-property g_shs_put "Item" 1)
	g_sheet2 (vlax-get-property g_shs_put "Item" 2)
  )
  (ex_put loa 2 "B" g_sheet1)
  (ex_put lab 2 "C" g_sheet1)
  (ex_put lbc 2 "D" g_sheet1)
  (ex_put lbd 2 "E" g_sheet1)
  (ex_put lbe 2 "F" g_sheet1)
  (ex_put lek 2 "G" g_sheet1)
  (ex_put lef 2 "H" g_sheet1)
  (ex_put lx 2 "I" g_sheet1)
  (ex_put ly 2 "J" g_sheet1)
  (ex_put lxy1 2 "K" g_sheet1)
  (ex_put oa 3 "B" g_sheet1)
  (ex_put n1 4 "B" g_sheet1)
  (ex_put (* fi (/ 180. pi)) 8 "B" g_sheet1)
  (setq fii (* pi 0.5)
	i 2
  )
  (ex_put muv 3 "O" g_sheet2)
  (repeat 12    
    (plansk '(0 0) vektorva fii)
    (ex_put (distance av bv) 3 i g_sheet2)
    (ex_put (distance '(0 0) bv) 4 i g_sheet2)
    (ex_put (distance '(0 0) dv) 5 i g_sheet2)
    (ex_put (distance ev bv) 6 i g_sheet2)
    (ex_put (distance '(0 0) fv) 7 i g_sheet2)
    (ex_put (distance '(0 0) ev) 8 i g_sheet2)
    (setq fii (- fii (/ pi 6.))
	  i (1+ i)
    )
  ); repeat
  (planusk (setq polyc '(0 0)) vektoraa fi)
  (ex_put mua 24 "A" g_sheet2)
  (ex_put (distance polyc ba) 22 "B" g_sheet2)
  (ex_put (distance polyc da) 22 "C" g_sheet2)
  (ex_put (distance polyc fa) 22 "D" g_sheet2)
  (ex_put (distance polyc ea) 22 "E" g_sheet2)
  (ex_put (distance aa ba) 22 "F" g_sheet2)
  (ex_put (distance ba ea) 22 "H" g_sheet2)  
  (ex_put (/ mabat lab) 24 "B" g_sheet2)
  (ex_put (/ mabct lbc) 24 "C" g_sheet2)
  (ex_put (/ maebt lbe) 24 "D" g_sheet2)
  (ex_put (/ maekt lek) 24 "E" g_sheet2)
  (vlax-invoke-method g_wb_put "SaveCopyAs" (getfiled "������� ��� ��������� ���� � ��������" "c:\\" "xls" 129))
  (vlax-invoke-method g_wb_put "Close" :vlax-false :vlax-false)
  (vlax-invoke-method g_oex "Quit")
  (mapcar
    (function (lambda (x)
      (if
        (and x (not (vlax-object-released-p x)))
        (vlax-release-object x)
      );_ if
              );_ lambda
    );_ function
    (list g_shs_put
	  g_wb_put
	  g_wkbs g_sheet1 g_sheet2
	  g_oex)
  )  
  (gc) 
)
(c:kin6)