(defun zakon (/ fi hip1 hip2
	      ds1 ds2
	      toch shag1 shag2 i)
  (load "tmm_kulak.lsp")
  (setq fi 0.
	mas1 nil
	mas2 nil
	toch (getreal "\n������� ��������: ")
	point (getpoint "\n��� �������� ����������� ������������ �������: ")
	shag1 (/ fu toch)
	shag2 (/ fs toch)
  )
  (while
   (<= fi fu)
   (setq ds1 (cond
               ((equal nomer 1) (dhi1 fi fu h))
               ((equal nomer 2) (dhi2 fi fu h))
               ((equal nomer 3) (dhi3 fi fu h))
               ((equal nomer 4) (dhi4 fi fu h))
             )
         hip1 (cond
               ((equal nomer 1) (hi1 fi fu h))
               ((equal nomer 2) (hi2 fi fu h))
               ((equal nomer 3) (hi3 fi fu h))
               ((equal nomer 4) (hi4 fi fu h))
             )	 
         mas1 (cons (polar (polar point (* 0.5 pi) hip1) pi ds1) mas1)
         fi (+ fi shag1)
    );end_of_setq
  );end_of_while
  (setq fi 0.)
  (while
   (<= fi fs)
   (setq ds2 (cond
               ((equal nomer 1) (dhi1 fi fs h))
               ((equal nomer 2) (dhi2 fi fs h))
               ((equal nomer 3) (dhi3 fi fs h))
               ((equal nomer 4) (dhi4 fi fs h))
             )
         hip2 (cond
               ((equal nomer 1) (hi1 fi fs h))
               ((equal nomer 2) (hi2 fi fs h))
               ((equal nomer 3) (hi3 fi fs h))
               ((equal nomer 4) (hi4 fi fs h))
             )       
         mas2 (cons (polar (polar point (* 0.5 pi) hip2) 0. ds2) mas2)
         fi (+ fi shag2)
    );end_of_setq
  );end_of_while
  (setq
    mas1 (cons (polar point (* 0.5 pi) h) mas1)
    mas2 (cons (polar point (* 0.5 pi) h) mas2)
    mas2 (reverse mas2)
    point1 (kas_func mas1 (+ (* 0.5 pi) tetd))
    point2 (kas_func mas2 (- (* 0.5 pi) tetd))
  )
;;;  (repeat (- (length mas1) 2)
;;;    (if (< (angle (nth (1+ i) mas1) (nth i mas1)) (+ (* 0.5 pi) tetd))
;;;      (setq point1 (nth (1+ i) mas1))
;;;    )
;;;    (setq i (1+ i))
;;;  )
;;;  (setq i 1)
;;;  (repeat (- (length mas2) 2)
;;;    (if (< (angle (nth i mas2) (nth (1+ i) mas2)) (- (* 0.5 pi) tetd))
;;;      (setq point2 (nth (1+ i) mas2))
;;;    )
;;;    (setq i (1+ i))
;;;  )
  (setq cross_kas (inters point1 (polar point1 (+ (* 1.5 pi) tetd) 100.)
			  point2 (polar point2 (- (* 1.5 pi) tetd) 100.)
			  nil
		  ); inters
	cross_kas1 (inters point1 (polar point1 (+ (* 1.5 pi) tetd) 100.)
			   point (polar point (* 0.5 pi) 10.)
			   nil
		   ); inters
	cross_kas2 (inters point2 (polar point2 (- (* 1.5 pi) tetd) 100.)
			   point (polar point (* 0.5 pi) 10.)
			   nil
		   ); inters
	ex1 (polar point 0. ex)
	ex2 (car (vl-sort (list (inters ex1 (polar ex1 (* 0.5 pi) 10.)
					point1 (polar point1 (+ (* 1.5 pi) tetd) 100.)
					nil
				)
				(inters ex1 (polar ex1 (* 0.5 pi) 10.)
					point2 (polar point2 (- (* 1.5 pi) tetd) 100.)
					nil
				)
			  ); list
			  '(lambda (e1 e2) (< (cadr e1) (cadr e2)))
		 )
	    )	 
	cross_kas1ex (inters point1 (polar point1 (+ (* 1.5 pi) tetd) 100.)
			     ex1 ex2
			     nil
		     ); inters
	cross_kas2ex (inters point2 (polar point2 (- (* 1.5 pi) tetd) 100.)
			     ex1 ex2
			     nil
		     ); inters
	min_radius (max (distance point cross_kas)
			(distance point cross_kas1ex)
			(distance point cross_kas2ex)
		   )
  )
);end of defun

(defun cherzak (/)
  (add_pl (append mas1 mas2))
  (li (list (car (vl-sort (list cross_kas1 cross_kas2)
			  '(lambda (e1 e2) (< (cadr e1) (cadr e2)))
		 ); vl-sort
	    ); car
	    (polar point (* 0.5 pi) h)
      ); list
  )
  (li (list ex1 (polar ex2 (* 1.5 pi) 10.)))
  (li (list (polar point1 (+ (* 0.5 pi) tetd) 20.)
	    (polar cross_kas (+ (* 1.5 pi) tetd) 20.)
      )
  )
  (li (list (polar point2 (- (* 0.5 pi) tetd) 20.)
	    (polar cross_kas (- (* 1.5 pi) tetd) 20.)
      )
  )
  
  (muu "l" mul "�/��")  
)

