;���� ��� ����������� �� ��������������� ������� � 17 ��� 2
(defun vvod (/ f)
  (initget 6)
  (setq f (getreal "\n������� ���� �� ")
        oa (getdist "\n������� OA [mm] ")
        n1 (getreal "\n������� n1 ")
	omeg (* pi n1 (/ 1. 30.))
        loa (getreal "\n������� OA [m] ")
;	m2 (getreal "\n������� m2 [kg] ")
	m3 (getreal "\n������� m3 [kg] ")
	pb (getreal "\n������� Pb [H] ")
	pd (getreal "\n������� Pd [H] ")
	;g2 (* m2 9.8)
	g3 (* m3 9.8)
        mul (/ loa oa)
	;����� �� �����
	
	;�������� � ���������
	mva (* omeg loa)
	maa (* omeg omeg loa)
	fi (* f (/ pi 180.))
  );end of setq
);end of vvod

;;;;;;;;;;;;;;;;;;;;;�������, ������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;----------------------------------------------------------------------------------
;;;;;;---=================������� ���������� ����� ���������================-------
(defun tochkim ()
  (setq
    o (getpoint "\n��������: ")
    a (polar o fi oa)
    c (polar o (* 1.5 pi) (* 5 oa))
    b (polar c (angle c a) (* 7 oa))
    d (polar c (angle a c) oa)
    s3 (polar c (angle c a) (* 3.5 oa))
    masivm (list o a b c d s3)
    ac (distance a c)
    lac (* mul ac)
  )
)
;----------------------------------------------------------------------------------
;--==������� ����� ����� ��������� � ���������� ��������� � ������ ���������===----
(defun plansk (/)
  (initget 1)
  (setq pb (getdist "\n������� ����� ������� �������� Va [mm] ")
  	muv (/ mva pb)
	pol (getpoint "\n������� ����� ����� ��������� ")  
  ;======����� ����� ���������========  
  	av12 (polar pol (+ (angle o a) (/ pi 2)) pb)
	av3 (inters av12 (polar av12 (angle b a) 100.)
		    pol (polar pol (- (angle b a) (/ pi 2.)) 100.)
		    nil
	    )
	s3v (polar pol (angle pol av3) (* (distance pol av3) (/ (distance c s3) (distance c a))))
	bv (polar pol (angle pol av3) (* (distance pol av3) (/ (distance c b) (distance c a))))
	dv (polar pol (angle av3 pol) (* (distance pol av3) (/ (distance c d) (distance c a))))
	mvb (* muv (distance pol bv))
	mvd (* muv (distance pol dv))
	mva3 (* muv (distance pol av3))
	mvs3 (* muv (distance pol s3v))
	mva32 (* muv (distance av12 av3))
	masv (list mva3 mvb mvd mvs3 mva32)
	masb (list "\\A1;A\\H0.7x;\\S^3;" "B" "D" "\\A1;S\\H0.7x;\\S^3;"
		   "\\A1;A\\H0.7x;\\S^3;\\H1.4286x;A\\H0.7x;\\S^1")
  ;========������� ��������====================
        ome3 (/ mva3 lac)	
  ;=====���������� ���������=====
	maa3cn (/ (expt mva3 2) lac)
	maa3a1k (* 2. ome3 mva32)
  );_end of setq
  (duga c a pol av3 0.3 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^3;\\H2;")
);_end of defun


;----------------------------------------------------------------------------------
;---------========������� ����� ����� ��������� � ������ ���������========---------
(defun planusk ()
  (initget 1)
  (setq pa (getdist "\n������� ����� ������� ��������� ����� B [mm] ")
	mua (/ maa pa)
	polu (getpoint "\n������� ����� ����� ��������� ")
	;=====����� �������� ���������� ���������=====
	laa3cn (/ maa3cn mua)
	laa3a1k (/ maa3a1k mua)
	;=====����� ����� ���������=====
	aa (polar polu (angle a o) pa)
	a3cn (polar polu (angle a c) laa3cn)
	aa3k (polar aa
		    (if (clockwise-p c a (polar a (angle pol av3) 100))
		      (+ (angle av12 av3) (* pi 0.5))
		      (- (angle av12 av3) (* pi 0.5))
		    )
		    laa3a1k
	     )
	aa3 (inters a3cn (polar a3cn (+ (* 0.5 pi) (angle a c)) 100)
		    aa3k (polar aa3k (angle a c) 100)
		    nil
	    )
	s3a (polar polu (angle polu aa3) (* (distance polu aa3) (/ (distance c s3) (distance c a))))
	ba (polar polu (angle polu aa3) (* (distance polu aa3) (/ (distance c b) (distance c a))))
	da (polar polu (angle aa3 polu) (* (distance polu aa3) (/ (distance c d) (distance c a))))
	;=====������ ���������� ���������=========
	mad (* mua (distance polu da))
	mab (* mua (distance polu ba))
	mas3 (* mua (distance polu s3a))
	ma3a1 (* mua (distance aa3 aa))
	;=====������ �������������� ���������=========
	ma3ct (* mua (distance a3cn aa3))
	ma3a1r (* mua (distance aa3k aa3))
	;������� ���������
	eps3 (/ ma3ct lac)	
  );_end of setq
  (duga c a a3cn aa3 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^3;\\H2;")  
);_end of defun


(defun cherm (/ i le dmu)
  (li (list o a)) (li (list b d))
  (mapcar '(lambda (t1) (add_circ 0.7 t1)) masivm)
  (add_text s3 "\\A1;S\\H0.7x;\\S^3;\\H1.4286x;")
  (add_text o "O")
  (add_text a "A")
  (add_text c "C")(add_text d "D") (add_text b "B")
  (add_opor o (+ (* pi 0.5) fi))
  (add_opor c pi)
  (add_block a (angle a c))  
  (duga o a a (polar a (+ (angle o a) (* 0.5 pi)) 10.) 0.5 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^1;\\H2;")
  (muul loa oa)
);_end of defun

(defun cherv (/)
  (add_tex pol "p,o,c" nil nil)
  (risskor pol bv "b" "v^B")
  (risskor pol dv "d" "v^D")
  (risskor pol av3 nil "v^A3")
  (risskor pol av12 nil "v^A1")
  (risskor pol s3v nil "v^S3")
  (risskor av12 av3 nil "v^A3A1")
  (add_text av3 "\\A1;a\\H0.7x;\\S^3;\\H1.42857x;")
  (add_text av12 "\\A1;a\\H0.7x;\\S^1;\\H1.42857x;")
  (add_text s3v "\\A1;s\\H0.7x;\\S^3;\\H1.42857x;")
  (muuv mva pb)
 ; (formv omeg lab)
);_end of defun

(defun chera (/)  
  (add_tex polu "\\A1;p\\H0.7x;\\S^1;\\H1.42857x;,o,c" nil nil)
  (risskor polu ba "b" "a^B")
  (risskor polu aa nil "a^A1")
  (risskor polu aa3 nil "a^A3")
  (risskor polu da "d" "a^D")
  (risskor aa aa3 nil "a^A3A1")
  (risskor polu s3a nil "a^S3")
  (risskor aa aa3k nil "ak^A3A1")
  (risskor aa3k aa3 nil "ar^A3A1")  
  (risskor polu a3cn nil "an^A3C")
  (risskor a3cn aa3 nil "a\U+03C4^A3C")
  (add_text s3a "\\A1;s\\H0.7x;\\S^3;\\H1.42857x;")
  (add_text aa "\\A1;a\\H0.7x;\\S^1;\\H1.42857x;")
  (add_text aa3 "\\A1;a\\H0.7x;\\S^3;\\H1.42857x;")
  (kor_cher aa aa3k av12 av3 "v^A3A1" "ak^A3A1" "\\A1;\\H2;\U+03C9\\H0.7x;\\S^3;\\H2;")
  (muua maa pa)
)

(defun omep ()
    
  ;���������
  (formsk2 muv (distance pol bv) "B" "pb" t)
  (formsk2 muv (distance pol dv) "D" "pd" t)
  (formsk2 muv (distance pol s3v) "S3" "ps\\H0.7x;\\S^3;\\H1.4286x;" t)
  (formsk2 muv (distance av12 av3) "A3A1" "a\\H0.7x;\\S^3;\\H1.4286x;a\\H0.7x;\\S^1;\\H1.4286x;" t)
;;;  
;;;  (add_text (getpoint "\n��������������� speed point D: ")
;;;	    (strcat "\\A1;pd=pc\U+00D7\\H0.7x;\\SED/EC;\\H1.4286x;="
;;;		    (rtos (distance pol cv) 2 0) "\U+00D7\\H0.7x;\\S" (rtos de 2 0) "/"
;;;		    (rtos ce 2 0)";\\H1.4286x;=" (rtos (* (distance pol cv) (/ de ce)) 2 1) " ��"
;;;	    )
;;;  )
;;;  (add_text (getpoint "\n��������������� speed point F: ")
;;;	    (strcat "\\A1;pf=pc\U+00D7\\H0.7x;\\SED/EC;\\H1.4286x;="
;;;		    (rtos (distance pol cv) 2 0) "\U+00D7\\H0.7x;\\S" (rtos ef 2 0) "/"
;;;		    (rtos ce 2 0)";\\H1.4286x;=" (rtos (* (distance pol cv) (/ ef ce)) 2 1) " ��"
;;;	    )
;;;  )
  (formuskt2 "AC" (distance a3cn aa3) mua)  
  (formusk "aAC" mva3 lac)     
  (formsk2 mua (distance polu ba) "B" "p\\H0.7x;\\S^1;\\H1.4286x;b" nil)
  (formsk2 mua (distance polu da) "D" "p\\H0.7x;\\S^1;\\H1.4286x;d" nil)

  (formsk2 mua (distance polu s3a) "S3" "p\\H0.7x;\\S^1;\\H1.4286x;s\\H0.7x;\\S^3;\\H1.4286x;" nil)
  (formsk2 mua (distance aa aa3) "A3A1" "a\\H0.7x;\\S^3;\\H1.4286x;a\\H0.7x;\\S^1;\\H1.4286x;" nil)
  
;;;  (add_text (getpoint "\n��������������� acel point D: ")
;;;	    (strcat "\\A1;p\\H0.7x;\\S^1;\\H1.4286x;d=p\\H0.7x;\\S^1;\\H1.4286x;c"
;;;		    "\U+00D7\\H0.7x;\\SED/EC;\\H1.4286x;="(rtos (distance polu ca) 2 0)
;;;		    "\U+00D7\\H0.7x;\\S" (rtos de 2 0) "/"(rtos ce 2 0)
;;;		    ";\\H1.4286x;=" (rtos (* (distance polu ca) (/ de ce)) 2 1) " ��"
;;;	    )
;;;  )
;;;  (add_text (getpoint "\n��������������� acel point F: ")
;;;	    (strcat "\\A1;p\\H0.7x;\\S^1;\\H1.4286x;f=p\\H0.7x;\\S^1;\\H1.4286x;c"
;;;		    "\U+00D7\\H0.7x;\\SEF/EC;\\H1.4286x;="(rtos (distance polu ca) 2 0)
;;;		    "\U+00D7\\H0.7x;\\S" (rtos ef 2 0) "/"(rtos ce 2 0)
;;;		    ";\\H1.4286x;=" (rtos (* (distance polu ca) (/ ef ce)) 2 1) " ��"
;;;	    )
;;;  )
  (ugsu "ome3AC" mva3 lac)  
  (ugsu "eps3AC" ma3ct lac)  
)


(defun grup23 ()
  (tochkim)
  (li (list b d)) (mapcar '(lambda (t1) (add_circ 0.7 t1)) (list a b c d s3))
  (add_text s3 "\\A1;S\\H0.7x;\\S^3;\\H1.4286x;")
  (add_text a "A")
  (add_text b "B")(add_text d "D")(add_text c "C") 
  (add_block a (angle a b)) 
  (setq sf3v (polar s3 (angle s3a polu) 20)	
	sf3 (* mas3 m3)
	g3v (polar s3 (/ pi -2) 20)
	Pbv (polar b 0. 20)
	Pdn (polar d (- (angle d a) (* 0.5 pi)) 20)
	
	r12nv (polar a (angle b a) 20)
	r03nv (polar c (angle b a) 20)
	r12tv (polar a (+ (angle b a) (* 0.5 pi)) 20)
	r03tv (polar c (+ (angle b a) (* 0.5 pi)) 20)
	r12t (/ (+ (moment g3 s3 g3v c)
		   (moment sf3 s3 sf3v c)
		   (moment Pd Pdn d c)
		   (moment Pb b Pbv c)
		)
	        ac
	     );_end of /
	r03t (/ (+ (moment g3 s3 g3v a)
		   (moment sf3 s3 sf3v a)
		   (moment Pd Pdn d a)
		   (moment Pb b Pbv a)
		)
	        ac
	     );_end of /
	r12tv (if (minusp r12t)
		(polar a (+ (angle b a) (/ pi 2)) 20)
		(polar a (- (angle b a) (/ pi 2)) 20)
	      )
	r03tv (if (minusp r03t)
		(polar c (+ (angle b a) (/ pi 2)) 20)
		(polar c (- (angle b a) (/ pi 2)) 20)
	      )
  );_end of setq
  (risskor s3 sf3v  nil "�^3")
  (risskor s3 g3v nil "G^3")  
  (risskor b pbv nil "P^B")
  (risskor pdn d nil "P^D")
 ; (risskor a r12nv  nil "Rn^12")
  (risskor a r12tv  nil "R^12")
  (risskor c r03nv  nil "Rn^03")
  (risskor c r03tv  nil "R\U+03C4^03")
  (plansil23)
);_end of defun

(defun plansil23 (/ )
  (setq asil4 (getpoint "\n������� ��� ������� ���� ��� ��� ������ 2-3: ")
	maxsil45 (getreal "\n������� � �� ����� ������� ���� �� �����: ")	
	muf23 (/ (max g3 pb pd sf3 (abs r12t) (abs r03t)) maxsil45)
	;����� ����� ��� R03t+G3+�3+PD+PB+R12+R03n=0
	bsil4 (polar asil4 (angle c r03tv) (/ (abs r03t) muf23));R03t
	csil4 (polar bsil4 (* pi -0.5) (/ g3 muf23));G3
	dsil4 (polar csil4 (angle s3a polu) (/ sf3 muf23));�3
	esil4 (polar dsil4 (- (angle b a) (* 0.5 pi)) (/ pd muf23));PD
	fsil4 (polar esil4 0. (/ pb muf23));PB
	gsil4 (inters fsil4 (polar fsil4 (+ (* 0.5 pi) (angle a b)) 100.);R12
		      asil4 (polar asil4 (angle a b) 100.);R03n
		      nil
	      )    
	r03n (* muf23 (distance gsil4 asil4))
	;������� ������ ������� R24
	r12 (* muf23 (distance gsil4 fsil4))
	r03 (* muf23 (distance gsil4 bsil4))
	ugolr12 (angle gsil4 fsil4)
  );_end of setq
  (cherplan23)
);_end of defun

(defun cherplan23 (/ gde);R12t+G2+�2+PD+PB+G3+�3+R05+R12n=0
  (risskor asil4 bsil4 "b" "R\U+03C4^03")
  (risskor bsil4 csil4 "c" "G^3")
  (risskor csil4 dsil4 "d" "�^3")
  (risskor dsil4 esil4 "e" "P^D")
  (risskor esil4 fsil4 "f" "P^B")
  (risskor fsil4 gsil4 "g" "R^12")
  (risskor gsil4 asil4 "a" "Rn^12")
  (risskor gsil4 bsil4 nil "R^12")
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���: ") muf23 "F" "�/��") 
)

(defun grup01 ()
  (tochkim)
  (duga o a a (polar a (+ (angle o a) (* 0.5 pi)) 10.) 0.25 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^1;\\H2;")
  (li (list o a)) (mapcar '(lambda (t1) (add_circ 0.7 t1)) (list o a))
  (add_text a "A")
  (add_text o "O") 
  (add_opor o (+ (* pi 0.5) fi))
  (setq r12v (polar a ugolr12 20)
	fy (/ (moment r12 a r12v o)
	        oa
	     );_end of /
	fyv (if (minusp fy)
		(polar a (+ (angle o a) (/ pi 2)) 20)
		(polar a (- (angle o a) (/ pi 2)) 20)
	      )
  );_end of setq
  (risskor a fyv  nil "F^y")
  (risskor a r12v nil "R^21")
  (duga o a a (polar a (if (minusp fy)
			 (+ (angle o a) (* 0.5 pi))
			 (- (angle o a) (* 0.5 pi))
		       ) 10.)
		      0.75 "\\A1;\\H2;M\\H0.7x;\\S^Y;\\H2;"
  )
  (setq muf01 (/ r12 (getdist "\nMaxR12"))
	asi01 (getpoint "\nPlan sil 01: ")
	bsi01 (polar asi01 (angle a r12v) (/ r12 muf01))
	csi01 (polar bsi01 (angle a fyv) (/ (abs fy) muf01))
	r01 (* muf01 (distance asi01 csi01))
  )
  (risskor asi01 bsi01 "b" "R^21")
  (risskor bsi01 csi01 "c" "F^Y")
  (risskor csi01 asi01 "a" "R^01")
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���: ") muf01 "F" "�/��") 
);_end of defun

;=====================================================================================================

(defun c:sem20_2 (/ osm le
ab omeg lab mul bc ce de ef bs ae dk fl lbc lce ldk lfl mvb mab fi 
a e b c d f l k s2 masivm pb muv pol bv cv dv fv kv s2v lv mvc mvd mvf mvl mvk mvs2 mvcb mvkd mvlf ome2 ome3 ome4 ome5 
macbn macen makdn malfn pa mua polu lcbn lcen lkdn llfn ba cen ca da fa lfn kdn la ka
		  mad mac mal mak maf macbt macet malft makdt eps2 eps3 eps4 eps5)
  (VL-LOAD-COM)
  (setq osm (getvar "OSMODE"))
  (setvar "OSMODE" 20919)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "TIMESI.TTF" )))
	le (append le (list (cons 2 "�����")))	
  )
  (entmakex le)
  (setvar "textstyle" "�����")
  (load "funkcii_tmm.lsp")
  (load "tmm_semi.lsp")
  (vvod)
  (tochkim)
  (cherm)
  (plansk)
  (cherv)
  (planusk)
  (chera)  
  (omep)
  (grup23)
  (grup01)
  (gukovsk)
  (alert (strcat "\n" (rtos fur) "\n" (rtos fy)))
  (setvar "OSMODE" osm)
  (setvar "dimtxsty" "�����")
)

(c:sem20_2)





(defun gukovsk (/ ugol_plus
pb muv pol av bv dv s2v mvb mvd mvs2 mvba masv masb ome2 maban
g2ver g3ver sf3_verg sf2_verg pd_v pb_v
momg2 momsf3 momsf2 mompd mompb)
  (initget 1 "Yes No")
  (if (equal (getkword "\n������� �� 90 [Yes/No]: ") "Yes")
    (setq ugol_plus (* 0.5 pi))
    (setq ugol_plus (* -0.5 pi))
  )
  (plansk)
  (setq qav (polar pol (+ (angle pol av) ugol_plus) (distance pol av))
	qbv (polar pol (+ (angle pol bv) ugol_plus) (distance pol bv))
	qdv (polar pol (+ (angle pol dv) ugol_plus) (distance pol dv))	
	qs2v (polar pol (+ (angle pol s2v) ugol_plus) (distance pol s2v))
  )
  (li (list pol qav qdv pol))
  (li (list pol qs2v))
  (li (list pol qbv))  
  (add_tex qav "a" nil nil)
  (add_tex qbv "b" nil nil)
  (add_tex qdv "d" nil nil)  
  (add_tex qs2v "\\A1;s\\H0.7x;\\S^2;" nil nil)
  (add_tex pol "p,o" nil nil)
  (setq g2ver  (polar qs2v (* -0.5 pi) 20)
	g3ver  (polar qbv (* -0.5 pi) 20)	
	sf3_verg (polar qbv (angle ba polu) 20)
	sf2_verg (polar qs2v (angle s2a polu) 20)	
	pd_v (polar qdv (+ (angle d a) (* 0.5 pi)) 20)
	pb_v (polar qbv 0. 20)
  );_end of setq
  (risskor qs2v g2ver nil "G^2")
  (risskor qs2v sf2_verg nil "�^2")
  (risskor qbv g3ver nil "G^3")
  (risskor qbv sf3_verg nil "�^3")
  (risskor qbv pb_v nil "P^B")
  (risskor qdv pd_v nil "P^D")
  (setq momg2 (moment g2 qs2v g2ver pol)
	momsf3 (moment sf3 qbv sf3_verg pol)
	momsf2 (moment sf2 qs2v sf2_verg pol)	
	mompd (moment pd qdv pd_v pol)
	mompb (moment pb qbv pb_v pol)
	fur (/ (+ momg2 momsf3 momsf2 mompd mompb)
	       pb
	    )
  );_end of setq
)