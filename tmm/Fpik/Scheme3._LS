;���� ��� ����������� �� ��������������� ������� � 17 ��� 2
(defun vvod (/ f)
  (initget 6)
  (setq f (getreal "\n������� ���� �� ")
        oa (getdist "\n������� OA [mm] ")
        n1 60.;(getreal "\n������� n1 ")
	omeg (* pi n1 (/ 1. 30.))
        loa 0.12;(getreal "\n������� OA [m] ")
	m2 40.;(getreal "\n������� m2 [kg] ")
	m3 150.;(getreal "\n������� m3 [kg] ")
	pb (getreal "\n������� Pb [H] ")
	pd (getreal "\n������� Pd [H] ")
	g2 (* m2 9.8)
	g3 (* m3 9.8)
        mul (/ loa oa)
	;����� �� �����
	ab (* 5. oa)
	bc (* 3. oa)
	oc (* 6. oa)
	ad (* 0.5 oa)
	as2 (* 3. oa)
	cs3 (* 2. oa)
	;�������� �����
	lab (* loa 5.)
	lbc (* 3. loa)
	loc (* 6. loa)
	lad (* 0.5 loa)
	las2 (* 3. loa)
	lcs3 (* 2. loa)
	;�������� � ���������
	mva (* omeg loa)
	maa (* omeg omeg loa)
	fi (* f (/ pi 180.))
  );end of setq
);end of vvod

;;;;;;;;;;;;;;;;;;;;;�������, ������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;----------------------------------------------------------------------------------
;;;;;;---=================������� ���������� ����� ���������================-------
(defun tochkim ()
  (setq
    o (getpoint "\n��������: ")
    a (polar o fi oa)
    c (polar o 0. oc)
    b (car (vl-sort
	     (circint c bc a ab)
	     (function (lambda (e1 e2) (> (cadr e1) (cadr e2)))) 
	   )
      )
    s2 (polar a (angle a b) as2)
    d (polar a (angle b a) ad)
    s3 (polar c (angle c b) cs3)
    masivm (list o a b c d s2 s3)       
  )
)
;----------------------------------------------------------------------------------
;--==������� ����� ����� ��������� � ���������� ��������� � ������ ���������===----
(defun plansk (/)
  (initget 1)
  (setq pb (getdist "\n������� ����� ������� �������� Va [mm] ")
  	muv (/ mva pb)
	pol (getpoint "\n������� ����� ����� ��������� ")  
  ;======����� ����� ���������========  
  	av (polar pol (+ (angle o a) (/ pi 2)) pb)		
	bv (inters av (polar av (+ (angle a b) (* 0.5 pi)) 1e15)
		   pol (polar pol (+ (angle c b) (* 0.5 pi)) 1e15)
		   nil
	   )
	dv (polar av (angle bv av) (* (distance av bv) (/ ad ab)))
	s2v (polar av (angle av bv) (* (distance av bv) (/ as2 ab)))
	s3v (polar pol (angle pol bv) (* (distance pol bv) (/ cs3 bc)))
	mvb (* muv (distance pol bv))
	mvd (* muv (distance pol dv))
	mvs2 (* muv (distance pol s2v))
	mvs3 (* muv (distance pol s3v))
	mvba (* muv (distance av bv))
	;masv (list mvb mvd mvs2 mvs3 mvba)
	;masb (list "C" "D" "F" "K" "L" "\\A1;S\\H0.7x;\\S^2;" "CB" "KD" "LF")
  ;========������� ��������====================
        ome2 (/ mvba lab)
	ome3 (/ mvb lbc)
  ;=====���������� ���������=====
	maban (/ (expt mvba 2) lab)
	mabcn (/ (expt mvb 2) lbc)
  );_end of setq
  (duga a b av bv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^2;\\H2;")
  (duga c b pol bv 0.4 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^3;\\H2;")
);_end of defun


(defun gukovsk (/ ugol_plus
pb muv pol av bv dv s2v mvb mvd mvs2 mvba masv masb ome2 maban
g2ver g3ver sf3_verg sf2_verg pd_v pb_v
momg2 momsf3 momsf2 mompd mompb)
  (initget 1 "Yes No")
  (if (equal (getkword "\n������� �� 90 [Yes/No]: ") "Yes")
    (setq ugol_plus (* 0.5 pi))
    (setq ugol_plus (* -0.5 pi))
  )
  (plansk)
  (setq qav (polar pol (+ (angle pol av) ugol_plus) (distance pol av))
	qbv (polar pol (+ (angle pol bv) ugol_plus) (distance pol bv))
	qdv (polar pol (+ (angle pol dv) ugol_plus) (distance pol dv))	
	qs2v (polar pol (+ (angle pol s2v) ugol_plus) (distance pol s2v))
	qs3v (polar pol (+ (angle pol s3v) ugol_plus) (distance pol s3v))
  )
  (li (list pol qbv qdv pol qav))
  (li (list pol qs2v))  
  (add_tex qav "a" nil nil)
  (add_tex qbv "b" nil nil)
  (add_tex qdv "d" nil nil)  
  (add_tex qs2v "\\A1;s\\H0.7x;\\S^2;" nil nil)
  (add_tex qs3v "\\A1;s\\H0.7x;\\S^3;" nil nil)
  (add_tex pol "p,o,c" nil nil)
  (setq g2ver  (polar qs2v (* -0.5 pi) 20)
	g3ver  (polar qs3v (* -0.5 pi) 20)	
	sf3_verg (polar qs3v (angle s3a polu) 20)
	sf2_verg (polar qs2v (angle s2a polu) 20)	
	pd_v (polar qdv (+ (angle d a) (* 0.5 pi)) 20)
	;pb_v (polar qbv 0. 20)
  );_end of setq
  (risskor qs2v g2ver nil "G^2")
  (risskor qs2v sf2_verg nil "�^2")
  (risskor qs3v g3ver nil "G^3")
  (risskor qs3v sf3_verg nil "�^3")
  ;(risskor qbv pb_v nil "P^B")
  (risskor qdv pd_v nil "P^D")
  (setq momg2 (moment g2 qs2v g2ver pol)
	momg3 (moment g3 qs3v g3ver pol)
	momsf3 (moment sf3 qs3v sf3_verg pol)
	momsf2 (moment sf2 qs2v sf2_verg pol)	
	mompd (moment pd qdv pd_v pol)
	;mompb (moment pb qbv pb_v pol)
	fur (/ (+ momg2 momsf3 momsf2 momg3 mompd ;mompb
		  )
	       pb
	    )
  );_end of setq
)




;----------------------------------------------------------------------------------
;---------========������� ����� ����� ��������� � ������ ���������========---------
(defun planusk ()
  (initget 1)
  (setq pa (getdist "\n������� ����� ������� ��������� ����� B [mm] ")
	mua (/ maa pa)
	polu (getpoint "\n������� ����� ����� ��������� ")
	;=====����� �������� ���������� ���������=====
	lban (/ maban mua)
	lbcn (/ mabcn mua)
	;=====����� ����� ���������=====
	aa (polar polu (angle a o) pa)
	ban (polar aa (angle b a) lban)
	bcn (polar polu (angle b c) lbcn)
	ba (inters ban (polar ban (+ (angle a b) (* 0.5 pi)) 1e15)
		   polu (polar polu (+ (angle b c) (* 0.5 pi)) 1e15)
		   nil
	   )
	da (polar aa (angle ba aa) (* (distance aa ba) (/ ad ab)))
	s2a (polar aa (angle aa ba) (* (distance aa ba) (/ as2 ab)))
	s3a (polar polu (angle polu ba) (* (distance polu ba) (/ cs3 bc)))
	;=====������ ���������� ���������=========
	mad (* mua (distance polu da))
	mab (* mua (distance polu ba))
	mas2 (* mua (distance polu s2a))
	mas3 (* mua (distance polu s3a))
	maba (* mua (distance aa ba))
	;=====������ �������������� ���������=========
	mabat (* mua (distance ban ba))
	mabct (* mua (distance bcn ba))
	;������� ���������
	eps2 (/ mabat lab)
	eps3 (/ mabct lbc)
  );_end of setq
  (duga a b ban ba 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^2;\\H2;")
  (duga c b bcn ba 0.6 "\\A1;\\H2;\U+03B5\\H0.7x;\\S^3;\\H2;") 
);_end of defun


(defun cherm (/ i le dmu)
  (li (list o a)) (li (list c b d))
  (mapcar '(lambda (t1) (add_circ 0.7 t1)) masivm)
  (add_text s2 "\\A1;S\\H0.7x;\\S^2;\\H1.4286x;")
  (add_text s3 "\\A1;S\\H0.7x;\\S^3;\\H1.4286x;")
  (add_text o "O")
  (add_text a "A")(add_text c "C")
  (add_text b "B")(add_text d "D")
  (add_opor o (+ (* pi 0.5) fi))
  (add_opor c (+ pi (angle o c)))  
  (duga o a a (polar a (+ (angle o a) (* 0.5 pi)) 10.) 0.5 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^1;\\H2;")
  (muul loa oa)
);_end of defun

(defun cherv (/)
  (add_tex pol "p,o,c" nil nil)
  (risskor pol bv "b" "v^B")
  (risskor pol dv "d" "v^D")
  (risskor pol av "a" "v^A")
  (risskor pol s2v "s^2" "v^S2")
  (risskor pol s3v "s^3" "v^S3")
  (li (list av dv))
  (risskor av bv nil "v^BA")  
  (muuv mva pb)
 ; (formv omeg lab)
);_end of defun

(defun chera (/)  
  (add_tex polu "\\A1;p\\H0.7x;\\S^1;\\H1.42857x;,a,c" nil nil)
  (risskor polu ba "b" "a^B")
  (risskor polu aa "a" "a^A")
  (risskor polu da "d" "a^D")
  (risskor aa ba nil "a^BA")
  (risskor polu s2a "s^2" "a^S2")
  (risskor polu s3a "s^3" "a^S3")
  (risskor aa ban nil "an^BA")  
  (risskor ban ba nil "a\U+03C4^BA")
  (risskor polu bcn nil "an^BC")
  (risskor bcn ba nil "a\U+03C4^BC")
  (li (list aa da)) 
  (muua maa pa)
)

(defun omep ()
    
  ;���������
  (formsk2 muv (distance pol bv) "B" "pb" t)
  (formsk2 muv (distance pol dv) "D" "pd" t)
  (formsk2 muv (distance pol s2v) "S2" "ps\\H0.7x;\\S^2;\\H1.4286x;" t)
  (formsk2 muv (distance pol s3v) "S3" "ps\\H0.7x;\\S^3;\\H1.4286x;" t)
  (formsk2 muv (distance av bv) "BA" "ab" t)
;;;  
;;;  (add_text (getpoint "\n��������������� speed point D: ")
;;;	    (strcat "\\A1;pd=pc\U+00D7\\H0.7x;\\SED/EC;\\H1.4286x;="
;;;		    (rtos (distance pol cv) 2 0) "\U+00D7\\H0.7x;\\S" (rtos de 2 0) "/"
;;;		    (rtos ce 2 0)";\\H1.4286x;=" (rtos (* (distance pol cv) (/ de ce)) 2 1) " ��"
;;;	    )
;;;  )
;;;  (add_text (getpoint "\n��������������� speed point F: ")
;;;	    (strcat "\\A1;pf=pc\U+00D7\\H0.7x;\\SED/EC;\\H1.4286x;="
;;;		    (rtos (distance pol cv) 2 0) "\U+00D7\\H0.7x;\\S" (rtos ef 2 0) "/"
;;;		    (rtos ce 2 0)";\\H1.4286x;=" (rtos (* (distance pol cv) (/ ef ce)) 2 1) " ��"
;;;	    )
;;;  )

  
  (formuskt2 "BA" (distance ban ba) mua)
  (formuskt2 "BC" (distance bcn ba) mua)
  
  (formusk "aBA" mvba lab)
  (formusk "aBC" mvb lbc)
     
  (formsk2 mua (distance polu ba) "B" "p\\H0.7x;\\S^1;\\H1.4286x;b" nil)
  (formsk2 mua (distance polu da) "D" "p\\H0.7x;\\S^1;\\H1.4286x;d" nil)

  (formsk2 mua (distance polu s2a) "S2" "p\\H0.7x;\\S^1;\\H1.4286x;s\\H0.7x;\\S^2;\\H1.4286x;" nil)
  (formsk2 mua (distance polu s3a) "S3" "p\\H0.7x;\\S^1;\\H1.4286x;s\\H0.7x;\\S^3;\\H1.4286x;" nil)
  (formsk2 mua (distance ba aa) "BA" "ba" nil)
  
;;;  (add_text (getpoint "\n��������������� acel point D: ")
;;;	    (strcat "\\A1;p\\H0.7x;\\S^1;\\H1.4286x;d=p\\H0.7x;\\S^1;\\H1.4286x;c"
;;;		    "\U+00D7\\H0.7x;\\SED/EC;\\H1.4286x;="(rtos (distance polu ca) 2 0)
;;;		    "\U+00D7\\H0.7x;\\S" (rtos de 2 0) "/"(rtos ce 2 0)
;;;		    ";\\H1.4286x;=" (rtos (* (distance polu ca) (/ de ce)) 2 1) " ��"
;;;	    )
;;;  )
;;;  (add_text (getpoint "\n��������������� acel point F: ")
;;;	    (strcat "\\A1;p\\H0.7x;\\S^1;\\H1.4286x;f=p\\H0.7x;\\S^1;\\H1.4286x;c"
;;;		    "\U+00D7\\H0.7x;\\SEF/EC;\\H1.4286x;="(rtos (distance polu ca) 2 0)
;;;		    "\U+00D7\\H0.7x;\\S" (rtos ef 2 0) "/"(rtos ce 2 0)
;;;		    ";\\H1.4286x;=" (rtos (* (distance polu ca) (/ ef ce)) 2 1) " ��"
;;;	    )
;;;  )
  (ugsu "ome2BA" mvba lab)
  (ugsu "ome3BC" mvb lbc)  
  (ugsu "eps2BA" mabat lab)
  (ugsu "eps3BC" mabct lbc)
)


(defun grup23 (/ znag2 znaf2 znapd znag3 znaf3)
  (tochkim)
  (li (list c b d)) (mapcar '(lambda (t1) (add_circ 0.7 t1)) (list a b d s2 s3 c))
  (add_text s2 "\\A1;S\\H0.7x;\\S^2;\\H1.4286x;")
  (add_text s3 "\\A1;S\\H0.7x;\\S^3;\\H1.4286x;")
  (add_text a "A")(add_text c "C")
  (add_text b "B")(add_text d "D")  
  (setq sf3v (polar s3 (angle s3a polu) 20)
	sf2 (* mas2 m2)
	sf3 (* mas3 m3)
	g3v (polar s3 (/ pi -2) 20)
	;Pbv (polar b 0. 30)
	Pdn (polar d (- (angle d a) (* 0.5 pi)) 30)
	sf2v (polar s2 (angle s2a polu) 20)	
	g2v (polar s2 (/ pi -2) 20)
	r12nv (polar a (angle b a) 20)
	r03nv (polar c (angle b c) 20)
	r12t (/ (+ (moment g2 s2 g2v b)
		   (moment sf2 s2 sf2v b)
		   (moment Pd Pdn d b)
		)
	        ab
	     );_end of /
	r12tv (if (minusp r12t)
		(polar a (+ (angle b a) (/ pi 2)) 20)
		(polar a (- (angle b a) (/ pi 2)) 20)
	      )
	r03t (/ (+ (moment g3 s3 g3v b)
		   (moment sf3 s3 sf3v b)	   
		)
	        bc
	     );_end of /
	r03tv (if (minusp r03t)
		(polar c (+ (angle b c) (/ pi 2)) 20)
		(polar c (- (angle b c) (/ pi 2)) 20)
	      )
  );_end of setq
  (risskor s2 sf2v  nil "�^2")
  (risskor s2 g2v nil "G^2")
  (risskor s3 sf3v  nil "�^3")
  (risskor s3 g3v  nil "G^3")
  ;(risskor b (polar b 0 20.)  nil "P^B")
  (risskor pdn d nil "P^D")
  (risskor a r12nv  nil "Rn^12")
  (risskor a r12tv  nil "R\U+03C4^12")
  (risskor c r03nv  nil "Rn^03")
  (risskor c r03tv  nil "R\U+03C4^03")
  (add_text (getpoint "\nR12t= ") (strcat "\\A1;R\\H0.7x;\\S\U+03C4^12;\\H1.4286x;=("
					  (if (minusp (moment g2 s2 g2v b))
					    (setq znag2 "-")
					    (setq znag2 "")
					  )
					  "G\\H0.7x;\\S^2;\\H1.4286x;\U+00D7h\\H0.7x;\\S^G2;\\H1.4286x;"
					  (if (minusp (moment sf2 s2 sf2v b))
					    (setq znaf2 "-")
					    (setq znaf2 "+")
					  )
					  "�\\H0.7x;\\S^2;\\H1.4286x;\U+00D7h\\H0.7x;\\S^�2;\\H1.4286x;"
					  (if (minusp (moment Pd Pdn d b))
					    (setq znapd "-")
					    (setq znapd "+")
					  )
					  "P\\H0.7x;\\S^D;\\H1.4286x;\U+00D7h\\H0.7x;\\S^PD;\\H1.4286x;)/ab=("
					  znag2 (rtos g2 2 1) "\U+00D7" (rtos (rastpr b s2 g2v) 2 1)
					  znaf2 (rtos sf2 2 2) "\U+00D7" (rtos (rastpr b s2 sf2v) 2 1)
					  znapd	(rtos pd 2 0) "\U+00D7" (rtos (rastpr b d Pdn) 2 1) ")/"(rtos ab)"=("
					  znag2 (rtos (abs (moment g2 s2 g2v b)) 2 2)
					  znaf2 (rtos (abs (moment sf2 s2 sf2v b)) 2 2)
					  znapd (rtos (abs (moment Pd Pdn d b)) 2 2) ")/" (rtos ab)"=" (rtos r12t 2 2)" �"
				  )
  )
  (add_text (getpoint "\nR03t= ") (strcat "\\A1;R\\H0.7x;\\S\U+03C4^03;\\H1.4286x;=("
					  (if (minusp (moment g3 s3 g3v b))
					    (setq znag3 "-")
					    (setq znag3 "")
					  )
					  "G\\H0.7x;\\S^3;\\H1.4286x;\U+00D7h\\H0.7x;\\S^G3;\\H1.4286x;"
					  (if (minusp (moment sf3 s3 sf3v b))
					    (setq znaf3 "-")
					    (setq znaf3 "+")
					  )
					  "�\\H0.7x;\\S^2;\\H1.4286x;\U+00D7h\\H0.7x;\\S^�2;\\H1.4286x;)/bc=("
					  znag3 (rtos g3 2 1) "\U+00D7" (rtos (rastpr b s3 g3v) 2 1)
					  znaf3 (rtos sf3 2 2) "\U+00D7" (rtos (rastpr b s3 sf3v) 2 1)")/" (rtos bc) "=("
					  znag3 (rtos (abs (moment g3 s3 g3v b)) 2 2)
					  znaf3 (rtos (abs (moment sf3 s3 sf3v b)) 2 2) ")/"(rtos bc)"=" (rtos r03t 2 2)" �"
				  )
  )  	    
  (plansil23)
);_end of defun

(defun plansil23 (/ maxsil23 
abr24t bcg4 cdsf4 deg5  
asil4 bsil4 csil4 dsil4 esil4 fsil4 gsil4 hsil4 isil4)
  (setq asil4 (getpoint "\n������� ��� ������� ���� ��� ��� ������ 2-3: ")
	maxsil23 (getreal "\n������� � �� ����� ������� ���� �� �����: ")	
	muf23 (/ (max g2 g3 pb pd sf2 sf3 (abs r12t) (abs r03t)) maxsil23)
	;����� ����� ��� R12t+G2+�2+PD+G3+�3+R03t+R03n+R12n=0
	bsil4 (polar asil4 (angle a r12tv) (/ (abs r12t) muf23));R12t
	csil4 (polar bsil4 (* pi -0.5) (/ g2 muf23));G2
	dsil4 (polar csil4 (angle s2a polu) (/ sf2 muf23));�2
	esil4 (polar dsil4 (- (angle b a) (* 0.5 pi)) (/ pd muf23));PD
	fsil4 (polar esil4 (* 1.5 pi) (/ g3 muf23));G3
	gsil4 (polar fsil4 (angle s3a polu) (/ sf3 muf23));�3
	hsil4 (polar gsil4 (angle c r03tv) (/ (abs r03t) muf23));R03t
	isil4 (inters hsil4 (polar hsil4 (angle b c) 1e15);R03n
		      asil4 (polar asil4 (angle a b) 1e15);R12n
		      nil
	      )    
	r12n (* muf23 (distance isil4 asil4))
	;������� ������ ������� R24
	r12 (sqrt (+ (* r12n r12n) (* r12t r12t)))
	r03 (* muf23 (distance hsil4 isil4))
	ugolr42 (angle bsil4 isil4)
  );_end of setq
  (cherplan23)
);_end of defun

(defun cherplan23 (/ gde);R12t+G2+�2+PD+G3+�3+R03t+R03n+R12n=0
  (risskor asil4 bsil4 "b" "R\U+03C4^12")
  (risskor bsil4 csil4 "c" "G^2")
  (risskor csil4 dsil4 "d" "�^2")
  (risskor dsil4 esil4 "e" "P^D")
  (risskor esil4 fsil4 "f" "G^3")
  (risskor fsil4 gsil4 "g" "�^3")
  (risskor gsil4 hsil4 "h" "R\U+03C4^03")
  (risskor hsil4 isil4 "i" "Rn^03")
  (risskor isil4 asil4 "a" "Rn^12")
  (risskor isil4 bsil4 nil "R^12")
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���: ") muf23 "F" "�/��") 
)

(defun grup01 ()
  (tochkim)
  (duga o a a (polar a (+ (angle o a) (* 0.5 pi)) 10.) 0.25 "\\A1;\\H2;\U+03C9\\H0.7x;\\S^1;\\H2;")
  (li (list o a)) (mapcar '(lambda (t1) (add_circ 0.7 t1)) (list o a))
  (add_text a "A")
  (add_text o "O") 
  (add_opor o (+ (* pi 0.5) fi))
  (setq r12v (polar a ugolr42 20)
	fy (/ (moment r12 a r12v o)
	        oa
	     );_end of /
	fyv (if (minusp fy)
		(polar a (+ (angle o a) (/ pi 2)) 20)
		(polar a (- (angle o a) (/ pi 2)) 20)
	      )
  );_end of setq
  (risskor a fyv  nil "F^y")
  (risskor a r12v nil "R^21")
  (duga o a a (polar a (if (minusp fy)
			 (+ (angle o a) (* 0.5 pi))
			 (- (angle o a) (* 0.5 pi))
		       ) 10.)
		      0.75 "\\A1;\\H2;M\\H0.7x;\\S^Y;\\H2;"
  )
  (setq muf01 (/ r12 (getdist "\nMaxR12"))
	asi01 (getpoint "\nPlan sil 01: ")
	bsi01 (polar asi01 (angle a r12v) (/ r12 muf01))
	csi01 (polar bsi01 (angle a fyv) (/ (abs fy) muf01))
	r01 (* muf01 (distance asi01 csi01))
  )
  (risskor asi01 bsi01 "b" "R^21")
  (risskor bsi01 csi01 "c" "F^Y")
  (risskor csi01 asi01 "a" "R^01")
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���: ") muf01 "F" "�/��") 
);_end of defun

;=====================================================================================================

(defun c:sem20_2 (/ osm le
ab omeg lab mul bc ce de ef bs ae dk fl lbc lce ldk lfl mvb mab fi 
a e b c d f l k s2 masivm pb muv pol bv cv dv fv kv s2v lv mvc mvd mvf mvl mvk mvs2 mvcb mvkd mvlf ome2 ome3 ome4 ome5 
macbn macen makdn malfn pa mua polu lcbn lcen lkdn llfn ba cen ca da fa lfn kdn la ka
		  mad mac mal mak maf macbt macet malft makdt eps2 eps3 eps4 eps5)
  (VL-LOAD-COM)
  (setq osm (getvar "OSMODE"))
  (setvar "OSMODE" 20919)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "TIMESI.TTF" )))
	le (append le (list (cons 2 "�����")))	
  )
  (entmakex le)
  (setvar "textstyle" "�����")
  (load "funkcii_tmm.lsp")
  (load "tmm_semi.lsp")
  (vvod)
  (tochkim)
  (cherm)
  (plansk)
  (cherv)
  (planusk)
  (chera)  
  (omep)
  (grup23)
  (grup01)
  (gukovsk)
  (alert (strcat "\n" (rtos fur) "\n" (rtos fy)))
  (setvar "OSMODE" osm)
  (setvar "dimtxsty" "�����")
)

(c:sem20_2)