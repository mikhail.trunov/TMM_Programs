;;;(defun new_funcia_kin (/ polus_mexan fii vektorva vektoraa
;;;		   	 polus_skor polus_uskor ugol0 ugolf)
;;;  (initget 65)  
;;;  (setq ugol0 
;;;	fii ugol0
;;;	polus_mexan (getpoint "\n������� ��� ������� ��������: ")
;;;  )
;;;  (repeat 12
;;;    (tochkim polus_mexan fii)
;;;    (cherm polus_mexan)
;;;    (setq fii (+ (/ pi 6.) fii))
;;;  )
;;;  (initget 65)
;;;  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���������: ") mul "l" "�/��")
;;;  (initget 7)
;;;  (setq vektorva (getdist "\n������� ����� ������� ����� A: "))
;;;  (repeat 12
;;;    ;(tochkim fii polus_mexan)
;;;    (initget 65)    
;;;    (plansk (setq pol (getpoint "\n������� ��� ������� ���� ���������: ")) vektorva fii)
;;;    (cherv pol)
;;;    (if (equal fi (- fii ugol0) 1.e-6)
;;;      (setq polus_skor pol)
;;;    )
;;;    (setq fii (+ (/ pi 6.) fii))
;;;  )
;;;  (initget 65)
;;;  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���������: ") muv "\U+03C5" "� �/��")
;;;  
;;;  (setq fi (+ fi ugol0 (* (/ pi 180.) (getreal "\n������� ������� ��������� � ��������� ���� [����]: "))))
;;;  ;(tochkim fi polus_mexan)
;;;  ;(plansk polus_mexan vektorva)
;;;  (initget 7)
;;;  (setq vektoraa (getdist "\n������� ����� ������� ��������� ����� A: "))
;;;  (initget 65)
;;;  (setq polus_uskor (getpoint "\n������� ��� ������� ���� ���������: "))  
;;;  (planusk polus_uskor vektoraa fi)
;;;  (chera polus_uskor)
;;;  (initget 65)
;;;  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���������: ") mua "A" "� �/��")
;;;  (initget 1 "Yes No")
;;;  (if (equal (getkword "\n������� � excel? [Yes/No]: ") "Yes")
;;;    (excel_export)
;;;  )  
;;;)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;�������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun c:kin1 (/ old le model
	       n1 loa mul lab ab lbc bc lcd cd ldf df la ax lb bx lc cx 
	       fi omeg1	va aan m2 m3 m5	i2 i3)
  (vl-load-com)
  (load "funkcii_tmm.LSP")
  (load "tmm\\mex_1\\for_1.LSP")
  (load "tmm_kin.LSP")
  (setq model (vla-get-ModelSpace (vla-get-ActiveDocument (vlax-get-acad-object))))  
  (setq old (getvar "osmode"))
  (setvar "osmode" 20903)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "timesi.TTF" )))	
  )
  (entmod le)  
  (vvod)
  (new_funcia_kin (+ (angle '(0 0) (list la lb))
		     (arccos (/ (+ (expt (+ loa lab) 2) (* la la) (* lb lb) (* -1 lbc lbc))
				(* 2. (+ loa lab) (sqrt (+ (* la la) (* lb lb))))
			     ); /
		     ); arccos
		  ); +
    		  1
    		  nil
    "���� ������������"
  )  
  (setvar "osmode" old)  
  (vla-ZoomExtents (vlax-get-acad-object))
);_end of defun

(defun excel_export (/ ugol0 g_oex g_wkbs g_wb_put g_shs_put g_sheet1 g_sheet2
		     fii i)
  (setq g_oex (vlax-get-or-create-object "Excel.Application"))
  (vlax-put-property g_oex "Visible" :vlax-false)  
  (setq g_wkbs (vlax-get-property g_oex "Workbooks")        
        g_wb_put (vla-open g_wkbs (strcat total_adress "blanks\\first_list_1.xls"))        
        g_shs_put (vlax-get-property g_wb_put "Worksheets")
	g_sheet1 (vlax-get-property g_shs_put "Item" 1)
	g_sheet2 (vlax-get-property g_shs_put "Item" 2)
  )
  (ex_put loa 2 "B" g_sheet1)
  (ex_put oa 3 "B" g_sheet1)
  (ex_put lab 2 "C" g_sheet1)
  (ex_put lbc 2 "D" g_sheet1)
  (ex_put lcd 2 "E" g_sheet1)
  (ex_put ldf 2 "F" g_sheet1)
  (ex_put la 2 "G" g_sheet1)
  (ex_put lb 2 "H" g_sheet1)
  (ex_put lc 2 "I" g_sheet1)
  (ex_put n1 4 "B" g_sheet1)
  (ex_put ugolf 8 "B" g_sheet1)
  (setq ugol0 (+ (angle '(0 0) (list la lb))
		     (arccos (/ (+ (expt (+ loa lab) 2) (* la la) (* lb lb) (* -1 lbc lbc))
				(* 2. (+ loa lab) (sqrt (+ (* la la) (* lb lb))))
			     ); /
		     ); arccos
		  ); +
	fii ugol0
	i 2
  )
  (repeat 12
    ;(tochkim fii polus_mexan)
    (plansk polus_mexan vektorva fii)
    (ex_put muv 3 "O" g_sheet2)
    (ex_put (distance bv polus_mexan) 3 i g_sheet2)
    (ex_put (distance bv av) 4 i g_sheet2)
    (ex_put (distance dv polus_mexan) 5 i g_sheet2)
    (ex_put (distance fv polus_mexan) 6 i g_sheet2)
    (ex_put (distance dv fv) 7 i g_sheet2)
    (ex_put (distance s2v polus_mexan) 8 i g_sheet2)
    (setq fii (+ fii (/ pi 6.))
	  i (1+ i)
    )
  ); repeat
  (ex_put mua 26 "I" g_sheet2)  
  (ex_put (distance polus_uskor ba) 23 "B" g_sheet2)
  (ex_put (distance ba aa) 23 "C" g_sheet2)
  (ex_put (distance polus_uskor da) 23 "D" g_sheet2)
  (ex_put (distance polus_uskor fa) 23 "E" g_sheet2)
  (ex_put (distance da fa) 23 "F" g_sheet2)
  (ex_put (distance polus_uskor s2a) 23 "G" g_sheet2)
  (ex_put (distance aa ban) 27 "C" g_sheet2)
  (ex_put (distance polus_uskor bcn) 27 "D" g_sheet2)
  (ex_put (distance da fdn) 27 "E" g_sheet2)  
  (ex_put (distance ban ba) 27 "F" g_sheet2)
  (ex_put (distance bcn ba) 27 "G" g_sheet2)
  (ex_put (distance fdn fa) 27 "H" g_sheet2)
  (vlax-invoke-method g_wb_put "SaveCopyAs" (getfiled "������� ��� ��������� ���� � ��������" "c:\\" "xls" 129))
  (vlax-invoke-method g_wb_put "Close" :vlax-false :vlax-false)
  (vlax-invoke-method g_oex "Quit")
  (mapcar
    (function (lambda (x)
      (if
        (and x (not (vlax-object-released-p x)))
        (vlax-release-object x)
      );_ if
              );_ lambda
    );_ function
    (list g_shs_put
	  g_wb_put
	  g_wkbs g_sheet1 g_sheet2
	  g_oex)
  )  
  (gc) 
)
(c:kin1)