(defun momentvit (/ polo os_x n_x max_mom fi polus
		  fkr_v dis_fkr fkr_n)
  (setq polus '(0 0)
	mom_mas nil
	os_x (getdist "\n����� ��� �����: ")
	mufi (/ pi os_x 0.5)
	n_x (getreal "\n���������� �������� [12]: ")
	shag_ugl (/ pi n_x 0.5)
	shag_os (/ os_x n_x)
	ugol0 (+ (angle '(0 0) (list la lb))
		 (arccos (/ (+ (expt (+ loa lab) 2) (* la la) (* lb lb) (* -1 lbc lbc))
			    (* 2. (+ loa lab) (sqrt (+ (* la la) (* lb lb))))
			 )
		 )
	    ); +
	fi ugol0
	tekush_ug 0.	
  );_ setq
  (tochkim fi '(0 0))
  (setq fkr_v f)
  (tochkim (+ pi fi) '(0 0))
  (setq fkr_n f
	dis_fkr (distance fkr_n fkr_v)
  )
  (repeat (fix (1+ n_x))    
    (plansk polus 70. fi)
    (setq polo (/ (distance fkr_v f) dis_fkr)
      	  fsop (cond
		 ((<= polo 0.6) 0.)
		 ((and (> polo 0.6) (<= polo 0.7))
		  (* fsopmax (- (* 10. polo) 6.))
		 )
		 ((and (> polo 0.7) (<= polo 0.9))
		  (* fsopmax (+ (* -0.5 polo) 1.35))
		 )
		 ((> polo 0.9)
		  (* fsopmax (+ (* -9. polo) 9.))
		 )
	       ); cond
	  mdfs (* fsop (/ (distance polus fv) 70.)
		  loa
		  (if (equal (angle polus fv) (* 1.5 pi))
		    -1.
		    0.;(cosinus polus fv polus (polar polus (/ pi 2) 5))
		  )
	       )
	  mdg5 (* g5 (/ (distance polus fv) 70.) (cosinus polus fv polus (polar polus (/ pi -2) 5)))
	  mdg3 (* g3 (/ (distance polus s3v) 70.) (cosinus polus s3v polus (polar polus (/ pi -2) 5)))
	  mdg2 (* g2 (/ (distance polus s2v) 70.) (cosinus polus s2v polus (polar polus (/ pi -2) 5)))
	  md_sum (+ mdfs
		   ;mdg5 mdg3
		   ;mdg2
		  )
	  mom_mas (cons (list tekush_ug md_sum) mom_mas)	  
	  tekush_ug (+ tekush_ug shag_os)
	  fi (+ fi shag_ugl)
    ); setq
  );_ repeat
  (initget 7)
  (setq	max_mom (car (vl-sort (mapcar '(lambda (p) (abs (cadr p))) mom_mas) '>))
	mumoment (/ max_mom (getdist "\n������� Ymax: "))
	mom_mas (mapcar '(lambda (lis)
			   	 (list (car lis) (/ (cadr lis) mumoment))
			 ) mom_mas)
  ); setq
  (add_pl mom_mas)
  (add_tex (getpoint "\n��� ������ ���������� ����������� ��� ������� �������: ") mumoment "M" "�*�/��")
)


(defun inercia (/ masshtab fi polus max_ii mu_ii
		omeg3 omeg2 mvf)
  (setq ;masshtab (getreal "\n��������� ����������� I: ")
	fi ugol0
	tekush_ug 0.
	mas_i nil
	polus '(0 0)
  )
  (repeat (length graf_int)    
    (plansk polus 70. fi)
    (setq i_sumar (+ i1	     
		     (* i2 (expt (/ omeg2 omeg1) 2))
		     (* m2 (expt (/ mvs2 omeg1) 2))
		     (* i3 (expt (/ omeg3 omeg1) 2))
		     (* m3 (expt (/ mvs3 omeg1) 2))
		     (* m5 (expt (/ mvf omeg1) 2))
		  ); +
	  ;i_sumar (* i_sumar masshtab)
	  mas_i (cons (list i_sumar tekush_ug) mas_i)
	  tekush_ug (+ tekush_ug shag_os)
	  fi (+ fi shag_ugl)
    ); setq
  ); repeat
  (initget 7)
  (setq	max_ii (car (vl-sort (mapcar '(lambda (p) (abs (car p))) mas_i) '>))
	mu_ii (/ max_ii (getdist "\n������� Ymax ��� ������� �������: "))
	mas_i (mapcar '(lambda (lis)
			       (list (/ (car lis) mu_ii) (cadr lis))
			 ) mas_i)
  ); setq
  (vl-cmdf "._ucs" '(-170. 200.) "")  
  (add_pl mas_i)
  (add_tex (getpoint "\n��� ������ ���������� ����������� ��� ������� �������: ") mu_ii "I" "�� �/��")
); inercia

(defun c:as ()
  (vl-load-com)
  (load "funkcii_tmm.LSP")
  (load "tmm\\mex_1\\for_1.LSP")
  (setq old (getvar "osmode"))
  (setvar "osmode" 20903)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "timesi.TTF" )))	
  ); setq
  (entmod le)
  (vvod)
  (momentvit)  
  (grafint (reverse mom_mas) -100.)  
  (deltat (reverse graf_int) '(0. -200.))
  (add_tex (getpoint "\n��� ������ ���������� ����������� ��� ������: ") (* mufi h mumoment) "A" "��/��")
  (inercia)
  (vitten '(0 -200) 200 170)
  (exel_expo_max)
)

(defun exel_expo_max (/ fi polus i omeg3 omeg2 mvf fsop polo g_oex g_wkbs g_wb_put g_shs_put g_sheet1
		      fkr_v fkr_n dis_fkr)
  (setq fi ugol0	
	mas_i nil
	polus '(0 0)
	g_oex (vlax-get-or-create-object "Excel.Application")
	i 1
  )
  (tochkim fi '(0 0))
  (setq fkr_v f)
  (tochkim (+ pi fi) '(0 0))
  (setq fkr_n f
	dis_fkr (distance fkr_n fkr_v)
  )
  (vlax-put-property g_oex "Visible" :vlax-false)  
  (setq g_wkbs (vlax-get-property g_oex "Workbooks")        
        g_wb_put (vla-open g_wkbs "c:\\Program files\\AutoCAD 2008\\Support\\blanks\\maxovik_1.xls")        
        g_shs_put (vlax-get-property g_wb_put "Worksheets")
	g_sheet1 (vlax-get-property g_shs_put "Item" 1)
	;g_sheet2 (vlax-get-property g_shs_put "Item" 2)
  )
  (repeat 13
    (plansk polus 70. fi)
    (setq i_sumar (+ i1	     
		     (* i2 (expt (/ omeg2 omeg1) 2))
		     (* m2 (expt (/ mvs2 omeg1) 2))
		     (* i3 (expt (/ omeg3 omeg1) 2))
		     (* m3 (expt (/ mvs3 omeg1) 2))
		     (* m5 (expt (/ mvf omeg1) 2))
		  ); +
    ); setq
    (ex_put i_sumar 6 i g_sheet1)   
    (setq i (1+ i)
	  fi (+ fi (/ pi 6.))
    ); setq
  ); repeat
  (setq fi ugol0
	i 1
	)
  (repeat 13
    (plansk polus 70. fi)
        (setq polo (/ (distance fkr_v f) dis_fkr)
      	  fsop (cond
		 ((<= polo 0.6) 0.)
		 ((and (> polo 0.6) (<= polo 0.7))
		  (* fsopmax (- (* 10. polo) 6.))
		 )
		 ((and (> polo 0.7) (<= polo 0.9))
		  (* fsopmax (+ (* -0.5 polo) 1.35))
		 )
		 ((> polo 0.9)
		  (* fsopmax (+ (* -9. polo) 9.))
		 )
	       ); cond
	  mdfs (* fsop (/ (distance polus fv) 70.)
		  loa
		  (if (equal (angle polus fv) (* 1.5 pi))
		    -1.
		    0.;(cosinus polus fv polus (polar polus (/ pi 2) 5))
		  )
	       )
	  vfkva (* (/ (distance polus fv) 70.)
		   (if (equal (angle polus fv) (* 1.5 pi))
		    -1.
		    0.
		  )
		)
    ); setq
    (ex_put fsop 3 i g_sheet1)
    (ex_put vfkva 4 i g_sheet1)
    (ex_put mdfs 5 i g_sheet1)
    (setq i (1+ i)
	  fi (+ fi (/ pi 6.))
    )
  ); repeat
  (ex_put omeg1 9 "B" g_sheet1)
  (ex_put (* h mumoment mufi) 9 "D" g_sheet1)
  (ex_put k_nerav 9 "A" g_sheet1)
  (vlax-invoke-method g_wb_put "SaveCopyAs" (getfiled "������� ��� ��������� ���� � ��������" "c:\\" "xls" 129))
  (vlax-invoke-method g_wb_put "Close" :vlax-false :vlax-false)
  (vlax-invoke-method g_oex "Quit")
  (mapcar
    (function (lambda (x)
      (if
        (and x (not (vlax-object-released-p x)))
        (vlax-release-object x)
      );_ if
              );_ lambda
    );_ function
    (list g_shs_put
	  g_wb_put
	  g_wkbs g_sheet1
	  g_oex)
  )  
  (gc)
)