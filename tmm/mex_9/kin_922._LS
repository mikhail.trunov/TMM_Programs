(defun new_funcia_kin (/ polus_mexan fii vektorva vektoraa
		   	 polus_skor polus_uskor ugolf pol) 
  (initget 1 "Yes No")
  (if (equal "Yes" (getkword "\n��������� ��� 12 ������ ���������? [Yes/No]: "))
    (progn
      (initget 65)  
      (setq fii (* 0.75 pi)
	    polus_mexan (getpoint "\n������� ��� ������� ��������: ")
      )
      (repeat 12
         (tochkim polus_mexan fii)
         (cherm polus_mexan)
         (setq fii (- fii (/ pi 6.)))
      )
      (add_opor polus_mexan pi)
      (add_text polus_mexan "O")
      (initget 65)
      (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���������: ") mul "l" "�/��")
      ;(setq fi (* 0.75 pi))
      (initget 1 "Yes No")
      (if (equal (getkword "\n��������� ��������� ��������? [Yes/No]: ") "Yes")
	(grafiki (polar
		   (polar polus_mexan (* 0.75 pi) (+ oa abc))
		   (* 0.25 pi)
		   (+ (/ dcil 2. mul) 6)
		 )
		 (polar
		   (polar polus_mexan (* 0.25 pi) (- abc oa))
		   (* 0.75 pi)
		   (+ (/ dcil 2. mul) 6)
		 )
		 polus_mexan (polar polus_mexan (* pi 0.5) 100.)
        )
      ); if
    ); progn
  )
  (initget 7)
  (setq vektorva (getdist "\n������� ����� ������� �������� ����� A: ")
	fii (* 0.75 pi)
	muv (/ va vektorva)
  )
  (initget 1 "Yes No")
  (if (equal "Yes" (getkword "\n��������� ��� 12 ������ ���������? [Yes/No]: "))
    (progn
      (repeat 12        
        (initget 65)    
        (plansk (setq pol (getpoint "\n������� ��� ������� ���� ���������: ")) vektorva fii)
        (cherv pol)
        (setq fii (- fii (/ pi 6)))
      ); repeat
      (initget 65)
      (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���������: ") muv "\U+03C5" "� �/��")
    ); progn
  ); if
  (initget 7)
  (setq vektoraa (getdist "\n������� ����� ������� ��������� ����� A: ")
	mua (/ aan vektoraa)
  )
  (initget 65)  
  (planusk (setq polus_uskor (getpoint "\n������� ��� ������� ���� ���������: "))
	   vektoraa (- (* 0.75 pi) fi))
  (chera polus_uskor)
  (initget 65)
  (add_tex (getpoint "\n������� ��� ������ ������� ��� ����� ���������: ") mua "a" "� �/��")
  (initget 1 "Yes No")
  (if (equal (getkword "\n������� � excel? [Yes/No]: ") "Yes")
    (excel_export)
  )  
)
;===� � � � �   � � � � � � �   � � � � � � � � � � � � � � �    � � � � � � �=====

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;�������� �������;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(defun c:kin9 (/ old le f a b c s2 s4 
muv av bv cv s2v s4v mvb mvba mvc mvca mvs2 mvs4 omeg2 omeg4 aban acan 
mua dban dcan aa ban ba can ca s2a s4a mab mabat mac macat mas2 mas4 
ugol0 fi n1 loa mul labc abc las24 as24 omeg1 va aan dcil va aan pmax k_nerav m24 m35 is1 i24 g24 g35)
  (vl-load-com)
  (load "funkcii_tmm.LSP")
  (load "tmm\\mex_9\\different_p.LSP")
  (load "tmm\\mex_9\\for_9.LSP")
  (setq old (getvar "osmode"))
  (setvar "osmode" 20903)
  (setq le (entget(tblobjname "style" (getvar "textstyle")))
	le (vl-remove (assoc 3 le) le)
	le (append le (list (cons 3 "times.TTF" )))	
  ); setq
  (entmod le)
  (vvod)
  (new_funcia_kin)
  (setvar "osmode" old)
  (vla-ZoomExtents (vlax-get-acad-object))
  (gc)
);_end of defun

(defun excel_export (/ g_oex g_wkbs g_wb_put g_shs_put g_sheet1 g_sheet2
		     fii i)
  (setq g_oex (vlax-get-or-create-object "Excel.Application"))
  (vlax-put-property g_oex "Visible" :vlax-false)  
  (setq g_wkbs (vlax-get-property g_oex "Workbooks")        
        g_wb_put (vla-open g_wkbs (strcat total_adress "blanks\\first_list_9.xls"))        
        g_shs_put (vlax-get-property g_wb_put "Worksheets")
	g_sheet1 (vlax-get-property g_shs_put "Item" 1)
	g_sheet2 (vlax-get-property g_shs_put "Item" 2)
  )
  (ex_put loa 2 "B" g_sheet1)  
  (ex_put oa 3 "B" g_sheet1)
  (ex_put n1 4 "B" g_sheet1)
  (ex_put labc 2 "C" g_sheet1)
  ;(ex_put f 8 "B" 1)
  (ex_put dcil 2 "E" g_sheet1)
  (ex_put (* fi (/ 180. pi)) 8 "B" g_sheet1)
  (setq fii (* 0.75 pi)
	i 2
  )
  (ex_put muv 3 "O" g_sheet2)
  (repeat 12    
    (plansk '(0 0) vektorva fii)
    (ex_put (distance '(0 0) bv) 3 i g_sheet2)
    (ex_put (distance bv av) 4 i g_sheet2)
    (ex_put (distance '(0 0) cv) 5 i g_sheet2)    
    (ex_put (distance av cv) 6 i g_sheet2)
    (ex_put (distance '(0 0) s2v) 7 i g_sheet2)
    (ex_put (distance '(0 0) s4v) 8 i g_sheet2)
    (setq fii (- fii (/ pi 6.))
	  i (1+ i)
    )
  ); repeat
  (planusk (setq polyc '(0 0)) vektoraa (- (* 0.75 pi) fi))
  (ex_put (distance polyc ba) 20 "B" g_sheet2)
  (ex_put (distance aa ba) 20 "C" g_sheet2)
  (ex_put (distance polyc ca) 20 "D" g_sheet2)
  (ex_put (distance aa ca) 20 "E" g_sheet2)
  (ex_put (distance polyc s2a) 20 "F" g_sheet2)
  (ex_put (distance polyc s4a) 20 "G" g_sheet2)
  (ex_put mua 23 "G" g_sheet2)
  (ex_put (distance aa ban) 24 "C" g_sheet2)
  (ex_put (distance aa can) 24 "D" g_sheet2)
  (ex_put (distance ban ba) 24 "E" g_sheet2)
  (ex_put (distance can ca) 24 "F" g_sheet2)
  (vlax-invoke-method g_wb_put "SaveCopyAs" (getfiled "������� ��� ��������� ���� � ��������" "c:\\" "xls" 129))
  (vlax-invoke-method g_wb_put "Close" :vlax-false :vlax-false)
  (vlax-invoke-method g_oex "Quit")
  (mapcar
    (function (lambda (x)
      (if
        (and x (not (vlax-object-released-p x)))
        (vlax-release-object x)
      );_ if
              );_ lambda
    );_ function
    (list g_shs_put
	  g_wb_put
	  g_wkbs g_sheet1 g_sheet2
	  g_oex)
  )  
  (gc) 
)
(defun grafiki (nachalo1 nachalo2 os1 os2 / list_p1 list_p2 polus p1 p2 polog1 polog2 tekush_ug shag_ugl n_x fuuu
		max_p1 max_p2 mup1 mup2 max_py1 max_py2 mupy1 mupy2)
  (setq tekush_ug 0.	
	n_x (getreal "\n���������� �������� [12]: ")
	shag_ugl (/ pi n_x 0.5)	
	polus polus_mexan
	fuuu (* 0.75 pi)	
  )
  (repeat (fix (1+ n_x))
   	  (plansk polus_mexan 70. fuuu)
   	  (setq polog1 (/ (distance (polar polus (* 0.75 pi) (+ oa abc)) b) (* 2. oa))
		polog2 (/ (distance (polar polus (* 0.25 pi) (+ oa abc)) c) (* 2. oa))
		p1 (* 0.2 pmax (davlb polus bv polog1))
;;;    	  	p1 (if (equal (* 0.75 pi) (angle polus bv) 1e-6)
;;;	      	       (* (/ pmax 0.99)
;;;		  	  (- (expt 100 (* -1. polog1)) 0.01)
;;;	       	       ); *
;;;	       	       (* (/ pmax 0.9)
;;;		  	  (- (expt 10 (* -1. polog1)) 0.1)
;;;	       	       ); *
;;;	     	   ); if
		p2 (* pmax (davlc polus cv polog2 0.2))
	  	list_p1 (cons (list polog1 (* p1 1e6)) list_p1)
		list_p2 (cons (list (- 1. polog2) (* p2 1e6)) list_p2)
	  	fuuu (- fuuu shag_ugl)
  	  ); setq
  );_ repeat
  (setq	max_p1 (car (vl-sort (mapcar '(lambda (p) (abs (cadr p))) list_p1) '>))
	max_p2 (car (vl-sort (mapcar '(lambda (p) (abs (cadr p))) list_p2) '>))
	mup (/ max_p (getdist "\n������� Ymax ��� P: "))
	list_p1 (mapcar '(lambda (lis)
			   	 (list (car lis) (/ (cadr lis) mup1))
			 ) list_p1)
	list_p2 (mapcar '(lambda (lis)
			   	 (list (car lis) (/ (cadr lis) mup2))
			 ) list_p2)
	max_py1 (car (vl-sort (mapcar '(lambda (p) (abs (car p))) list_p1) '>))
	max_py2 (car (vl-sort (mapcar '(lambda (p) (abs (car p))) list_p2) '>))
	mupy1 (/ max_py1 (* 2. oa))
	mupy2 (/ max_py2 (* 2. oa))
	list_p1 (mapcar '(lambda (lis)
			   	 (list (/ (car lis) mupy1) (cadr lis))
			 ) list_p1)
	list_p2 (mapcar '(lambda (lis)
			   	 (list (/ (car lis) mupy2) (cadr lis))
			 ) list_p2)
  ); setq
  (add_pl list_p1)  
  (vla-move
    (vlax-ename->vla-object (entlast))
    (vlax-3d-point (trans '(0 0) 1 0))
    (vlax-3d-point (trans nachalo1 1 0))
  )
  (vla-rotate
    (vlax-ename->vla-object (entlast))
    (vlax-3d-point (trans nachalo1 1 0))
    (* pi -0.25)
  )
  (add_pl list_p2)
  (vla-move
    (vlax-ename->vla-object (entlast))
    (vlax-3d-point (trans '(0 0) 1 0))
    (vlax-3d-point (trans nachalo2 1 0))
  )
  (vla-rotate
    (vlax-ename->vla-object (entlast))
    (vlax-3d-point (trans nachalo2 1 0))
    (* pi 0.25)
  )
  ;(mak_mirr (entlast) os1 os2)
  (add_tex (getpoint "\n������� ��� ������ ������� ��� �������� ��������: ") mup1 "P" "��/��")
  (add_tex (getpoint "\n������� ��� ������ ������� ��� �������� ��������: ") mup2 "P" "��/��")
)
(c:kin9)