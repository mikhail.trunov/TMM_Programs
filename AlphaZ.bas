Attribute VB_Name = "AlphaZ"
Sub Ug_poInv()
Dim Invaluta, PI, ugmax, ugmin, alpha As Double
PI = Application.WorksheetFunction.PI
ugmax = PI / 6
ugmin = PI / 9
Invaluta = Worksheets(1).Cells(4, 11).Value

For i = 0 To 110
  alpha = 0.5 * (ugmax + ugmin)
  If Math.Tan(alpha) - alpha > Invaluta Then
     ugmax = alpha
  Else
     ugmin = alpha
  End If
Next i
Worksheets(1).Cells(5, 11).Value = alpha
End Sub
